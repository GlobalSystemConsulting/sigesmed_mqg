/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.rdg;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.ItemFile;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.ItemFileDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.TipoEspecializado;
import java.util.List;

/**
 *
 * @author ucsm
 */
public interface ItemFileDao  extends GenericDao<ItemFile>{
    public ItemFile buscarPorID(int clave);
    public String buscarUltimoCodigo();
    public List<ItemFile> buscarHijosPorIdPadre(ItemFile padre,int usuCod);
    public List<ItemFile> buscarHijosPorIdPadreUgel(ItemFile padre, int orgID, int usuCod);
    public int eliminarHijosByPadre(ItemFile padre);
    public List<ItemFile> buscarAllHijosPorIdPadre(ItemFile padre);
    public List<ItemFile> buscarEliminados();
    public int eliminarTotalHijosByPadre(ItemFile padre);
    public List<ItemFile> listPorTipoEsp(TipoEspecializado tes);
    public boolean buscarNombreRepetido (ItemFile item);
    public Integer buscarIdPadre(ItemFile item);
    public ItemFile obtenerUltimaVersion(ItemFile item, ItemFile padre);
    public int obtenerNumDocPorCarpetaYTipo(ItemFile carpeta, ItemFile temp);
    public boolean desactivarVersionesAnteriores(ItemFile item,ItemFile padre);
    public List<ItemFile> buscarCarpetas();
    public boolean esPropio(int usuID, ItemFile item);
    public ItemFile buscarCompartido(ItemFileDetalle det); 
    public void eliminarPlazoDeCarpeta(ItemFile item);
    public void desactivarDocPorTipoDocumento(TipoEspecializado  tipo);
    public List<ItemFile> buscarHistorialPorDocumento(ItemFile item);
    public List<String> buscarTiposDocumentos(int usuCod, int orgID);
    public int buscarValorPorTipo(TipoEspecializado tipo);//Para Reporte
    public List<ItemFile> obtenerTodasCarpetas();
    public void activarDocPorTipoDocumento(TipoEspecializado  tipo);
}
