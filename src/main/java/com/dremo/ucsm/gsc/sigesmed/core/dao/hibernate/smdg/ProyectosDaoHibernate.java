/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.smdg;

import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.*;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.Proyectos;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Administrador
 */
public class ProyectosDaoHibernate extends GenericDaoHibernate<Proyectos> implements ProyectosDao{
    @Override
    public String buscarUltimoCodigo() {
        String codigo = null;
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        try{
//            //listar ultimo codigo de tramite
//            String hql = "SELECT a.apoId FROM Apoderado a ORDER BY a.apoId DESC ";
//            Query query = session.createQuery(hql);
//            query.setMaxResults(1);
//            codigo = ((String)query.uniqueResult());
//            
//            //solo cuando no hay ningun registro aun
//            if(codigo==null)
//                codigo = "0000";
//        
//        }catch(Exception e){
//            System.out.println("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());
//            throw new UnsupportedOperationException("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());            
//        }
//        finally{
//            session.close();
//        }
        return codigo;
    }

    @Override
    public List<Object[]> listarProyectos(int orgID) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Object[]> proyectos = null;
        Transaction t = session.beginTransaction();
        Query query = null;
        try{
            
            query = session.createSQLQuery("SELECT p.pro_id, p.pro_nom, p.pro_res, p.pro_ini, p.pro_fin, \n" +
                "pd.pde_doc, pe.nom, pe.ape_pat, pe.ape_mat, pe.dni, avg(pa.pac_ava) \n" +
                "FROM institucional.proyectos p \n" +
                "LEFT JOIN institucional.proyecto_detalle pd ON p.pro_id = pd.pro_id \n" +
                "LEFT JOIN trabajador t ON p.pro_res = t.tra_id \n" +
                "LEFT JOIN pedagogico.persona pe ON pe.per_id = t.per_id \n" +
                "LEFT JOIN institucional.proyecto_actividades pa ON pa.pro_id = p.pro_id AND pa.est_reg = 'A' \n" +
                "WHERE p.est_reg = 'A' AND t.org_id ="+ orgID+"\n" +
                "GROUP BY p.pro_id, p.pro_nom, p.pro_res, p.pro_ini, p.pro_fin, pd.pde_doc, pe.nom, pe.ape_pat, pe.ape_mat, pe.dni \n" +
                "ORDER BY p.pro_id;");  
                        
//            String hql = "SELECT p FROM PlantillaFichaInstitucional p join fetch p.tipo t";
            proyectos = query.list();
            t.commit();
           
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar las proyectos \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar las proyectos \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return proyectos;
    }
    @Override
    public List<Object[]> listarTrabajadores(int traid) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Object[]> trabajadores = null;
        Transaction t = session.beginTransaction();
        Query query = null;
        try{
            
            query = session.createSQLQuery("select t.tra_id, p.dni, p.nom, p.ape_pat, p.ape_mat from trabajador t \n" +
                                            "join pedagogico.persona p on t.per_id = p.per_id \n" +
                                            "where t.org_id = " + traid );
                        
//            String hql = "SELECT p FROM PlantillaFichaInstitucional p join fetch p.tipo t";
            trabajadores = query.list();
            t.commit();
            
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los trabajadores \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los trabajadores \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return trabajadores;
    }

    @Override
    public List<Object[]> listarProyectosDocente(int idDoc, int idOrg) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String sql = "SELECT p.pro_id, p.pro_nom, p.pro_res, p.pro_ini, p.pro_fin, " +
                    "pd.pde_doc, pe.nom, pe.ape_pat, pe.ape_mat, pe.dni, avg(pa.pac_ava) " +
                    "FROM institucional.proyectos p " +
                    "LEFT JOIN institucional.proyecto_detalle pd ON p.pro_id = pd.pro_id " +
                    "LEFT JOIN trabajador t ON p.pro_res = t.tra_id " +
                    "LEFT JOIN pedagogico.persona pe ON pe.per_id = t.per_id " +
                    "JOIN institucional.proyecto_actividades pa ON pa.pro_id = p.pro_id AND pa.est_reg = 'A' " +
                    "WHERE p.est_reg = 'A' AND p.usu_cre_id =:usu AND p.org_id =:orga " +
                    "GROUP BY p.pro_id, p.pro_nom, p.pro_res, p.pro_ini, p.pro_fin, pd.pde_doc, pe.nom, pe.ape_pat, pe.ape_mat, pe.dni " +
                    "ORDER BY p.pro_id;";
            SQLQuery query = session.createSQLQuery(sql);
            query.setInteger("usu",idDoc);
            query.setInteger("orga",idOrg);
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
}
