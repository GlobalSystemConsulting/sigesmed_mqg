/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.LibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.Asiento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.DetalleCuenta;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.RegistroCompras;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.RegistroVentas;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author RE
 */
public class ListarTransaccionesConDocumentoPorOrganizacionTX implements ITransaction{
    
     @Override
    public WebResponse execute(WebRequest wr){
        //Parte de la operacion con la Base de Datos
        
        JSONObject requestData = (JSONObject)wr.getData();
        int organizacionID = requestData.getInt("organizacionID");
        
        List<List<Object>> objetos=null;      
        LibroCajaDao libroDao = (LibroCajaDao)FactoryDao.buildDao("sci.LibroCajaDao");
       
       
        try {
         //   objetos= libroDao.listarAsientosConCompraVenta(organizacionID);
            
        } catch (Exception e) {
            System.out.println("No se pudo Listar las Transacciones\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar las Transacciones", e.getMessage() );
        }
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        
        for(List<Object> list:objetos ){
              JSONObject oResponse = new JSONObject();
              
            if(list.get(0)!=null){
                Asiento objA=(Asiento)list.get(0);               
                  
                oResponse.put("operacionID",objA.getOpeId());
                oResponse.put("glosa",objA.getGloOpe());
              //oResponse.put("importe",importe);
                oResponse.put("numeroD",objA.getNumDoc());
             // oResponse.put("observacion",observacion);
                oResponse.put("estado",objA.getEstReg());
                oResponse.put("libro",objA.getCodLibro());
              
                JSONObject fa = new JSONObject(); 
                    fa.put("y",objA.getFecAsi().getYear());fa.put("m",objA.getFecAsi().getMonth());fa.put("d",objA.getFecAsi().getDate());

                    oResponse.put("fecha",fa);
                  
                String registro= ""+objA.getCodLibro();
                
                  for(DetalleCuenta det:objA.getDetalleCuentas()){                       
                      
                       
                       if(det.getNatDetCue()){
                           JSONObject jDebe = new JSONObject();         
                                jDebe.put("cuentaContableID", det.getCuentaContable().getCueConId());
                                jDebe.put("nombre",det.getCuentaContable().getNomCue());  
                                jDebe.put("importe",det.getImpDetCue()); 
                           oResponse.put("importe",det.getImpDetCue());
                           oResponse.put("debe",jDebe);
                       }
                       else{
                            JSONObject jHaber = new JSONObject();         
                                jHaber.put("cuentaContableID", det.getCuentaContable().getCueConId());
                                jHaber.put("nombre",det.getCuentaContable().getNomCue());  
                                jHaber.put("importe",det.getImpDetCue()); 
                            oResponse.put("importe",det.getImpDetCue());
                            oResponse.put("haber",jHaber);
                                   }
                       
                  }
                   
                                                                  
              
                
                 if(list.get(1)!=null && registro.equals("C")){
                     RegistroCompras objC=(RegistroCompras)list.get(1);
                     
                    JSONObject jTipoPago = new JSONObject();         
                        jTipoPago.put("tipoPagoID", objC.getTipoPago().getTipPagId());
                        jTipoPago.put("nombre",objC.getTipoPago().getNom());        
                    oResponse.put("tipoPago",jTipoPago);  
                    
                     JSONObject jClienteProveedor = new JSONObject();         
                        jClienteProveedor.put("clienteProveedorID", objC.getClienteProveedor().getCliProId());
                        jClienteProveedor.put("datos",objC.getClienteProveedor().getDat());        
                        jClienteProveedor.put("estado",objC.getClienteProveedor().getEstReg());   
                        jClienteProveedor.put("tipoDocumento",objC.getClienteProveedor().getTipoDocumentoIdentidad().getNom());        
                        jClienteProveedor.put("estado",objC.getClienteProveedor().getEstReg()); 
                    oResponse.put("clienteProveedor",jClienteProveedor);   
                     
                 }                 
                 else if(list.get(1)!=null && registro.equals("V")){
                     RegistroVentas objV=(RegistroVentas)list.get(1);
                    JSONObject jTipoPago = new JSONObject();         
                        jTipoPago.put("tipoPagoID", objV.getTipoPago().getTipPagId());
                        jTipoPago.put("nombre",objV.getTipoPago().getNom());        
                    oResponse.put("tipoPago",jTipoPago);    
                    
                     JSONObject jClienteProveedor = new JSONObject();         
                        jClienteProveedor.put("clienteProveedorID", objV.getClienteProveedor().getCliProId());
                        jClienteProveedor.put("datos",objV.getClienteProveedor().getDat());        
                        jClienteProveedor.put("estado",objV.getClienteProveedor().getEstReg());   
                        jClienteProveedor.put("tipoDocumento",objV.getClienteProveedor().getTipoDocumentoIdentidad().getNom());        
                        jClienteProveedor.put("estado",objV.getClienteProveedor().getEstReg()); 
                    oResponse.put("clienteProveedor",jClienteProveedor);
                    
                    
                 }
                 
          
            
        }                    
          miArray.put(oResponse);
       
        }
         return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    
    }
}
