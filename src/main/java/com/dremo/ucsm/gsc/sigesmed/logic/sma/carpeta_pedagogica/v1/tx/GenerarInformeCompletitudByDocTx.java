/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.carpeta_pedagogica.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.ContenidoSeccionesCarpetaPedagogicaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.DocenteCarpetaPedagogicaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica.DocenteCarpetaPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica.EsquemaCarpeta;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class GenerarInformeCompletitudByDocTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(GenerarInformeCompletitudByDocTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        return generarInformeCarpeta(data);
    }

    private WebResponse generarInformeCarpeta(JSONObject data) {
        try{
            ContenidoSeccionesCarpetaPedagogicaDao contDao = (ContenidoSeccionesCarpetaPedagogicaDao) FactoryDao.buildDao("sma.ContenidoSeccionesCarpetaPedagogicaDao");
            DocenteCarpetaPedagogicaDao conDocDao = (DocenteCarpetaPedagogicaDao) FactoryDao.buildDao("sma.DocenteCarpetaPedagogicaDao");
            List<EsquemaCarpeta> esquema = contDao.listarEsquemaCarpeta(data.getInt("car"));
            List<DocenteCarpetaPedagogica> contDoc = conDocDao.listarContenidosDocente(data.getInt("org"), data.getInt("car"), data.getInt("doc"));
            Map<String, Integer> contenidosDocente = new TreeMap<String, Integer>();
            for(DocenteCarpetaPedagogica dcp:contDoc){
                contenidosDocente.put(dcp.getDes(), dcp.getEstAva());
            }
            
            data.put("carEta", contDoc.get(0).getCarpeta().getEta().toString());
            String b64 = generarBase64(esquema, contenidosDocente, data);
            return WebResponse.crearWebResponseExito("Se listo correctamente los datos",new JSONObject().put("file",b64));
        }catch (Exception e){
            logger.log(Level.SEVERE,"generarReporteCarpeta",e);
            return WebResponse.crearWebResponseError("No se puede crear el reporte");
        }
    }
    private String generarBase64(List<EsquemaCarpeta> esquema, Map<String, Integer> contenidosDocente, JSONObject data) throws Exception{
        Map<String,List<EsquemaCarpeta>> seccionesCarpeta = new TreeMap<>();
        for(EsquemaCarpeta esq:esquema){
            String nomSeccion = esq.getSecNom();
            if(nomSeccion != null){
                if(seccionesCarpeta.containsKey(nomSeccion)){
                    //si existe
                    seccionesCarpeta.get(nomSeccion).add(esq);
                }else{
                    // no contiene
                    List<EsquemaCarpeta> archivosSeccion = new ArrayList<>();
                    archivosSeccion.add(esq);
                    seccionesCarpeta.put(nomSeccion,archivosSeccion);
                }
            }
        }
        
        //Creación del reporte
        Mitext mitext = new Mitext(false,"");
        mitext.agregarTitulo("Informe de Completitud de Documentos de Carpeta Pedagogica " + data.getInt("carEta"));
        mitext.agregarParrafo("");
        
        OrganizacionDao orgDao = (OrganizacionDao) FactoryDao.buildDao("sma.OrganizacionDao");
        Organizacion org = null;
        switch(data.getInt("opcion")){
            //IE
            case 1: 
                    org = orgDao.buscarPorId(data.getInt("org"));
                    float[] columnWidths1={7,8};
                    Table tabla1 = new Table(columnWidths1);  
                    tabla1.setWidthPercent(100);
                    tabla1.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("INSTITUCION EDUCATIVA").setBold().setFontSize(11).setTextAlignment(TextAlignment.LEFT)));
                    tabla1.addCell(new Cell(0,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : " + org.getNom()).setFontSize(12)).setTextAlignment(TextAlignment.LEFT));

                    tabla1.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("DOCENTE").setBold().setFontSize(11).setTextAlignment(TextAlignment.LEFT)));
                    tabla1.addCell(new Cell(0,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : " + data.getString("docDat")).setFontSize(12)).setTextAlignment(TextAlignment.LEFT));
                    mitext.agregarTabla(tabla1);
                    break;
            //UGEL
            case 2: 
                    org = orgDao.buscarPorId(data.getInt("ugelId"));
                    float[] columnWidths2={7,8};
                    Table tabla2 = new Table(columnWidths2);
                    tabla2.setWidthPercent(100);
                    
                    tabla2.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("UNIDAD DE GESTIÓN EDUCATIVA LOCAL").setBold().setFontSize(11).setTextAlignment(TextAlignment.LEFT)));
                    tabla2.addCell(new Cell(0,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : " + org.getNom()).setFontSize(12)).setTextAlignment(TextAlignment.LEFT));
                    
                    tabla2.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("INSTITUCION EDUCATIVA").setBold().setFontSize(11).setTextAlignment(TextAlignment.LEFT)));
                    tabla2.addCell(new Cell(0,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : " + data.getString("ieDat")).setFontSize(12)).setTextAlignment(TextAlignment.LEFT));

                    tabla2.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("DOCENTE").setBold().setFontSize(12).setTextAlignment(TextAlignment.LEFT)));
                    tabla2.addCell(new Cell(0,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : " + data.getString("docDat")).setFontSize(12)).setTextAlignment(TextAlignment.LEFT));
                    mitext.agregarTabla(tabla2);
                    break;
            
            //DRE
            case 3: 
                    org = orgDao.buscarPorId(data.getInt("dreId"));
                    float[] columnWidths3={7,8};
                    Table tabla3 = new Table(columnWidths3);
                    tabla3.setWidthPercent(100);
                    
                    tabla3.addCell(new Cell(0,0).setBorder(Border.NO_BORDER).add(new Paragraph("DIRECCION REGIONAL DE EDUCACIÓN").setBold().setFontSize(11).setTextAlignment(TextAlignment.LEFT)));
                    tabla3.addCell(new Cell(0,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : " + org.getNom()).setFontSize(12)).setTextAlignment(TextAlignment.LEFT));
                    
                    tabla3.addCell(new Cell(1,0).setBorder(Border.NO_BORDER).add(new Paragraph("UNIDAD DE GESTIÓN EDUCATIVA LOCAL").setBold().setFontSize(11).setTextAlignment(TextAlignment.LEFT)));
                    tabla3.addCell(new Cell(0,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : " + data.getString("ugelDat")).setFontSize(12)).setTextAlignment(TextAlignment.LEFT));
                    
                    tabla3.addCell(new Cell(1,0).setBorder(Border.NO_BORDER).add(new Paragraph("INSTITUCION EDUCATIVA").setBold().setFontSize(11).setTextAlignment(TextAlignment.LEFT)));
                    tabla3.addCell(new Cell(0,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : " + data.getString("ieDat")).setFontSize(12)).setTextAlignment(TextAlignment.LEFT));

                    tabla3.addCell(new Cell(1,0).setBorder(Border.NO_BORDER).add(new Paragraph("DOCENTE").setBold().setFontSize(11).setTextAlignment(TextAlignment.LEFT)));
                    tabla3.addCell(new Cell(0,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : " + data.getString("docDat")).setFontSize(12)).setTextAlignment(TextAlignment.LEFT));
                    mitext.agregarTabla(tabla3);
                    break;
        }
        

        PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
        Paragraph p = new Paragraph("Secciones de Carpeta Pedagogica");
        p.setKeepTogether(true);
        p.setFont(bold).setFontSize(12);
        mitext.getDocument().add(p);
        int i = 1;
        for(Map.Entry<String,List<EsquemaCarpeta>> entry : seccionesCarpeta.entrySet()){
            p = new Paragraph("SECCION " + i +": " + entry.getKey());
            p.setKeepTogether(true);
            p.setFont(bold).setFontSize(12);
            mitext.getDocument().add(p);

            int j = 1;
            int docsCompletos = 0;
            GTabla gtabla = new GTabla(new float[]{0.5f,2.5f,1.0f});
            gtabla.build(new String[]{"N°","Nombre de Documento","Estado"});
            for(EsquemaCarpeta esq : entry.getValue()){
                int estAva = contenidosDocente.get(esq.getConNom());
                if(estAva == 1) docsCompletos++;
                gtabla.processLine(new String[]{String.valueOf(j),esq.getConNom(), mostrarDescripcionEstado(estAva)});
                j++;
            }
            i++;
            mitext.agregarTabla(gtabla);
            
            float[] columnWidthsDt={3,3};
            Table t = new Table(columnWidthsDt);
            t.setWidthPercent(100);
            t.addCell(new Cell(0,0).setBorder(Border.NO_BORDER).add(new Paragraph("Documentos completos: ").setBold().setFontSize(12).setTextAlignment(TextAlignment.LEFT)));
            t.addCell(new Cell(0,1).setBorder(Border.NO_BORDER).add(new Paragraph(docsCompletos + "/" + (j-1)).setFontSize(12)).setTextAlignment(TextAlignment.LEFT));
            mitext.agregarTabla(t); 
            mitext.agregarParrafo("");

        }
        mitext.cerrarDocumento();
        return mitext.encodeToBase64();
    }
    
    private String mostrarDescripcionEstado(Integer estAva){
        switch(estAva){
            case 1: return "Completo";
            case 2: return "En Proceso";
            case 3: return "Pendiente";
        }
        return null;
    }
}
