package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.CronogramaSedeCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.CursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.HorarioSedeCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.ObjetivoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.OrganizacionCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.PerfilCapacitadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.SedeCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CronogramaSedeCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.HorarioSedeCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.ObjetivoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.PerfilCapacitador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.SedeCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;
import org.json.JSONArray;
import org.json.JSONObject;

public class EditarCursoCapacitacionTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(EditarCursoCapacitacionTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();

            OrganizacionCapacitacionDao organizacionDao = (OrganizacionCapacitacionDao) FactoryDao.buildDao("capacitacion.OrganizacionCapacitacionDao");
            CursoCapacitacionDao cursoCapacitacionDao = (CursoCapacitacionDao) FactoryDao.buildDao("capacitacion.CursoCapacitacionDao");
            ObjetivoCapacitacionDao objetivoCapacitacionDao = (ObjetivoCapacitacionDao) FactoryDao.buildDao("capacitacion.ObjetivoCapacitacionDao");
            PerfilCapacitadorDao perfilCapacitadorDao = (PerfilCapacitadorDao) FactoryDao.buildDao("capacitacion.PerfilCapacitadorDao");
            SedeCapacitacionDao sedeCapacitacionDao = (SedeCapacitacionDao) FactoryDao.buildDao("capacitacion.SedeCapacitacionDao");
            CronogramaSedeCapacitacionDao cronogramaSedeCapacitacionDao = (CronogramaSedeCapacitacionDao) FactoryDao.buildDao("capacitacion.CronogramaSedeCapacitacionDao");
            HorarioSedeCapacitacionDao horarioSedeCapacitacionDao = (HorarioSedeCapacitacionDao) FactoryDao.buildDao("capacitacion.HorarioSedeCapacitacionDao");

            Organizacion organizacion = organizacionDao.buscarPorId(data.getInt("org"));
            Organizacion organizacionAut = organizacionDao.buscarPorId(data.getInt("orgAut"));
            
            CursoCapacitacion course = new CursoCapacitacion();
            course.setCurCapId(data.getInt("cod"));
            course.setTip(data.getString("tip"));
            course.setNom(data.getString("nom").toUpperCase());
            course.setOrganizacion(organizacion);
            course.setOrganizacionAut(organizacionAut);
            course.setUsuMod(data.getInt("usuMod"));
            course.setFecCre(new Date());
            course.setEstReg('A');
            course.setFecMod(new Date());
            course.setEst('A');
            
            JSONArray sedes = data.getJSONArray("sed");
            Date fecIniCap = null;
            Date fecFinCap = null;
            
            for(int i = 0;i < sedes.length();i++) {
                JSONObject sede = sedes.getJSONObject(i);
                
                SedeCapacitacion location = sedeCapacitacionDao.buscarConCronograma(sede.getInt("cod"));
                location.setLoc(sede.getString("loc"));
                location.setDir(sede.getString("dir"));
                location.setRef(sede.getString("ref"));
                sedeCapacitacionDao.update(location);
                
                JSONObject cronograma = sede.getJSONObject("cro");
                
                CronogramaSedeCapacitacion schedule = location.getCronograma();
                Date fecIni = HelpTraining.getStartOfDay(DatatypeConverter.parseDateTime(cronograma.getString("fecIni")).getTime());
                Date fecFin = HelpTraining.getEndOfDay(DatatypeConverter.parseDateTime(cronograma.getString("fecFin")).getTime());
                schedule.setDniRes(cronograma.getString("dniRes"));
                schedule.setNomRes(cronograma.getString("nomRes"));
                schedule.setFecIni(fecIni);
                schedule.setFecFin(fecFin);
                cronogramaSedeCapacitacionDao.update(schedule);
                                
                JSONArray horarios = sede.getJSONObject("hor").getJSONArray("shifts");

                for(int j = 0;j < horarios.length();j++) {
                    JSONObject horario = horarios.getJSONObject(j);
                    
                    HorarioSedeCapacitacion shift = horarioSedeCapacitacionDao.buscarPorId(horario.getInt("cod"));
                    Date turIni = HelpTraining.getStartOfTime(DatatypeConverter.parseDateTime(horario.getString("turIni")).getTime());
                    Date turFin = HelpTraining.getStartOfTime(DatatypeConverter.parseDateTime(horario.getString("turFin")).getTime());
                    shift.setDia(horario.getInt("dia"));
                    shift.setTurIni(turIni);
                    shift.setTurFin(turFin);
                    horarioSedeCapacitacionDao.update(shift);
                }
                
                if (fecIniCap == null || fecIniCap.after(fecIni)) {
                    fecIniCap = fecIni;
                }

                if (fecFinCap == null || fecFinCap.before(fecFin)) {
                    fecFinCap = fecFin;
                }
            }
            
            course.setFecIni(fecIniCap);
            course.setFecFin(fecFinCap);
            
            if (course.getFecIni().before(new Date()))
                if (course.getFecFin().before(new Date()))
                    course.setEst('F');
                else
                    course.setEst('I');
            
            if(data.getJSONObject("obj").getBoolean("est")) {
                JSONObject objetivo = data.getJSONObject("obj").getJSONObject("obj");
                Set<ObjetivoCapacitacion> goals = new HashSet<>();                
                JSONArray objetivos = objetivo.getJSONArray("array");
                
                if(objetivo.getString("modo").equals("Busqueda")) {
                    for(int i = 0;i < objetivos.length();i++)
                        goals.add(objetivoCapacitacionDao.buscarPorId(objetivos.getInt(i)));
                } else {
                    for(int i = 0;i < objetivos.length();i++) {
                        ObjetivoCapacitacion goal = new ObjetivoCapacitacion(objetivos.getJSONObject(i).getString("tip").charAt(0),
                            objetivos.getJSONObject(i).getString("des"), "Fuente"
                        );

                        objetivoCapacitacionDao.insert(goal);
                        goals.add(goal);
                    }
                }
                
                course.setObjetivosCapacitacion(goals);
            }
            
            if(data.getJSONObject("per").getBoolean("est")) {
                JSONObject perfil = data.getJSONObject("per").getJSONObject("obj");
                Set<PerfilCapacitador> profiles = new HashSet<>();
                JSONArray perfiles = perfil.getJSONArray("array");
                
                if(perfil.getString("modo").equals("Busqueda")) {
                    for(int i = 0;i < perfiles.length();i++)
                        profiles.add(perfilCapacitadorDao.buscarPorId(perfiles.getInt(i)));
                } else {
                    for(int i = 0;i < perfiles.length();i++) {
                        PerfilCapacitador profile = new PerfilCapacitador(perfiles.getJSONObject(i).getString("tip").charAt(0),
                            perfiles.getJSONObject(i).getString("des"), "Fuente"
                        );

                        perfilCapacitadorDao.insert(profile);
                        profiles.add(profile);
                    }
                }
                
                course.setPerfilesCapacitacion(profiles);
            }
            
            cursoCapacitacionDao.update(course);
            return WebResponse.crearWebResponseExito("Exito al editar el curso", WebResponse.OK_RESPONSE);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "editarCursoCapacitacion", e);
            return WebResponse.crearWebResponseError("Error al editar el curso", WebResponse.BAD_RESPONSE);
        }
    }
}
