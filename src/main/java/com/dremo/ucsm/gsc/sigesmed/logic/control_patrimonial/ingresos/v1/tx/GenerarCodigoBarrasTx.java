/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

/**
 *
 * @author harold
 */

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.BienesMueblesDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesMuebles;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.onbarcode.barcode.Code128;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Random;
import org.json.JSONArray;
import org.json.JSONObject;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author harold
 */
public class GenerarCodigoBarrasTx implements ITransaction {
    
    
    private static final String CHARACTER_SET = "0123456789";
    private static Random rnd = new Random();
    
    @Override
    public WebResponse execute(WebRequest wr) {
        
        JSONObject requestData = (JSONObject) wr.getData();        
        
        String barCode = "";
        
        try {
            
            int cod_bie = requestData.getInt("cod_bie");
            
            BienesMuebles bm = null;
            BienesMueblesDAO bie_mue_dao = (BienesMueblesDAO)FactoryDao.buildDao("scp.BienesMueblesDAO");
            bm = bie_mue_dao.obtener_bien(cod_bie);
            
            if (bm.getCod_ba_bie().equals("0")) {
                
                barCode = generateBarCodeNumber(cod_bie, 10);
                createBarCode(cod_bie, barCode);
                
                bm.setCod_ba_bie(barCode);
                
                bie_mue_dao.update(bm);
            
            } else {
                barCode = bm.getCod_ba_bie();
            }           
            
            Mitext document = null;            
            
            document = new Mitext(true, "Codigo de Barras");                       
            
            String pathFinal = ServicioREST.PATH_SIGESMED + File.separator + "archivos" + File.separator + "control_patrimonial"
                + File.separator + "codigo_barras" + File.separator + barCode + ".png";
            
            document.agregarImagen(pathFinal);            
            
            document.agregarParrafo(bm.getDes_bie());
            document.newLine(2);
            
            //document.add(createFirstTable());
            document.cerrarDocumento();
            System.out.println("Done");
            
            JSONArray responseData = new JSONArray();
            
            JSONObject oResponse = new JSONObject();
            oResponse.put("datareporte", document.encodeToBase64());
            responseData.put(oResponse);
            
            return WebResponse.crearWebResponseExito("Se genero el codigo de barras exitosamente", responseData);
            
        } catch (Exception e) {
            System.out.println(e);
            return WebResponse.crearWebResponseError("Error al generar el codigo de barras", e.getMessage());
        }
    }
    
    /**
     * Crea la imagen con el codigo de barras
     * del bien mueble
     * @throws Exception 
     */
     
    public void createBarCode(int bien_id, String barCodeBien) throws Exception{
        
        Code128 barcode = new Code128();       
                
        //barcode.setData("6889265987");
        barcode.setData(barCodeBien);
        barcode.setX(2);
        barcode.setY(60);
        barcode.setBarcodeWidth(150);
        barcode.setBarcodeHeight(80);

        barcode.setLeftMargin(0);
        barcode.setRightMargin(0);
        //barcode.drawBarcode("/home/harold/Downloads/barcode-code128.png");

        String pathFinal = ServicioREST.PATH_SIGESMED + File.separator + "archivos" + File.separator + "control_patrimonial"
                + File.separator + "codigo_barras";

        (new File(pathFinal)).mkdir();

        barcode.drawBarcode(pathFinal + "/"+ barCodeBien +".png");
    }
    
    private String generateBarCodeNumber(int bien_id, int length) {
        
        String strBienId = String.valueOf(bien_id);
        
        int tam = strBienId.length();
        
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < length - tam; i++) {
            builder.append(CHARACTER_SET.charAt(rnd.nextInt(CHARACTER_SET.length())));
        }
        return strBienId + builder.toString();
        
    }
}
