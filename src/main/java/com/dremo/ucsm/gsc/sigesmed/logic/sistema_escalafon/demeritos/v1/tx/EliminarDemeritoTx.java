/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.demeritos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DemeritoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Demerito;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class EliminarDemeritoTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(EliminarDemeritoTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        Integer demId = 0;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            demId = requestData.getInt("demId");          
        
        }catch(Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"eliminarDemerito",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar", e.getMessage() );
        }
        
        DemeritoDao demeritoDao = (DemeritoDao)FactoryDao.buildDao("se.DemeritoDao");
        try{
            demeritoDao.deleteAbsolute(new Demerito(demId));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar el demerito\n" + e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el demerito", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("El demerito se elimino correctamente");
    }
    
}
