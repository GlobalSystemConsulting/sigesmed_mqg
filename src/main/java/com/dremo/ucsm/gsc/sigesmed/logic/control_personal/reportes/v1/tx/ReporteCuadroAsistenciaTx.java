/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.reportes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.AsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.Inasistencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.Justificacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.JustificacionInasistenciaTrabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.RegistroAsistencia;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import com.dremo.ucsm.gsc.sigesmed.util.GCell;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Base64;
import org.jfree.data.general.DefaultPieDataset;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Carlos
 */
public class ReporteCuadroAsistenciaTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date fechaReporte;
        Date fechaFinReporte;
        Organizacion org;
        String diaSemana[]={"D","L","M","M","J","V","S"};
        String diaOfMonth[]={"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"};
//        String cab1[]={"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","INASIST.JUST","TARD.JUST","ACTA VACAC","ACTA 3 DIAS","INASIST.INJUST.","TARD.INJUST","PERMISOS","HUELGA"};
//        String cab2[]={"N","APELLIDOS Y NOMBRES","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","DIAS","HORA","HORA","MIN","DIAS","DIAS","DIAS","HORAS","HORAS","MIN","DIAS","HORAS","MIN","DIAS"};//
        JSONObject requestData = (JSONObject)wr.getData();
        String nombreOrg = "";
        try{
           
            String fecha=requestData.getString("fecha");
            fechaReporte=df.parse(fecha);
            nombreOrg = requestData.getString("nombreOrg");
            Integer org_=requestData.getInt("organizacionID");
            org=new Organizacion(org_);
            Integer diaLast=DateUtil.obtenerUltimoDiaMesSegunFecha(fechaReporte);
            fechaFinReporte=DateUtil.addDays(fechaReporte, diaLast);
            fechaFinReporte=DateUtil.sumarRestarMinutosFecha(fechaFinReporte, 1);
//            Integer diaOfWeek=DateUtil.obtenerDiaSemanaSegunFecha(fechaReporte);
//            int j=diaOfWeek-1;
//            for(int i=0;i<diaLast;i++)
//            {
//                cab1[i+1]=diaOfMonth[i];
//                cab2[i+2]=diaSemana[j++];
//                if(j==7) j=0;
//            }
    
        }catch(Exception e){
            System.out.println("No se pudo verificar los datos \n"+e);
            return WebResponse.crearWebResponseError("No se pudo verificar los datos ", e.getMessage() );
        }
        List<Trabajador> trabajadores=null;
        
        try{
            AsistenciaDao asistenciaDao = (AsistenciaDao)FactoryDao.buildDao("cpe.AsistenciaDao");
            trabajadores=asistenciaDao.listarConsolidadoAsistenciaTrabajadorByFecha(fechaReporte, fechaFinReporte, org);
  
            
            
        }catch(Exception e){
            System.out.println("No se pudo verificar la asistencia \n"+e);
            return WebResponse.crearWebResponseError("No se pudo verificar la asistencia ", e.getMessage() );
        }
        
        
        //Creando el reporte....        
        Mitext m = null;        
        try {
            m = new Mitext();
            m.newLine(1);
            String titulo = "CUADRO DE ASISTENCIA E INASISTENCIA DEL PERSONAL DOCENTE Y AUXILIARES - " + DateUtil.getAnhoFecha((Date)fechaReporte);
            m.setStyle(1, 12, false, false, false);
            m.agregarParrafoMyEstilo(titulo, 1);
            m.newLine(1);
        } catch (Exception ex) {
            System.out.println("No se pudo crear el documento \n"+ex);
            Logger.getLogger(ReporteCuadroAsistenciaTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        String mes="";
        switch(DateUtil.obtenerMesSegunFecha(fechaReporte))
        {
            case 0 :mes="ENERO";
                break;
            case 1 :mes="FEBRERO";
                break;    
            case 2 :mes="MARZO";
                break;    
            case 3 :mes="ABRIL";
                break;    
            case 4 :mes="MAYO";
                break;    
            case 5 :mes="JUNIO";
                break;    
            case 6 :mes="JULIO";
                break;    
            case 7 :mes="AGOSTO";
                break;    
            case 8 :mes="SEPTIEMBRE";
                break;    
            case 9 :mes="OCTUBRE";
                break;    
            case 10 :mes="NOVIEMBRE";
                break;    
            case 11 :mes="DICIEMBRE";
                break;    
                
        }
        
        
        
        
        float[] columnWidthsD={4,6,1,4,6,1,1,1,1,1};
        Table tabla = new Table(columnWidthsD);
        tabla.setWidthPercent(100);
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("INSTITUCION EDUCATIVA").setFontSize(8).setTextAlignment(TextAlignment.LEFT)));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+nombreOrg).setFontSize(8)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));

        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("CODIGO MODULAR").setFontSize(8).setTextAlignment(TextAlignment.LEFT)));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : ").setFontSize(8)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,5).setBorder(Border.NO_BORDER));

        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("NIVEL EDUCATIVO").setFontSize(8)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+mes).setFontSize(8)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));

        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("MES").setFontSize(8)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+mes+" "+DateUtil.obtenerAnioSegunFecha(fechaReporte)).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,5).setBorder(Border.NO_BORDER));
        
        float[] columnWidths = new float[]{2, 10, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 5};
        float[] columnWidths2 = new float[]{2, 10, 5, 1, 1, 31, 1, 3, 5};
        GTabla t = new GTabla(columnWidths);
        
       
        
        try {
            t.build("N", 8, 2, 1);
            t.build("APELLIDOS Y NOMBRES", 8, 2, 1);
            t.build("CARGO tutor(a)", 8, 2, 1);
            t.build("CONDICION", 3, 2, 1);
            t.build("JORNADA LABORAL", 3, 2, 1);
            t.build("", 6, 1, 31);
            t.build("DIAS ASISTIDOS", 3, 2, 1);
            t.build("INASISTENCIAS",3, 1, 3);
            t.build("OBSERVACIONES", 6, 2, 1);
            for (String s : diaOfMonth) {
                t.build(s, 6, 1, 1);
            }
            t.build("Dias", 6, 1, 1);
            t.build("HRs", 6, 1, 1);
            t.build("MIN", 6, 1, 1);
            t.setWidthPercent(100);
        } catch (IOException ex) {
            Logger.getLogger(ReporteCuadroAsistenciaTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
        
        GCell[] cell = new GCell[]{
            t.createCellCenter(1, 1), t.createCellLeft(1, 1), t.createCellCenter(1, 1),
            t.createCellCenter(1, 1), t.createCellCenter(1, 1), t.createCellCenter(1, 1),
            t.createCellCenter(1, 1), t.createCellCenter(1, 1), t.createCellCenter(1, 1),
            t.createCellCenter(1, 1), t.createCellCenter(1, 1), t.createCellCenter(1, 1),
            t.createCellCenter(1, 1), t.createCellCenter(1, 1), t.createCellCenter(1, 1),
            t.createCellCenter(1, 1), t.createCellCenter(1, 1), t.createCellCenter(1, 1),
            t.createCellCenter(1, 1), t.createCellCenter(1, 1), t.createCellCenter(1, 1),
            t.createCellCenter(1, 1), t.createCellCenter(1, 1), t.createCellCenter(1, 1),
            t.createCellCenter(1, 1), t.createCellCenter(1, 1), t.createCellCenter(1, 1),
            t.createCellCenter(1, 1), t.createCellCenter(1, 1), t.createCellCenter(1, 1),
            t.createCellCenter(1, 1), t.createCellCenter(1, 1), t.createCellCenter(1, 1),
            t.createCellCenter(1, 1), t.createCellCenter(1, 1), t.createCellCenter(1, 1),
            t.createCellCenter(1, 1), t.createCellCenter(1, 1), t.createCellCenter(1, 1),
            t.createCellCenter(1, 1), t.createCellCenter(1, 1)};        
        
        String[] fila_data = new String[]{"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""};
        Integer diasAsistidos = 0;
        Integer inasistenciaJustificadaDias = 0;
        Integer tardanzaJustificadaMin = 0;
        Integer tardanzaJustificadaHora = 0;
        Integer inasistenciaInjustificadaDias = 0;
        Integer tardanzaInjuustificadaMin = 0;
        Integer tardanzaInjuustificadaHora = 0;
        
        
        for (int i = 0; i < trabajadores.size(); i++) {
            
           fila_data[0] = "" + (i + 1) + "";
            fila_data[1] = trabajadores.get(i).getPersona().getNombrePersonaAP();
            fila_data[2] = trabajadores.get(i).getTraCar() != null ? trabajadores.get(i).getTraCar().getCrgTraNom() : "";
            fila_data[3] = trabajadores.get(i).getCondicion() == null ? "" : trabajadores.get(i).getCondicion() + "";
            fila_data[4] = trabajadores.get(i).getJornada() == null ? "" : trabajadores.get(i).getJornada() + "";
            
            
            for (RegistroAsistencia ra : trabajadores.get(i).getAsistencias()) {
                switch (DateUtil.obtenerDiaSegunFecha(ra.getHoraIngreso())) {
                    case 1:
                        if (ra.getEstAsi().equals("1") && fila_data[5].equals("")) {
                            fila_data[5] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[5].equals("")) {
                            diasAsistidos++;
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[5] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[5] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[5] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[5] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[5] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[5] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[5] = "TJ";
                                }

                            } else {
                                fila_data[5] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }

                        } else if (ra.getEstAsi().equals("3") && fila_data[5].equals("")) {

                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                inasistenciaJustificadaDias++;
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[5] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[5] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[5] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[5] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[5] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[5] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[5] = "FJ";
                                }

                            } else {
                                fila_data[5] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;
                    case 2:
                        if (ra.getEstAsi().equals("1") && fila_data[6].equals("")) {
                            fila_data[6] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[6].equals("")) {
                            diasAsistidos++;
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[6] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[6] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[6] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[6] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[6] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[6] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[6] = "TJ";
                                }

                            } else {
                                fila_data[6] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }
                        } else if (ra.getEstAsi().equals("3") && fila_data[6].equals("")) {
                            
                            if (ra.getJustificacion() != null) {
                                inasistenciaJustificadaDias++;
                                Justificacion jus = ra.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[6] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[6] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[6] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[6] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[6] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[6] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[6] = "FJ";
                                }

                            } else {
                                fila_data[6] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;
                    case 3:
                        if (ra.getEstAsi().equals("1") && fila_data[7].equals("")) {
                            fila_data[7] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[7].equals("")) {
                            diasAsistidos++;
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[7] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[7] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[7] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[7] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[7] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[7] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[7] = "TJ";
                                }

                            } else {
                                fila_data[7] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }

                        } else if (ra.getEstAsi().equals("3") && fila_data[7].equals("")) {

                            if (ra.getJustificacion() != null) {
                                inasistenciaJustificadaDias++;
                                Justificacion jus = ra.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[7] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[7] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[7] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[7] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[7] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[7] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[7] = "FJ";
                                }

                            } else {
                                fila_data[7] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;
                    case 4:
                        if (ra.getEstAsi().equals("1") && fila_data[8].equals("")) {
                            fila_data[8] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[8].equals("")) {
                            diasAsistidos++;    
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[8] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[8] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[8] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[8] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[8] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[8] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[8] = "TJ";
                                }

                            } else {
                                fila_data[8] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }

                        } else if (ra.getEstAsi().equals("3") && fila_data[8].equals("")) {

                            if (ra.getJustificacion() != null) {
                                inasistenciaJustificadaDias++;
                                Justificacion jus = ra.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[8] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[8] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[8] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[8] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[8] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[8] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[8] = "FJ";
                                }

                            } else {
                                fila_data[8] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;
                    case 5:
                        if (ra.getEstAsi().equals("1") && fila_data[9].equals("")) {
                            fila_data[9] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[9].equals("")) {
                            diasAsistidos++;
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[9] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[9] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[9] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[9] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[9] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[9] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[9] = "TJ";
                                }

                            } else {
                                fila_data[9] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }

                        } else if (ra.getEstAsi().equals("3") && fila_data[9].equals("")) {

                            if (ra.getJustificacion() != null) {
                                inasistenciaJustificadaDias++;
                                Justificacion jus = ra.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[9] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[9] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[9] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[9] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[9] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[9] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[9] = "FJ";
                                }

                            } else {
                                fila_data[9] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;
                    case 6:
                        if (ra.getEstAsi().equals("1") && fila_data[10].equals("")) {
                            fila_data[10] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[10].equals("")) {
                            diasAsistidos++;
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[10] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[10] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[10] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[10] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[10] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[10] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[10] = "TJ";
                                }

                            } else {
                                fila_data[10] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }

                        } else if (ra.getEstAsi().equals("3") && fila_data[10].equals("")) {

                            if (ra.getJustificacion() != null) {
                                inasistenciaJustificadaDias++;
                                Justificacion jus = ra.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[10] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[10] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[10] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[10] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[10] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[10] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[10] = "FJ";
                                }

                            } else {
                                fila_data[10] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;
                    case 7:
                        if (ra.getEstAsi().equals("1") && fila_data[11].equals("")) {
                            fila_data[11] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[11].equals("")) {
                            diasAsistidos++;
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[11] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[11] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[11] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[11] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[11] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[11] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[11] = "TJ";
                                }

                            } else {
                                fila_data[11] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }

                        } else if (ra.getEstAsi().equals("3") && fila_data[11].equals("")) {

                            if (ra.getJustificacion() != null) {
                                inasistenciaJustificadaDias++;
                                Justificacion jus = ra.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[11] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[11] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[11] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[11] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[11] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[11] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[11] = "FJ";
                                }

                            } else {
                                fila_data[11] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;
                    case 8:
                        if (ra.getEstAsi().equals("1") && fila_data[12].equals("")) {
                            fila_data[12] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[12].equals("")) {
                            diasAsistidos++;
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[12] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[12] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[12] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[12] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[12] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[12] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[12] = "TJ";
                                }

                            } else {
                                fila_data[12] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }

                        } else if (ra.getEstAsi().equals("3") && fila_data[12].equals("")) {

                            if (ra.getJustificacion() != null) {
                                inasistenciaJustificadaDias++;
                                Justificacion jus = ra.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[12] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[12] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[12] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[12] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[12] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[12] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[12] = "FJ";
                                }

                            } else {
                                fila_data[12] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;
                    case 9:
                        if (ra.getEstAsi().equals("1") && fila_data[13].equals("")) {
                            fila_data[13] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[13].equals("")) {
                            diasAsistidos++;
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[13] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[13] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[13] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[13] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[13] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[13] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[13] = "TJ";
                                }

                            } else {
                                fila_data[13] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }

                        } else if (ra.getEstAsi().equals("3") && fila_data[13].equals("")) {
                            if (ra.getJustificacion() != null) {
                                inasistenciaJustificadaDias++;
                                Justificacion jus = ra.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[13] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[13] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[13] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[13] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[13] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[13] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[13] = "FJ";
                                }

                            } else {
                                fila_data[13] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;
                    case 10:
                        if (ra.getEstAsi().equals("1") && fila_data[14].equals("")) {
                            fila_data[14] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[14].equals("")) {
                            diasAsistidos++;
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[14] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[14] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[14] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[14] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[14] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[14] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[14] = "TJ";
                                }

                            } else {
                                fila_data[14] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }

                        } else if (ra.getEstAsi().equals("3") && fila_data[14].equals("")) {
                            if (ra.getJustificacion() != null) {
                                inasistenciaJustificadaDias++;
                                Justificacion jus = ra.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[14] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[14] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[14] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[14] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[14] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[14] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[14] = "FJ";
                                }

                            } else {
                                fila_data[14] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;
                    case 11:
                        if (ra.getEstAsi().equals("1") && fila_data[15].equals("")) {
                            fila_data[15] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[15].equals("")) {
                            diasAsistidos++;
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[15] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[15] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[15] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[15] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[15] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[15] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[15] = "TJ";
                                }

                            } else {
                                fila_data[15] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }

                        } else if (ra.getEstAsi().equals("3") && fila_data[15].equals("")) {
                            if (ra.getJustificacion() != null) {
                                inasistenciaJustificadaDias++;
                                Justificacion jus = ra.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[15] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[15] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[15] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[15] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[15] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[15] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[15] = "FJ";
                                }

                            } else {
                                fila_data[15] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;
                    case 12:
                        if (ra.getEstAsi().equals("1") && fila_data[16].equals("")) {
                            fila_data[16] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[16].equals("")) {
                            diasAsistidos++;
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[16] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[16] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[16] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[16] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[16] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[16] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[16] = "TJ";
                                }

                            } else {
                                fila_data[16] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }

                        } else if (ra.getEstAsi().equals("3") && fila_data[16].equals("")) {
                            if (ra.getJustificacion() != null) {
                                inasistenciaJustificadaDias++;
                                Justificacion jus = ra.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[16] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[16] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[16] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[16] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[16] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[16] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[16] = "FJ";
                                }

                            } else {
                                fila_data[16] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;
                    case 13:
                        if (ra.getEstAsi().equals("1") && fila_data[17].equals("")) {
                            fila_data[17] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[17].equals("")) {
                            diasAsistidos++;
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[17] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[17] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[17] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[17] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[17] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[17] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[17] = "TJ";
                                }

                            } else {
                                fila_data[17] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }

                        } else if (ra.getEstAsi().equals("3") && fila_data[17].equals("")) {
                            if (ra.getJustificacion() != null) {
                                inasistenciaJustificadaDias++;
                                Justificacion jus = ra.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[17] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[17] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[17] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[17] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[17] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[17] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[17] = "FJ";
                                }

                            } else {
                                fila_data[17] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;
                    case 14:
                        if (ra.getEstAsi().equals("1") && fila_data[18].equals("")) {
                            fila_data[18] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[18].equals("")) {
                            diasAsistidos++;
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[18] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[18] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[18] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[18] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[18] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[18] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[18] = "TJ";
                                }

                            } else {
                                fila_data[18] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }

                        } else if (ra.getEstAsi().equals("3") && fila_data[18].equals("")) {
                            if (ra.getJustificacion() != null) {
                                inasistenciaJustificadaDias++;
                                Justificacion jus = ra.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[18] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[18] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[18] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[18] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[18] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[18] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[18] = "FJ";
                                }

                            } else {
                                fila_data[18] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;
                    case 15:
                        if (ra.getEstAsi().equals("1") && fila_data[19].equals("")) {
                            fila_data[19] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[19].equals("")) {
                            diasAsistidos++;
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[19] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[19] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[19] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[19] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[19] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[19] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[19] = "TJ";
                                }

                            } else {
                                fila_data[19] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }

                        } else if (ra.getEstAsi().equals("3") && fila_data[19].equals("")) {
                            if (ra.getJustificacion() != null) {
                                inasistenciaJustificadaDias++;
                                Justificacion jus = ra.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[19] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[19] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[19] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[19] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[19] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[19] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[19] = "FJ";
                                }

                            } else {
                                fila_data[19] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;
                    case 16:
                        if (ra.getEstAsi().equals("1") && fila_data[20].equals("")) {
                            fila_data[20] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[20].equals("")) {
                            diasAsistidos++;
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[20] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[20] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[20] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[20] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[20] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[20] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[20] = "TJ";
                                }

                            } else {
                                fila_data[20] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }

                        } else if (ra.getEstAsi().equals("3") && fila_data[20].equals("")) {
                            if (ra.getJustificacion() != null) {
                                inasistenciaJustificadaDias++;
                                Justificacion jus = ra.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[20] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[20] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[20] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[20] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[20] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[20] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[20] = "FJ";
                                }

                            } else {
                                fila_data[20] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;
                    case 17:
                        if (ra.getEstAsi().equals("1") && fila_data[21].equals("")) {
                            fila_data[21] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[21].equals("")) {
                            diasAsistidos++;
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[21] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[21] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[21] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[21] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[21] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[21] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[21] = "TJ";
                                }

                            } else {
                                fila_data[21] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }

                        } else if (ra.getEstAsi().equals("3") && fila_data[21].equals("")) {
                            if (ra.getJustificacion() != null) {
                                inasistenciaJustificadaDias++;
                                Justificacion jus = ra.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[21] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[21] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[21] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[21] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[21] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[21] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[21] = "FJ";
                                }

                            } else {
                                fila_data[21] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;
                    case 18:
                        if (ra.getEstAsi().equals("1") && fila_data[22].equals("")) {
                            fila_data[22] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[22].equals("")) {
                            diasAsistidos++;
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[22] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[22] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[22] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[22] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[22] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[22] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[22] = "TJ";
                                }

                            } else {
                                fila_data[22] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }

                        } else if (ra.getEstAsi().equals("3") && fila_data[22].equals("")) {
                            if (ra.getJustificacion() != null) {
                                inasistenciaJustificadaDias++;
                                Justificacion jus = ra.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[22] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[22] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[22] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[22] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[22] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[22] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[22] = "FJ";
                                }

                            } else {
                                fila_data[22] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;
                    case 19:
                        if (ra.getEstAsi().equals("1") && fila_data[23].equals("")) {
                            fila_data[23] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[23].equals("")) {
                            diasAsistidos++;
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[23] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[23] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[23] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[23] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[23] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[23] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[23] = "TJ";
                                }

                            } else {
                                fila_data[23] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }

                        } else if (ra.getEstAsi().equals("3") && fila_data[23].equals("")) {
                            if (ra.getJustificacion() != null) {
                                inasistenciaJustificadaDias++;
                                Justificacion jus = ra.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[23] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[23] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[23] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[23] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[23] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[23] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[23] = "FJ";
                                }

                            } else {
                                fila_data[23] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;
                    case 20:
                        if (ra.getEstAsi().equals("1") && fila_data[24].equals("")) {
                            fila_data[24] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[24].equals("")) {
                            diasAsistidos++;
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[24] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[24] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[24] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[24] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[24] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[24] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[24] = "TJ";
                                }

                            } else {
                                fila_data[24] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }

                        } else if (ra.getEstAsi().equals("3") && fila_data[24].equals("")) {
                            if (ra.getJustificacion() != null) {
                                inasistenciaJustificadaDias++;
                                Justificacion jus = ra.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[24] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[24] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[24] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[24] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[24] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[24] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[24] = "FJ";
                                }

                            } else {
                                fila_data[24] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;
                    case 21:
                        if (ra.getEstAsi().equals("1") && fila_data[25].equals("")) {
                            fila_data[25] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[25].equals("")) {
                            diasAsistidos++;
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[25] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[25] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[25] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[25] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[25] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[25] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[25] = "TJ";
                                }

                            } else {
                                fila_data[25] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }

                        } else if (ra.getEstAsi().equals("3") && fila_data[25].equals("")) {
                            if (ra.getJustificacion() != null) {
                                inasistenciaJustificadaDias++;
                                Justificacion jus = ra.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[25] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[25] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[25] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[25] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[25] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[25] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[25] = "FJ";
                                }

                            } else {
                                fila_data[25] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;
                    case 22:
                        if (ra.getEstAsi().equals("1") && fila_data[26].equals("")) {
                            fila_data[26] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[26].equals("")) {
                            diasAsistidos++;
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[26] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[26] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[26] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[26] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[26] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[26] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[26] = "TJ";
                                }

                            } else {
                                fila_data[26] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }

                        } else if (ra.getEstAsi().equals("3") && fila_data[26].equals("")) {
                            if (ra.getJustificacion() != null) {
                                inasistenciaJustificadaDias++;
                                Justificacion jus = ra.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[26] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[26] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[26] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[26] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[26] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[26] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[26] = "FJ";
                                }

                            } else {
                                fila_data[26] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;
                    case 23:
                        if (ra.getEstAsi().equals("1") && fila_data[27].equals("")) {
                            fila_data[27] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[27].equals("")) {
                            diasAsistidos++;
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[27] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[27] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[27] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[27] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[27] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[27] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[27] = "TJ";
                                }

                            } else {
                                fila_data[27] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }

                        } else if (ra.getEstAsi().equals("3") && fila_data[27].equals("")) {
                            if (ra.getJustificacion() != null) {
                                inasistenciaJustificadaDias++;
                                Justificacion jus = ra.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[27] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[27] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[27] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[27] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[27] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[27] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[27] = "FJ";
                                }

                            } else {
                                fila_data[27] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;
                    case 24:
                        if (ra.getEstAsi().equals("1") && fila_data[28].equals("")) {
                            fila_data[28] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[28].equals("")) {
                            diasAsistidos++;
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[28] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[28] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[28] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[28] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[28] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[28] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[28] = "TJ";
                                }

                            } else {
                                fila_data[28] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }

                        } else if (ra.getEstAsi().equals("3") && fila_data[28].equals("")) {
                            if (ra.getJustificacion() != null) {
                                inasistenciaJustificadaDias++;
                                Justificacion jus = ra.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[28] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[28] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[28] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[28] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[28] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[28] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[28] = "FJ";
                                }

                            } else {
                                fila_data[28] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;
                    case 25:
                        if (ra.getEstAsi().equals("1") && fila_data[29].equals("")) {
                            fila_data[29] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[29].equals("")) {
                            diasAsistidos++;
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[29] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[29] = "P";
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[29] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[29] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[29] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[29] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[29] = "TJ";
                                }

                            } else {
                                fila_data[29] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }

                        } else if (ra.getEstAsi().equals("3") && fila_data[29].equals("")) {
                            if (ra.getJustificacion() != null) {
                                inasistenciaJustificadaDias++;
                                Justificacion jus = ra.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[29] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[29] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[29] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[29] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[29] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[29] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[29] = "FJ";
                                }

                            } else {
                                fila_data[29] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;
                    case 26:
                        if (ra.getEstAsi().equals("1") && fila_data[30].equals("")) {
                            fila_data[30] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[30].equals("")) {
                            diasAsistidos++;
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[30] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[30] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[30] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[30] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[30] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[30] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[30] = "TJ";
                                }

                            } else {
                                fila_data[30] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }

                        } else if (ra.getEstAsi().equals("3") && fila_data[30].equals("")) {
                            if (ra.getJustificacion() != null) {
                                inasistenciaJustificadaDias++;
                                Justificacion jus = ra.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[30] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[30] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[30] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[30] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[30] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[30] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[30] = "FJ";
                                }

                            } else {
                                fila_data[30] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;
                    case 27:
                        if (ra.getEstAsi().equals("1") && fila_data[31].equals("")) {
                            fila_data[31] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[31].equals("")) {
                            diasAsistidos++;
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[31] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[31] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[31] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[31] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[31] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[31] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[31] = "TJ";
                                }

                            } else {
                                fila_data[31] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }

                        } else if (ra.getEstAsi().equals("3") && fila_data[31].equals("")) {
                            if (ra.getJustificacion() != null) {
                                inasistenciaJustificadaDias++;
                                Justificacion jus = ra.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[31] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[31] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[31] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[31] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[31] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[31] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[31] = "FJ";
                                }

                            } else {
                                fila_data[31] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;
                    case 28:
                        if (ra.getEstAsi().equals("1") && fila_data[32].equals("")) {
                            fila_data[32] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[32].equals("")) {
                            diasAsistidos++;
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[32] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[32] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[32] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[32] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[32] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[32] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[32] = "TJ";
                                }

                            } else {
                                fila_data[32] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }

                        } else if (ra.getEstAsi().equals("3") && fila_data[32].equals("")) {
                            if (ra.getJustificacion() != null) {
                                inasistenciaJustificadaDias++;
                                Justificacion jus = ra.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[32] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[32] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[32] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[32] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[32] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[32] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[32] = "FJ";
                                }

                            } else {
                                fila_data[32] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;
                    case 29:
                        if (ra.getEstAsi().equals("1") && fila_data[33].equals("")) {
                            fila_data[33] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[33].equals("")) {
                            diasAsistidos++;
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[33] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[33] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[33] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[33] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[33] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[33] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[33] = "TJ";
                                }

                            } else {
                                fila_data[33] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }

                        } else if (ra.getEstAsi().equals("3") && fila_data[33].equals("")) {
                            if (ra.getJustificacion() != null) {
                                inasistenciaJustificadaDias++;
                                Justificacion jus = ra.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[33] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[33] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[33] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[33] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[33] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[33] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[33] = "FJ";
                                }

                            } else {
                                fila_data[33] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;
                    case 30:
                        if (ra.getEstAsi().equals("1") && fila_data[34].equals("")) {
                            fila_data[34] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[34].equals("")) {
                            diasAsistidos++;
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[34] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[34] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[34] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[34] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[34] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[34] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[34] = "TJ";
                                }

                            } else {
                                fila_data[34] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }

                        } else if (ra.getEstAsi().equals("3") && fila_data[34].equals("")) {
                            if (ra.getJustificacion() != null) {
                                inasistenciaJustificadaDias++;
                                Justificacion jus = ra.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[34] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[34] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[34] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[34] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[34] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[34] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[34] = "FJ";
                                }

                            } else {
                                fila_data[34] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;
                    case 31:
                        if (ra.getEstAsi().equals("1") && fila_data[35].equals("")) {
                            fila_data[35] = "A";
                            diasAsistidos++;
                        } else if (ra.getEstAsi().equals("2") && fila_data[35].equals("")) {
                            diasAsistidos++;
                            if (ra.getJustificacion() != null) {
                                Justificacion jus = ra.getJustificacion();
                                tardanzaJustificadaMin = tardanzaJustificadaMin + ra.getMinTardanza();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[35] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[35] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[35] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[35] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[35] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[35] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[35] = "TJ";
                                }

                            } else {
                                fila_data[35] = "T";
                                tardanzaInjuustificadaMin = tardanzaInjuustificadaMin + ra.getMinTardanza();
                            }

                        } else if (ra.getEstAsi().equals("3") && fila_data[35].equals("")) {
                            if (ra.getJustificacion() != null) {
                                inasistenciaJustificadaDias++;
                                Justificacion jus = ra.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[35] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[35] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[35] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[35] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[35] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[35] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[35] = "FJ";
                                }

                            } else {
                                fila_data[35] = "F";
                                inasistenciaInjustificadaDias++;
                            }
                        }
                        break;

                }
            }
            
            for(Inasistencia ina:trabajadores.get(i).getInasistencias())
            {
                switch(DateUtil.obtenerDiaSegunFecha(ina.getInaFecha()))
                {
                    case 1:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[5] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[5] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[5] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[5] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[5] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[5] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[5] = "FJ";
                                }
                            
                        } else {
                            fila_data[5] = "F";
                            inasistenciaInjustificadaDias++;
                        }
                        break;
                    case 2:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[6] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[6] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[6] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[6] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[6] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[6] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[6] = "FJ";
                                }
                            
                        } else {
                            fila_data[6] = "F";
                            inasistenciaInjustificadaDias++;
                        }
                        break;
                    case 3:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[7] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[7] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[7] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[7] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[7] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[7] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[7] = "FJ";
                                }
                            
                        } else {
                            fila_data[7] = "F";
                            inasistenciaInjustificadaDias++;
                        }
                        break;    
                    case 4:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[8] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[8] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[8] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[8] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[8] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[8] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[8] = "FJ";
                                }
                            
                        } else {
                            fila_data[8] = "F";
                            inasistenciaInjustificadaDias++;
                        }
                        break;    
                    case 5:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[9] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[9] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[9] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[9] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[9] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[9] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[9] = "FJ";
                                }
                            
                        } else {
                            fila_data[9] = "F";
                            inasistenciaInjustificadaDias++;
                        }
                        break;
                    case 6:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[10] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[10] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[10] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[10] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[10] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[10] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[10] = "FJ";
                                }
                            
                        } else {
                            fila_data[10] = "F";
                            inasistenciaInjustificadaDias++;
                        }
                        break;
                    case 7:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[11] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[11] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[11] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[11] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[11] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[11] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[11] = "FJ";
                                }
                            
                        } else {
                            fila_data[11] = "F";
                            inasistenciaInjustificadaDias++;
                        }
                        break;    
                    case 8:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[12] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[12] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[12] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[12] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[12] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[12] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[12] = "FJ";
                                }
                            
                        } else {
                            fila_data[12] = "F";
                            inasistenciaInjustificadaDias++;
                        } 
                        break;  
                    case 9:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[13] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[13] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[13] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[13] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[13] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[13] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[13] = "FJ";
                                }
                            
                        } else {
                            fila_data[13] = "F";
                            inasistenciaInjustificadaDias++;
                        }
                        break;
                    case 10:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[14] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[14] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[14] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[14] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[14] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[14] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[14] = "FJ";
                                }
                            
                        } else {
                            fila_data[14] = "F";
                            inasistenciaInjustificadaDias++;
                        } 
                        break;
                    case 11:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[15] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[15] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[15] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[15] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[15] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[15] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[15] = "FJ";
                                }
                            
                        } else {
                            fila_data[15] = "F";
                            inasistenciaInjustificadaDias++;
                        }
                        break;    
                    case 12:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[16] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[16] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[16] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[16] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[16] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[16] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[16] = "FJ";
                                }
                            
                        } else {
                            fila_data[16] = "F";
                            inasistenciaInjustificadaDias++;
                        } 
                        break;      
                    case 13:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[17] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[17] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[17] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[17] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[17] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[17] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[17] = "FJ";
                                }
                            
                        } else {
                            fila_data[17] = "F";
                            inasistenciaInjustificadaDias++;
                        } 
                        break;
                    case 14:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[18] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[18] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[18] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[18] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[18] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[18] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[18] = "FJ";
                                }
                            
                        } else {
                            fila_data[18] = "F";
                            inasistenciaInjustificadaDias++;
                        }    
                        break;
                    case 15:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[19] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[19] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[19] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[19] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[19] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[19] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[19] = "FJ";
                                }
                            
                        } else {
                            fila_data[19] = "F";
                            inasistenciaInjustificadaDias++;
                        } 
                        break;    
                    case 16:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[20] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[20] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[20] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[20] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[20] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[20] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[20] = "FJ";
                                }
                            
                        } else {
                            fila_data[20] = "F";
                            inasistenciaInjustificadaDias++;
                        }
                        break;    
                    case 17:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[21] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[21] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[21] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[21] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[21] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[21] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[21] = "FJ";
                                }
                            
                        } else {
                            fila_data[21] = "F";
                            inasistenciaInjustificadaDias++;
                        }
                        break;
                    case 18:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[22] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[22] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[22] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[22] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[22] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[22] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[22] = "FJ";
                                }
                            
                        } else {
                            fila_data[22] = "F";
                            inasistenciaInjustificadaDias++;
                        }
                        break;
                    case 19:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[23] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[23] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[23] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[23] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[23] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[23] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[23] = "FJ";
                                }
                            
                        } else {
                            fila_data[23] = "F";
                            inasistenciaInjustificadaDias++;
                        }
                        break;    
                    case 20:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[24] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[24] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[24] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[24] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[24] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[24] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[24] = "FJ";
                                }
                            
                        } else {
                            fila_data[24] = "F";
                            inasistenciaInjustificadaDias++;
                        }
                        break;  
                    case 21:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[25] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[25] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[25] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[25] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[25] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[25] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[25] = "FJ";
                                }
                            
                        } else {
                            fila_data[25] = "F";
                            inasistenciaInjustificadaDias++;
                        }
                        break;
                    case 22:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[26] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[26] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[26] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[26] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[26] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[26] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[26] = "FJ";
                                }
                            
                        } else {
                            fila_data[26] = "F";
                            inasistenciaInjustificadaDias++;
                        }
                        break;
                    case 23:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[27] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[27] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[27] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[27] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[27] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[27] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[27] = "FJ";
                                }
                            
                        } else {
                            fila_data[27] = "F";
                            inasistenciaInjustificadaDias++;
                        }
                        break;    
                    case 24:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[28] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[28] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[28] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[28] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[28] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[28] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[28] = "FJ";
                                }
                            
                        } else {
                            fila_data[28] = "F";
                            inasistenciaInjustificadaDias++;
                        }
                        break;       
                    case 25:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[29] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[29] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[29] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[29] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[29] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[29] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[29] = "FJ";
                                }
                            
                        } else {
                            fila_data[29] = "F";
                            inasistenciaInjustificadaDias++;
                        }
                        break;
                    case 26:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[30] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[30] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[30] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[30] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[30] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[30] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[30] = "FJ";
                                }
                            
                        } else {
                            fila_data[30] = "F";
                            inasistenciaInjustificadaDias++;
                        }
                        break;    
                    case 27:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[31] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[31] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[31] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[31] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[31] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[31] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[31] = "FJ";
                                }
                            
                        } else {
                            fila_data[31] = "F";
                            inasistenciaInjustificadaDias++;
                        }
                        break;  
                    case 28:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[32] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[32] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[32] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[32] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[32] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[32] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[32] = "FJ";
                                }
                            
                        } else {
                            fila_data[32] = "F";
                            inasistenciaInjustificadaDias++;
                        }
                        break;
                    case 29:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[33] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[33] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[33] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[33] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[33] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[33] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[33] = "FJ";
                                }
                            
                        } else {
                            fila_data[33] = "F";
                        }
                        break;
                    case 30:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[34] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[34] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[34] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[34] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[34] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[34] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[34] = "FJ";
                                }
                            
                        } else {
                            fila_data[34] = "F";
                            inasistenciaInjustificadaDias++;
                        }
                        break;    
                    case 31:
                        if (ina.getJustificacion() != null) {
                            inasistenciaJustificadaDias++;
                            JustificacionInasistenciaTrabajador jus = ina.getJustificacion();
                                if (jus.getEstReg().equals("1")) {
                                    fila_data[35] = "L";
                                } else if (jus.getEstReg().equals("2")) {
                                    fila_data[35] = "P";
                                    
                                } else if (jus.getEstReg().equals("3")) {
                                    fila_data[35] = "C";
                                } else if (jus.getEstReg().equals("4")) {
                                    fila_data[35] = "V";
                                } else if (jus.getEstReg().equals("5")) {
                                    fila_data[35] = "O";
                                } else if (jus.getEstReg().equals("6")) {
                                    fila_data[35] = "M";
                                } else if (jus.getEstReg().equals("7")) {
                                    fila_data[35] = "FJ";
                                }
                            
                        } else {
                            fila_data[35] = "F";
                            inasistenciaInjustificadaDias++;
                        }
                        break;       
                        
                }
            }
            
            fila_data[36] = diasAsistidos + "";
            fila_data[37] = "" + (inasistenciaInjustificadaDias + inasistenciaJustificadaDias) + "";
            Integer tardanzaMin = tardanzaInjuustificadaMin + tardanzaJustificadaMin;
            Integer tardanzaHor = tardanzaJustificadaHora + tardanzaInjuustificadaHora;
            if (tardanzaMin > 0) {
                tardanzaHor = tardanzaHor + tardanzaMin / 60;
                fila_data[38] = "" + tardanzaMin % 60 + "";
                fila_data[39] = tardanzaHor + "";
                fila_data[40] = "TARDANZA";
            } else {
                fila_data[38] = "0";
                fila_data[39] = "0";
                fila_data[40] = "";
            }
            t.processLineCell(fila_data, cell, 7);
            for (int k = 0; k < fila_data.length; ++k) {
                fila_data[k] = "";
            }
            inasistenciaJustificadaDias = 0;
            tardanzaJustificadaMin = 0;
            tardanzaJustificadaHora = 0;
            inasistenciaInjustificadaDias = 0;
            tardanzaInjuustificadaMin = 0;
            tardanzaInjuustificadaHora = 0;
        }

        //fin tabla
        m.agregarTabla(tabla);    
        m.agregarParrafo("");;
        m.agregarTabla(t);
        m.agregarParrafo("");
        float[] columnWidthsL={1,2,6,4,2,6,4,2,6,4};
        Table leyenda = new Table(columnWidthsL);
        leyenda.setWidthPercent(100);
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("A").setFontSize(8).setTextAlignment(TextAlignment.LEFT)));
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(": Asistencia").setFontSize(8)).setTextAlignment(TextAlignment.LEFT));
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));

        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("O").setFontSize(8).setTextAlignment(TextAlignment.LEFT)));
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(": Onomastico").setFontSize(8)).setTextAlignment(TextAlignment.LEFT));
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));

        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("F").setFontSize(8)).setTextAlignment(TextAlignment.LEFT));
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(": Falta").setFontSize(8)).setTextAlignment(TextAlignment.LEFT));
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("L").setFontSize(8).setTextAlignment(TextAlignment.LEFT)));
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(": Liciencia").setFontSize(8)).setTextAlignment(TextAlignment.LEFT));
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));

        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("C").setFontSize(8).setTextAlignment(TextAlignment.LEFT)));
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(": Comision de Serv.").setFontSize(8)).setTextAlignment(TextAlignment.LEFT));
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));

        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("M").setFontSize(8)).setTextAlignment(TextAlignment.LEFT));
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(": Atenc.Medica").setFontSize(8)).setTextAlignment(TextAlignment.LEFT));
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("V").setFontSize(8)).setTextAlignment(TextAlignment.LEFT));
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(": Vacaciones").setFontSize(8)).setTextAlignment(TextAlignment.LEFT));
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));

        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("T").setFontSize(8)).setTextAlignment(TextAlignment.LEFT));
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(": Tardanza").setFontSize(8)).setTextAlignment(TextAlignment.LEFT));
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("P").setFontSize(8)).setTextAlignment(TextAlignment.LEFT));
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(": Permiso con Papeleta").setFontSize(8)).setTextAlignment(TextAlignment.LEFT));
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("FJ").setFontSize(8)).setTextAlignment(TextAlignment.LEFT));
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(": Falta Justificada").setFontSize(8)).setTextAlignment(TextAlignment.LEFT));
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("TJ").setFontSize(8)).setTextAlignment(TextAlignment.LEFT));
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(": Tard.Justificada").setFontSize(8)).setTextAlignment(TextAlignment.LEFT));
        leyenda.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        m.agregarTabla(leyenda);
        m.cerrarDocumento();
                 
        JSONArray miArray = new JSONArray();
        JSONObject oResponse = new JSONObject();        
        oResponse.put("datareporte",m.encodeToBase64());
        miArray.put(oResponse);                       
        
        return WebResponse.crearWebResponseExito("Se genero el reporte correctamente",miArray);        
        
    }
    
    
}