/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.familiares.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ParientesDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TipoParienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Parientes;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoPariente;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class AgregarParienteTx implements ITransaction {
    private static  final Logger logger = Logger.getLogger(AgregarParienteTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *   Parte para la lectura, verificacion y validacion de datos
         */
        Parientes parientes = null;
        Persona pariente = null; 

        Integer parId = 0, perId = 0;
        Integer tpaId = 0;
        int caso = 0;

        String parApePat = "",
                parApeMat = "",
                parNom = "",
                parDni = "",
                parNum1 = "",
                parNum2 = "",
                parFij = "",
                parEmail = "",
                parSex = "";
        
        Date parFecNac = null;
        
        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
        
        PersonaDao personaDao = (PersonaDao) FactoryDao.buildDao("se.PersonaDao");

        try {
            JSONObject requestData = (JSONObject) wr.getData();

            perId = requestData.getInt("perId");
            tpaId = requestData.getInt("tipoPariente");
            parApePat = requestData.getString("parApePat");
            parApeMat = requestData.getString("parApeMat");
            parNom = requestData.getString("parNom");
            parDni = requestData.getString("parDni");
            parFecNac = sdi.parse(requestData.getString("parFecNac").substring(0, 10));
            parFij = requestData.getString("parFij");
            parNum1 = requestData.getString("parNum1");
            parNum2 = requestData.getString("parNum2");
            parEmail = requestData.getString("parEmail");
            parSex = requestData.getString("parSex");
            caso = requestData.getInt("caso");
            
            if (caso == 1) {//Solo agregar parentesco
                parId = requestData.getInt("parId");
                pariente = personaDao.buscarPersonaPorId(parId);
            } else {
                pariente = new Persona(parApePat, parApeMat, parNom, parDni, parFecNac, parNum1, parNum2, parFij, parEmail, parSex.charAt(0), wr.getIdUsuario(), new Date(), 'A');
            }
        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"Datos nuevo pariente",e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        //Fin

        /*
        *  Parte para la operacion en la Base de Datos
         */
        if (caso == 1) {
            try {
                personaDao.update(pariente);
            } catch (Exception e) {
                logger.log(Level.SEVERE, "Actualizar pariente", e);
                System.out.println(e);
            }
        } else {
            try {
                personaDao.insert(pariente);
            } catch (Exception e) {
                logger.log(Level.SEVERE, "Agregar nuevo pariente", e);
                System.out.println(e);
            }
        }
        parId = pariente.getPerId();
        
        ParientesDao parienteDao = (ParientesDao) FactoryDao.buildDao("se.ParientesDao");
        parientes = new Parientes(parId, perId, new TipoPariente(tpaId), wr.getIdUsuario(), new Date(), "A");

        try {
            parienteDao.insert(parientes);
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar parentesco",e);
            System.out.println("No se pudo registrar pariente\n" + e);
            return WebResponse.crearWebResponseError("No se pudo registrar pariente", e.getMessage());
        }
        
        //Fin       

        /*
        *  Repuesta Correcta
         */
        JSONObject oResponse = new JSONObject();

        oResponse.put("parId", parientes.getPerId());
        oResponse.put("parApePat", pariente.getApePat() == null ? "" : pariente.getApePat());
        oResponse.put("parApeMat", pariente.getApeMat() == null ? "" : pariente.getApeMat());
        oResponse.put("parNom", pariente.getNom() == null ? "" : pariente.getNom());
        oResponse.put("tipoParienteId", parientes.getParentesco().getTpaId());
        //oResponse.put("parentesco", parientes.getParentesco().getTpaDes());
        oResponse.put("parDni", pariente.getDni());
        oResponse.put("parFecNac", pariente.getFecNac() == null ? "" : sdi.format(pariente.getFecNac()));
        oResponse.put("parFij", pariente.getFij() == null ? "" : pariente.getFij());
        oResponse.put("parNum1", pariente.getNum1() == null ? "" : pariente.getNum1());
        oResponse.put("parNum2", pariente.getNum2() == null ? "" : pariente.getNum2());
        oResponse.put("parEmail", pariente.getEmail() == null ? "" : pariente.getEmail());
        oResponse.put("parSex", pariente.getSex());
                
        return WebResponse.crearWebResponseExito("El registro del Pariente se realizo correctamente", oResponse);
        //Fin
        
    }

}
