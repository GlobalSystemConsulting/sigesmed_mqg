/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.LibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.GCell;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.io.font.FontConstants;
import static com.itextpdf.io.font.FontConstants.HELVETICA_BOLD;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.border.SolidBorder;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import java.io.IOException;
import java.math.BigDecimal;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author RE
 */
public class ReporteLibroCajaTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */        
      List<String[]> tablaC= new ArrayList<>();
      String[] data;
       try{ 
           
         //  JSONObject request = (JSONObject)wr.getData();
           
           JSONObject requestData = (JSONObject)wr.getData();
                      System.out.println("requestData-------------"+requestData);

           JSONArray tablaLibroCaja= requestData.getJSONArray("tablaLibroCaja");
           System.out.println("tablaLibroCaja-------------"+tablaLibroCaja);
           JSONObject datos= (JSONObject)requestData.get("datos");
           System.out.println("datos------------------"+datos);
           String orgPadre = datos.getString("organizacionPadre");
           String org = datos.getString("nombre");
           String cod = datos.getString("codigo");
           String dir = datos.getString("direccion");
           System.out.println("-----okjkkkkkkkkkkkk");
          String [] dat={"Institucion Educativa",": "+org,"Código Modular",": "+cod,"Direccion ",": "+dir,"UGEL ",": "+orgPadre};
          data=dat;
          
           if(tablaLibroCaja.length() > 0){
               for(int i = 0; i < tablaLibroCaja.length();i++){
                  
                Object o =  tablaLibroCaja.get(i);
                JSONObject objOpe = (JSONObject)o;    

                String n = String.valueOf(i);

                String fecha= objOpe.getString("fecha"); 
                //      Date a= new Date(fecha);     
                //      DateFormat fe = new SimpleDateFormat("dd-MM-yyyy");
                //      String fecOpe = fe.format(a);
                //     Date fec= new Date(objFec.getString("getDate"));  
                //     DateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                //     String fecOpe = formato.format(fec);

                JSONObject objTipPag= (JSONObject)objOpe.get("tipoPago");                                     
                String medPag = objTipPag.getString("nombre");

                String desOpe = String.valueOf(objOpe.get("glosa"));

                JSONObject objRazSoc= (JSONObject)objOpe.get("clienteProveedor"); 
                String razSoc = objRazSoc.getString("datos");

                String numDoc = objOpe.getString("numeroD");
                String lib = objOpe.getString("libro");

                JSONObject objDeb= (JSONObject)objOpe.get("debe");

                int codCueDebInt = objDeb.getInt("cuentaContableID");
                String codCueDeb = String.valueOf(codCueDebInt);
                String desCueDeb = objDeb.getString("nombre");
                double impDe = objDeb.getDouble("importe");
                BigDecimal d=new BigDecimal (impDe).setScale(2);
                String impDeb = String.valueOf(d);

                JSONObject objHab= (JSONObject)objOpe.get("haber");
                 int codCueDHabInt = objDeb.getInt("cuentaContableID");
                String codCueHab = String.valueOf(codCueDHabInt);
                String desCueHab = objHab.getString("nombre");
                double impHa = objHab.getDouble("importe");
                BigDecimal h = new BigDecimal(impHa).setScale(2);
                String impHab = String.valueOf(h);

                 String[]row1 ={n,fecha,medPag,desOpe,razSoc,numDoc,codCueDeb,desCueDeb,impDeb,"",codCueHab,desCueHab,"",impHab}; 


                 tablaC.add(row1);
                    
                    
               }        
           }
       }
        catch(Exception e){
            System.out.println("No se pudo Listar el directorio interno \n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar el directorio interno ", e.getMessage() );
        }
      
      
   //   for(int i=0;i<1000;i++){
          
  //                String[]contenido ={"N","Fecha","Medio Pago","Descripción Operación","Razon Social","N° Doc.","Cod. Cta","Descripción Cuenta","Debe","Haber","Cod. Cta","Descripción Cuenta","Debe","Haber"};
   //       tablaC.add(contenido);
   //   }

     
        //Creando el reporte....        
        Mitext m = null;        
        try {
            m = new Mitext(true,"SIGESMED");
            m.agregarTitulo("REPORTE DEL LIBRO CAJA  " );
        } catch (Exception ex) {
            System.out.println("No se pudo crear el documento \n"+ex);
            Logger.getLogger(ReporteLibroCajaTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        // m.agregarParrafo("LIBRO CAJA SIGESMED");
        //agregar tabla
        
        float[] columnWidthsD={4,6,1,4,6,1,1,1,1,1};
        Table tabla = new Table(columnWidthsD);
        tabla.setWidthPercent(100);
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(data[0]).setFontSize(10).setBold().setTextAlignment(TextAlignment.LEFT)));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(data[1]).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));

        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(data[2]).setFontSize(10).setBold().setTextAlignment(TextAlignment.LEFT)));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(data[3]).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,5).setBorder(Border.NO_BORDER));

        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(data[4]).setFontSize(10).setBold()).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(data[5]).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));

        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(data[6]).setFontSize(10).setBold()).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(data[7]).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,5).setBorder(Border.NO_BORDER));
        
        
        
        
       
        
       float[] columnWidths={1,2,2,4,4,2,2,4,2,2};
        GTabla t = new GTabla(columnWidths);
        t.addHeaderCell(new Cell(1, 10).setBorder(Border.NO_BORDER));
        t.addFooterCell(new Cell(1,10));
        String[]encabezado ={"N","Fecha","Medio Pago","Descripción Operación","Razon Social","N° Doc.","Cod. Cta","Descripción Cuenta","Debe","Haber"};
        GCell[] cell ={t.createCellCenter(2,1),t.createCellCenter(2,1),t.createCellCenter(2,1),t.createCellLeft(2,1),t.createCellLeft(2,1),t.createCellCenter(2,1),
                      t.createCellCenter(1,1),t.createCellLeft(1,1),t.createCellRight(1,1),t.createCellRight(1,1),
                      t.createCellCenter(1,1),t.createCellLeft(1,1),t.createCellRight(1,1),t.createCellRight(1,1)};
         
        try {
            t.build(encabezado,5);
        } catch (IOException ex) {
            Logger.getLogger(ReporteLibroCajaTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        
       //   String[]contenido ={"N°","Fecha","Glosa"};
        for(String []c:tablaC){           
              t.processLineCell(c,cell);
        
        }
           
        
        
        //fin tabla
        m.agregarTabla(tabla);
        m.agregarTabla(t);
        
//        agregar grafico
        
//        MChart chart = new MChart();
//        
//        DefaultPieDataset dataset = new DefaultPieDataset( );
//        dataset.setValue( "IPhone 5s" , new Double( 20 ) );  
//        dataset.setValue( "SamSung Grand" , new Double( 20 ) );   
//        dataset.setValue( "MotoG" , new Double( 40 ) );    
//        dataset.setValue( "Nokia Lumia" , new Double( 10 ) );  
//        
//        try {
//            m.agregarGrafico(chart.createPieChart(dataset, "Grafica de Ejemplo"), 400, 300);
//        } catch (IOException ex) {
//            System.out.println("No se pudo agregar el grafico \n"+ex);
//            Logger.getLogger(ReporteTx.class.getName()).log(Level.SEVERE, null, ex);
//        }
        
        m.cerrarDocumento();  
                                
        JSONArray miArray = new JSONArray();
        JSONObject oResponse = new JSONObject();        
        oResponse.put("datareporte",m.encodeToBase64());
        miArray.put(oResponse);                       
        
        return WebResponse.crearWebResponseExito("Se genero el reporte correctamente",miArray);        
        
    }
    
    
}