/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.monitoreo_sesion_apredizaje.plantilla_ficha_monitoreo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.CompromisoGestionPedagogicaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.CompromisoGestionPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.PlantillaFichaMonitoreo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class RegistrarCompromisoGPTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(RegistrarCompromisoGPTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        String comDes = data.getString("comDes");
        Character comTip = data.getString("comTip").charAt(0);
        int plaId = data.getInt("plaId");
        int usuId = data.getInt("usuId");

        return registrarCompromisoGP(comDes, comTip, plaId, usuId);
    }

    private WebResponse registrarCompromisoGP(String comDes, Character comTip, int plaId, int usuId) {
        try{
            CompromisoGestionPedagogicaDao comGesPedDao = (CompromisoGestionPedagogicaDao) FactoryDao.buildDao("sma.CompromisoGestionPedagogicaDao");

            CompromisoGestionPedagogica compromiso = new CompromisoGestionPedagogica();
            compromiso.setComGesPedDes(comDes);
            compromiso.setComGesPedTip(comTip);
            compromiso.setPlantillaFM(new PlantillaFichaMonitoreo(plaId));
            compromiso.setFecMod(new Date());
            compromiso.setUsuMod(usuId);
            compromiso.setEstReg("A");
            comGesPedDao.insert(compromiso);
            
            JSONObject oResponse = new JSONObject();
            oResponse.put("comId", compromiso.getComGesPedId());
            oResponse.put("comDes", compromiso.getComGesPedDes());
            oResponse.put("comTip", compromiso.getComGesPedTip());
            oResponse.put("plaId", plaId);

            return WebResponse.crearWebResponseExito("Se registro el compromiso de gestión pedagógica correctamente",oResponse);
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarCompromisoGestionPedagogica",e);
            return WebResponse.crearWebResponseError("Error al registrar el compromiso de gestión pedagógica");
        }
    }
    
}
