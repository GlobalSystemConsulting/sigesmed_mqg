/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.rdg;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.ItemFile;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.ItemFileDetalle;
import java.util.List;

/**
 *
 * @author ucsm
 */
public interface ItemFileDetalleDao extends GenericDao<ItemFileDetalle>{
    public List<ItemFileDetalle> buscarByArch(ItemFile arch);
    public int eliminarPorIde(int ide);
    public void eliminarComparticionPorDocYUsu(int ideDoc, int ideUsu);
    public void eliminarComparticionDefault(int ideDoc);
    public boolean verificarExisteComparticion(int userID, ItemFile item);
    public boolean esCompartido(ItemFile item);
    public List<ItemFileDetalle> buscarCompartidos(int usuCod,ItemFile padre);
    public void actualizarComparticionConNuevaVersionDoc(int ideDocNew, int ideDocOld);
    public void compartirDocumentosEntreLosRolesXOrganización(int ideDoc,int ideUsu,int ideOrg, String estado);
}
