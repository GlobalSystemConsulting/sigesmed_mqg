/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.rdg;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.PlazoDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.ItemFile;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.Plazo;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author ucsm
 */
public class PlazoDaoHibernate  extends GenericDaoHibernate<Plazo> implements PlazoDao{

    @Override
    public Plazo buscarPorID(Plazo plazo) {
         Plazo p = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql="SELECT p FROM Plazo p WHERE p.plzIde=:p1";
            Query q=session.createQuery(hql);
            q.setParameter("p1", plazo.getPlzIde());
            p=(Plazo)q.uniqueResult();

        }catch(Exception e){
            System.out.println("No se pudo encontrar el itemFile \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar por codigo el itemFile \\n "+ e.getMessage());
        }
        finally{
            session.close();
            return p;
        }
    }
    
}
