/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reconocimientos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ReconocimientoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Reconocimiento;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Yemi
 */
public class ActualizarReconocimientoTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ActualizarReconocimientoTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try{
     
            DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
            
            JSONObject requestData = (JSONObject)wr.getData();
        
            Integer recId = requestData.getInt("recId");
            Character mot = requestData.getString("mot").charAt(0);
            String numRes = requestData.getString("numRes");
            Date fecRes = requestData.getString("fecRes").equals("")?null:sdi.parse(requestData.getString("fecRes").substring(0, 10));
            String entEmi = requestData.getString("entEmi");      
            
            return actualizarReconocimiento(recId, mot, numRes, fecRes, entEmi);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar reconocimiento",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    
    private WebResponse actualizarReconocimiento(Integer recId, Character mot, String numRes, Date fecRes, String entEmi) {
        try{
            ReconocimientoDao reconocimientoDao = (ReconocimientoDao)FactoryDao.buildDao("se.ReconocimientoDao");        
            Reconocimiento reconocimiento = reconocimientoDao.buscarPorId(recId);

            reconocimiento.setMot(mot);
            reconocimiento.setNumRes(numRes);
            reconocimiento.setFecRes(fecRes);
            reconocimiento.setEntEmi(entEmi);
            
            reconocimientoDao.update(reconocimiento);

            JSONObject oResponse = new JSONObject();
            DateFormat sdo = new SimpleDateFormat("yyyy-MM-dd");
            oResponse.put("recId", reconocimiento.getRecId());
            oResponse.put("mot", reconocimiento.getMot());
            oResponse.put("numRes", reconocimiento.getNumRes()==null?"":reconocimiento.getNumRes());
            oResponse.put("fecRes", reconocimiento.getFecRes()==null?"":sdo.format(reconocimiento.getFecRes()));
            oResponse.put("entEmi", reconocimiento.getEntEmi()==null?"":reconocimiento.getEntEmi());
            
            return WebResponse.crearWebResponseExito("Reconocimiento actualizado exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarReconocimiento",e);
            return WebResponse.crearWebResponseError("Error, el reconocimiento no fue actualizado");
        }
    } 
    
}
