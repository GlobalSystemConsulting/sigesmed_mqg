/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.IndicadorCompromisoGestionPedagogicaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.IndicadorCompromisoGestionPedagogica;
import org.hibernate.Session;

public class IndicadorCompromisoGestionPedagogicaDaoHibernate extends GenericDaoHibernate<IndicadorCompromisoGestionPedagogica> implements IndicadorCompromisoGestionPedagogicaDao{

    @Override
    public IndicadorCompromisoGestionPedagogica buscarPorId(int indComGesPedId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        IndicadorCompromisoGestionPedagogica icgp = (IndicadorCompromisoGestionPedagogica)session.get(IndicadorCompromisoGestionPedagogica.class, indComGesPedId);
        session.close();
        return icgp;
    }
    
}
