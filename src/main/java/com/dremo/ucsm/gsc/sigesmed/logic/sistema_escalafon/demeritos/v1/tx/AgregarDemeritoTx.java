/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.demeritos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DemeritoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Demerito;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class AgregarDemeritoTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(AgregarDemeritoTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *   Parte para la lectura, verificacion y validacion de datos
         */
        Demerito demerito = null;
        
        Integer ficEscId = 0;

        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");

        try {
            JSONObject requestData = (JSONObject) wr.getData();

            ficEscId = requestData.getInt("ficEscId");
            String entEmi = requestData.getString("entEmi");
            String numRes = requestData.getString("numRes");
            Date fecRes = requestData.getString("fecRes").equals("")?null:sdi.parse(requestData.getString("fecRes").substring(0, 10));
            Boolean sep = requestData.getBoolean("sep");
            Date fecIni = requestData.getString("fecIni").equals("")?null:sdi.parse(requestData.getString("fecIni").substring(0, 10));
            Date fecFin = requestData.getString("fecFin").equals("")?null:sdi.parse(requestData.getString("fecFin").substring(0, 10));
            String mot = requestData.getString("mot");
            
            demerito = new Demerito(new FichaEscalafonaria(ficEscId), entEmi, numRes, fecRes, sep, fecIni, fecFin, mot, wr.getIdUsuario(), new Date(), 'A');

        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"Datos nuevo estudio de postgrado",e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        //Fin

        /*
        *  Parte para la operacion en la Base de Datos
         */
        //si el pariente no existe en la tabla persona
        DemeritoDao demeritoDao = (DemeritoDao) FactoryDao.buildDao("se.DemeritoDao");
        try {
            demeritoDao.insert(demerito);
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar nuevo demerito",e);
            System.out.println(e);
        }
        
        //Fin       

        /*
        *  Repuesta Correcta
         */
        JSONObject oResponse = new JSONObject();
        oResponse.put("demId", demerito.getDemId());
        oResponse.put("entEmi", demerito.getEntEmi()==null?"":demerito.getEntEmi());
        oResponse.put("numRes", demerito.getNumRes()==null?"":demerito.getNumRes());
        oResponse.put("fecRes", demerito.getFecRes()==null?"":sdi.format(demerito.getFecRes()));
        oResponse.put("sep", demerito.getSep()==null?false:demerito.getSep());
        oResponse.put("fecIni", demerito.getFecIni()==null?"":sdi.format(demerito.getFecIni()));
        oResponse.put("fecFin", demerito.getFecFin()==null?"":sdi.format(demerito.getFecFin()));
        oResponse.put("mot", demerito.getMot()==null?"":demerito.getMot());
                
        return WebResponse.crearWebResponseExito("El registro del demerito se realizo correctamente", oResponse);
        //Fin
    }
    
}
