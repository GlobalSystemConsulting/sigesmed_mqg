/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.PeriodosPlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.PlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.PlanNivelDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Nivel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.PeriodosPlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Periodo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanNivel;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.ArrayList;
import java.util.Date;
import jdk.nashorn.internal.objects.NativeArray;
import org.json.JSONArray;
/**
 *
 * @author abel
 */
public class InsertarPlanEstudiosTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        PlanEstudios nuevo = null;
        JSONArray niveles = null;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();            
            int orgId = requestData.getInt("organizacionID");
            String nombre = requestData.getString("nombre");
            niveles = requestData.getJSONArray("niveles");
            
            Date año = new Date();        
            año.setSeconds(0);
            año.setMinutes(0);
            año.setHours(0);
            año.setMonth(0);
            año.setDate(1);
            
            nuevo = new PlanEstudios(0,nombre,orgId,año,new Date(),wr.getIdUsuario(),'A');
            System.out.print(nuevo.toString());
           
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar el el plan de estudios, datos incorrectos", e.getMessage() );
        }
        try{
            PlanEstudiosDao planDao = (PlanEstudiosDao)FactoryDao.buildDao("mech.PlanEstudiosDao");
            planDao.insert(nuevo);
            
            if(niveles.length() > 0){
                nuevo.setNiveles(new ArrayList<PlanNivel>());
                for(int i = 0; i < niveles.length();i++){
                    JSONObject bo =niveles.getJSONObject(i);
                    
                    PlanNivelDao planNivelDao = (PlanNivelDao)FactoryDao.buildDao("mech.PlanNivelDao");
                    
                    
                    
                    PlanNivel uno = new PlanNivel();
                    uno.setDes(bo.getString("descripcion"));
                    uno.setPlanEstudios(nuevo);
                    uno.setPlaEstId(48);
                    uno.setPerId(bo.getString("periodoID").charAt(0));
                    uno.setTurId(bo.getString("turnoID").charAt(0));
                    uno.setJorId(bo.getInt("jornadaID"));
                    uno.setEstReg('A');
                    planNivelDao.insert(uno);
                                 
                    
                    nuevo.getNiveles().add( new PlanNivel(bo.getString("descripcion"),nuevo, bo.getString("periodoID").charAt(0),bo.getString("turnoID").charAt(0),bo.getInt("jornadaID"),new Date(),wr.getIdUsuario(),'A') );
                    System.out.println("NIVELES");
                }
            }
            
            planDao.reiniciarPlazasMagisteriales(nuevo.getOrgId());
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar el el plan de estudios", e.getMessage() );
        }
        try{
            //Crear periodos Plan de estudios
            PeriodosPlanEstudiosDao periodosPlanEstudiosDao = (PeriodosPlanEstudiosDao)FactoryDao.buildDao("maestro.plan.PeriodosPlanEstudiosDao");
            for(int i = 0; i < niveles.length();i++){
                JSONObject bo =niveles.getJSONObject(i);                
                int etapas = 0;
                if(bo.getString("periodoID").equals("B")){
                    //Bimestre
                    etapas = 4;
                }                    
                if(bo.getString("periodoID").equals("T")){
                    //Trimestre
                    etapas = 3;
                }
                for (int j = 1; j <= etapas; j++) {
                    PeriodosPlanEstudios pAux = new PeriodosPlanEstudios();
                    pAux.setPlanEstudios(nuevo);
                    pAux.setPeriodo(new Periodo(bo.getString("periodoID").charAt(0)));
                    pAux.setNivel(new Nivel(bo.getInt("nivelID")));
                    pAux.setUsuMod(wr.getIdUsuario());
                    pAux.setEta(j);                
                    periodosPlanEstudiosDao.insert(pAux);
                }   
                System.out.println("periodos");
            }
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar los periodos del plan de estudios", e.getMessage() );
        }
        //Fin
        
        
        
        JSONObject res = new JSONObject();
        res.put("planID", nuevo.getPlaEstId());
        
        JSONArray aPlanNiveles = new JSONArray();
        for(PlanNivel pn : nuevo.getNiveles() )
            aPlanNiveles.put(pn.getPlaNivId());
        
        res.put("IDs", aPlanNiveles);
        
        return WebResponse.crearWebResponseExito("El registro el Plan de Estudios correctamente",res);
    }    
    
    
}

