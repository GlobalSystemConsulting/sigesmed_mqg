/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.rol.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.RolDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Rol;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class ListarRolTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        try{
        
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar los Roles", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<Rol> roles = null;
        RolDao rolDao = (RolDao)FactoryDao.buildDao("RolDao");
        try{
            roles = rolDao.buscarTodos(Rol.class);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar Roles\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Roles", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Rol rol:roles ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("rolID",rol.getRolId() );
            oResponse.put("abreviatura",rol.getAbr());
            oResponse.put("nombre",rol.getNom());
            oResponse.put("descripcion",rol.getDes());
            oResponse.put("fecha",rol.getFecMod().toString());
            oResponse.put("estado",""+rol.getEstReg());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}

