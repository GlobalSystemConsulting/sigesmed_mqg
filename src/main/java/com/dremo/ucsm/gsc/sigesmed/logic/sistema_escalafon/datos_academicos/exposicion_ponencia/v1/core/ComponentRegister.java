/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.exposicion_ponencia.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.exposicion_ponencia.v1.tx.ActualizarExposicionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.exposicion_ponencia.v1.tx.AgregarExposicionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.exposicion_ponencia.v1.tx.EliminarExposicionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.exposicion_ponencia.v1.tx.ListarExposicionesTx;

/**
 *
 * @author Yemi
 */
public class ComponentRegister implements IComponentRegister{

    @Override
    public WebComponent createComponent() {
        WebComponent seComponent = new WebComponent(Sigesmed.MODULO_ESCALAFON);

        seComponent.setName("exposicion_ponencia");
        seComponent.setVersion(1);
        
        seComponent.addTransactionGET("listarExposiciones", ListarExposicionesTx.class);
	seComponent.addTransactionPOST("agregarExposicion", AgregarExposicionTx.class);
        seComponent.addTransactionPOST("actualizarExposicion", ActualizarExposicionTx.class);
        seComponent.addTransactionDELETE("eliminarExposicion", EliminarExposicionTx.class);
        return seComponent;
    }
    
}
