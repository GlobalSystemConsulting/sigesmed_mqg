/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mpf;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mpf.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.HistoricoNotasEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.ListaUtiles;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Seccion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.AreaModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.NivelModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.AreaExonerada;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.Estudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.Matricula;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.SaludControles;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.BandejaTarea;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.EstudianteAsistenciaModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.GradoIeEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.RegistroAuxiliarCompetencia;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Carlos
 */
public class EstudianteDaoHibernate extends GenericDaoHibernate<Matricula> implements EstudianteDao {

    @Override
    public List<Matricula> listarHijosDisponibles(Integer persona) {
        List<Matricula> lista = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT DISTINCT ma FROM  com.dremo.ucsm.gsm.sigesmed.core.entity.mpf.Matricula ma  JOIN FETCH ma.planNivel pn  JOIN FETCH ma.organizacionByOrgDesId JOIN FETCH ma.estudiante ee JOIN FETCH ee.persona pp JOIN FETCH pp.parientes pa WHERE pa.personaByParId=:p1 AND ma.act=true  ";

            Query query = session.createQuery(hql);
            query.setParameter("p1", persona);

            lista = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo buscar los estudiantes \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar los estudiantes \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return lista;
    }
    
    @Override
    public Matricula getEstudiante(Long persona) {
        Matricula lista = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT  ma FROM  com.dremo.ucsm.gsm.sigesmed.core.entity.mpf.Matricula ma  JOIN FETCH ma.planNivel pn  JOIN FETCH ma.organizacionByOrgDesId JOIN FETCH ma.estudiante ee JOIN FETCH ee.persona pp  WHERE ma.estudianteId=:p1 AND ma.act=true  ";

            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            query.setParameter("p1", persona);

            lista = (Matricula)query.uniqueResult();

        } catch (Exception e) {
            System.out.println("No se pudo buscar el dato del estudiante \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar los estudiantes \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return lista;
    }

    @Override
    public Matricula getDatosEstudiante(Long matriculaId) {
        Matricula registro = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT ma FROM com.dremo.ucsm.gsm.sigesmed.core.entity.mpf.Matricula ma JOIN FETCH ma.planNivel pn JOIN FETCH pn.turno JOIN FETCH ma.grado JOIN FETCH ma.seccion LEFT JOIN FETCH ma.personaByApoId JOIN FETCH ma.estudiante ee LEFT JOIN FETCH ee.datosNacimiento dn LEFT JOIN FETCH dn.pais  JOIN FETCH ee.persona JOIN FETCH ma.gradoIeEstudiantes  gie  WHERE gie.estReg<>'E'  AND gie.act=true AND ma.matId=:p1 ";//JOIN FETCH ee.saludControles sc

            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            query.setParameter("p1", matriculaId);

            registro = (Matricula) query.uniqueResult();

        } catch (Exception e) {
            System.out.println("No se pudo buscar la Matricula \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar la Matricula \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return registro;

    }

    @Override
    public List<SaludControles> getSaludControlesByEstudiante(Estudiante estudiante) {
        List<SaludControles> lista = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT  sc FROM  SaludControlesMpf sc   WHERE sc.estReg<>'E'  AND sc.estudiante =:p1 ORDER BY sc.fecCon DESC";

            Query query = session.createQuery(hql);
            query.setParameter("p1", estudiante.getPerId());

            lista = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo buscar los Controles de Salud  \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar los Controles de Salud \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return lista;

    }

    @Override
    public Matricula getDatosMatricula(Long matriculaId) {
        Matricula registro = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT ma FROM com.dremo.ucsm.gsm.sigesmed.core.entity.mpf.Matricula ma JOIN FETCH ma.planNivel pn JOIN FETCH pn.turno JOIN FETCH ma.grado JOIN FETCH ma.seccion    WHERE   ma.matId=:p1 ";//JOIN FETCH ee.saludControles sc

            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            query.setParameter("p1", matriculaId);

            registro = (Matricula) query.uniqueResult();

        } catch (Exception e) {
            System.out.println("No se pudo buscar la Matricula \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar la Matricula \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return registro;
    }
    
    @Override
    public Matricula getDatosOnlyMatricula(Long matriculaId) {
        Matricula registro = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT ma FROM com.dremo.ucsm.gsm.sigesmed.core.entity.mpf.Matricula ma  WHERE   ma.matId=:p1  AND ma.estReg<>'E'";//JOIN FETCH ee.saludControles sc

            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            query.setParameter("p1", matriculaId);

            registro = (Matricula) query.uniqueResult();

        } catch (Exception e) {
            System.out.println("No se pudo buscar la Matricula \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar la Matricula \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return registro;
    }
    
    @Override
    public Matricula getDatosMatriculaAndPlan(Long matriculaId) {
        Matricula registro = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT ma FROM com.dremo.ucsm.gsm.sigesmed.core.entity.mpf.Matricula ma JOIN FETCH ma.planNivel WHERE   ma.matId=:p1  AND ma.estReg<>'E'";//JOIN FETCH ee.saludControles sc

            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            query.setParameter("p1", matriculaId);

            registro = (Matricula) query.uniqueResult();

        } catch (Exception e) {
            System.out.println("No se pudo buscar la Matricula \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar la Matricula \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return registro;
    }

    @Override
    public List ListarTutores(Integer grado, Character seccion, Integer planEstudios) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();

        List data = null;

        try {
            SQLQuery query = session.createSQLQuery("SELECT DISTINCT pp.per_id id, pp.ape_pat paterno, pp.ape_mat materno, pp.nom nombre FROM pedagogico.persona pp INNER JOIN pedagogico.docente dd  ON dd.doc_id=pp.per_id INNER JOIN institucional.plaza_magisterial pm ON pm.doc_id=dd.doc_id INNER JOIN institucional.distribucion_hora_grado dhg ON dhg.pla_mag_id=pm.pla_mag_id AND dhg.gra_id=" + grado + " AND dhg.sec_id='" + seccion + "' AND dhg.pla_est_id=" + planEstudios + " WHERE dhg.hor_tut>0");
            query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
            data = query.list();
            t.commit();

        } catch (Exception e) {
            t.rollback();
            System.out.println("No se pudo Listar los Tutores \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Tutores \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return data;

    }

    @Override
    public List<AreaExonerada> listarCursosExonerados(Estudiante estudiante) {
        List<AreaExonerada> lista = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT  ae FROM  AreaExoneradaMPF  ae JOIN FETCH ae.areaCurricular  WHERE ae.estudiante=:p1 AND ae.estReg<>'E'  ";

            Query query = session.createQuery(hql);
            query.setParameter("p1", estudiante);

            lista = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo buscar los cursos \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar los cursos \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return lista;

    }

//    @Override
//    public List<NivelModel> buscarNiveles(int planEstudiosID) {
//        List<NivelModel> objetos = null;
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        try {
//            String hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.mech.NivelModel(pn.plaNivId,pn.des,pn.jornada.nivId,pn.jornada.jorEscId,pn.jornada.nom,pn.turId,pn.perId,pn.jornada.disCurId) FROM PlanNivel pn WHERE pn.plaEstId=:p1 ORDER BY pn.jornada.nivId";
//            Query query = session.createQuery(hql);
//            query.setParameter("p1", planEstudiosID);
//            objetos = query.list();
//
//        } catch (Exception e) {
//            System.out.println("No se pudo Listar los niveles model del plan de estudios\\n " + e.getMessage());
//            throw new UnsupportedOperationException("No se pudo Listar los niveles model del plan de estudios\\n " + e.getMessage());
//        } finally {
//            session.close();
//        }
//        return objetos;
//    }
//
//    @Override
//    public List<AreaModel> buscarAreas(int disCurID,int gradoId,List<Integer> exoneraciones) {
//        List<AreaModel> objetos = null;
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        try {
//            String hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.AreaModel(a.area.areCurId,a.area.nom,a.grado.graId,a.grado.nom) FROM AreaCurricularHora a WHERE a.area.disCurId=:p1 AND a.grado.graId=:p2 AND a.area.areCurId NOT IN (:p3)";
//            Query query = session.createQuery(hql);
//            query.setParameter("p1", disCurID);
//            query.setParameter("p2", gradoId);
//            query.setParameterList("p3", exoneraciones);
//            objetos = query.list();
//
//        } catch (Exception e) {
//            System.out.println("No se pudo Listar las areas model del plan de estudios\\n " + e.getMessage());
//            throw new UnsupportedOperationException("No se pudo Listar las areas model del plan de estudios\\n " + e.getMessage());
//        } finally {
//            session.close();
//        }
//        return objetos;
//    }
//    
//    @Override
//    public List<AreaModel> buscarTalleres(int disCurID, int gradoId, List<Integer> exoneraciones){
//        List<AreaModel> objetos = null;
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        try{
//            String hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.AreaModel(a.area.areCurId,a.area.nom,a.grado.graId,a.grado.nom) FROM PlanHoraArea a WHERE a.area.disCurId=:p1 and a.area.esTal=true AND a.grado.graId=:p2 AND a.area.areCurId NOT IN (:p3)";
//            Query query = session.createQuery(hql);
//            query.setParameter("p1", disCurID);
//            query.setParameter("p2", gradoId);
//            query.setParameterList("p3", exoneraciones);
//            objetos = query.list();
//            
//        }catch(Exception e){
//            System.out.println("No se pudo Listar las areas model del plan de estudios\\n "+ e.getMessage());
//            throw new UnsupportedOperationException("No se pudo Listar las areas model del plan de estudios\\n "+ e.getMessage());            
//        }
//        finally{
//            session.close();
//        }
//        return objetos;
//    }

    @Override
    public List<AreaModel> buscarAreasByPlanEstudios(int plaEstudiosId,int gradoId,char seccionId) {
        List<AreaModel> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.AreaModel(dha.areCurId,dha.area.nom,pp.perId,pp.nom,pp.apePat,pp.apeMat) FROM DistribucionHoraArea dha JOIN  dha.distribucionGrado dhg JOIN  dhg.plazaMagisterial pm JOIN  pm.docente dc JOIN  dc.persona pp  WHERE dhg.plaEstId=:p1 AND dhg.graId=:p2 AND dhg.secId=:p3";
            Query query = session.createQuery(hql);
            query.setParameter("p1", plaEstudiosId);
            query.setParameter("p2", gradoId);
            query.setParameter("p3", seccionId);
            objetos = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo Listar las areas model del plan de estudios\\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las areas model del plan de estudios\\n " + e.getMessage());
        } finally {
            session.close();
        }
        return objetos;
    }
    
    @Override
    public List<AreaModel> buscarAreasByPlanEstudios(int plaEstudiosId,int gradoId,char seccionId,List<Integer> exoneraciones) {
        List<AreaModel> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.AreaModel(dha.areCurId,dha.area.nom,pp.perId,pp.nom,pp.apePat,pp.apeMat) FROM DistribucionHoraArea dha JOIN  dha.area a JOIN  dha.distribucionGrado dhg JOIN  dhg.plazaMagisterial pm JOIN  pm.docente dc JOIN  dc.persona pp  WHERE dhg.plaEstId=:p1 AND dhg.graId=:p2 AND dhg.secId=:p3 AND a.areCurId NOT IN (:p4)";
            Query query = session.createQuery(hql);
            query.setParameter("p1", plaEstudiosId);
            query.setParameter("p2", gradoId);
            query.setParameter("p3", seccionId);
            query.setParameterList("p4", exoneraciones);
            objetos = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo Listar las areas model del plan de estudios\\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las areas model del plan de estudios\\n " + e.getMessage());
        } finally {
            session.close();
        }
        return objetos;
    }

    @Override
    public AreaModel buscarAreasById(int planID,int gradoID,int areaId) {
        AreaModel objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.AreaModel(dha.areCurId,dha.area.nom,pp.perId,pp.nom,pp.apePat,pp.apeMat) FROM DistribucionHoraArea dha JOIN  dha.distribucionGrado dhg JOIN  dhg.plazaMagisterial pm JOIN  pm.docente dc JOIN  dc.persona pp  WHERE dhg.plaEstId=:p1 AND dhg.graId=:p2 AND dha.areCurId=:p3";
            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            query.setParameter("p1", planID);
            query.setParameter("p2", gradoID);
            query.setParameter("p3", areaId);
            objetos = (AreaModel) query.uniqueResult();

        } catch (Exception e) {
            System.out.println("No se pudo Listar las areas model del plan de estudios\\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las areas model del plan de estudios\\n " + e.getMessage());
        } finally {
            session.close();
        }
        return objetos;
    }
    
    @Override
    public ListaUtiles listarUtilesByArea(Grado gradoId, Seccion seccion, AreaCurricular area,Organizacion org) {
        ListaUtiles registro = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT lu FROM ListaUtilesMpf lu JOIN FETCH lu.listas la  WHERE   lu.estReg<>'E' AND la.estReg<>'E' AND lu.grado=:p1 AND lu.seccion=:p2 AND lu.area=:p3 AND lu.organizacion=:p4";//JOIN FETCH ee.saludControles sc

            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            query.setParameter("p1", gradoId);
            query.setParameter("p2", seccion);
            query.setParameter("p3", area);
            query.setParameter("p4", org);

            registro = (ListaUtiles) query.uniqueResult();

        } catch (Exception e) {
            System.out.println("No se pudo buscar la Lista de Utiles \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar la Lista de Utiles \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return registro;
    
    }
    
    @Override
    public ListaUtiles listarUtiles(Grado gradoId, Seccion seccion,Organizacion org) {
        ListaUtiles registro = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT lu FROM ListaUtilesMpf lu JOIN FETCH lu.listas la  WHERE   lu.estReg<>'E' AND la.estReg<>'E' AND lu.grado=:p1 AND lu.seccion=:p2 AND  lu.organizacion=:p3";//JOIN FETCH ee.saludControles sc

            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            query.setParameter("p1", gradoId);
            query.setParameter("p2", seccion);
            query.setParameter("p3", org);

            registro = (ListaUtiles) query.uniqueResult();

        } catch (Exception e) {
            System.out.println("No se pudo buscar la Lista de Utiles \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar la Lista de Utiles \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return registro;
    
    }
    
    @Override
    public PlanEstudios getPlanEstudiosById(Integer id) {
        PlanEstudios registro = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "FROM PlanEstudios pe  WHERE   pe.estReg<>'E' AND pe.plaEstId=:p1";

            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            query.setParameter("p1", id);
            registro = (PlanEstudios) query.uniqueResult();

        } catch (Exception e) {
            System.out.println("No se pudo buscar el Plan de Estudios \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar el Plan de Estudios \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return registro;
    
    }
    
    @Override
    public List<BandejaTarea> buscarTareasPorAlumnoAndArea(int planID, Long alumnoID, int areaId) {
        List<BandejaTarea> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT DISTINCT bt FROM BandejaTareaMpf bt JOIN FETCH bt.tareaEscolar te LEFT JOIN FETCH bt.documentos WHERE bt.tareaEscolar.plaEstId=:p1 and bt.alumnoID=:p2 AND te.areCurId=:p3";
            Query query = session.createQuery(hql);
            query.setParameter("p1", planID);
            query.setParameter("p2", alumnoID);
            query.setParameter("p3", areaId);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las tareas por plan de estudios y alumno\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las tareas por plan de estudios y alumno\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }

    @Override
    public List<EstudianteAsistenciaModel> buscarAsistenciaByAlumno(Long matriculaID, Integer area, Date desde, Date hasta) {
        List<EstudianteAsistenciaModel> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.EstudianteAsistenciaModel(aef.asiEstId,aef.fecAsi,aef.tipAsi,aef.estAsi,jj.justificacionId,jj.desJus,jj.docAdjJus) FROM AsistenciaEstudianteMpf aef LEFT JOIN aef.justificacion jj WHERE aef.estReg<>'E' AND aef.matriculaId =:p1 AND aef.areaId=:p2 AND aef.fecAsi>=:p3 AND aef.fecAsi<=:p4 ORDER BY aef.fecAsi";
            Query query = session.createQuery(hql);
            query.setParameter("p1", matriculaID);
            query.setParameter("p2", area);
            query.setParameter("p3", desde);
             query.setParameter("p4", hasta);
            objetos = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo Listar las asistencias\\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las asistencias\\n " + e.getMessage());
        } finally {
            session.close();
        }
        return objetos;
    
    }

    @Override
    public GradoIeEstudiante getGradoIeeActual(Matricula matricula) {
        GradoIeEstudiante registro = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT gie FROM GradoIeEstudianteMpf gie JOIN FETCH gie.grado JOIN FETCH gie.seccion WHERE gie.estReg<>'E'  AND gie.act=true AND gie.matriculaEstudiante=:p1 ";//JOIN FETCH ee.saludControles sc

            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            query.setParameter("p1", matricula);

            registro = (GradoIeEstudiante) query.uniqueResult();

        } catch (Exception e) {
            System.out.println("No se pudo buscar la Matricula \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar la Matricula \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return registro;
    }

    @Override
    public List<RegistroAuxiliarCompetencia> listarNotasByCompetencias(GradoIeEstudiante gradoIee) {
        List<RegistroAuxiliarCompetencia> lista = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT DISTINCT ma FROM RegistroAuxiliarCompetenciaMpf  ma  JOIN FETCH ma.comp co  JOIN FETCH ma.areaCurricular acu JOIN FETCH ma.periodoPlanEstudios pp  JOIN FETCH pp.periodo pr WHERE ma.estReg<>'E' AND ma.gradoEst=:p1  GROUP BY acu.areCurId,ma.regAuxComId,co.comId,pp.perPlaEstId,pr.perId";

            Query query = session.createQuery(hql);
            query.setParameter("p1", gradoIee);

            lista = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo buscar las Notas \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar las Notas \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return lista;
    
    }
    
    @Override
    public String getNotaAreaByPeriodo(int  gradoIee,int areaId,int etapa) {
        String nota="";
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try {
            Query query = session.createSQLQuery("SELECT hne.not_are FROM pedagogico.historico_notas_estudiante hne \n"
                    + "INNER JOIN institucional.periodos_plan_estudios ppe ON ppe.per_pla_est_id=hne.per_eva_id AND ppe.est_reg<>'E' \n"
                    + "WHERE hne.gra_ie_est_id="+gradoIee+" AND hne.are_cur_id="+areaId+" AND ppe.eta="+etapa);  
            

             query.setMaxResults(1);
             nota=(String)query.uniqueResult();

        } catch (Exception e) {
            System.out.println("No se pudo buscar las Notas \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar las Notas \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return nota;
    
    }
    
    @Override
    public String getNotaPromAreaFinal(int  gradoIee,int areaId) {
        String nota="";
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try {
            Query query = session.createSQLQuery("SELECT rnf.not_prom FROM pedagogico.registro_nota_final rnf \n"
                    + "WHERE rnf.gra_ie_est_id="+gradoIee+" AND rnf.are_cur_id="+areaId+" AND rnf.est_reg<>'E'");  
            

             query.setMaxResults(1);
             nota=(String)query.uniqueResult();

        } catch (Exception e) {
            System.out.println("No se pudo buscar las Notas \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar las Notas \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return nota;
    
    }
    
    @Override
    public String getNotaRecAreaFinal(int  gradoIee,int areaId) {
        String nota="";
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try {
            Query query = session.createSQLQuery("SELECT rnf.not_rec FROM pedagogico.registro_nota_final rnf \n"
                    + "WHERE rnf.gra_ie_est_id="+gradoIee+" AND rnf.are_cur_id="+areaId+" AND rnf.est_reg<>'E'");  
             query.setMaxResults(1);
             nota=(String)query.uniqueResult();

        } catch (Exception e) {
            System.out.println("No se pudo buscar las Notas \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar las Notas \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return nota;
    
    }
    
    
}
