/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.estudio_complementario.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.estudio_complementario.v1.tx.ActualizarEstudioComplementarioTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.estudio_complementario.v1.tx.AgregarEstudioComplementarioTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.estudio_complementario.v1.tx.EliminarEstudioComplementarioTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.estudio_complementario.v1.tx.ListarEstudiosComplementariosTx;

/**
 *
 * @author Yemi
 */
public class ComponentRegister implements IComponentRegister{

    @Override
    public WebComponent createComponent() {
        WebComponent seComponent = new WebComponent(Sigesmed.MODULO_ESCALAFON);

        seComponent.setName("estudio_complementario");
        seComponent.setVersion(1);
        
        seComponent.addTransactionGET("listarEstudiosComplementarios", ListarEstudiosComplementariosTx.class);
        seComponent.addTransactionPOST("agregarEstudioComplementario", AgregarEstudioComplementarioTx.class);
        seComponent.addTransactionPOST("actualizarEstudioComplementario", ActualizarEstudioComplementarioTx.class);
        seComponent.addTransactionDELETE("eliminarEstudioComplementario", EliminarEstudioComplementarioTx.class);
        return seComponent;
    }
    
}
