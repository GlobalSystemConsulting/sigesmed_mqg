package com.dremo.ucsm.gsc.sigesmed.core.entity.mech;

import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="plan_nivel" ,schema="institucional" )
public class PlanNivel  implements java.io.Serializable {

    @Id
    @Column(name="pla_niv_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_plannivel", sequenceName="institucional.plan_nivel_pla_niv_id_seq" )
    @GeneratedValue(generator="secuencia_plannivel")
    private int plaNivId;
    @Column(name="des", length=256)
    private String des;
    
    @Column(name="pla_est_id",insertable = false,updatable = false)
    private int plaEstId;    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="pla_est_id",insertable = true,updatable = true)
    private PlanEstudios planEstudios;
    
    @Column(name="tur_id")
    private char turId;    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="tur_id",updatable = false,insertable = false)
    private Turno turno;
    
    @Column(name="per_id")
    private char perId;    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="per_id",updatable = false,insertable = false)
    private Periodo periodo;
    
    @Column(name="jor_esc_id")
    private int jorId;    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="jor_esc_id",updatable = false,insertable = false)
    private JornadaEscolar jornada;
    
    @OneToMany(mappedBy="planNivel",fetch=FetchType.EAGER)
    private List<MetaAtencion> metas;
    
    @OneToMany(mappedBy="planNivel",fetch=FetchType.EAGER)
    @OrderBy(value="plaNivId,graId")
    private List<PlanHoraArea> horasDisponibles;
    
    @Column(name="est_reg")
    private char estReg;
    
    public PlanNivel() {
    }
    public PlanNivel(int plaNivId) {
        this.plaNivId = plaNivId;
    }
    public PlanNivel( String des,PlanEstudios plan,char perId,char turId,int jorId, Date fecMod, int usuMod, char estReg) {
       this.plaNivId = 0;
       this.des = des;
       this.planEstudios = plan;
       this.perId = perId;
       this.turId = turId;
       this.jorId = jorId;
    }
    public PlanNivel(int plaNivId, String des, int plaEstId,char perId,char turId,int jorId, Date fecMod, int usuMod, char estReg) {
       this.plaNivId = plaNivId;
       this.des = des;
       this.plaEstId = plaEstId;
       this.planEstudios = new PlanEstudios(plaEstId);
       this.perId = perId;
       this.turId = turId;
       this.jorId = jorId;
    }
   
     
    public int getPlaNivId() {
        return this.plaNivId;
    }    
    public void setPlaNivId(int plaNivId) {
        this.plaNivId = plaNivId;
    }
       
    public String getDes() {
        return this.des;
    }
    public void setDes(String des) {
        this.des = des;
    }
    
    
    public int getPlaEstId() {
        return this.plaEstId;
    }    
    public void setPlaEstId(int plaEstId) {
        this.plaEstId = plaEstId;
    }
    
    public PlanEstudios getPlanEstudios() {
        return this.planEstudios;
    }
    public void setPlanEstudios(PlanEstudios planEstudios) {
        this.planEstudios = planEstudios;
    }
    
    public char getTurId() {
        return this.turId;
    }    
    public void setTurId(char turId) {
        this.turId = turId;
    }
    
    public Turno getTurno() {
        return this.turno;
    }
    public void setTurno(Turno turno) {
        this.turno = turno;
    }
    
    public char getPerId() {
        return this.perId;
    }    
    public void setPerId(char perId) {
        this.perId = perId;
    }
    
    public Periodo getPeriodo() {
        return this.periodo;
    }
    public void setPeriodo(Periodo periodo) {
        this.periodo = periodo;
    }
    
    public int getJorId() {
        return this.jorId;
    }    
    public void setJorId(int jorId) {
        this.jorId = jorId;
    }
    
    public JornadaEscolar getJornada() {
        return this.jornada;
    }
    public void setJornada(JornadaEscolar jornada) {
        this.jornada = jornada;
    }
    
    public List<MetaAtencion> getMetas() {
        return this.metas;
    }
    public void setMetas(List<MetaAtencion> metas) {
        this.metas = metas;
    }
    
    public List<PlanHoraArea> getHorasDisponibles() {
        return this.horasDisponibles;
    }
    public void setHorasDisponibles(List<PlanHoraArea> horasDisponibles) {
        this.horasDisponibles = horasDisponibles;
    }
    
     public char getEstReg() {
        return this.estReg;
    }
    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
}


