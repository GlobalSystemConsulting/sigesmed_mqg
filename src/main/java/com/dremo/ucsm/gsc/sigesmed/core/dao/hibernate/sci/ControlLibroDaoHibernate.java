/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sci;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.ControlLibroDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.ControlLibro;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;


/**
 *
 * @author Administrador
 */
public class ControlLibroDaoHibernate  extends GenericDaoHibernate<ControlLibro> implements ControlLibroDao{
    
     public List<ControlLibro> listarControles(int organizacionID,int year){
        List<ControlLibro> listDatos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            //listar trbajdor
            String hql = "SELECT cl FROM ControlLibro cl  WHERE cl.estReg!='E' AND cl.organizacion.orgId=:p1 AND YEAR(cl.fecCie)=:p2";
            Query query = session.createQuery(hql);
            query.setParameter("p1", organizacionID);
            query.setParameter("p2", year);
            listDatos = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo Listar \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return listDatos;
     }
     
     @Override
     public List<ControlLibro> listarControlesEstado(int organizacionID,int year,char estado){
        List<ControlLibro> listDatos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            //listar trbajdor
            String hql = "SELECT cl FROM ControlLibro cl  WHERE cl.estReg:=p3 AND cl.organizacion.orgId=:p1 AND YEAR(cl.fecCie)=:p2";
            Query query = session.createQuery(hql);
            query.setParameter("p1", organizacionID);
            query.setParameter("p2", year);
             query.setParameter("p3", estado);
            listDatos = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo Listar \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return listDatos;
     }
     @Override
     public List<ControlLibro> listarControlesT(int organizacionID,int year){
        List<ControlLibro> listDatos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            //listar trbajdor
            String hql = "SELECT cl FROM ControlLibro cl  WHERE cl.organizacion.orgId=:p1 AND YEAR(cl.fecCie)=:p2";
            Query query = session.createQuery(hql);
            query.setParameter("p1", organizacionID);
            query.setParameter("p2", year);
           
            listDatos = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo Listar \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return listDatos;
     }

    @Override
    public ControlLibro buscarControl(int organizacionID) {
       ControlLibro control = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            //listar trbajdor
            String hql = "SELECT Max(cl) FROM ControlLibro cl  WHERE cl.estReg = 'A' AND cl.organizacion.orgId=:p1 ";
            Query query = session.createQuery(hql);
            query.setParameter("p1", organizacionID);
            control = (ControlLibro) query.uniqueResult();

        } catch (Exception e) {
            System.out.println("No se pudo obtener el control del libro - SQL \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener el control del libro - SQL -U \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return control;
        
    }
     
}
