/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.smdg;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.Proyectos;
import java.util.List;

/**
 *
 * @author Administrador
 */
public interface ProyectosDao extends GenericDao<Proyectos>{
    public String buscarUltimoCodigo();
    public List<Object[]> listarProyectos(int orgId);
    public void merge(Proyectos proyecto);
    public List<Object[]> listarTrabajadores(int traid);
    List<Object[]> listarProyectosDocente(int idDoc,int idOrg);
}
