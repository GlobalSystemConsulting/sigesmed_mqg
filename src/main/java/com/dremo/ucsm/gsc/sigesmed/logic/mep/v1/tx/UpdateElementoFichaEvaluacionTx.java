package com.dremo.ucsm.gsc.sigesmed.logic.mep.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mep.FichaEvaPerslDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.ElementoFicha;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.EscalaValoracionFicha;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.IndicadorFichaEvaluacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Administrador on 24/08/2016.
 */
public class UpdateElementoFichaEvaluacionTx extends MepGeneralTx implements ITransaction {
    @Override
    public WebResponse execute(WebRequest wr) {
        String type = wr.getMetadataValue("type");
        //String id = wr.getMetadataValue("id");
        JSONObject data = (JSONObject)wr.getData();
        if(type.equals(MEP.TIPO_ELEMENTO_INDICADOR))return updateIndicador(data,data.getInt("id"));
        else return updateElement(builtElementByType(type,data),data.getInt("id"));

    }
    private WebResponse updateElement(ElementoFicha elemento, int id){

        try{
            FichaEvaPerslDaoHibernate fdh = new FichaEvaPerslDaoHibernate();
            ElementoFicha eresult = fdh.updateElementoFicha(id, elemento);
            if(eresult != null){
                WebResponse response = formWebResponse(true);
                response.setData(new JSONObject(eresult.toString()));
                return response;
            }
            else{
                return formWebResponse(false);
            }

        }catch (JSONException e ){
            return formWebResponse(false);
        }
    }
    private WebResponse updateIndicador(JSONObject data,int id){
        try{
            FichaEvaPerslDaoHibernate fdh = new FichaEvaPerslDaoHibernate();
            IndicadorFichaEvaluacion ife = new IndicadorFichaEvaluacion(
                    data.getString(MEP.CAMPO_INDICADOR_NOMBRE),
                    data.getString(MEP.CAMPO_INDICADOR_DESC),
                    data.optString(MEP.CAMPO_INDICADOR_DOC_VER, null));
            IndicadorFichaEvaluacion result = fdh.updateIndicadorFicha(id, ife);
            if(result != null){
                JSONObject rdata = new JSONObject(result.toString());
                WebResponse response = formWebResponse(true);
                response.setData(rdata);
                return response;
            }else{
                return formWebResponse(false);
            }

        }catch (Exception e){
            return formWebResponse(false);
        }
    }

}
