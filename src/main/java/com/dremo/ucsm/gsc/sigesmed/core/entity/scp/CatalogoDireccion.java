/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scp;



import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name="catalogo_direccion", schema="administrativo")
public class CatalogoDireccion  implements java.io.Serializable{
    
    @Id
    @Column(name="dir_id", unique= true , nullable=false)
    @SequenceGenerator(name="secuencia_det_inv_tra",sequenceName="administrativo.catalogo_direccion_dir_id_seq")
    @GeneratedValue(generator="secuencia_det_inv_tra")
    private int dir_id;
    
    @Column(name="dir_nom")
    private String dir_nom;
    
    @Column(name="usu_mod")
    private int usu_mod;
    
    @Column(name="fec_mod")
    private Date fec_mod;
    
    @Column(name="est_reg")
    private char est_reg;

    public CatalogoDireccion() {
    }

    public CatalogoDireccion(int dir_id, String dir_nom, int usu_mod, Date fec_mod, char est_reg) {
        this.dir_id = dir_id;
        this.dir_nom = dir_nom;
        this.usu_mod = usu_mod;
        this.fec_mod = fec_mod;
        this.est_reg = est_reg;
    }

    public void setDir_id(int dir_id) {
        this.dir_id = dir_id;
    }

    public void setDir_nom(String dir_nom) {
        this.dir_nom = dir_nom;
    }

    public void setUsu_mod(int usu_mod) {
        this.usu_mod = usu_mod;
    }

    public void setFec_mod(Date fec_mod) {
        this.fec_mod = fec_mod;
    }

    public void setEst_reg(char est_reg) {
        this.est_reg = est_reg;
    }

    public int getDir_id() {
        return dir_id;
    }

    public String getDir_nom() {
        return dir_nom;
    }

    public int getUsu_mod() {
        return usu_mod;
    }

    public Date getFec_mod() {
        return fec_mod;
    }

    public char getEst_reg() {
        return est_reg;
    }
    
    
    
}
