/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.rol.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.RolDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.FuncionSistema;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Rol;
import com.dremo.ucsm.gsc.sigesmed.core.entity.RolFuncion;
import java.util.ArrayList;;
import java.util.Date;
import org.json.JSONArray;

/**
 *
 * @author Administrador
 */
public class ActualizarRolTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Rol actualizarRol = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            int rolID = requestData.getInt("rolID");
            String abreviatura = requestData.getString("abreviatura");
            String nombre = requestData.getString("nombre");
            String descripcion = requestData.getString("descripcion");
            String estado = requestData.getString("estado");
            JSONArray funciones = requestData.getJSONArray("funciones");
            
            actualizarRol = new Rol(rolID, abreviatura, nombre, descripcion,new Date(), 1, estado.charAt(0), new ArrayList<RolFuncion>());
            
            //int i =1;
            for(int i = 0; i < funciones.length();i++){
                JSONObject bo = funciones.getJSONObject(i);
                actualizarRol.getRolFunciones().add( new RolFuncion(new FuncionSistema( bo.getInt("funcionID") ) , actualizarRol,i+1, bo.getString("dependencias"),bo.getInt("tipo")) );
            }
            /*for(Object bo: requestData.getJSONArray("funciones").getList()){
                actualizarRol.getRolFunciones().add( new RolFuncion(new FuncionSistema( Integer.parseInt( ((JSONObject)bo).getString("funcionID") ) ) , actualizarRol,i++ ) );
            }*/
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        RolDao rolDao = (RolDao)FactoryDao.buildDao("RolDao");
        try{
            rolDao.eliminarFunciones(actualizarRol.getRolId());
            rolDao.update(actualizarRol);
        
        }catch(Exception e){
            System.out.println("No se pudo actualizar el Rol\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar Rol", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Rol se actualizo correctamente");
        //Fin
    }
    
}
