package com.dremo.ucsm.gsc.sigesmed.core.dao.web;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.ActividadCalendario;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;

public interface ActividadCalendarioDao extends GenericDao<ActividadCalendario> {

    List<ActividadCalendario> listarActividades(int usuCod);

    ActividadCalendario buscarActividad(int actCod);

    List<ActividadCalendario> buscarInstitucionalesInicio(int usuCod);
    
    List<ActividadCalendario> buscarInstitucionalesFin(int usuCod);
    
    List<Integer> buscarFuncionalidades(int usuCod, int rolCod);
    
    ActividadCalendario buscarActividadConPadre(int actCod);

    List<ActividadCalendario> buscarActividades(int actCod);
    
    JSONArray buscarActividadesFecha(int usuCod);
}
