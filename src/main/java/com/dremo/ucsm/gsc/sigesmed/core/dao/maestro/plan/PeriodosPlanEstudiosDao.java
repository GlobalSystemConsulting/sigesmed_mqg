package com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.PeriodosPlanEstudios;

/**
 *
 * @author Jerson Herrera
 */
public interface PeriodosPlanEstudiosDao extends GenericDao<PeriodosPlanEstudios>{
    
}
