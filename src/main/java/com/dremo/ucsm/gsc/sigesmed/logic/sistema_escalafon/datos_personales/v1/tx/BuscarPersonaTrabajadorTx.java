/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_personales.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FichaEscalafonariaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author alex
 */
public class BuscarPersonaTrabajadorTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(BuscarPersonaTrabajadorTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {

        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
        String perDNI = "";
        try {
            JSONObject requestData = (JSONObject) wr.getData();
            perDNI = requestData.getString("perDni");
        } catch (Exception e) {
            System.out.println(e);
            return WebResponse.crearWebResponseError("El DNI ingresado es incorrecto");
        }

        Persona persona = null;
        FichaEscalafonaria fichaEscalafonaria = new FichaEscalafonaria();
        FichaEscalafonariaDao fichaEscalafonariaDao = (FichaEscalafonariaDao) FactoryDao.buildDao("se.FichaEscalafonariaDao");
        PersonaDao personaDao = (PersonaDao) FactoryDao.buildDao("se.PersonaDao");
        int caso = 0;

        /*3 Casos: 
        *   1.- Tiene: persona, trabajador, ficha
        *   2.- Tiene: persona
        *   0.- No tiene: persona, trabajador, ficha
         */
        try {
            fichaEscalafonaria = fichaEscalafonariaDao.buscarPorDNI(perDNI);
            if (fichaEscalafonaria != null) {
                persona = fichaEscalafonaria.getTrabajador().getPersona();
                caso = 1;
            } else {
                persona = personaDao.buscarPorDNI(perDNI);
                if (persona != null) {
                    caso = 2;
                }
            }
        } catch (Exception e) {
            System.out.println("\n" + e);
            return WebResponse.crearWebResponseError("No se pudo buscar por dni", e.getMessage());
        }

        if (caso == 1) {

            JSONObject oRes = new JSONObject();
            JSONObject oPersona = new JSONObject();
            JSONObject oFicha = new JSONObject();
            JSONObject oTrabajador = new JSONObject();

            //Persona
            Integer perId = persona.getPerId();
            String dni = persona.getDni();
            String apePat = persona.getApePat();
            String apeMat = persona.getApeMat();
            String nom = persona.getNom();
            Character sex = persona.getSex();
            Character estCiv = persona.getEstCiv();
            String fij = persona.getFij();
            String num1 = persona.getNum1();
            String num2 = persona.getNum2();
            String email = persona.getEmail();
            String depNac = persona.getDepNac();
            String proNac = persona.getProNac();
            String disNac = persona.getDisNac();
            Date fecNac = persona.getFecNac();

            //Trabajador
            Integer traId = fichaEscalafonaria.getTrabajador().getTraId();
            Character traCon = fichaEscalafonaria.getTrabajador().getTraCon();

            //Ficha
            Integer ficEscId = fichaEscalafonaria.getFicEscId();
            String autEss = fichaEscalafonaria.getAutEss();
            String sisPen = fichaEscalafonaria.getSisPen();
            String nomAfp = fichaEscalafonaria.getNomAfp();
            String codCuspp = fichaEscalafonaria.getCodCuspp();
            Date fecIngAfp = fichaEscalafonaria.getFecIngAfp();
            Boolean perDis = fichaEscalafonaria.getPerDis();
            String regCon = fichaEscalafonaria.getRegCon();
            Character gruOcu = fichaEscalafonaria.getGruOcu();

            oPersona.put("perId", perId);
            oPersona.put("dni", dni);
            oPersona.put("apePat", apePat == null ? "" : apePat);
            oPersona.put("apeMat", apeMat == null ? "" : apeMat);
            oPersona.put("nom", nom == null ? "" : nom);
            oPersona.put("sex", sex == null ? "" : sex);
            oPersona.put("estCiv", estCiv == null ? "" : estCiv);
            oPersona.put("fij", fij == null ? "" : fij);
            oPersona.put("num1", num1 == null ? "" : num1);
            oPersona.put("num2", num2 == null ? "" : num2);
            oPersona.put("email", email == null ? "" : email);
            oPersona.put("depNac", depNac == null ? "" : depNac);
            oPersona.put("proNac", proNac == null ? "" : proNac);
            oPersona.put("disNac", disNac == null ? "" : disNac);
            oPersona.put("fecNac", fecNac == null ? "" : sdi.format(fecNac));
            oPersona.put("caso", caso);

            oTrabajador.put("traId", traId);
            oTrabajador.put("traCon", traCon == null ? "" : traCon);

            oFicha.put("ficEscId", ficEscId);
            oFicha.put("autEss", autEss == null ? "" : autEss);
            oFicha.put("sisPen", sisPen == null ? "" : sisPen);
            oFicha.put("nomAfp", nomAfp == null ? "" : nomAfp);
            oFicha.put("codCuspp", codCuspp == null ? "" : codCuspp);
            oFicha.put("fecIngAfp", fecIngAfp == null ? "" : sdi.format(fecIngAfp));
            oFicha.put("perDis", perDis == null ? "" : perDis);
            oFicha.put("regCon", regCon == null ? "" : regCon);
            oFicha.put("gruOcu", gruOcu == null ? "" : gruOcu);

            oRes.put("persona", oPersona);
            oRes.put("trabajador", oTrabajador);
            oRes.put("ficha", oFicha);

            return WebResponse.crearWebResponseExito("La ficha escalafonaria ya se encuentra registrado", oRes);
        } else {
            if (caso == 2) {
                JSONObject oRes = new JSONObject();
                JSONObject oPersona = new JSONObject();

                Integer perId = persona.getPerId();
                String dni = persona.getDni();
                String apePat = persona.getApePat();
                String apeMat = persona.getApeMat();
                String nom = persona.getNom();
                Character sex = persona.getSex();
                Date fecNac = persona.getFecNac();
                String fij = persona.getFij();
                String num1 = persona.getNum1();
                String num2 = persona.getNum2();
                String email = persona.getEmail();

                oPersona.put("perId", perId);
                oPersona.put("dni", dni);
                oPersona.put("apePat", apePat == null ? "" : apePat);
                oPersona.put("apeMat", apeMat == null ? "" : apeMat);
                oPersona.put("nom", nom == null ? "" : nom);
                oPersona.put("sex", sex == null ? "" : sex);
                oPersona.put("fecNac", fecNac == null ? "" : fecNac);
                oPersona.put("fij", fij == null ? "" : fij);
                oPersona.put("num1", num1 == null ? "" : num1);
                oPersona.put("num2", num2 == null ? "" : num2);
                oPersona.put("email", email == null ? "" : email);
                oPersona.put("caso", caso);

                oRes.put("persona", oPersona);

                return WebResponse.crearWebResponseExito("Se encontró a la persona, pero no tiene ficha escalafonaria", oRes);
            } else {
                return WebResponse.crearWebResponseError("La persona no esta registrado en el sistema");
            }
        }
    }
}
