/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.ma;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Docente;
import javax.persistence.*;
import java.util.Date;
import java.util.logging.Logger;
/**
 *
 * @author abel
 */
@Entity
@Table(name = "configuracion_nota_docente", schema = "pedagogico")
public class ConfiguracionNota implements java.io.Serializable{
    
    @Id
    @Column(name="conf_not_id" , unique=true, nullable=false)
    @SequenceGenerator(name="conf_not_id",sequenceName="pedagogico.configuracion_nota_docente_conf_not_id_seq")
    @GeneratedValue(generator="conf_not_id")
    private int conf_not_id;
    
    @Id
    @Column(name="doc_id")
    private int doc_id;
    
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "doc_id", insertable=false , updatable=false)
    private Docente docente;
    
   
    
    
    @Column(name="conf_cod" , length=2)
    private String conf_cod;
    
    @Column(name="conf_des" , length=20)
    private String conf_des;
    
    @Column(name="conf_not_min" )
    private int conf_not_min;
    
    @Column(name="conf_not_max")
    private int conf_not_max;
    
    @Column(name="est_reg" , length=1)
    private String estReg;
    
    @Column(name="usu_mod")
    private int usu_mod;
    
    @Column(name = "org_id")
    private int org_id;

    public void setDocente(Docente docente) {
        this.docente = docente;
    }

    public void setDoc_id(int doc_id) {
        this.doc_id = doc_id;
    }

    public void setConf_cod(String conf_cod) {
        this.conf_cod = conf_cod;
    }

    public void setConf_des(String conf_des) {
        this.conf_des = conf_des;
    }

    public void setConf_not_min(int conf_not_min) {
        this.conf_not_min = conf_not_min;
    }

    public void setConf_not_max(int conf_not_max) {
        this.conf_not_max = conf_not_max;
    }

    public void setEst_reg(String est_reg) {
        this.estReg = est_reg;
    }

    public void setUsu_mod(int usu_mod) {
        this.usu_mod = usu_mod;
    }

    public Docente getDocente() {
        return docente;
    }

    public int getDoc_id() {
        return doc_id;
    }

    public String getConf_cod() {
        return conf_cod;
    }

    public String getConf_des() {
        return conf_des;
    }

    public int getConf_not_min() {
        return conf_not_min;
    }

    public int getConf_not_max() {
        return conf_not_max;
    }

    public String getEst_reg() {
        return estReg;
    }

    public int getUsu_mod() {
        return usu_mod;
    }

    public void setOrg_id(int org_id) {
        this.org_id = org_id;
    }

    public int getOrg_id() {
        return org_id;
    }

    public void setConf_not_id(int conf_not_id) {
        this.conf_not_id = conf_not_id;
    }

    public int getConf_not_id() {
        return conf_not_id;
    }
    
    

    public ConfiguracionNota() {
    }

    public ConfiguracionNota(int conf_not_id ,int doc_id, String conf_cod, String conf_des, int conf_not_min, int conf_not_max, String est_reg, int usu_mod , int org_id) {
        this.conf_not_id = conf_not_id;
        this.doc_id = doc_id;
        this.conf_cod = conf_cod;
        this.conf_des = conf_des;
        this.conf_not_min = conf_not_min;
        this.conf_not_max = conf_not_max;
        this.estReg = est_reg;
        this.usu_mod = usu_mod;
        this.org_id = org_id;
    }

    public ConfiguracionNota(Docente docente, String conf_cod, String conf_des, int conf_not_min, int conf_not_max, String est_reg, int usu_mod) {
        this.docente = docente;
        this.conf_cod = conf_cod;
        this.conf_des = conf_des;
        this.conf_not_min = conf_not_min;
        this.conf_not_max = conf_not_max;
        this.estReg = est_reg;
        this.usu_mod = usu_mod;
    }
    
    
    
    
    
    
}
