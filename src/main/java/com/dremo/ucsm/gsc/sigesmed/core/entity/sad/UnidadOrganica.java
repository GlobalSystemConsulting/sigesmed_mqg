/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sad;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Area;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 *
 * @author Jeferson
 */

@Entity
@Table(name="unidad_organica" , schema ="administrativo")
public class UnidadOrganica implements java.io.Serializable{
    
    @Id
    @Column(name="uni_org_id" , unique=true , nullable=false)
    @SequenceGenerator(name="secuencia_uni_org", sequenceName="administrativo.unidad_organica_uni_org_id_seq")
    @GeneratedValue(generator="secuencia_uni_org")
    private int uni_org_id;
    
    //ARE_ID
    
    @Id
    private int are_id;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="are_id" , insertable=false , updatable=false)
    private Area area;
   
   
    
    @Column(name="abr")
    private String abr;
    
    @Column(name="nom")
    private String nombre;
    
    @Column(name="des")
    private String descripcion;
    
    @OneToMany(mappedBy="uni_org" , cascade = CascadeType.PERSIST)
    private List<SerieDocumental> series_documentales;
    
    @Column(name="est_reg")
    private char est_reg;
    
    public UnidadOrganica(){
        
    }
    public UnidadOrganica(int uni_org_id){
        this.uni_org_id = uni_org_id;
    }
    public UnidadOrganica(int uni_org_id , int are_id){
        
        this.uni_org_id = uni_org_id;
        this.are_id = are_id;
        
        this.area = new Area(are_id);
    }
    public UnidadOrganica(int uni_org_id , int are_id,String abr,String nombre , String descripcion){
        
        this.uni_org_id = uni_org_id;
        this.are_id = are_id;
        this.abr = abr;
        this.nombre = nombre;
        this.descripcion = descripcion;
        
        this.area = new Area(are_id);
    }
    public UnidadOrganica(int uni_org_id , int are_id,String abr,String nombre , String descripcion , char estado){
        
        this.uni_org_id = uni_org_id;
        this.are_id = are_id;
        this.abr = abr;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.est_reg = estado;
                
        this.area = new Area(are_id);
    }
    public void setUniOrgId(int uni_org_id){
        this.uni_org_id = uni_org_id;
    }
    public int getUniOrgId(){
        return this.uni_org_id; 
    }
    public void setArea(Area area){
        this.area = area;
    }
    public Area getArea(){
        //PENDIENTE
        return this.area;
    }
    public void setabr(String abr){
        this.abr = abr;
    }
    public String getabr(){
        return this.abr;
    }
    
    public void setnombre(String nombre){
        this.nombre = nombre;
    }
    public String getnombre(){
        return this.nombre;
    }
    
    public void setdescripcion(String descripcion){
        this.descripcion = descripcion;
    }
    
    public String getdescripcion(){
        return this.descripcion;
    }
    public List<SerieDocumental> getSeries(){
        return this.series_documentales;
    }
    public void setSeriesDocumentales(List<SerieDocumental> series){
        this.series_documentales = series;
    }
    public void setEst_reg(char est_reg) {
        this.est_reg = est_reg;
    }

    public char getEst_reg() {
        return est_reg;
    }
}
