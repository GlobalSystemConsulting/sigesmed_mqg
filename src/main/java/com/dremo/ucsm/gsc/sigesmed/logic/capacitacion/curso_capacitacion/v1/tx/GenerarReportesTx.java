package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.AsistenciaParticipanteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.CursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.EvaluacionDesarrolloDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.ParticipanteCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.SedeCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.TemarioCursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CapacitadorCursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.HorarioSedeCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.ParticipanteCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.SedeCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.GCell;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class GenerarReportesTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(GenerarReportesTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();
            Mitext report = null;

            switch (data.getString("tipCod")) {
                case "A":
                    switch (data.getString("modCod")) {
                        case "I":
                            report = createAttendacePersonal(data.getInt("capCod"), data.getInt("sedCod"), data.getInt("docCod"));
                            break;

                        case "G":
                            report = createAttendaceGroup(data.getInt("capCod"), data.getInt("sedCod"));
                            break;
                    }
                    break;

                case "R":
                    switch (data.getString("modCod")) {
                        case "I":
                            report = createOutputPersonal(data.getInt("capCod"), data.getInt("sedCod"), data.getInt("docCod"));
                            break;

                        case "G":
                            report = createOutputGroup(data.getInt("capCod"), data.getInt("sedCod"));
                            break;
                    }
                    break;
            }

            JSONObject response = new JSONObject();
            response.put("report", report.encodeToBase64());

            return WebResponse.crearWebResponseExito("El reporte fue generado correctamente", WebResponse.OK_RESPONSE).setData(response);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "generarReportes", e);
            return WebResponse.crearWebResponseError("No se pudo generar el reporte", WebResponse.BAD_RESPONSE);
        }
    }

    private Table createHeading(int capCod, int sedCod, boolean repTip, int docCod) {
        CursoCapacitacionDao cursoCapacitacionDao = (CursoCapacitacionDao) FactoryDao.buildDao("capacitacion.CursoCapacitacionDao");
        CursoCapacitacion course = cursoCapacitacionDao.buscarPorId(capCod);

        String capTip = course.getTip();
        String capNom = course.getNom();
        String orgOrg = course.getOrganizacion().getNom();
        String orgAut = course.getOrganizacionAut().getNom();

        SedeCapacitacionDao sedeCapacitacionDao = (SedeCapacitacionDao) FactoryDao.buildDao("capacitacion.SedeCapacitacionDao");
        SedeCapacitacion sede = sedeCapacitacionDao.buscarSedeReporte(sedCod);

        if (capTip.equals("Presencial")) {
            capNom += " " + sede.getPro() + " - " + sede.getDis() + " - " + sede.getLoc();
        }

        float[] colWidthsTitle = {5, 5, 5, 5};
        Table tabla = new Table(colWidthsTitle);
        tabla.setWidthPercent(100);

        tabla.addCell(new Cell()
                .setBorder(Border.NO_BORDER)
                .add(new Paragraph("Capacitación")
                        .setBold()
                        .setFontSize(11))
                .setTextAlignment(TextAlignment.LEFT));

        tabla.addCell(new Cell(1, 3)
                .setBorder(Border.NO_BORDER)
                .add(new Paragraph(" : " + capNom)
                        .setFontSize(10))
                .setTextAlignment(TextAlignment.LEFT));

        tabla.addCell(new Cell()
                .setBorder(Border.NO_BORDER)
                .add(new Paragraph("Institución Organizadora")
                        .setBold()
                        .setFontSize(11))
                .setTextAlignment(TextAlignment.LEFT));

        tabla.addCell(new Cell()
                .setBorder(Border.NO_BORDER)
                .add(new Paragraph(" : " + orgOrg)
                        .setFontSize(10))
                .setTextAlignment(TextAlignment.LEFT));

        tabla.addCell(new Cell()
                .setBorder(Border.NO_BORDER)
                .add(new Paragraph("Institución Autorizadora")
                        .setBold()
                        .setFontSize(11))
                .setTextAlignment(TextAlignment.LEFT));

        tabla.addCell(new Cell().setBorder(Border.NO_BORDER)
                .setBorder(Border.NO_BORDER)
                .add(new Paragraph(" : " + orgAut)
                        .setFontSize(10))
                .setTextAlignment(TextAlignment.LEFT));

        tabla.addCell(new Cell().setBorder(Border.NO_BORDER)
                .add(new Paragraph("Tipo de Capacitación")
                        .setBold()
                        .setFontSize(11))
                .setTextAlignment(TextAlignment.LEFT));

        tabla.addCell(new Cell()
                .setBorder(Border.NO_BORDER)
                .add(new Paragraph(" : " + capTip)
                        .setFontSize(10))
                .setTextAlignment(TextAlignment.LEFT));

        tabla.addCell(new Cell()
                .setBorder(Border.NO_BORDER)
                .add(new Paragraph("Horario(s)")
                        .setBold()
                        .setFontSize(11))
                .setTextAlignment(TextAlignment.LEFT));

        Cell c_2 = new Cell();
        c_2.setBorder(Border.NO_BORDER);
        c_2.setTextAlignment(TextAlignment.LEFT);

        for (HorarioSedeCapacitacion horario : sede.getHorarios()) {
            Paragraph p = new Paragraph(" : " + getDate(horario.getDia(), horario.getTurIni(), horario.getTurFin()));
            p.setFontSize(10);
            c_2.add(p);
        }

        tabla.addCell(c_2);

        tabla.addCell(new Cell().setBorder(Border.NO_BORDER)
                .add(new Paragraph("Capacitador(es)")
                        .setBold()
                        .setFontSize(11))
                .setTextAlignment(TextAlignment.LEFT));

        if(repTip) {
            Cell c_1 = new Cell(1,3);
            c_1.setBorder(Border.NO_BORDER);
            c_1.setTextAlignment(TextAlignment.LEFT);

            for (CapacitadorCursoCapacitacion capacitador : sede.getCapacitadoresCursoCapacitacion()) {
                Paragraph p = new Paragraph(" : " + capacitador.getPer().getApePat() + " " + capacitador.getPer().getApeMat() + " " + capacitador.getPer().getNom());
                p.setFontSize(10);
                c_1.add(p);
            }

            tabla.addCell(c_1);
            
            tabla.addCell(new Cell()
                .setBorder(Border.NO_BORDER)
                .add(new Paragraph("Participante")
                        .setBold()
                        .setFontSize(11))
                .setTextAlignment(TextAlignment.LEFT));

            ParticipanteCapacitacionDao participanteCapacitacionDao = (ParticipanteCapacitacionDao) FactoryDao.buildDao("capacitacion.ParticipanteCapacitacionDao");
            ParticipanteCapacitacion participante = participanteCapacitacionDao.buscarPorId(docCod, sedCod);
            Persona persona = participante.getPersona();
            
            tabla.addCell(new Cell(1, 3)
                .setBorder(Border.NO_BORDER)
                .add(new Paragraph(" : " + persona.getApePat() + " " + persona.getApeMat() + ", " + persona.getNom())
                        .setFontSize(10))
                .setTextAlignment(TextAlignment.LEFT));
        } else {
            Cell c_1 = new Cell();
            c_1.setBorder(Border.NO_BORDER);
            c_1.setTextAlignment(TextAlignment.LEFT);

            for (CapacitadorCursoCapacitacion capacitador : sede.getCapacitadoresCursoCapacitacion()) {
                Paragraph p = new Paragraph(" : " + capacitador.getPer().getApePat() + " " + capacitador.getPer().getApeMat() + " " + capacitador.getPer().getNom());
                p.setFontSize(10);
                c_1.add(p);
            }

            tabla.addCell(c_1);
        }
        
        return tabla;
    }

    private Mitext createAttendaceGroup(int capCod, int sedCod) {
        Mitext document = null;

        try {
            document = new Mitext(false);
            document.setStyle(1, 13, false, false, false);
            document.agregarParrafoMyEstilo("ASISTENCIA GRUPAL", 1);
            document.newLine(1);

            Table heading = createHeading(capCod, sedCod, false, 0);

            String headers[] = {"N°", "APELLIDOS Y NOMBRES", "A", "T", "F", "J", "P", "G"};
            float[] widths = {1, 12, 2, 2, 2, 2, 2, 2};

            GTabla tableAttendance = new GTabla(widths);
            tableAttendance.build(headers, 10);
            tableAttendance.setWidthPercent(100);

            GCell[] cell = {
                tableAttendance.createCellCenter(1, 1), tableAttendance.createCellLeft(1, 1),
                tableAttendance.createCellCenter(1, 1), tableAttendance.createCellCenter(1, 1),
                tableAttendance.createCellCenter(1, 1), tableAttendance.createCellCenter(1, 1),
                tableAttendance.createCellCenter(1, 1), tableAttendance.createCellCenter(1, 1)};

            ParticipanteCapacitacionDao participanteCapacitacionDao = (ParticipanteCapacitacionDao) FactoryDao.buildDao("capacitacion.ParticipanteCapacitacionDao");
            AsistenciaParticipanteDao asistenciaParticipanteDao = (AsistenciaParticipanteDao) FactoryDao.buildDao("capacitacion.AsistenciaParticipanteDao");
            List<ParticipanteCapacitacion> participantes = participanteCapacitacionDao.listarPorSede(sedCod);
            String row_data[];
            int i = 1;

            if (participantes.isEmpty()) {
                document.agregarParrafo("No existen participantes para listar");
            } else {
                for (ParticipanteCapacitacion participante : participantes) {
                    row_data = resetRowAttGro();
                    row_data[0] = String.valueOf(i);

                    Persona persona = participante.getPersona();
                    row_data[1] = persona.getApePat() + " " + persona.getApeMat() + ", " + persona.getNom();

                    Map<Character, Integer> attendance = asistenciaParticipanteDao.contarReporte(sedCod, persona.getPerId());
                    for (Character key : attendance.keySet()) {
                        switch (key) {
                            case 'A':
                                row_data[2] = String.valueOf(attendance.get(key));
                                break;
                            case 'T':
                                row_data[3] = String.valueOf(attendance.get(key));
                                break;
                            case 'F':
                                row_data[4] = String.valueOf(attendance.get(key));
                                break;
                            case 'J':
                                row_data[5] = String.valueOf(attendance.get(key));
                                break;
                            case 'P':
                                row_data[6] = String.valueOf(attendance.get(key));
                                break;
                            case 'G':
                                row_data[7] = String.valueOf(attendance.get(key));
                                break;
                        }
                    }

                    tableAttendance.processLineCell(row_data, cell, 9);
                    i++;
                }

                document.agregarTabla(heading);
                document.agregarParrafo("");
                document.agregarTabla(tableAttendance);
                document.agregarParrafo("");

                float[] colWidthsLegend = {1, 3, 1, 3, 1, 3};
                Table legend = new Table(colWidthsLegend);
                legend.setWidthPercent(100);

                legend.addCell(new Cell()
                        .setBorder(Border.NO_BORDER)
                        .add(new Paragraph("A")
                                .setFontSize(8)
                                .setBold())
                        .setTextAlignment(TextAlignment.CENTER));

                legend.addCell(new Cell()
                        .setBorder(Border.NO_BORDER)
                        .add(new Paragraph(" : Asistencia a Tiempo")
                                .setFontSize(7))
                        .setTextAlignment(TextAlignment.LEFT));

                legend.addCell(new Cell()
                        .setBorder(Border.NO_BORDER)
                        .add(new Paragraph("T")
                                .setFontSize(8)
                                .setBold())
                        .setTextAlignment(TextAlignment.CENTER));

                legend.addCell(new Cell()
                        .setBorder(Border.NO_BORDER)
                        .add(new Paragraph(" : Tarde")
                                .setFontSize(7))
                        .setTextAlignment(TextAlignment.LEFT));

                legend.addCell(new Cell()
                        .setBorder(Border.NO_BORDER)
                        .add(new Paragraph("F")
                                .setFontSize(8)
                                .setBold())
                        .setTextAlignment(TextAlignment.CENTER));

                legend.addCell(new Cell()
                        .setBorder(Border.NO_BORDER)
                        .add(new Paragraph(" : Falta")
                                .setFontSize(7))
                        .setTextAlignment(TextAlignment.LEFT));

                legend.addCell(new Cell()
                        .setBorder(Border.NO_BORDER)
                        .add(new Paragraph("J")
                                .setFontSize(8)
                                .setBold())
                        .setTextAlignment(TextAlignment.CENTER));

                legend.addCell(new Cell()
                        .setBorder(Border.NO_BORDER)
                        .add(new Paragraph(" : Falta Justificada")
                                .setFontSize(7))
                        .setTextAlignment(TextAlignment.LEFT));

                legend.addCell(new Cell()
                        .setBorder(Border.NO_BORDER)
                        .add(new Paragraph("P")
                                .setFontSize(8)
                                .setBold())
                        .setTextAlignment(TextAlignment.CENTER));

                legend.addCell(new Cell()
                        .setBorder(Border.NO_BORDER)
                        .add(new Paragraph(" : Falta Pendiente de Justificación")
                                .setFontSize(7))
                        .setTextAlignment(TextAlignment.LEFT));

                legend.addCell(new Cell()
                        .setBorder(Border.NO_BORDER)
                        .add(new Paragraph("G")
                                .setFontSize(8)
                                .setBold())
                        .setTextAlignment(TextAlignment.CENTER));

                legend.addCell(new Cell()
                        .setBorder(Border.NO_BORDER)
                        .add(new Paragraph(" : Asistencia Generada")
                                .setFontSize(7))
                        .setTextAlignment(TextAlignment.LEFT));

                document.agregarTabla(legend);
            }

            document.cerrarDocumento();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "generarReportes", e);
        }

        return document;
    }

    private Mitext createAttendacePersonal(int capCod, int sedCod, int docCod) {
        Mitext document = null;

        try {
            document = new Mitext(false);
            document.setStyle(1, 13, false, false, false);
            document.agregarParrafoMyEstilo("ASISTENCIA INDIVIDUAL", 1);
            document.newLine(1);

            Table heading = createHeading(capCod, sedCod, true, docCod);

            String headers[] = {"FECHA", "HORARIO", "ESTADO"};
            float[] widths = {5, 10, 3};
            
            GTabla tableAttendance = new GTabla(widths);
            tableAttendance.build(headers, 10);
            tableAttendance.setWidthPercent(100);

            GCell[] cell = {
                tableAttendance.createCellCenter(1, 1), tableAttendance.createCellLeft(1, 1),
                tableAttendance.createCellCenter(1, 1)};

            AsistenciaParticipanteDao asistenciaParticipanteDao = (AsistenciaParticipanteDao) FactoryDao.buildDao("capacitacion.AsistenciaParticipanteDao");
            List<Date> fechas = asistenciaParticipanteDao.obtenerFechas(sedCod, docCod);
            String row_data[];

            if (fechas.isEmpty()) {
                document.agregarParrafo("No existen fechas para listar");
            } else {
                for (Date fecha : fechas) {
                    row_data = resetRowAttPer(String.valueOf(fecha));

                    Map<String, Character> attendances = asistenciaParticipanteDao.obtenerRegistros(sedCod, docCod, fecha);

                    for(String key : attendances.keySet()) {
                        row_data[1] = key;
                        row_data[2] = String.valueOf(attendances.get(key));
                        tableAttendance.processLineCell(row_data, cell, 9);
                        resetRowAttPer(String.valueOf(fecha));
                    }

                }

                document.agregarTabla(heading);
                document.agregarParrafo("");
                document.agregarTabla(tableAttendance);
                document.agregarParrafo("");

                float[] colWidthsLegend = {1, 3, 1, 3, 1, 3};
                Table legend = new Table(colWidthsLegend);
                legend.setWidthPercent(100);

                legend.addCell(new Cell()
                        .setBorder(Border.NO_BORDER)
                        .add(new Paragraph("A")
                                .setFontSize(8)
                                .setBold())
                        .setTextAlignment(TextAlignment.CENTER));

                legend.addCell(new Cell()
                        .setBorder(Border.NO_BORDER)
                        .add(new Paragraph(" : Asistencia a Tiempo")
                                .setFontSize(7))
                        .setTextAlignment(TextAlignment.LEFT));

                legend.addCell(new Cell()
                        .setBorder(Border.NO_BORDER)
                        .add(new Paragraph("T")
                                .setFontSize(8)
                                .setBold())
                        .setTextAlignment(TextAlignment.CENTER));

                legend.addCell(new Cell()
                        .setBorder(Border.NO_BORDER)
                        .add(new Paragraph(" : Tarde")
                                .setFontSize(7))
                        .setTextAlignment(TextAlignment.LEFT));

                legend.addCell(new Cell()
                        .setBorder(Border.NO_BORDER)
                        .add(new Paragraph("F")
                                .setFontSize(8)
                                .setBold())
                        .setTextAlignment(TextAlignment.CENTER));

                legend.addCell(new Cell()
                        .setBorder(Border.NO_BORDER)
                        .add(new Paragraph(" : Falta")
                                .setFontSize(7))
                        .setTextAlignment(TextAlignment.LEFT));

                legend.addCell(new Cell()
                        .setBorder(Border.NO_BORDER)
                        .add(new Paragraph("J")
                                .setFontSize(8)
                                .setBold())
                        .setTextAlignment(TextAlignment.CENTER));

                legend.addCell(new Cell()
                        .setBorder(Border.NO_BORDER)
                        .add(new Paragraph(" : Falta Justificada")
                                .setFontSize(7))
                        .setTextAlignment(TextAlignment.LEFT));

                legend.addCell(new Cell()
                        .setBorder(Border.NO_BORDER)
                        .add(new Paragraph("P")
                                .setFontSize(8)
                                .setBold())
                        .setTextAlignment(TextAlignment.CENTER));

                legend.addCell(new Cell()
                        .setBorder(Border.NO_BORDER)
                        .add(new Paragraph(" : Falta Pendiente de Justificación")
                                .setFontSize(7))
                        .setTextAlignment(TextAlignment.LEFT));

                legend.addCell(new Cell()
                        .setBorder(Border.NO_BORDER)
                        .add(new Paragraph("G")
                                .setFontSize(8)
                                .setBold())
                        .setTextAlignment(TextAlignment.CENTER));

                legend.addCell(new Cell()
                        .setBorder(Border.NO_BORDER)
                        .add(new Paragraph(" : Asistencia Generada")
                                .setFontSize(7))
                        .setTextAlignment(TextAlignment.LEFT));

                document.agregarTabla(legend);
            }

            document.cerrarDocumento();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "generarReportes", e);
        }

        return document;
    }

    private Mitext createOutputGroup(int capCod, int sedCod) {
        Mitext document = null;

        try {
            document = new Mitext(false);
            document.setStyle(1, 13, false, false, false);
            document.agregarParrafoMyEstilo("RENDIMIENTO GRUPAL", 1);
            document.newLine(1);

            Table heading = createHeading(capCod, sedCod, false, 0);

            String headers[] = {"N°", "APELLIDOS Y NOMBRES", "Tareas", "Evaluaciones Continuas", "Promedio"};
            float[] widths = {1, 12, 3, 3, 3};

            GTabla tableOutput = new GTabla(widths);
            tableOutput.build(headers, 10);
            tableOutput.setWidthPercent(100);

            GCell[] cell = {
                tableOutput.createCellCenter(1, 1), tableOutput.createCellLeft(1, 1),
                tableOutput.createCellCenter(1, 1), tableOutput.createCellCenter(1, 1),
                tableOutput.createCellCenter(1, 1)};

            ParticipanteCapacitacionDao participanteCapacitacionDao = (ParticipanteCapacitacionDao) FactoryDao.buildDao("capacitacion.ParticipanteCapacitacionDao");
            EvaluacionDesarrolloDao evaluacionDesarrolloDao = (EvaluacionDesarrolloDao) FactoryDao.buildDao("capacitacion.EvaluacionDesarrolloDao");
            List<ParticipanteCapacitacion> participantes = participanteCapacitacionDao.listarPorSede(sedCod);
            String row_data[];
            int i = 1;
            DecimalFormat format = new DecimalFormat("#.00");
            
            if (participantes.isEmpty()) {
                document.agregarParrafo("No existen participantes para listar");
            } else {
                for (ParticipanteCapacitacion participante : participantes) {
                    row_data = resetRowOutGro();
                    row_data[0] = String.valueOf(i);

                    Persona persona = participante.getPersona();
                    row_data[1] = persona.getApePat() + " " + persona.getApeMat() + ", " + persona.getNom();

                    Map<Character, Double> attendance = evaluacionDesarrolloDao.contarReporte(sedCod, persona.getPerId());
                    double avg = 0;
                    for (Character key : attendance.keySet()) {
                        avg += attendance.get(key);
                        
                        switch (key) {
                            case 'T':
                                row_data[2] = format.format(attendance.get(key));                                
                                break;
                                
                            case 'E':
                                row_data[3] = format.format(attendance.get(key));
                                break;
                        }
                    }
                    
                    row_data[4] = format.format(avg/attendance.size());
                    tableOutput.processLineCell(row_data, cell, 9);
                    i++;
                }

                document.agregarTabla(heading);
                document.agregarParrafo("");
                document.agregarTabla(tableOutput);
                document.agregarParrafo("");
            }

            document.cerrarDocumento();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "generarReportes", e);
        }

        return document;
    }

    private Mitext createOutputPersonal(int capCod, int sedCod, int docCod) {
        Mitext document = null;

        try {
            document = new Mitext(false);
            document.setStyle(1, 13, false, false, false);
            document.agregarParrafoMyEstilo("RENDIMIENTO INDIVIDUAL", 1);
            document.newLine(1);

            Table heading = createHeading(capCod, sedCod, true, docCod);

            String headers[] = {"Tema", "Tipo", "Nombre", "Nota"};
            float[] widths = {5, 3, 4, 2};

            GTabla tableOutput = new GTabla(widths);
            tableOutput.build(headers, 10);
            tableOutput.setWidthPercent(100);

            GCell[] cell = {
                tableOutput.createCellLeft(1, 1), tableOutput.createCellCenter(1, 1), 
                tableOutput.createCellLeft(1, 1), tableOutput.createCellCenter(1, 1)};

            TemarioCursoCapacitacionDao temarioCursoCapacitacionDao = (TemarioCursoCapacitacionDao) FactoryDao.buildDao("capacitacion.TemarioCursoCapacitacionDao");
            EvaluacionDesarrolloDao evaluacionDesarrolloDao = (EvaluacionDesarrolloDao) FactoryDao.buildDao("capacitacion.EvaluacionDesarrolloDao");
            Map<Integer, String> topic=  temarioCursoCapacitacionDao.obtenerTemas(sedCod);
            String row_data[];
            DecimalFormat format = new DecimalFormat("#.00");
            
            if (topic.isEmpty()) {
                document.agregarParrafo("No existen temas para listar");
            } else {
                for(Integer key : topic.keySet()) {
                    row_data = resetRowOutPer("");
                    row_data[0] = String.valueOf(topic.get(key));
                    
                    List<Object []> rows = evaluacionDesarrolloDao.reporteParticipante(sedCod, docCod, key);
                    for(Object [] row: rows) {
                        row_data[1] = (row[2].toString().charAt(0) == 'E')?"Evaluación Continua":"Tarea";
                        row_data[2] = row[1].toString();
                        row_data[3] = format.format(Double.parseDouble(row[0].toString()));
                        
                        tableOutput.processLineCell(row_data, cell, 9);
                        resetRowOutPer(String.valueOf(topic.get(key)));
                    }
                }

                document.agregarTabla(heading);
                document.agregarParrafo("");
                document.agregarTabla(tableOutput);
                document.agregarParrafo("");
            }

            document.cerrarDocumento();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "generarReportes", e);
        }

        return document;
    }

    private String[] resetRowAttGro() {
        String row_data[] = {"", "", "0", "0", "0", "0", "0", "0"};
        return row_data;
    }
    
    private String[] resetRowAttPer(String value) {
        String row_data[] = new String[3];
        row_data[0] = value;
        row_data[1] = "";
        row_data[2] = "";
        
        return row_data;
    }
    
    private String[] resetRowOutGro() {
        String row_data[] = {"", "", "", "", ""};
        return row_data;
    }
    
    private String[] resetRowOutPer(String value) {
        String row_data[] = new String[4];
        row_data[0] = value;
        row_data[1] = "";
        row_data[2] = "";
        row_data[3] = "";
        
        return row_data;
    }

    private String getDate(int day, Date ini, Date fin) {
        String name = "";
        DateFormat format = new SimpleDateFormat("HH:mm");

        switch (day) {
            case 1:
                name = "Domingo " + format.format(ini) + " - " + format.format(fin);
                break;
            case 2:
                name = "Lunes " + format.format(ini) + " - " + format.format(fin);
                break;
            case 3:
                name = "Martes " + format.format(ini) + " - " + format.format(fin);
                break;
            case 4:
                name = "Miércoles " + format.format(ini) + " - " + format.format(fin);
                break;
            case 5:
                name = "Jueves " + format.format(ini) + " - " + format.format(fin);
                break;
            case 6:
                name = "Viernes " + format.format(ini) + " - " + format.format(fin);
                break;
            case 7:
                name = "Sábado " + format.format(ini) + " - " + format.format(fin);
                break;
        }

        return name;
    }
}
