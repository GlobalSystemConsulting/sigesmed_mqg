/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.mech;


import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.AreaCurricularHora;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.CicloEducativo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.DisenoCurricularMECH;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.JornadaEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.ModalidadEducacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Nivel;
import java.util.List;

/**
 *
 * @author abel
 */

public interface DisenoCurricularDao extends GenericDao<DisenoCurricularMECH>{
    
    public DisenoCurricularMECH buscarVigentePorOrganizacion(int organizacionID);
    public List<DisenoCurricularMECH> buscarConOrganizacion();
    
    public List<Nivel> buscarNiveles(int disenoCurricularID);
    public List<Grado> buscarGrados(int disenoCurricularID);
    public List<CicloEducativo> buscarCiclos(int disenoCurricularID);
    public List<ModalidadEducacion> buscarModalidades(int disenoCurricularID);
    public List<JornadaEscolar> buscarJornadaEscolar(int disenoCurricularID);
    public List<JornadaEscolar> listarJornadaEscolarConHoras();
    
    public void insertarNivel(Nivel nivel);
    public void insertarCiclo(CicloEducativo ciclo);
    public void insertarJornadaEscolar(JornadaEscolar jornada);
    public void insertarModalidad(ModalidadEducacion modalidad);
    public void insertarArea(AreaCurricular area);
    public void insertarGrado(Grado grado);
    public void insertarGradoArea(int gradoID, int areaID, int gradoPos, int areaPos);
    public List<Object[]> listarGradoAreas();
    
    public void eliminarGradoArea(int gradoID, int areaID);
    
    public void insertarAreaHora(AreaCurricularHora areaHora);
    public void eliminarAreaHora(AreaCurricularHora areaHora);
    
    public void eliminarArea(AreaCurricular area);
    public void eliminarGrado(Grado grado);
    public void eliminarCiclo(CicloEducativo ciclo);
    public void eliminarNivel(Nivel nivel);
}