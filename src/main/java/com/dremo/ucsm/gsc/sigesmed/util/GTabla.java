/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.util;

/**
 *
 * @author Administrador
 */

import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.layout.element.Table;
import java.util.List;

import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.layout.border.SolidBorder;
import com.itextpdf.layout.property.TextAlignment;
import java.io.IOException;

public class GTabla extends Table{
 
    private PdfFont helveticaBold = null;
    private PdfFont helvetica = null;

    public GTabla(float[] columnWidths) {
        super(columnWidths);
        this.setWidthPercent(100);
    }
    
//    public GTabla() throws IOException {
//        
//        helveticaBold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
////        table = new Table(columnasAncho);
////        for(String s:columnasTitulos){
////            table.addHeaderCell(new Cell().add(new Paragraph(s).setFont(helveticaBold)).setFontSize(9).setBorder(new SolidBorder(Color.BLACK, 0.5f)));
////        }
////        
////        for(T t:list){
////            processLine(t);
////        }
//        
//    }
    
    public void build(String[] columnasTitulos) throws IOException{
        
        helveticaBold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
        helvetica = PdfFontFactory.createFont(FontConstants.HELVETICA);
        //table = new Table(columnasAncho);
        for(String s:columnasTitulos){
            this.addHeaderCell(new Cell().setBackgroundColor(Color.LIGHT_GRAY).add(new Paragraph(s).setTextAlignment(TextAlignment.CENTER).setFont(helveticaBold)).setFontSize(9).setBorder(new SolidBorder(Color.BLACK, 0.5f)));
        }

    }  
    
    public void build(String[] columnasTitulos,Integer size) throws IOException{
        
        helveticaBold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
        helvetica = PdfFontFactory.createFont(FontConstants.HELVETICA);
        //table = new Table(columnasAncho);
        for(String s:columnasTitulos){
            this.addHeaderCell(new Cell().setBackgroundColor(Color.LIGHT_GRAY).add(new Paragraph(s).setTextAlignment(TextAlignment.CENTER).setFont(helveticaBold)).setFontSize(size).setBorder(new SolidBorder(Color.BLACK, 0.5f)));
        }

    }  
    
    public void build(){
        
    }
    
    public void processLine(String[] row){
        
        for(String s:row){
            this.addCell(new Cell().add(new Paragraph(s).setTextAlignment(TextAlignment.CENTER).setFont(helvetica)).setFontSize(9).setBorder(new SolidBorder(Color.BLACK, 0.5f)));
        }
        
    }
    public void addCell(String text, Color color){
        this.addCell(new Cell().add(new Paragraph(text).setTextAlignment(TextAlignment.CENTER).setFont(helvetica)).setFontSize(9).setBorder(new SolidBorder(Color.BLACK,0.5f)).setBackgroundColor(color));
    }
    public void build(String nombreColumna, Integer tamanioLetra, int rowspan, int colspan) throws IOException {
        this.helveticaBold = PdfFontFactory.createFont((String)"Helvetica-Bold");
        this.helvetica = PdfFontFactory.createFont((String)"Helvetica");
        this.addHeaderCell(new Cell(rowspan, colspan).setBackgroundColor(Color.LIGHT_GRAY).add(new Paragraph(nombreColumna)).setTextAlignment(TextAlignment.CENTER).setFont(helveticaBold).setFontSize(tamanioLetra).setBorder(new SolidBorder(Color.BLACK, 0.5f)));
    }
    public void processLineCell(String[] row,GCell[]cell){        
        for(int i=0;i<row.length;i++){  
            if(cell[i].isIsBold())
                this.addCell(new Cell(cell[i].getRowspan(),cell[i].getColspan()).add(new Paragraph(row[i]).setFont(helveticaBold)).setTextAlignment(cell[i].getTextAlignment()).setFontSize(9).setBorder(new SolidBorder(Color.BLACK, 0.5f)));               
            else
            {
                this.addCell(new Cell(cell[i].getRowspan(),cell[i].getColspan()).add(new Paragraph(row[i]).setFont(helvetica)).setTextAlignment(cell[i].getTextAlignment()).setFontSize(9).setBorder(new SolidBorder(Color.BLACK, 0.5f)));
            }               
        }      
    }
    public void processLineCell(String[] row,GCell[]cell,Integer tamanioLetra){        
        for(int i=0;i<row.length;i++){            
             this.addCell(new Cell(cell[i].getRowspan(),cell[i].getColspan()).add(new Paragraph(row[i]).setFont(helvetica)).setTextAlignment(cell[i].getTextAlignment()).setFontSize(tamanioLetra).setBorder(new SolidBorder(Color.BLACK, 0.5f)));               
        }      
    }
   
    
    public GCell createCellLeft(int rowspan ,int colspan){
        GCell cell = new GCell(rowspan, colspan,"left");
        cell.setTextAlignment(cell.getTextAlignment());
        
        return cell;
    }
    public GCell createCellCenter( int rowspan ,int colspan) {
        GCell cell = new GCell(rowspan, colspan);
        cell.setTextAlignment(cell.getTextAlignment());
        return cell;
    }
    public GCell createCellRight(int rowspan ,int colspan) {
        GCell cell = new GCell(rowspan, colspan,"right");
        cell.setTextAlignment(cell.getTextAlignment());
        return cell;
    }
    
     public GCell createCellLeft(int rowspan ,int colspan,boolean isBold){
        GCell cell = new GCell(rowspan, colspan,"left",isBold);
        cell.setTextAlignment(cell.getTextAlignment());
        
        return cell;
    }
    public GCell createCellCenter( int rowspan ,int colspan,boolean isBold) {
        GCell cell = new GCell(rowspan, colspan,isBold);
        cell.setTextAlignment(cell.getTextAlignment());
        return cell;
    }
    public GCell createCellRight(int rowspan ,int colspan,boolean isBold) {
        GCell cell = new GCell(rowspan, colspan,"right",isBold);
        cell.setTextAlignment(cell.getTextAlignment());
        return cell;
    }

}
