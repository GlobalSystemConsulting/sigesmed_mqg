package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrador on 10/10/2016.
 */
@Entity(name = "CarpetaDigitalMaesto")
@Table(name = "carpeta_pedagogica",schema = "pedagogico")
public class CarpetaPedagogica implements java.io.Serializable {
    @Id
    @Column(name="car_dig_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_carpeta_pedagogica", sequenceName="pedagogico.carpeta_pedagogica_car_dig_id_seq" )
    @GeneratedValue(generator="secuencia_carpeta_pedagogica")
    private int carDigId;

    @Column(name = "eta", nullable = false)
    private int eta;

    @Column(name = "act")
    private Boolean act;

    @Column(name = "des", length = 60)
    private String des;

    @Temporal(TemporalType.DATE)
    @Column(name="fec_cre", nullable = false)
    private Date fecCre;

    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;

    @Column(name = "est_reg",length = 1)
    private Character estReg;

    public CarpetaPedagogica() {
    }
    
    public CarpetaPedagogica(int carPedId) {
        this.carDigId = carPedId;
    }

    public CarpetaPedagogica(int eta, String des, Date fecCre) {
        this.eta = eta;
        this.des = des;
        this.fecCre = fecCre;


        this.fecMod = new Date();
        this.estReg = 'A';
    }
    public CarpetaPedagogica(int eta, String des, Date fecCre, Boolean act) {
        this.eta = eta;
        this.des = des;
        this.fecCre = fecCre;
        this.act = act;

        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public CarpetaPedagogica(Date fecCre) {
        this.fecCre = fecCre;

        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public int getCarDigId() {
        return carDigId;
    }

    public void setCarDigId(int carDigId) {
        this.carDigId = carDigId;
    }

    public Date getFecCre() {
        return fecCre;
    }

    public void setFecCre(Date fecCre) {
        this.fecCre = fecCre;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public int getEta() {
        return eta;
    }

    public void setEta(int eta) {
        this.eta = eta;
    }

    public Boolean getAct() {
        return act;
    }

    public void setAct(Boolean act) {
        this.act = act;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }
}
