/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import javax.persistence.CascadeType;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Administrador
 */
@Entity(name = "com.dremo.ucsm.gsm.sigesmed.core.entity.se.Parientes")
@Table(name = "parientes", schema="public")
@IdClass(ParientesPK.class)

public class Parientes implements Serializable {
    
    @Id
    @Column(name = "par_id")
    private Integer parId;
    
    @Id
    @Column(name = "per_id")
    private Integer perId;
    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "tpa_id", referencedColumnName = "tpa_id")
    private TipoPariente parentesco;
    
    @ManyToOne(cascade = CascadeType.ALL, fetch=FetchType.LAZY)    
    @JoinColumn(name = "per_id", insertable = false, updatable = false)    
    private Persona persona;
    
    @ManyToOne(cascade = CascadeType.ALL, fetch=FetchType.LAZY)    
    @JoinColumn(name = "par_id", insertable = false, updatable = false)
    private Persona pariente;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "est_reg")
    private String estReg = "A";

    public Parientes() {
    }
    
    public Parientes(Integer parId, Integer perId) {
        this.parId = parId;
        this.perId = perId;     
    }

    public Parientes(Integer parId, Integer perId, TipoPariente parentesco) {
        this.parId = parId;
        this.perId = perId;
        this.parentesco = parentesco;        
    }
    
    public Parientes(Persona pariente,Persona persona, TipoPariente parentesco) {
        this.pariente = pariente;
        this.persona = persona;
        this.parentesco = parentesco;        
    }
    
    public Parientes(Integer parId, Integer perId, TipoPariente parentesco, Integer usuMod, Date fecMod, String estReg) {
        this.parId = parId;
        this.perId = perId;
        this.parentesco = parentesco;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
        
    }

    public Persona getPariente() {
        return pariente;
    }

    public void setPariente(Persona pariente) {
        this.pariente = pariente;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public TipoPariente getParentesco() {
        return parentesco;
    }

    public void setParentesco(TipoPariente parentesco) {
        this.parentesco = parentesco;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public long getPerId() {
        return perId;
    }

    public void setPerId(Integer perId) {
        this.perId = perId;
    }

    public long getParId() {
        return parId;
    }

    public void setParId(Integer parId) {
        this.parId = parId;
    }

    @Override
    public String toString() {
        return "Parientes{" + "parId=" + parId + ", perId=" + perId + ", parentesco=" + parentesco + ", persona=" + persona + ", pariente=" + pariente + ", fecMod=" + fecMod + ", usuMod=" + usuMod + ", estReg=" + estReg + '}';
    }
    
    
        
}

