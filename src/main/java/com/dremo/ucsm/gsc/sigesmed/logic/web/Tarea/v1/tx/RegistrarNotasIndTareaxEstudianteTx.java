/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.NotaEvaluacionIndicadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.IndicadorAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.SesionAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.TareaEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.NotaEvaluacionIndicador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.GradoIEEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.TareaIndicador;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RegistrarNotasIndTareaxEstudianteTx implements ITransaction {

    private static Logger logger = Logger.getLogger(RegistrarNotasIndTareaxEstudianteTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        System.out.println(data);
        int idSession = data.optInt("sesId");
        char tipPer = data.getString("tipPer").charAt(0);
        int numPer = data.getInt("numPer");
        int idGra = data.getInt("graId");
        int idUsuDoc = data.getInt("usuId");
        int notNum = data.getInt("nota");
        String notLit = data.getString("notLit");
        int tarIdMM = data.getInt("tarIdMM");
        int idOrg = data.getInt("idOrg");
        char idSeccion = data.getString("idSeccion").charAt(0);
        int idAlumno = data.getInt("idAlumno");
        return registrarNotas(idSession, tipPer, numPer, idGra, idUsuDoc, notNum, notLit, tarIdMM, idOrg, idSeccion, idAlumno);
    }

    private WebResponse registrarNotas(int idSession, char tipPer, int numPer, int idGra,
            int idUsuDoc, int notNum, String notLit, int tarIdMM, int idOrg, char idSeccion, int idAlumno) {
        try {
            SesionAprendizajeDao sesionDao = (SesionAprendizajeDao) FactoryDao.buildDao("maestro.plan.SesionAprendizajeDao");
            NotaEvaluacionIndicadorDao notaDao = (NotaEvaluacionIndicadorDao) FactoryDao.buildDao("ma.NotaEvaluacionIndicadorDao");
            IndicadorAprendizajeDao indicadorDao = (IndicadorAprendizajeDao) FactoryDao.buildDao("maestro.plan.IndicadorAprendizajeDao");
            EstudianteDao estDao = (EstudianteDao) FactoryDao.buildDao("maestro.EstudianteDao");
            TareaEscolarDao tareaDao = (TareaEscolarDao) FactoryDao.buildDao("web.TareaEscolarDao");
            List<TareaIndicador> indicadores = tareaDao.obtenerIndicadoresTarea(tarIdMM);
            GradoIEEstudiante graOrgEst = tareaDao.obtenerGradoIEEstudiante(idOrg, idGra, idSeccion, idAlumno);
            String mensaje = "";
            for (int j = 0; j < indicadores.size(); j++) {
                int idInd = indicadores.get(j).getInd_apr_id();

                if (idSession == 0) {
                    List<NotaEvaluacionIndicador> nots = notaDao.buscarNotaIndicadoresEstudiante(idInd, graOrgEst.getGraOrgEstId(), tipPer, numPer);
                    for (NotaEvaluacionIndicador n : nots) {
                        n.setNot_lit(notLit);
                        n.setNotaEva(String.valueOf(notNum));
                        n.setFecMod(new Date());
                        notaDao.update(n);
                    }
                    mensaje = "Se actualizo correctamente el Registro de Evaluacion-Indicador";
                    break;
                } else {
                    if (!notaDao.buscarNota(idSession, idInd, graOrgEst.getGraOrgEstId(), tipPer, numPer)) {
                        //nuevo registro
                        NotaEvaluacionIndicador nei = new NotaEvaluacionIndicador(numPer, notLit, String.valueOf(notNum));
                        nei.setSesion(sesionDao.buscarSesionById(idSession));
                        nei.setPeriodo(notaDao.buscarPeriodoId(tipPer));
                        nei.setIndicador(indicadorDao.buscarPorId(idInd));
                        nei.setGradoEstudiante(estDao.buscarGradoIEEstudiante(graOrgEst.getGraOrgEstId()));
                        nei.setUsuMod(idUsuDoc);
                        notaDao.insert(nei);

                        mensaje = "Se registro correctamente";
                    } else {
                        NotaEvaluacionIndicador nei = notaDao.buscarNotaIndicadorEstudiante(idSession, idInd, graOrgEst.getGraOrgEstId(), tipPer, numPer);
                        nei.setNot_lit(notLit);
                        nei.setNotaEva(String.valueOf(notNum));
                        nei.setFecMod(new Date());
                        notaDao.update(nei);

                        mensaje = "Se actualizo correctamente";
                    }
                }
            }
            return WebResponse.crearWebResponseExito(mensaje);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "registrarNotasTareaxIndicador", e);
            return WebResponse.crearWebResponseError("No se pudo registrar las notas de los indicadores afectados por la tareas");
        }
    }
}
