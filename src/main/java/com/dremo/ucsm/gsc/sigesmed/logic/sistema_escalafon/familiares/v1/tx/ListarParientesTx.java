/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.familiares.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ParientesDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Parientes;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class ListarParientesTx implements ITransaction {
    private static  final Logger logger = Logger.getLogger(ListarParientesTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
        Integer traId = requestData.getInt("traId");
                
        List<Parientes> parientes = null;
        ParientesDao parientesDao = (ParientesDao)FactoryDao.buildDao("se.ParientesDao");
        
        try{
            parientes = parientesDao.listarxTrabajador(traId);
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar parientes",e);
            System.out.println("No se pudo listar los parientes\n"+e);
            return WebResponse.crearWebResponseError("No se pudo los parientes ", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
        JSONArray miArray = new JSONArray();
        for(Parientes p:parientes ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("parId", p.getPariente().getPerId());
            oResponse.put("parApePat", p.getPariente().getApePat()== null ? "" :  p.getPariente().getApePat());
            oResponse.put("parApeMat", p.getPariente().getApeMat()==null ? "": p.getPariente().getApeMat());
            oResponse.put("parNom", p.getPariente().getNom()==null ? "": p.getPariente().getNom());
            oResponse.put("tipoParienteId", p.getParentesco().getTpaId());
            oResponse.put("parentesco", p.getParentesco().getTpaDes());
            oResponse.put("parDni", p.getPariente().getDni());
            oResponse.put("parFecNac", p.getPariente().getFecNac()==null?"":sdi.format(p.getPariente().getFecNac()));
            oResponse.put("parFij", p.getPariente().getFij()==null?"":p.getPariente().getFij());
            oResponse.put("parNum1", p.getPariente().getNum1()==null?"":p.getPariente().getNum1());
            oResponse.put("parNum2", p.getPariente().getNum2()==null?"":p.getPariente().getNum2());
            oResponse.put("parEmail", p.getPariente().getEmail()==null?"":p.getPariente().getEmail());
            oResponse.put("parSex", p.getPariente().getSex());
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Los parientes fueron listados exitosamente", miArray);
    }
    
}
