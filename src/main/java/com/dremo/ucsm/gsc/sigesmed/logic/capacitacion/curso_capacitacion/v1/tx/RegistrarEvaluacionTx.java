package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.EvaluacionCursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.EvaluacionCursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.SedeCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;
import org.json.JSONArray;
import org.json.JSONObject;

public class RegistrarEvaluacionTx implements ITransaction {
    
    private static final Logger logger = Logger.getLogger(RegistrarEvaluacionTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            JSONObject data = (JSONObject) wr.getData();            
            JSONArray evaluaciones = data.getJSONArray("evaluaciones");
            
            EvaluacionCursoCapacitacionDao evaluacionCursoCapacitacionDao = (EvaluacionCursoCapacitacionDao) FactoryDao.buildDao("capacitacion.EvaluacionCursoCapacitacionDao");
            EvaluacionCursoCapacitacion evaOption = null;
                        
            for(int i = 0;i < evaluaciones.length();i++) {
                SedeCapacitacion sede = new SedeCapacitacion();
                sede.setSedCapId(evaluaciones.getJSONObject(i).getInt("sed"));
                Date fecIni = HelpTraining.getStartOfDay(DatatypeConverter.parseDateTime(evaluaciones.getJSONObject(i).getString("ini")).getTime());                
                Date fecFin = HelpTraining.getEndOfDay(DatatypeConverter.parseDateTime(evaluaciones.getJSONObject(i).getString("fin")).getTime());                        
                char tip = evaluaciones.getJSONObject(i).getString("tip").charAt(0);
                String nom = evaluaciones.getJSONObject(i).getString("nom");
                
                String obs = evaluaciones.getJSONObject(i).optString("obs", "");
                boolean ord = evaluaciones.getJSONObject(i).optBoolean("ord", false);
                int per = evaluaciones.getJSONObject(i).optInt("int", 1);
                int min = evaluaciones.getJSONObject(i).optInt("min",10);
                
                
                EvaluacionCursoCapacitacion evaluacion = new EvaluacionCursoCapacitacion(sede, fecIni, fecFin, tip, 'A', nom);
                evaluacion.setObs(obs);
                evaluacion.setOrdAle(ord);
                evaluacion.setNumInt(per);
                evaluacion.setTiempo_min(min);
                
                evaluacionCursoCapacitacionDao.insert(evaluacion);
                evaOption = evaluacion;
            }     
            
            JSONObject answer = new JSONObject();
            
            if(data.optString("tip","").equals("N")) {
                answer.put("id", evaOption.getEvaCurCapId());
                
                if (evaOption.getFecIni().before(new Date())) {
                    if (evaOption.getFecFin().before(new Date())) {
                        answer.put("est", "F");
                        evaOption.setEstReg('F');
                    } else {
                        answer.put("est", "I");
                        evaOption.setEstReg('I');
                    }
                    
                    evaluacionCursoCapacitacionDao.update(evaOption);
                } else
                    answer.put("est", "A");
            }
            
            return WebResponse.crearWebResponseExito("Las evaluaciones del curso de capacitación fueron creadas correctamente", WebResponse.OK_RESPONSE).setData(answer);
        } catch (Exception e){
            logger.log(Level.SEVERE,"registrarEvaluacion",e);
            return WebResponse.crearWebResponseError("No se pudo registrar las evaluaciones del curso de capacitación",WebResponse.BAD_RESPONSE);
        }
    }
}
