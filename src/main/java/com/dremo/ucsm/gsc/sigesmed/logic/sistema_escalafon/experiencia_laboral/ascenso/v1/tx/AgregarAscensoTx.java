/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.ascenso.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.AscensoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Ascenso;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class AgregarAscensoTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(AgregarAscensoTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
         /*
        *   Parte para la lectura, verificacion y validacion de datos
         */
        Ascenso ascenso = null;
        
        Integer ficEscId = 0;

        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");

        try {
            JSONObject requestData = (JSONObject) wr.getData();

            ficEscId = requestData.getInt("ficEscId");
            String numRes = requestData.getString("numRes");
            Date fecRes = requestData.getString("fecRes").equals("")?null:sdi.parse(requestData.getString("fecRes").substring(0, 10));
            Date fecEfe = requestData.getString("fecEfe").equals("")?null:sdi.parse(requestData.getString("fecEfe").substring(0, 10));
            String esc = requestData.getString("esc");
            
            ascenso = new Ascenso(new FichaEscalafonaria(ficEscId), numRes, fecRes, fecEfe, esc, wr.getIdUsuario(), new Date(), 'A');
            System.out.println(ascenso);
            

        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"Datos nuevo ascenso",e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        //Fin

        /*
        *  Parte para la operacion en la Base de Datos
         */
        //si el pariente no existe en la tabla persona
        AscensoDao ascensoDao = (AscensoDao) FactoryDao.buildDao("se.AscensoDao");
        try {
            ascensoDao.insert(ascenso);
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar nuevo ascenso",e);
            System.out.println(e);
        }
        
        //Fin       

        /*
        *  Repuesta Correcta
         */
        JSONObject oResponse = new JSONObject();
        oResponse.put("ascId", ascenso.getAscId());
        oResponse.put("numRes", ascenso.getNumRes()==null?"":ascenso.getNumRes());
        oResponse.put("fecRes", ascenso.getFecRes()==null?"":sdi.format(ascenso.getFecRes()));
        oResponse.put("fecEfe", ascenso.getFecEfe()==null?"":sdi.format(ascenso.getFecEfe()));
        oResponse.put("esc", ascenso.getEsc()==null?"":ascenso.getEsc());
        
        System.out.println("Ascenso: " + oResponse.toString());
        
        return WebResponse.crearWebResponseExito("El registro del ascenso se realizo correctamente", oResponse);
        //Fin
        
    }
    
}
