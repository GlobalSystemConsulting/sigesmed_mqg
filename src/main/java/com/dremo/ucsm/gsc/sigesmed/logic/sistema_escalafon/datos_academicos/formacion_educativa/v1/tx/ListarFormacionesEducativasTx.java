/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.formacion_educativa.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FormacionEducativaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FormacionEducativa;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class ListarFormacionesEducativasTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ListarFormacionesEducativasTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
        Integer ficEscId = requestData.getInt("ficEscId");
                
        List<FormacionEducativa> forEdu = null;
        FormacionEducativaDao forEduDao = (FormacionEducativaDao)FactoryDao.buildDao("se.FormacionEducativaDao");
        
        try{
            forEdu = forEduDao.listarxFichaEscalafonaria(ficEscId);
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar formaciones educativas",e);
            System.out.println("No se pudo listar las formaiones educativas\n"+e);
            return WebResponse.crearWebResponseError("No se pudo listar las formaciones educativas", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
        JSONArray miArray = new JSONArray();
        for(FormacionEducativa fe:forEdu ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("forEduId", fe.getForEduId());
            oResponse.put("tip", fe.getTipFor()==null?"":fe.getTipFor());
            oResponse.put("niv", fe.getNivAca()==null?"":fe.getNivAca());
            oResponse.put("numTit", fe.getNumTit()==null?"":fe.getNumTit());
            oResponse.put("esp", fe.getEspAca()==null?"":fe.getEspAca());
            oResponse.put("estCon", fe.getEstCon()==null?"":fe.getEstCon());
            oResponse.put("fecExp", fe.getFecExp()==null?"":sdi.format(fe.getFecExp()));
            oResponse.put("cenEst", fe.getCenEst()==null?"":fe.getCenEst());
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Las formaciones educativas fueron listadas exitosamente", miArray);
    }
    
}
