package com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.HorarioSedeCapacitacion;
import java.util.Date;
import java.util.List;

public interface HorarioSedeCapacitacionDao extends GenericDao<HorarioSedeCapacitacion> {
    HorarioSedeCapacitacion buscarPorId(int horCod);
    List<HorarioSedeCapacitacion> buscarPorSede(int sedCod);
    List<HorarioSedeCapacitacion> buscarParaOnTime(int sedCod, int dia, Date tiempo);
}
