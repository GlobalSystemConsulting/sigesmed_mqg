package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi;

import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.EstudianteMMI;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

public class EstudianteMMIDaoHibernate extends GenericMMIDaoHibernate<EstudianteMMI> {

    public EstudianteMMI find4CodPer(long perId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        EstudianteMMI estudianteMMI = null;
        String hql;
        Query query;
        try {
            hql = "FROM EstudianteMMI es WHERE es.estReg != 'E' and es.perId= :hqlPerId";
            query = session.createQuery(hql);
            query.setLong("hqlPerId", perId);
            query.setMaxResults(1);
            estudianteMMI = (EstudianteMMI) query.uniqueResult();
        } catch (Exception ex) {
            System.out.println("No se encontro al Alumno con el perId " + perId);
            throw ex;
        } finally {
            session.close();
        }
        return estudianteMMI;
    }
    
    public EstudianteMMI find4Dni(String dni) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        EstudianteMMI estudianteMMI = null;
        String hql;
        Query query;
        try {
            hql = "FROM EstudianteMMI es WHERE es.estReg != 'E' and es.persona.dni= :hdlDni";
            query = session.createQuery(hql);
            query.setString("hdlDni", dni);
            query.setMaxResults(1);
            estudianteMMI = (EstudianteMMI) query.uniqueResult();
        } catch (Exception ex) {
            System.out.println("No se encontro al Alumno con el DNI: " + dni);
            throw ex;
        } finally {
            session.close();
        }
        return estudianteMMI;
    }
    
    public EstudianteMMI find4PerId(String perId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        EstudianteMMI estudianteMMI = null;
        String hql;
        Query query;
        try {
            hql = "FROM EstudianteMMI es WHERE es.estReg != 'E' and es.persona.perId= :hqlPerId";
            query = session.createQuery(hql);
            query.setLong("hqlPerId", Long.parseLong(perId));
            query.setMaxResults(1);
            estudianteMMI = (EstudianteMMI) query.uniqueResult();
        } catch (Exception ex) {
            System.out.println("No se encontro al Alumno con el Codigo: " + perId);
            throw ex;
        } finally {
            session.close();
        }
        return estudianteMMI;
    }

    public EstudianteMMI find4CodEst(String codEst) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        EstudianteMMI estudianteMMI = null;
        String hql;
        Query query;
        try {
            hql = "FROM EstudianteMMI es WHERE es.estReg != 'E' and es.codEst= :hdlCodEst";
            query = session.createQuery(hql);
            query.setString("hdlCodEst", codEst);
            query.setMaxResults(1);
            estudianteMMI = (EstudianteMMI) query.uniqueResult();
        } catch (Exception ex) {
            System.out.println("No se encontro al Alumno con el Codigo de Estudiante: " + codEst);
            throw ex;
        } finally {
            session.close();
        }
        return estudianteMMI;
    }

    public List<EstudianteMMI> findAll4CodEst(String codEst) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<EstudianteMMI> estudiantes;
        String hql;
        Query query;
        try {
            hql = "FROM EstudianteMMI es WHERE es.estReg != 'E' and es.codEst LIKE :hqlCodEst";
            query = session.createQuery(hql);
            query.setString("hqlCodEst", "%" + codEst + "%");
            estudiantes = query.list();
        } catch (Exception ex) {
            System.out.println("Error al buscar estudiantes por Codigo de estudiante " + codEst);
            throw ex;
        } finally {
            session.close();
        }
        return estudiantes;
    }

    public List<EstudianteMMI> findAll4Dni(String dni) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<EstudianteMMI> estudiantes;
        String hql;
        Query query;
        try {
            hql = "FROM EstudianteMMI es WHERE es.estReg != 'E' and es.persona.dni LIKE :hqlDni";
            query = session.createQuery(hql);
            query.setString("hqlDni", "%" + dni + "%");
            estudiantes = query.list();
        } catch (Exception ex) {
            System.out.println("Error al buscar estudiantes por DNI" + dni);
            throw ex;
        } finally {
            session.close();
        }
        return estudiantes;
    }

    public List<EstudianteMMI> findAll4NomCom(String nombres) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<EstudianteMMI> estudiantes;
        String hql;
        Query query;
        try {
            hql = "FROM EstudianteMMI es "
                    + "WHERE es.estReg != 'E' and "
                    + "es.persona.nom LIKE :hqlNombres";
            query = session.createQuery(hql);
            query.setString("hqlNombres", "%" + nombres + "%");
            estudiantes = query.list();
        } catch (Exception ex) {
            System.out.println("Error al buscar estudiantes por Nombres" + nombres);
            throw ex;
        } finally {
            session.close();
        }
        return estudiantes;
    }

    public List<EstudianteMMI> findAll4ApePat(String apePat) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<EstudianteMMI> estudiantes;
        String hql;
        Query query;
        try {
            hql = "FROM EstudianteMMI es "
                    + "WHERE es.estReg != 'E' and "
                    + "es.persona.apePat LIKE :hqlApePat";
            query = session.createQuery(hql);
            query.setString("hqlApePat", "%" + apePat + "%");
            estudiantes = query.list();
        } catch (Exception ex) {
            System.out.println("Error al buscar estudiantes por Apellido Paterno" + apePat);
            throw ex;
        } finally {
            session.close();
        }
        return estudiantes;
    }

    public List<EstudianteMMI> findAll4ApeMat(String apeMat) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<EstudianteMMI> estudiantes;
        String hql;
        Query query;
        try {
            hql = "FROM EstudianteMMI es "
                    + "WHERE es.estReg != 'E' and "
                    + "es.persona.apeMat LIKE :hqlApeMat";
            query = session.createQuery(hql);
            query.setString("hqlApeMat", "%" + apeMat + "%");
            estudiantes = query.list();
        } catch (Exception ex) {
            System.out.println("Error al buscar estudiantes por Apellido Materno" + apeMat);
            throw ex;
        } finally {
            session.close();
        }
        return estudiantes;
    }

    public List<EstudianteMMI> findEstudiantes(String gradoBusqueda, String seccionBusqueda, String orgBusqueda, String orgUsuario) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<EstudianteMMI> estudiantes;
        String hql;
        Query query;
        try {
            hql = "FROM EstudianteMMI es "
                    + "WHERE es.estReg != 'E' AND "
                    + "es.ultGraCul = :hqlUltGraCul ";

            if (!seccionBusqueda.equals("*")) {
                hql = hql + " AND es.ultSec = '" + seccionBusqueda + "' ";
            }

            if (orgBusqueda.equals("2")) {
                hql = hql + " AND es.orgId = " + orgUsuario;
            }

            query = session.createQuery(hql);
            query.setString("hqlUltGraCul", gradoBusqueda);
            estudiantes = query.list();

        } catch (Exception ex) {
            throw ex;
        } finally {
            session.close();
        }
        return estudiantes;
    }
    public List<EstudianteMMI> findAllEstudiantesOrg(String orgUsuario) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<EstudianteMMI> estudiantes;
        String hql;
        Query query;
        try {
            hql = "FROM EstudianteMMI es "
                    + "WHERE es.estReg != 'E' AND "
                    + "es.orgId = " + orgUsuario;

           

            query = session.createQuery(hql);
            
            estudiantes = query.list();

        } catch (Exception ex) {
            throw ex;
        } finally {
            session.close();
        }
        return estudiantes;
    }
}
