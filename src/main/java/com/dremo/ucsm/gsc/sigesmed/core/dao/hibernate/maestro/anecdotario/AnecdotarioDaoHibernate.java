package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.maestro.anecdotario;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.anecdotario.AnecdotarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Estudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Grado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Matricula;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.anecdotario.AccionesCorrectivas;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.anecdotario.Anecdotario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.SesionAprendizaje;
import org.hibernate.*;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;

import java.util.List;

/**
 * Created by Administrador on 19/12/2016.
 */
public class AnecdotarioDaoHibernate extends GenericDaoHibernate<Anecdotario> implements AnecdotarioDao{
    @Override
    public Anecdotario buscarAnecdota(int idAnecdota) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Anecdotario anecdota = (Anecdotario)session.get(Anecdotario.class,idAnecdota);
            return anecdota;
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public Anecdotario buscarAnecdotaConAcciones(int idAnecdota) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(Anecdotario.class)
                    .setFetchMode("accionesCorrectivas",FetchMode.JOIN)
                    .add(Restrictions.eq("aneId",idAnecdota));

            return (Anecdotario)query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public Estudiante buscarEstudiante(int idEstu) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Estudiante estudiante = (Estudiante) session.get(Estudiante.class,idEstu);
            return estudiante;
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<Grado> listarGradosDocente(int org, int doc) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String sql = "SELECT ni.niv_id,ni.nom as nivnom,gr.gra_id,gr.abr,gr.nom,gr.des,sec.sec_id FROM institucional.distribucion_hora_grado dis\n" +
                    "  INNER JOIN grado gr ON gr.gra_id = dis.gra_id\n" +
                    "  INNER JOIN nivel ni ON ni.niv_id = gr.niv_id" +
                    "  INNER JOIN institucional.seccion sec ON sec.sec_id = dis.sec_id\n" +
                    "  INNER JOIN institucional.plaza_magisterial plz ON plz.pla_mag_id = dis.pla_mag_id\n" +
                    "  INNER JOIN institucional.plan_estudios plan ON plan.pla_est_id = dis.pla_est_id\n" +
                    "  WHERE plz.doc_id =:docId AND plan.org_id =:orgId ORDER BY gr.nom ASC ;";
            SQLQuery query = session.createSQLQuery(sql);
            query.setInteger("docId",doc);
            query.setInteger("orgId",org);
            //query.addEntity(Grado.class);
            query.setResultTransformer(Transformers.aliasToBean(Grado.class));
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<Grado> listarGradosDocente(int org, int idPlan, int doc) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String sql = "SELECT ni.niv_id,ni.nom as nivnom,gr.gra_id,gr.abr,gr.nom,gr.des,sec.sec_id FROM institucional.distribucion_hora_grado dis\n" +
                    "  INNER JOIN grado gr ON gr.gra_id = dis.gra_id\n" +
                    "  INNER JOIN nivel ni ON ni.niv_id = gr.niv_id" +
                    "  INNER JOIN institucional.seccion sec ON sec.sec_id = dis.sec_id\n" +
                    "  INNER JOIN institucional.plaza_magisterial plz ON plz.pla_mag_id = dis.pla_mag_id\n" +
                    "  WHERE plz.doc_id =:docId AND dis.pla_est_id =:planId ORDER BY gr.nom ASC ;";
            SQLQuery query = session.createSQLQuery(sql);
            query.setInteger("docId",doc);
            query.setInteger("planId",idPlan);
            //query.addEntity(Grado.class);
            query.setResultTransformer(Transformers.aliasToBean(Grado.class));
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado buscarGrado(int idGrado) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado grado = (com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado) session.get(com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado.class,idGrado);
            return grado;
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<Estudiante> listarEstudiantes(int idOrg,int idGrad,Character idSecc) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            /*String hql = "SELECT e FROM EstudianteMaestro e" +
                    " INNER JOIN e.persona p INNER JOIN  e.matriculas m INNER JOIN m.ie org" +
                    " INNER JOIN m.gradosMatricula graMat INNER JOIN graMat.grado gra INNER JOIN graMat.seccion secc" +
                    " WHERE org.orgId =:orgId AND m.act=:mAct AND graMat.act =:graMatAct AND gra.graId =:graId AND secc.sedId =:idSecc ORDER BY p.apePat ASC";*/
            String hql = "SELECT e FROM EstudianteMaestro e" +
                    " INNER JOIN e.persona p INNER JOIN  e.matriculas m" +
                    " INNER JOIN m.gradosMatricula graMat" +
                    " WHERE m.ie.orgId =:orgId AND m.act=:mAct AND graMat.act =:graMatAct AND graMat.grado.graId =:graId AND graMat.seccion.sedId =:idSecc ORDER BY p.apePat ASC";
            Query query = session.createQuery(hql);
            query.setInteger("orgId",idOrg);
            query.setBoolean("mAct",true);
            query.setBoolean("graMatAct",true);
            query.setInteger("graId", idGrad);
            query.setCharacter("idSecc",idSecc);
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<Anecdotario> listarAnecdotasEstudiante(int idEstudiante,int idDocente) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(Anecdotario.class)
                    .add(Restrictions.eq("estReg", 'A'));
            query.createCriteria("estudiante").add(Restrictions.eq("per_id",idEstudiante));
            query.createCriteria("docente").add(Restrictions.eq("doc_id",idDocente));
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<AccionesCorrectivas> listarAccionesAnecdotario(int idAnecdota) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(AccionesCorrectivas.class)
                    .addOrder(Order.asc("aneAcCorId"))
                    .createCriteria("anecdotario").add(Restrictions.eq("aneId", idAnecdota));
            return query.list();

        }catch (Exception e){
            throw  e;
        }finally {
            session.close();
        }
    }

    @Override
    public void eliminarAccionesCorrectivas(int idAnec) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            String hql = "DELETE AccionesCorrectivas WHERE anecdotario.aneId =:idAnec";
            Query query = session.createQuery(hql);
            query.setInteger("idAnec",idAnec);
            query.executeUpdate();
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            throw  e;
        }finally {
            session.close();
        }
    }

    @Override
    public Matricula buscarMatriculaActual(int idEst, int idOrg) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(Matricula.class)
                    .add(Restrictions.eq("estudiante.per_id",idEst))
                    .add(Restrictions.eq("ie.orgId",idOrg))
                    .add(Restrictions.eq("act",true))
                    .setMaxResults(1);

            return (Matricula) criteria.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
}
