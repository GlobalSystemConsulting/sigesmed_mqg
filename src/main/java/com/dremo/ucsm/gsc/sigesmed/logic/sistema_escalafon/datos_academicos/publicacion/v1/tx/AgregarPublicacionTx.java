/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.publicacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.PublicacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Publicacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class AgregarPublicacionTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(AgregarPublicacionTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
         /*
        *   Parte para la lectura, verificacion y validacion de datos
         */
        Publicacion publicacion = null;
        
        Integer ficEscId = 0;

        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");

        try {
            JSONObject requestData = (JSONObject) wr.getData();

            ficEscId = requestData.getInt("ficEscId");
            String nomEdi = requestData.getString("nomEdi");
            String tipPub = requestData.getString("tipPub");
            String titPub = requestData.getString("titPub");
            String graPar = requestData.getString("graPar");
            String lug = requestData.getString("lug");
            Date fecPub = requestData.getString("fecPub").equals("")?null:sdi.parse(requestData.getString("fecPub").substring(0, 10));
            String numReg = requestData.getString("numReg");
            
            publicacion = new Publicacion(new FichaEscalafonaria(ficEscId), nomEdi, tipPub, titPub, graPar, lug, fecPub, numReg, wr.getIdUsuario(), new Date(), 'A');

        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"Datos nueva exposicion",e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        //Fin

        /*
        *  Parte para la operacion en la Base de Datos
         */
        //si el pariente no existe en la tabla persona
        PublicacionDao publicacionDao = (PublicacionDao) FactoryDao.buildDao("se.PublicacionDao");
        try {
            publicacionDao.insert(publicacion);
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar nueva publicacion",e);
            System.out.println(e);
        }
        
        //Fin       

        /*
        *  Repuesta Correcta
         */
        JSONObject oResponse = new JSONObject();
        oResponse.put("pubId", publicacion.getPubId());
        oResponse.put("nomEdi", publicacion.getNomEdi() == null ? "" : publicacion.getNomEdi());
        oResponse.put("tipPub", publicacion.getTipPub() == null ? "" : publicacion.getTipPub());
        oResponse.put("titPub", publicacion.getTitPub() == null ? "" : publicacion.getTitPub());
        oResponse.put("graPar", publicacion.getGraPar() == null ? "" : publicacion.getGraPar());
        oResponse.put("lug", publicacion.getLug() == null ? "" : publicacion.getLug());
        oResponse.put("fecPub", publicacion.getFecPub() == null ? "" : sdi.format(publicacion.getFecPub()));
        oResponse.put("numReg", publicacion.getNumReg() == null ? "" : publicacion.getNumReg());
                
        return WebResponse.crearWebResponseExito("El registro de la publicacion se realizo correctamente", oResponse);
        //Fin
    }
    
}
