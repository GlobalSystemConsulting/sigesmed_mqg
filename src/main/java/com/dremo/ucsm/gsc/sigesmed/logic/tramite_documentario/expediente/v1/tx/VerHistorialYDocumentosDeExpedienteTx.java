/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.ExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.HistorialExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.DocumentoExpediente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.HistorialExpediente;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;

/**
 *
 * @author abel
 */
public class VerHistorialYDocumentosDeExpedienteTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int expedienteID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            expedienteID = requestData.getInt("expedienteID");
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar el historial y documentos del expediente, datos incorrectos", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<DocumentoExpediente> documentos = null;
        List<HistorialExpediente> historial = null;
        ExpedienteDao expedienteDao = (ExpedienteDao)FactoryDao.buildDao("std.ExpedienteDao");
        HistorialExpedienteDao historialDao = (HistorialExpedienteDao)FactoryDao.buildDao("std.HistorialExpedienteDao");
        try{
            documentos =expedienteDao.buscarDocumentosPorExpediente(expedienteID);
            historial =historialDao.buscarPorExpediente(expedienteID);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar el historial y documentos del expediente", e.getMessage() );
        }
        //Fin
        JSONObject oResponse = new JSONObject();
        
        JSONArray aDocumentos = new JSONArray();
        for(DocumentoExpediente d: documentos){
            JSONObject oDocumento = new JSONObject();
            oDocumento.put("documentoID",d.getDocExpId());
            oDocumento.put("observacion",d.getDes() );
            oDocumento.put("nombreArchivo",d.getArcAdj());
            if(d.getHistorialId()!=null)
                oDocumento.put("url",Sigesmed.UBI_ARCHIVOS+"/expediente/salientes/");
            else
                oDocumento.put("url",Sigesmed.UBI_ARCHIVOS+"/expediente/");
            
            oDocumento.put("tipoDocumentoID",d.getTipoDocumento().getTipDocId());
            oDocumento.put("tipoDocumento",d.getTipoDocumento().getNom());
            
            oDocumento.put("historialID",d.getHistorialId());
            
            aDocumentos.put(oDocumento);
        }
        oResponse.put("documentos",aDocumentos);
        JSONArray aHistoriales = new JSONArray();
        for(HistorialExpediente h: historial){
            JSONObject oHistorial = new JSONObject();
            oHistorial.put("historialID",h.getHisExpId());
            oHistorial.put("observacion",h.getObservacion() );
            oHistorial.put("areaID",h.getAreaId());
            oHistorial.put("area",h.getArea().getNom());
            
            oHistorial.put("estadoID",h.getEstado().getEstExpId());
            oHistorial.put("estado",h.getEstado().getNom());
            
            oHistorial.put("responsableID",h.getResId());
            oHistorial.put("responsable",h.getResponsable().getNombrePersona());
            aHistoriales.put(oHistorial);
        }
        oResponse.put("historial",aHistoriales);
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",oResponse);
    }
    
}

