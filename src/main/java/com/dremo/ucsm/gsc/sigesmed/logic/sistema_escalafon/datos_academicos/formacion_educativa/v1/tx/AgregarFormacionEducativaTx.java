/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.formacion_educativa.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FormacionEducativaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FormacionEducativa;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class AgregarFormacionEducativaTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(AgregarFormacionEducativaTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
         /*
        *   Parte para la lectura, verificacion y validacion de datos
         */
        FormacionEducativa forEdu = null;
        
        Integer ficEscId = 0;

        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");

        try {
            JSONObject requestData = (JSONObject) wr.getData();

            ficEscId = requestData.getInt("ficEscId");
            Character tip = requestData.getString("tip").charAt(0);
            String niv = requestData.getString("niv");
            String numTit = requestData.getString("numTit");
            String esp = requestData.getString("esp");
            Boolean estCon = requestData.getBoolean("estCon");
            Date fecExp = requestData.getString("fecExp").equals("")?null:sdi.parse(requestData.getString("fecExp").substring(0, 10));
            String cenEst = requestData.getString("cenEst");
            
            forEdu = new FormacionEducativa(new FichaEscalafonaria(ficEscId), tip, niv, numTit, esp, estCon , fecExp, cenEst, wr.getIdUsuario(), new Date(), 'A');

        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"Datos nueva formacion educativa",e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        //Fin

        /*
        *  Parte para la operacion en la Base de Datos
         */
        //si el pariente no existe en la tabla persona
        FormacionEducativaDao forEduDao = (FormacionEducativaDao) FactoryDao.buildDao("se.FormacionEducativaDao");
        try {
            forEduDao.insert(forEdu);
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar nueva formacion educativa",e);
            System.out.println(e);
        }
        
        //Fin       

        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("forEduId", forEdu.getForEduId());
        oResponse.put("tip", forEdu.getTipFor());
        oResponse.put("niv", forEdu.getNivAca());
        oResponse.put("numTit", forEdu.getNumTit()==null?"":forEdu.getNumTit());
        oResponse.put("esp", forEdu.getEspAca()==null?"":forEdu.getEspAca());
        oResponse.put("estCon", forEdu.getEstCon()==null?"":forEdu.getEstCon());
        oResponse.put("fecExp", forEdu.getFecExp()==null?"":sdi.format(forEdu.getFecExp()));
        oResponse.put("cenEst", forEdu.getCenEst()==null?"":forEdu.getCenEst());
                
        return WebResponse.crearWebResponseExito("El registro de la formacion educativa se realizo correctamente", oResponse);
        //Fin
        
    }
    
    
}
