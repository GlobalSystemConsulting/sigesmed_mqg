/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.di;

import com.dremo.ucsm.gsc.sigesmed.core.dao.di.ParientesDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Parientes;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Administrador
 */
public class ParientesDaoHibernate extends GenericDaoHibernate<Parientes> implements ParientesDao{
    
    @Override
    public String buscarUltimoCodigo() {
        String codigo = null;
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        try{
//            //listar ultimo codigo de tramite
//            String hql = "SELECT a.apoId FROM Apoderado a ORDER BY a.apoId DESC ";
//            Query query = session.createQuery(hql);
//            query.setMaxResults(1);
//            codigo = ((String)query.uniqueResult());
//            
//            //solo cuando no hay ningun registro aun
//            if(codigo==null)
//                codigo = "0000";
//        
//        }catch(Exception e){
//            System.out.println("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());
//            throw new UnsupportedOperationException("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());            
//        }
//        finally{
//            session.close();
//        }
        return codigo;
    }
    
    @Override
    public List<Parientes> listarParientesxTrabajador(int traId){
        
        List<Parientes> parientes = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT pa FROM com.dremo.ucsm.gsc.sigesmed.core.entity.di.Trabajador as t, Parientes as pa "
                    + "join fetch pa.pariente "
                    + "join fetch pa.parentesco "
                    + "WHERE t.traId=:p1 AND pa.estReg = 'A' and pa.persona = t.persona";
            
            Query query = session.createQuery(hql);
            query.setParameter("p1", traId);
            parientes = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo Listar el Directorio Interno \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar el Directorio Interno \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return parientes;
    }
    
}
