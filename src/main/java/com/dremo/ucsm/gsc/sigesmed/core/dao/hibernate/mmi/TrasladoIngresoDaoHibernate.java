package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi;

import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.TrasladoIngreso;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class TrasladoIngresoDaoHibernate extends GenericMMIDaoHibernate<TrasladoIngreso> {

    //Suponiendo que un estudiante solo tiene una matricula activa
    public List<TrasladoIngreso> getTrasladosByOrganizacionOrigen(Integer orgOriId) {
        String hql;
        Query query;
        Session session;
        Transaction transaction;
        List<TrasladoIngreso> response = null;
        session = HibernateUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        try {
            hql = "FROM TrasladoIngreso ti WHERE ti.estReg != 'E' AND ti.orgOriId = :hqlOrgOriId";
            query = session.createQuery(hql);
            query.setInteger("hqlOrgOriId", orgOriId);
            response = query.list();
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            session.close();
        }
        return response;
    }
    
    //Suponiendo que un estudiante solo tiene una matricula activa
    public List<TrasladoIngreso> getTrasladosByOrganizacionDestino(Integer orgDesId) {
        String hql;
        Query query;
        Session session;
        Transaction transaction;
        List<TrasladoIngreso> response = null;
        session = HibernateUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        try {
            hql = "FROM TrasladoIngreso ti WHERE ti.estReg != 'E' AND ti.orgDesId = :hqlOrgDesId";
            query = session.createQuery(hql);
            query.setInteger("hqlOrgDesId", orgDesId);
            response = query.list();
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            session.close();
        }
        return response;
    }

    public TrasladoIngreso getTrasladoById(Integer traIngId) {
        String hql;
        Query query;
        Session session;
        Transaction transaction;
        TrasladoIngreso response = null;
        session = HibernateUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        try {
            hql = "FROM TrasladoIngreso ti WHERE ti.estReg != 'E' AND ti.traIngId = :hqlTraIngId";
            query = session.createQuery(hql);
            query.setInteger("hqlTraIngId", traIngId);
            response = (TrasladoIngreso) query.uniqueResult();
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            session.close();
        }
        return response;
    }

}
