package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name = "perfil_capacitador", schema = "pedagogico")
@DynamicUpdate(value = true)
public class PerfilCapacitador implements Serializable {
    @Id
    @Column(name = "per_cap_id", unique = true, nullable = false)
    @SequenceGenerator(name = "secuencia_perfil_capacitador", sequenceName = "pedagogico.perfil_capacitador_per_cap_id_seq")
    @GeneratedValue(generator = "secuencia_perfil_capacitador")
    private int perCapId;

    @Column(name = "tip", nullable = false, length = 1)
    private Character tip;

    @Column(name = "des")
    private String des;

    @Column(name = "fue", length = 15)
    private String fue;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec")
    private Date fec;

    @ManyToMany(mappedBy = "perfilesCapacitacion")
    private Set<CursoCapacitacion> cursosCapacitacion = new HashSet<>(0);
    
    public PerfilCapacitador() {}
    
    public PerfilCapacitador(Character tip, String des, String fue) {
        this.tip = tip;
        this.des = des;
        this.fue = fue;
    }

    public int getPerCapId() {
        return perCapId;
    }

    public void setPerCapId(int perCapId) {
        this.perCapId = perCapId;
    }

    public Character getTip() {
        return tip;
    }

    public void setTip(Character tip) {
        this.tip = tip;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getFue() {
        return fue;
    }

    public void setFue(String fue) {
        this.fue = fue;
    }

    public Date getFec() {
        return fec;
    }

    public void setFec(Date fec) {
        this.fec = fec;
    }

    public Set<CursoCapacitacion> getCursosCapacitacion() {
        return cursosCapacitacion;
    }

    public void setCursosCapacitacion(Set<CursoCapacitacion> cursosCapacitacion) {
        this.cursosCapacitacion = cursosCapacitacion;
    }
}
