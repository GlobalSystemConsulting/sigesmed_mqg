/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.ma.reporte.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.ListaArticuloDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.ListaUtilesDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ArticuloEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ListaUtiles;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.scec.reporte.v1.tx.ReporteReunionTx;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.LineSeparator;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.TextAlignment;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class ReporteListaTx implements ITransaction{
private static final Logger logger = Logger.getLogger(ReporteReunionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        return reporteLista(data.getInt("lis"));
    }
    
    public WebResponse reporteLista(int lis){        
        try{
            ListaUtilesDao listaUtilesDao = (ListaUtilesDao) FactoryDao.buildDao("ma.ListaUtilesDao");
            String reportb64 = crearReporteMemoria(lis);
            return WebResponse.crearWebResponseExito("Se creó el reporte satisfactoriamente",new JSONObject().put("archivo",reportb64));
        }
        catch (Exception e){
           logger.log(Level.SEVERE,"error ",e);
            return WebResponse.crearWebResponseError("Error", WebResponse.BAD_RESPONSE);
        }
    }
    
    private String crearReporteMemoria(int lis) throws Exception {
        
        Paragraph p,p1,p2,p3;
        
        ListaArticuloDao  lDao = (ListaArticuloDao) FactoryDao.buildDao("ma.ListaArticuloDao");
        ListaUtilesDao luDao = (ListaUtilesDao) FactoryDao.buildDao("ma.ListaUtilesDao");
        PersonaDao personaDao = (PersonaDao) FactoryDao.buildDao("PersonaDao");;
        ListaUtiles lUtiles = luDao.buscarPorId(lis);
        
        Persona pers = personaDao.buscarPorID(lUtiles.getUsuId().getUsuId());
  
        System.out.println("ver usuario "+pers.getApeMat());
        Mitext mitext = new Mitext(false, "SIGESMED");
        mitext.newLine(5);
        mitext.agregarTitulo(lUtiles.getTituloLista());

        JSONObject jsonSub = new JSONObject();
        jsonSub.put("Nivel",lUtiles.getNivel().getNom());
        jsonSub.put("Año",lUtiles.getAnioLista());
        jsonSub.put("Institución",lUtiles.getOrganizacion().getNom());
        
        char c = lUtiles.getSeccion().getSedId();
        String sec = String.valueOf(c);
        jsonSub.put("Sección",sec);
        jsonSub.put("Grado",lUtiles.getGrado().getNom());
        jsonSub.put("Área",lUtiles.getArea().getNom());
 
        mitext.agregarSubtitulos(jsonSub);
        
        PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
           
        p3 = new Paragraph("Docente: "+pers.getNom()+" "+pers.getApePat()+" "+pers.getApeMat());
        p3.setKeepTogether(true);
        p3.setMarginTop(5);
        p3.setFont(bold).setFontSize(10);
        mitext.getDocument().add(p3);
        
        p = new Paragraph("Lista de Artículos");
        p.setKeepTogether(true);
        p.setMarginTop(15);
        p.setFont(bold).setFontSize(12);
        mitext.getDocument().add(p);   
        List<ArticuloEscolar> artis = lDao.listarArticulosDeLista(lis);
        Map<String,List<ArticuloEscolar>> grupos = new HashMap();
        for (ArticuloEscolar art:artis){
            String tip = art.getTipoArticulo();
            if(grupos.containsKey(tip)){
                grupos.get(tip).add(art);
            }else{
                grupos.put(tip, new ArrayList<ArticuloEscolar>());
                grupos.get(tip).add(art);
            }
        }
      
        for(Map.Entry<String,List<ArticuloEscolar>> entrada:grupos.entrySet()){
            
            mitext.agregarParrafo(entrada.getKey());
            GTabla gtabla;
            gtabla = new GTabla(new float[]{1.0f,2.5f,4.0f,1.0f,1.0f});
            gtabla.build(new String[]{"Cantidad","Artículo","Descripción","Precio", "Subtotal"});
                        
            List<ArticuloEscolar> articulos = entrada.getValue();
            
            for(ArticuloEscolar a:articulos){
                gtabla.processLine(new String[]{
                        String.valueOf(lDao.buscarArticulo(lis, a.getArtEscId()).getCanArt()),
                        a.getNomArticulo(),
                        a.getDesArticulo(),
                        String.valueOf(lDao.buscarArticulo(lis, a.getArtEscId()).getPreArt()),
                        String.valueOf(lDao.buscarArticulo(lis, a.getArtEscId()).getPreArt() * lDao.buscarArticulo(lis, a.getArtEscId()).getCanArt())
                });
            }
            gtabla.setBorder(Border.NO_BORDER);
            mitext.agregarTabla(gtabla);
        }
        
        String preTot = Double.toString(lUtiles.getPrecioTotal());
        p2 = new Paragraph("Precio Total : "+ preTot);
        p2.setTextAlignment(TextAlignment.RIGHT);
        mitext.getDocument().add(p2); 
        
        p1 = new Paragraph("Recomendaciones: ");
        p1.setKeepTogether(true);
        p1.setFont(bold).setFontSize(12);
        p1.setMarginTop(15);
        mitext.getDocument().add(p1);   
        
         GTabla gtabla2 = new GTabla(new float[]{5.0f});
         gtabla2.addCell(new Paragraph(lUtiles.getRecomendacionLista()));
         mitext.agregarTabla(gtabla2);
         
        mitext.cerrarDocumento();
        return mitext.encodeToBase64();    
    }
}

