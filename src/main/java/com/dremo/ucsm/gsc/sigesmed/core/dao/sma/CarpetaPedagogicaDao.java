package com.dremo.ucsm.gsc.sigesmed.core.dao.sma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica.CarpetaPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica.SeccionCarpetaPedagogica;

import java.util.List;

/**
 * Created by Administrador on 10/10/2016.
 */
public interface CarpetaPedagogicaDao extends GenericDao<CarpetaPedagogica>{
    CarpetaPedagogica buscarCarpetaPorEtapa(int eta);
    CarpetaPedagogica buscarCarpetaPorId(int eta);
    SeccionCarpetaPedagogica buscarSeccionPorId(int idSeccion);
    List<SeccionCarpetaPedagogica> listarSeccionesCarpeta(int idCarpeta);

}
