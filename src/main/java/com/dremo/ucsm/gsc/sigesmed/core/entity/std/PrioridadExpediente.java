package com.dremo.ucsm.gsc.sigesmed.core.entity.std;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="prioridad_expediente" ,schema="administrativo")
public class PrioridadExpediente  implements java.io.Serializable  {

    @Id
    @Column(name="pri_exp_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_prioridadexpediente", sequenceName="administrativo.prioridad_expediente_pri_exp_id_seq" )
    @GeneratedValue(generator="secuencia_prioridadexpediente")
    private int priExpId;
    @Column(name="nom", nullable=false, length=32)
    private String nom;
    @Column(name="des", length=256)
    private String des;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", length=29)
    private Date fecMod;
    @Column(name="usu_mod")
    private Integer usuMod;
    @Column(name="est_reg", length=1)
    private char estReg;

    public PrioridadExpediente() {
    }
    public PrioridadExpediente(int priExpId) {
        this.priExpId = priExpId;
    }
    public PrioridadExpediente(int priExpId, String nom, String des, Date fecMod, Integer usuMod, char estReg) {
       this.priExpId = priExpId;
       this.nom = nom;
       this.des = des;
       this.fecMod = fecMod;
       this.usuMod = usuMod;
       this.estReg = estReg;
    }
   
    public int getPriExpId() {
        return this.priExpId;
    }
    public void setPriExpId(int priExpId) {
        this.priExpId = priExpId;
    }
    
    public String getNom() {
        return this.nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    public String getDes() {
        return this.des;
    }
    public void setDes(String des) {
        this.des = des;
    }

    public Date getFecMod() {
        return this.fecMod;
    }
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }
    
    public Integer getUsuMod() {
        return this.usuMod;
    }
    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }
    
    public char getEstReg() {
        return this.estReg;
    }
    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
}


