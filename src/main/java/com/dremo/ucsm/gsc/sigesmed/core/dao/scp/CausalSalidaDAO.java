/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.scp;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.CausalAlta;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.CausalSalida;
import java.util.List;

/**
 *
 * @author harold
 */
public interface CausalSalidaDAO extends GenericDao<CausalSalida>{
    
    public List<CausalSalida> listarCausalSalida();
}
