/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mech;


import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.AreaCurricularHora;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.CicloEducativo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.DisenoCurricularMECH;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.JornadaEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.ModalidadEducacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Nivel;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.DisenoCurricularDao;

/**
 *
 * @author abel
 */
public class DisenoCurricularDaoHibernate extends GenericDaoHibernate<DisenoCurricularMECH> implements DisenoCurricularDao{

    @Override
    public DisenoCurricularMECH buscarVigentePorOrganizacion(int organizacionID){
        
        DisenoCurricularMECH objeto = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            for(int i=0;i<3;i++){
                String hql = "SELECT dc FROM DisenoCurricularMECH dc WHERE dc.orgId=:p1 and dc.estReg ='A' ";
                Query query = session.createQuery(hql);
                query.setParameter("p1", organizacionID );
                query.setMaxResults(1);
                //buscando 
                objeto =  (DisenoCurricularMECH)query.uniqueResult();
 
                if(objeto==null){

                    hql = "SELECT o.org_pad_id FROM organizacion o WHERE o.org_id=:p1 and o.est_reg ='A' ";
                    query = session.createSQLQuery(hql);
                    query.setParameter("p1", organizacionID );
                    query.setMaxResults(1);

                    Integer orgID =  (Integer)query.uniqueResult();                
                    if(orgID==null){
                        System.out.println("no tiene padre");
                        return null;
                    }
                    organizacionID = orgID;
                }
                else
                    return objeto;
            }
                
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar el plan de estudios vigente\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar el plan de estudios vigente\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return null;
    }
    @Override
    public List<DisenoCurricularMECH> buscarConOrganizacion(){
        return null;
    }
    
    @Override
    public List<Nivel> buscarNiveles(int disenoCurricularID){
        return null;
    }
    @Override
    public List<Grado> buscarGrados(int disenoCurricularID){
        return null;
    }
    @Override
    public List<CicloEducativo> buscarCiclos(int disenoCurricularID){
        return null;
    }
    @Override
    public List<ModalidadEducacion> buscarModalidades(int disenoCurricularID){
        return null;
    }
    @Override
    public List<JornadaEscolar> buscarJornadaEscolar(int disenoCurricularID){
        return null;
    }
    @Override
    public List<JornadaEscolar> listarJornadaEscolarConHoras(){
        List<JornadaEscolar> lista = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar jornadas escolares
            String hql = "SELECT DISTINCT j FROM JornadaEscolar j LEFT JOIN FETCH j.areaHoras WHERE j.estReg!='E'";
            Query query = session.createQuery(hql);
            lista = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las jornadas con horas\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las jornadas con horas\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return lista;
    }
    @Override
    public void insertarNivel(Nivel nivel){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            Nivel o = ((Nivel)session.merge(nivel));
            miTx.commit();            
            nivel.setNivId(o.getNivId());
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo registrar el nivel\\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo registrar el nivel \\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
    @Override
    public void insertarCiclo(CicloEducativo ciclo){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            CicloEducativo o = ((CicloEducativo)session.merge(ciclo));
            miTx.commit();            
            ciclo.setCicEduId(o.getCicEduId());
            /*session.persist(ciclo);
            miTx.commit();*/
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo registrar el ciclo \\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo registrar el ciclo \\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
    @Override
    public void insertarJornadaEscolar(JornadaEscolar jornada){
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            JornadaEscolar o = ((JornadaEscolar)session.merge(jornada));
            miTx.commit();            
            jornada.setJorEscId(o.getJorEscId());
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo registrar la jornada escolar \\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo registrar la jornada escolar \\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
    @Override
    public void insertarModalidad(ModalidadEducacion modalidad){
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            session.persist(modalidad);
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo registrar la modalidad \\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo registrar la modalidad \\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
        
    }
    @Override
    public void insertarArea(AreaCurricular area){
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            AreaCurricular o = ((AreaCurricular)session.merge(area));
            miTx.commit();            
            area.setAreCurId(o.getAreCurId());
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo registrar el area \\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo registrar el area \\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
        
    }
    @Override
    public void insertarGrado(Grado grado){
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            if(grado.getGraId() == 0)
                session.persist(grado);
            else{
                String hql = "UPDATE Grado g SET g.abr=:p2,g.nom=:p3,g.des=:p4,g.nivId=:p5,g.cicEduId=:p6,g.fecMod=:p7,g.usuMod=:p8 WHERE g.graId =:p1";

                Query query = session.createQuery(hql);
                query.setParameter("p1", grado.getGraId());
                query.setParameter("p2", grado.getAbr());
                query.setParameter("p3", grado.getNom());
                query.setParameter("p4", grado.getDes());
                query.setParameter("p5", grado.getNivId());
                query.setParameter("p6", grado.getCicEduId());
                query.setParameter("p7", new Date());
                query.setParameter("p8", grado.getUsuMod());
                query.executeUpdate();
            }
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo registrar el grado \\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo registrar el grado \\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
    @Override
    public void insertarGradoArea(int gradoID, int areaID, int gradoPos, int areaPos){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            String hql = "insert into grado_area ( gra_id, are_cur_id, gra_pos, are_pos ) VALUES ('"+ gradoID+"','" + areaID+"','" + gradoPos+"','" + areaPos+"')";                
            
            Query query = session.createSQLQuery(hql);
            query.executeUpdate();
            
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo insertar el grado area " + this.getClass().getName() + "\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    @Override
    public void eliminarGradoArea(int gradoID, int areaID){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            String hql = "delete from grado_area where gra_id=:p1 and are_cur_id=:p2 ";
            
            Query query = session.createSQLQuery(hql);
            query.setParameter("p1", gradoID);
            query.setParameter("p2", areaID);
            query.executeUpdate();
            
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo eliminar el grado area " + this.getClass().getName() + "\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    @Override
    public List<Object[]> listarGradoAreas(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "select g.dis_cur_id,g.niv_id,g.cic_id,g.gra_id,ga.are_cur_id, ga.gra_pos, ga.are_pos from grado g join grado_area ga on g.gra_id=ga.gra_id where g.est_reg='A' ORDER BY g.dis_cur_id, g.cic_id, g.gra_id";
            
            Query query = session.createSQLQuery(hql);
            
            return query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo listar grado con areas " + this.getClass().getName() + "\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    
    @Override
    public void insertarAreaHora(AreaCurricularHora areaHora){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            AreaCurricularHora o = ((AreaCurricularHora)session.merge(areaHora));
            miTx.commit();            
            areaHora.setAreHorId(o.getAreHorId());
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo actualizar el area hora\\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo actualizar el area hora\\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
    
    @Override
    public void eliminarAreaHora(AreaCurricularHora areaHora){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        
        try{
            session.delete(areaHora);
            miTx.commit();
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo actualizar el area hora\\n" + e.getMessage());
            throw new UnsupportedOperationException("No se actualizar el area hora\\n" + e.getMessage());
        }finally{
            session.close();
        }
    }
    
    public void eliminarArea(AreaCurricular area){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        
        try{
            session.delete(area);
            miTx.commit();
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo elimnar el area curricular\\n" + e.getMessage());
            throw new UnsupportedOperationException("No se pudo elimnar el area curricular\\n" + e.getMessage());
        }finally{
            session.close();
        }
    }
    
    public void eliminarGrado(Grado grado){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        
        try{
            session.delete(grado);
            miTx.commit();
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo elimnar el grado\\n" + e.getMessage());
            throw new UnsupportedOperationException("No se pudo elimnar el grado\\n" + e.getMessage());
        }finally{
            session.close();
        }
    }
    
    @Override
    public void eliminarCiclo(CicloEducativo ciclo){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        
        try{
            session.delete(ciclo);
            miTx.commit();
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo elimnar el ciclo\\n" + e.getMessage());
            throw new UnsupportedOperationException("No se pudo elimnar el ciclo\\n" + e.getMessage());
        }finally{
            session.close();
        }
    }
    
    @Override
    public void eliminarNivel(Nivel nivel){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        
        try{
            session.delete(nivel);
            miTx.commit();
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo elimnar el nivel\\n" + e.getMessage());
            throw new UnsupportedOperationException("No se pudo elimnar el nivel\\n" + e.getMessage());
        }finally{
            session.close();
        }
    }
}
