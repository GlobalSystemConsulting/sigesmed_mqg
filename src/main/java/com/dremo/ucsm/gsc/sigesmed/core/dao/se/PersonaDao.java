
package com.dremo.ucsm.gsc.sigesmed.core.dao.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Persona;

/**
 *
 * @author abel
 */
public interface PersonaDao extends GenericDao<Persona>{
    public Persona buscarPersonaPorId(Integer perId);
    public Persona buscarPorDNI(String perDNI);
}
