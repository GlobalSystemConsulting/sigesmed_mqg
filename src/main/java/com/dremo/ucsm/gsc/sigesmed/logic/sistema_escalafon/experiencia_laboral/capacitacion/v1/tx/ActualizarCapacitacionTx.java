/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Capacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Yemi
 */
public class ActualizarCapacitacionTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ActualizarCapacitacionTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try{
     
            DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
            
            JSONObject requestData = (JSONObject)wr.getData();
        
            Integer capId = requestData.getInt("capId");
            String nom = requestData.getString("nom");
            String tip = requestData.getString("tip");
            Date fec = requestData.getString("fec").equals("")?null:sdi.parse(requestData.getString("fec").substring(0, 10));
            Integer cal = requestData.getInt("cal");
            String lug = requestData.getString("lug");      
            
            return actualizarCapacitacion(capId, nom, tip, fec, cal, lug);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar capacitacion",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    
    private WebResponse actualizarCapacitacion(Integer capId, String nom, String tip, Date fec, Integer cal, String lug) {
        try{
            CapacitacionDao capacitacionDao = (CapacitacionDao)FactoryDao.buildDao("se.CapacitacionDao");        
            Capacitacion capacitacion = capacitacionDao.buscarPorId(capId);

            capacitacion.setNom(nom);
            capacitacion.setTip(tip);
            capacitacion.setFec(fec);
            capacitacion.setCal(cal);
            capacitacion.setLug(lug);
            
            capacitacionDao.update(capacitacion);

            JSONObject oResponse = new JSONObject();
            DateFormat sdo = new SimpleDateFormat("yyyy-MM-dd");
            oResponse.put("capId", capacitacion.getCapId());
            oResponse.put("nom", capacitacion.getNom()==null?"":capacitacion.getNom());
            oResponse.put("tip", capacitacion.getTip()==null?"":capacitacion.getTip());
            oResponse.put("fec", capacitacion.getFec()==null?"":sdo.format(capacitacion.getFec()));
            oResponse.put("cal", capacitacion.getCal()==null?0:capacitacion.getCal());
            oResponse.put("lug", capacitacion.getLug()==null?"":capacitacion.getLug());
            return WebResponse.crearWebResponseExito("Capacitacion actualizada exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarCapacitacion",e);
            return WebResponse.crearWebResponseError("Error, la capacitacion no fue actualizada");
        }
    } 
    
}
