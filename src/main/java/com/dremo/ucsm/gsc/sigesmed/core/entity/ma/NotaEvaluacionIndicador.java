package com.dremo.ucsm.gsc.sigesmed.core.entity.ma;

import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.GradoIEEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadorAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.SesionAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Periodo;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrador on 16/01/2017.
 */
@Entity
@Table(name = "nota_evaluacion_indicador",schema = "pedagogico")
public class NotaEvaluacionIndicador implements java.io.Serializable {
    @Id
    @Column(name = "not_eva_ind_id",nullable = false,unique = true)
    @SequenceGenerator(name = "nota_evaluacion_indicador_not_eva_ind_id_seq", sequenceName = "pedagogico.nota_evaluacion_indicador_not_eva_ind_id_seq")
    @GeneratedValue(generator = "nota_evaluacion_indicador_not_eva_ind_id_seq")
    private int notEvaIndId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "per_eva_id")
    private Periodo periodo;
    
    @Column(name = "num_per")
    private int numPer;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gra_ie_est_id")
    private GradoIEEstudiante gradoEstudiante;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ses_apr_id")
    private SesionAprendizaje sesion;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ind_apr_id")
    private IndicadorAprendizaje indicador;
    
    
    
    @Column(name = "nota_eva",length = 4)
    private String notaEva;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;
    
    @Column(name = "est_reg",length = 1)
    private Character estReg;
    
    @Column(name="not_lit" ,length = 2)
    private String not_lit;

    public NotaEvaluacionIndicador() {
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public NotaEvaluacionIndicador(int numPer, String not_lit, String notaEva) {
        this.numPer = numPer;
        this.not_lit = not_lit;
        this.notaEva = notaEva;

        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public int getNotEvaIndId() {
        return notEvaIndId;
    }

    public void setNotEvaIndId(int notEvaIndId) {
        this.notEvaIndId = notEvaIndId;
    }

    public Periodo getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Periodo periodo) {
        this.periodo = periodo;
    }

    public int getNumPer() {
        return numPer;
    }

    public void setNumPer(int numPer) {
        this.numPer = numPer;
    }

    public GradoIEEstudiante getGradoEstudiante() {
        return gradoEstudiante;
    }

    public void setGradoEstudiante(GradoIEEstudiante gradoEstudiante) {
        this.gradoEstudiante = gradoEstudiante;
    }

    public SesionAprendizaje getSesion() {
        return sesion;
    }

    public void setSesion(SesionAprendizaje sesion) {
        this.sesion = sesion;
    }

    public IndicadorAprendizaje getIndicador() {
        return indicador;
    }

    public void setIndicador(IndicadorAprendizaje indicador) {
        this.indicador = indicador;
    }

 

    public String getNotaEva() {
        return notaEva;
    }

    public void setNotaEva(String notaEva) {
        this.notaEva = notaEva;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    @Override
    public String toString() {
        return "NotaEvaluacionIndicador{" + "notEvaIndId=" + notEvaIndId + ", numPer=" + numPer + ", not_lit=" + not_lit + ", notaEva=" + notaEva + '}';
    }

    public void setNot_lit(String not_lit) {
        this.not_lit = not_lit;
    }

    public String getNot_lit() {
        return not_lit;
    }
    
    
    
}
