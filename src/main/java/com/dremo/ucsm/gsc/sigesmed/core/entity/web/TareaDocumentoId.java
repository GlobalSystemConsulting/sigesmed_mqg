/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.web;

import java.io.Serializable;

/**
 *
 * @author abel
 */
public final class TareaDocumentoId implements Serializable {
    
    private static final long serialVersionUID = 1562260205094677677L;
    private Integer bandejaTarea;
    private int tarDocId;

    public TareaDocumentoId() {
    }

    public TareaDocumentoId(Integer bandejaTarea, int tarDocId) {
        this.setBandejaTarea(bandejaTarea);
        this.setTarDocId(tarDocId);
    }

    @Override
    public int hashCode() {
        return ((this.getBandejaTarea() == null
                ? 0 : this.getBandejaTarea().hashCode())
                ^ ((int) this.getTarDocId()));
    }

    @Override
    public boolean equals(Object otherOb) {

        if (this == otherOb) {
            return true;
        }
        if (!(otherOb instanceof TareaDocumentoId)) {
            return false;
        }
        TareaDocumentoId other = (TareaDocumentoId) otherOb;
        return ((this.getBandejaTarea() == null
                ? other.getBandejaTarea() == null : this.getBandejaTarea()
                .equals(other.getBandejaTarea()))
                && (this.getTarDocId() == other.getTarDocId()));
    }

    @Override
    public String toString() {
        return "" + getBandejaTarea() + "-" + getTarDocId();
    }

    public Integer getBandejaTarea() {
        return bandejaTarea;
    }

    public void setBandejaTarea(Integer bandejaTarea) {
        this.bandejaTarea = bandejaTarea;
    }

    public int getTarDocId() {
        return tarDocId;
    }

    public void setTarDocId(int tarDocId) {
        this.tarDocId = tarDocId;
    }
    
}
