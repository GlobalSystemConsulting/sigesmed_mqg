package com.dremo.ucsm.gsc.sigesmed.logic.maestro.carpeta.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.carpeta.ContenidoSeccionCarpetaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.carpeta.DocenteCarpetaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.ContenidoSeccionesCarpetaPedagogicaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.DocenteCarpeta;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.RutaContenidoCarpeta;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica.EsquemaCarpeta;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sma.carpeta_pedagogica.ArchivoCarPed;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 28/12/2016.
 */
public class ListarDetalleCarpetaDocenteTx implements ITransaction{
    private static Logger logger = Logger.getLogger(ListarDetalleCarpetaDocenteTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idCar = data.getInt("car");
        int idOrg = data.getInt("org");
        int idDoc = data.getInt("doc");
        return listarDetalleCarpeta(idCar,idOrg,idDoc);
    }

    public WebResponse listarDetalleCarpeta(int idCar, int idOrg, int idDoc) {
        try{
            DocenteCarpetaDao conDocDao = (DocenteCarpetaDao) FactoryDao.buildDao("maestro.carpeta.DocenteCarpetaDao");
            ContenidoSeccionesCarpetaPedagogicaDao cscpDao = (ContenidoSeccionesCarpetaPedagogicaDao) FactoryDao.buildDao("sma.ContenidoSeccionesCarpetaPedagogicaDao");
            
            List<EsquemaCarpeta> archivos = cscpDao.listarEsquemaCarpeta(idCar);
            List<DocenteCarpeta> contenidosDocente = conDocDao.listarContenidosDocente(idOrg, idCar, idDoc);
            
            JSONArray jsonArchivos = new JSONArray(EntityUtil.listToJSONString(
                    new String[]{"carDigId","secCarPedId","ord","secNom","conSecCarPedId","conNom"},
                    new String[]{"carId","secId","secOrd","secNom","conId","conNom"},
                    archivos
            ));
            
            for(int i=0; i<jsonArchivos.length();i++){
                DocenteCarpeta docenteContenido = buscarPorDes(contenidosDocente, jsonArchivos.getJSONObject(i).getString("conNom"));
                System.out.println(docenteContenido);
                if(docenteContenido.getEstAva()==1 || docenteContenido.getEstAva()==2){
                    RutaContenidoCarpeta ruta = conDocDao.buscarRutaConId(docenteContenido.getDocCarPedId());
                    String route = ServicioREST.PATH_SIGESMED + File.separator + Sigesmed.UBI_ARCHIVOS + File.separator + ArchivoCarPed.ARCHIVO_CAR_PED_PATH + File.separator;
                    Path path = Paths.get(route + ruta.getNomFil());
                    if(Files.exists(path)){
                        jsonArchivos.getJSONObject(i).put("fileExiste", true);
                        jsonArchivos.getJSONObject(i).put("rutConMen", "");
                    } else{
                        jsonArchivos.getJSONObject(i).put("fileExiste", false);
                        jsonArchivos.getJSONObject(i).put("rutConMen", "El archivo no esta disponible");
                    } 
                    jsonArchivos.getJSONObject(i).put("rutConCarId", ruta.getRutConCarId());
                    jsonArchivos.getJSONObject(i).put("tipUsu", ruta.getTipUsu());
                    jsonArchivos.getJSONObject(i).put("nomFil", ruta.getNomFil());
                    jsonArchivos.getJSONObject(i).put("pat", ruta.getPat());
                }
                else{
                    jsonArchivos.getJSONObject(i).put("fileExiste", false);
                    jsonArchivos.getJSONObject(i).put("rutConMen", "No se ha subido el archivo");
                    jsonArchivos.getJSONObject(i).put("rutConCarId", "");
                    jsonArchivos.getJSONObject(i).put("tipUsu", "");
                    jsonArchivos.getJSONObject(i).put("nomFil", "");
                    jsonArchivos.getJSONObject(i).put("pat", "");
                }
            }
            return WebResponse.crearWebResponseExito("Se creo con exito la tarea",jsonArchivos);
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarDetalleCarpeta",e);
            return WebResponse.crearWebResponseError("No se puede listar los detalles de la carpeta");
        }
    }
    
    public DocenteCarpeta buscarPorDes(List<DocenteCarpeta> contenidosDocente,String conNom){
        for(DocenteCarpeta cd:contenidosDocente){
            if(cd.getDes().equals(conNom))
                return cd;
        }
        return null;
    }
    
}
