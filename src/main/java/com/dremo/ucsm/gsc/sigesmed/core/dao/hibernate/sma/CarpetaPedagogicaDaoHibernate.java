package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.CarpetaPedagogicaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica.CarpetaPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica.DocenteCarpetaPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica.SeccionCarpetaPedagogica;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import org.hibernate.Transaction;

/**
 * Created by Administrador on 10/10/2016.
 */
public class CarpetaPedagogicaDaoHibernate extends GenericDaoHibernate<CarpetaPedagogica> implements CarpetaPedagogicaDao{
    @Override
    public CarpetaPedagogica buscarCarpetaPorEtapa(int eta) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(CarpetaPedagogica.class)
                    .add(Restrictions.eq("eta",eta));
            return (CarpetaPedagogica) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public CarpetaPedagogica buscarCarpetaPorId(int eta) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(CarpetaPedagogica.class)
                    .add(Restrictions.eq("carDigId",eta));
            return (CarpetaPedagogica) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public SeccionCarpetaPedagogica buscarSeccionPorId(int idSeccion) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(SeccionCarpetaPedagogica.class)
                    .add(Restrictions.eq("secCarPedId",idSeccion));
            return (SeccionCarpetaPedagogica) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<SeccionCarpetaPedagogica> listarSeccionesCarpeta(int idCarpeta) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT s FROM SeccionCarpetaMaestro s LEFT JOIN FETCH s.contenidos c WHERE s.carpeta.carDigId =:idCar AND (c.estReg='A' OR c.estReg IS NULL) AND s.estReg='A'";
            Query query = session.createQuery(hql);
            query.setInteger("idCar",idCarpeta);
            query.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
    
    public List<DocenteCarpetaPedagogica> listarPorIE(int orgId) {
        List<DocenteCarpetaPedagogica> docenteCarPed = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            String hql = "SELECT dcp FROM com.dremo.ucsm.gsc.sigesmed.core.entity.sma.carpeta_pedagogica.DocenteCarpetaPedagogica as dcp "
                    + "join fetch dcp.docente d "
                    + "join fetch dcp.organizacion o "
                    + "WHERE o.orgId = " + orgId; 
            Query query = session.createQuery(hql);            
            docenteCarPed = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar las carpetas pedagogics de los docentes de la I.E\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar las carpetas pedagogics de los docentes de la I.E\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return docenteCarPed;
    }
}
