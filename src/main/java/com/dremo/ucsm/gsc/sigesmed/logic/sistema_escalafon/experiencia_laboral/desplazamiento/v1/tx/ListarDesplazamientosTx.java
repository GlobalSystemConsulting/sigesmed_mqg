/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.desplazamiento.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DesplazamientoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Desplazamiento;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class ListarDesplazamientosTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ListarDesplazamientosTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
         /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
        Integer ficEscId = requestData.getInt("ficEscId");
                
        List<Desplazamiento> desplazamientos = null;
        DesplazamientoDao desplazamientoDao = (DesplazamientoDao)FactoryDao.buildDao("se.DesplazamientoDao");
        
        try{
            desplazamientos = desplazamientoDao.listarxFichaEscalafonaria(ficEscId);
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar desplazamientos",e);
            System.out.println("No se pudo listar los desplazamientos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo listar los desplazamientos", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
        JSONArray miArray = new JSONArray();
        for(Desplazamiento d:desplazamientos ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("desId", d.getDesId());
            oResponse.put("tip", d.getTip());
            oResponse.put("numRes", d.getNumRes()==null?"":d.getNumRes());
            oResponse.put("fecRes", d.getFecRes()==null?"":sdi.format(d.getFecRes()));
            oResponse.put("insEdu", d.getInsEdu()==null?"":d.getInsEdu());
            oResponse.put("car", d.getCar()==null?"":d.getCar());
            oResponse.put("jorLab", d.getJorLab()==null?"":d.getJorLab());
            oResponse.put("fecIni", d.getFecIni()==null?"":sdi.format(d.getFecIni()));
            oResponse.put("fecTer", d.getFecTer()==null?"":sdi.format(d.getFecTer()));
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Los desplazamientos fueron listados exitosamente", miArray);
    }
}
