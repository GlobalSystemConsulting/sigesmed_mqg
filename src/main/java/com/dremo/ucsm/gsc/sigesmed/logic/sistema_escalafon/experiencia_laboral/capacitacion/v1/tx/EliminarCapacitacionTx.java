/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Capacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class EliminarCapacitacionTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(EliminarCapacitacionTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        Integer capId = 0;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            capId = requestData.getInt("capId");          
        
        }catch(Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"eliminarCapacitacion",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar", e.getMessage() );
        }
        
        CapacitacionDao capacitacionDao = (CapacitacionDao)FactoryDao.buildDao("se.CapacitacionDao");
        try{
            capacitacionDao.deleteAbsolute(new Capacitacion(capId));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar la capacitacion\n" + e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la capacitacion", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("La capacitacion se elimino correctamente");
    }
    
}
