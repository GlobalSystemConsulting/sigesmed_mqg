package com.dremo.ucsm.gsc.sigesmed.logic.ma.notas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.NotaEvaluacionIndicadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.RegistroAuxiliarDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.PlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.RegistroAuxiliar;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.RegistroAuxiliarCompetencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Estudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.GradoIEEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadorAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 25/01/2017.
 */
public class ListarNotasRegistroAuxiliarTx implements ITransaction{
    private Logger logger = Logger.getLogger(ListarNotasRegistroAuxiliarTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject)wr.getData();
        int idPeriodo = data.getInt("per");
        int idArea = data.getInt("are");
        int idOrg = data.getInt("org");
        int idUser = data.getInt("usr");
        int idGrado = data.getInt("gra");
        String secc = data.getString("secc");
        int idComp = data.getInt("comp");
        int idPlan = data.optInt("pla", -1);

        return listarNotasRegistroAuxiliar(idPeriodo, idArea, idOrg, idUser, idGrado, secc, idComp, idPlan);
    }

    private WebResponse listarNotasRegistroAuxiliar(int idPeriodo, int idArea, int idOrg, int idUser, int idGrado, String secc, int idComp, int idPlan) {
        try{

            PlanEstudiosDao planDao = (PlanEstudiosDao) FactoryDao.buildDao("mech.PlanEstudiosDao");
            RegistroAuxiliarDao regisDao = (RegistroAuxiliarDao) FactoryDao.buildDao("ma.RegistroAuxiliarDao");
            NotaEvaluacionIndicadorDao notaDao = (NotaEvaluacionIndicadorDao) FactoryDao.buildDao("ma.NotaEvaluacionIndicadorDao");

            List<IndicadorAprendizaje> indicadores = regisDao.listarIndicadoresPeriodo(
                    idPeriodo,idOrg,idUser,
                    idPlan == -1 ? planDao.buscarVigentePorOrganizacion(idOrg).getPlaEstId() : idPlan,
                    idGrado,idArea,idComp);
            JSONArray indicadoresJSON = new JSONArray(EntityUtil.listToJSONString(
                    new String[]{"indAprId","nomInd","eva"},
                    new String[]{"id","nom","eva"},
                    indicadores
            ));
            List<GradoIEEstudiante> estudiantesGrado = notaDao.listarEstudiantesGradoActual(idOrg, idGrado, secc.charAt(0));
            JSONArray estudiantesJSON = new JSONArray();
            for(GradoIEEstudiante gramat : estudiantesGrado){
                Estudiante estudiante= gramat.getMatriculaEstudiante().getEstudiante();
                JSONObject jsonEstudiante = new JSONObject(EntityUtil.objectToJSONString(
                        new String[]{"perId", "dni", "nom", "apePat", "apeMat", "fecNac"},
                        new String[]{"id", "dni", "nom", "pat", "mat", "nac"},
                        estudiante.getPersona()
                ));
                jsonEstudiante.put("idgra",gramat.getGraOrgEstId());
                jsonEstudiante.put("notas",new JSONArray());
                for(IndicadorAprendizaje indicador : indicadores){
                    RegistroAuxiliar regAux = regisDao.buscarNotaEstudiante(indicador.getIndAprId(),
                            idUser,idPeriodo,idArea,gramat.getGraOrgEstId());
                    if(regAux == null){
                        jsonEstudiante.getJSONArray("notas").put(new JSONObject().put("ind",indicador.getIndAprId()));
                    }else{
                        jsonEstudiante.getJSONArray("notas").put(new JSONObject()
                                .put("ind",indicador.getIndAprId())
                                .put("id",regAux.getRegAuxId())
                                .put("not",regAux.getNotInd())
                                .put("notIndDoc",regAux.getNotIndDoc())
                                .put("notLit",regAux.getNot_ind_lit())
                                .put("notLitDoc",regAux.getNot_ind_lit_doc())
                                .put("edi",false)
                                .put("preedi",false));
                    }
                }
                // Aqui la nota final
                RegistroAuxiliarCompetencia notComp = regisDao.buscarNotaCompetenciaEsp(idComp,idArea,idPeriodo,gramat.getGraOrgEstId());
                if(notComp != null){
                    jsonEstudiante.put("notComp",notComp.getNota());
                    jsonEstudiante.put("notCompLit",notComp.getNot_com_lit());
                }else {
                    jsonEstudiante.put("notComp","");
                    jsonEstudiante.put("notCompLit","");
                }
                estudiantesJSON.put(jsonEstudiante);
            }
            JSONObject results = new JSONObject().put("indicadores",indicadoresJSON).put("estudiantes",estudiantesJSON);
            return WebResponse.crearWebResponseExito("Se listo las notas correctamente",results);
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarNotas",e);
            return WebResponse.crearWebResponseError("No se puede listar los datos");
        }
    }
}
