/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.ConfiguracionNotaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.NotaEvaluacionIndicadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.RegistroAuxiliarDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.DocenteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.IndicadorAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.TareaEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ConfiguracionNota;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.RegistroAuxiliar;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.GradoIEEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.PeriodosPlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.TareaIndicador;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class RegistrarPromedioIndicadoresxEstudianteTx implements ITransaction {

    List<ConfiguracionNota> config_docente = null;

    @Override
    public WebResponse execute(WebRequest wr) {

        JSONObject data = (JSONObject) wr.getData();
        System.out.println(data);
        int areCurId = data.optInt("areCurId");
        char tipPer = data.getString("tipPer").charAt(0);
        int numPer = data.getInt("numPer");
        int idGra = data.getInt("graId");
        int idUsuDoc = data.getInt("usuId");
        int notNum = data.getInt("nota");
        String notLit = data.getString("notLit");
        int tarIdMM = data.getInt("tarIdMM");

        int idOrg = data.getInt("idOrg");
        char idSeccion = data.getString("idSeccion").charAt(0);
        int idAlumno = data.getInt("idAlumno");
        int idPlaEst = data.getInt("idPlaEst");

        try {

            RegistroAuxiliarDao regisDao = (RegistroAuxiliarDao) FactoryDao.buildDao("ma.RegistroAuxiliarDao");
            NotaEvaluacionIndicadorDao notaDao = (NotaEvaluacionIndicadorDao) FactoryDao.buildDao("ma.NotaEvaluacionIndicadorDao");
            IndicadorAprendizajeDao indicadorDao = (IndicadorAprendizajeDao) FactoryDao.buildDao("maestro.plan.IndicadorAprendizajeDao");
            DocenteDao docDao = (DocenteDao) FactoryDao.buildDao("maestro.DocenteDao");
            EstudianteDao estDao = (EstudianteDao) FactoryDao.buildDao("maestro.EstudianteDao");
            TareaEscolarDao tareaDao = (TareaEscolarDao) FactoryDao.buildDao("web.TareaEscolarDao");
            ConfiguracionNotaDao configNot = (ConfiguracionNotaDao) FactoryDao.buildDao("ma.ConfiguracionNotaDao");
            config_docente = configNot.listarConfiguracionDocente(idUsuDoc, idOrg);
            List<TareaIndicador> indicadores = tareaDao.obtenerIndicadoresTarea(tarIdMM);
            GradoIEEstudiante graOrgEst = tareaDao.obtenerGradoIEEstudiante(idOrg, idGra, idSeccion, idAlumno);
            String mensaje = "";

            Docente docente = docDao.buscarDocentePorId(idUsuDoc);
            for (int j = 0; j < indicadores.size(); j++) {
                int idInd = indicadores.get(j).getInd_apr_id();
                JSONArray notas = notaDao.buscarNotasxIndicadorxEstudiante(idInd, graOrgEst.getGraOrgEstId(), tipPer, numPer);
                Double promedio = 0.0;
                for (int k = 0; k < notas.length(); k++) {
                    promedio += Double.parseDouble(notas.getJSONObject(k).getString("nota"));
                }
                String nota = "";
                String nota_lit = "";
                if (notas.length() != 0) {
                    promedio = promedio / notas.length();
                    promedio = (Math.round(promedio * 10.0) / 10.0);
                    nota = String.valueOf(promedio);
                    nota_lit = obtenerNotaLiteral(promedio);
                } else {
                    nota = String.valueOf(notNum);
                    nota_lit = notLit;
                }

                PeriodosPlanEstudios perPlaEst = regisDao.buscarPeriodoPorAtributos(idPlaEst, tipPer, numPer);
                if (!regisDao.buscarNotaFinalIndicador(idInd, docente.getDoc_id(), perPlaEst.getPerPlaEstId(), areCurId, graOrgEst.getGraOrgEstId())) {
                    RegistroAuxiliar regAux = new RegistroAuxiliar(nota);
                    regAux.setNotIndDoc(nota);
                    regAux.setNot_ind_lit(nota_lit);
                    regAux.setNot_ind_lit_doc(nota_lit);
                    regAux.setIndicador(indicadorDao.buscarPorId(idInd));
                    regAux.setPeriodo(perPlaEst);
                    regAux.setDocente(docente);
                    regAux.setArea(docDao.buscarAreaPorId(areCurId));
                    regAux.setGradoEstudiante(estDao.buscarGradoIEEstudiante(graOrgEst.getGraOrgEstId()));
                    regAux.setUsuMod(idUsuDoc);
                    regisDao.insert(regAux);
                    mensaje = "Se registro el promedio correctamente";

                } else {
                    RegistroAuxiliar regAux = regisDao.buscarNotaEstudiante(idInd, docente.getDoc_id(), perPlaEst.getPerPlaEstId(), areCurId, graOrgEst.getGraOrgEstId());
                    regAux.setNotInd(nota);
                    regAux.setNotIndDoc(nota);
                    regAux.setNot_ind_lit(nota_lit);
                    regAux.setNot_ind_lit_doc(nota_lit);
                    regAux.setFecMod(new Date());
                    regisDao.update(regAux);
                    mensaje = "Se actualizo el promedio correctamente";
                }
            }

        } catch (Exception e) {
            System.out.println("No se pudo registrar las notas de los indicadores\n" + e);
            return WebResponse.crearWebResponseError("No se pudo registrar las notas de los indicadores", e.getMessage());
        }
        JSONObject oResponse = new JSONObject();
        return WebResponse.crearWebResponseExito("Se pudo registrar las notas de los indicadores correctamente", oResponse);
        //Fin
    }

    public String obtenerNotaLiteral(Double not) {
        for (ConfiguracionNota conf : config_docente) {
            if ((int) Math.round(not) >= conf.getConf_not_min() && (int) Math.round(not) <= conf.getConf_not_max()) {
                return conf.getConf_cod();
            }
        }
        return "";
    }
}
