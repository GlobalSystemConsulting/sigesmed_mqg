/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.desplazamiento.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DesplazamientoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Desplazamiento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class AgregarDesplazamientoTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(AgregarDesplazamientoTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
         /*
        *   Parte para la lectura, verificacion y validacion de datos
         */
        Desplazamiento desplazamiento = null;
        
        Integer ficEscId = 0;

        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");

        try {
            JSONObject requestData = (JSONObject) wr.getData();

            ficEscId = requestData.getInt("ficEscId");
            Character tip = requestData.getString("tip").charAt(0);
            String numRes = requestData.getString("numRes");
            Date fecRes = requestData.getString("fecRes").equals("")?null:sdi.parse(requestData.getString("fecRes").substring(0, 10));
            String insEdu = requestData.getString("insEdu");
            String car = requestData.getString("car");
            String jorLab = requestData.getString("jorLab");
            Date fecIni = requestData.getString("fecIni").equals("")?null:sdi.parse(requestData.getString("fecIni").substring(0, 10));
            Date fecTer = requestData.getString("fecTer").equals("")?null:sdi.parse(requestData.getString("fecTer").substring(0, 10));
            
            desplazamiento = new Desplazamiento(new FichaEscalafonaria(ficEscId), tip, numRes, fecRes, insEdu, car, jorLab, fecIni, fecTer, wr.getIdUsuario(), new Date(), 'A');
            System.out.println(desplazamiento);
        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"Datos nuevo desplazamiento",e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        //Fin

        /*
        *  Parte para la operacion en la Base de Datos
         */
        //si el pariente no existe en la tabla persona
        DesplazamientoDao publicacionDao = (DesplazamientoDao) FactoryDao.buildDao("se.DesplazamientoDao");
        try {
            publicacionDao.insert(desplazamiento);
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar nuevo desplazamiento",e);
            System.out.println(e);
        }
        
        //Fin       

        /*
        *  Repuesta Correcta
         */
        JSONObject oResponse = new JSONObject();
        oResponse.put("desId", desplazamiento.getDesId());
        oResponse.put("tip", desplazamiento.getTip());
        oResponse.put("numRes", desplazamiento.getNumRes()==null?"":desplazamiento.getNumRes());
        oResponse.put("fecRes", desplazamiento.getFecRes()==null?"":sdi.format(desplazamiento.getFecRes()));
        oResponse.put("insEdu", desplazamiento.getInsEdu()==null?"":desplazamiento.getInsEdu());
        oResponse.put("car", desplazamiento.getCar()==null?"":desplazamiento.getCar());
        oResponse.put("jorLab", desplazamiento.getJorLab()==null?"":desplazamiento.getJorLab());
        oResponse.put("fecIni", desplazamiento.getFecIni()==null?"":sdi.format(desplazamiento.getFecIni()));
        oResponse.put("fecTer", desplazamiento.getFecTer()==null?"":sdi.format(desplazamiento.getFecTer()));
        
        return WebResponse.crearWebResponseExito("El registro del desplazamiento se realizo correctamente", oResponse);
        //Fin
    }
}
