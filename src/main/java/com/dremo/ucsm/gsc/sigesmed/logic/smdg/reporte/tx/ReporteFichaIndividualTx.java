/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.reporte.tx;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.ReporteTx;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.pdf.PdfName;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Table;
import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class ReporteFichaIndividualTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */        
//        request.setData({evaluador:$scope.datosEvaluador, plantilla:$scope.plantilla, grupos:$scope.grupos});                
        Table tablaEvaluador = new Table(6);
        tablaEvaluador.setFontSize(8);
        
        Table tablaCuerpo = new Table(6);
        tablaCuerpo.setFontSize(8);
        
        Table tablaResultados = new Table(6);
        tablaResultados.setFontSize(8);
                
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            JSONObject evaluador = requestData.getJSONObject("evaluador");
            JSONObject plantilla = requestData.getJSONObject("plantilla");
            
            tablaEvaluador.addCell(new Cell(1, 6).add("Codigo: " + plantilla.optString("codigo")).setBorder(Border.NO_BORDER));
            tablaEvaluador.addCell(new Cell(1, 6).add("Nombre: " + plantilla.optString("nombre")).setBorder(Border.NO_BORDER));
            tablaEvaluador.addCell(new Cell(1, 6).add("Tipo: " + plantilla.optString("tipoNom")).setBorder(Border.NO_BORDER));
            
            tablaEvaluador.addCell(new Cell(1, 2).add("Aplicador cargo: " + evaluador.optString("cargo")).setBorder(Border.NO_BORDER));
            tablaEvaluador.addCell(new Cell(1, 2).add("Nombre de Aplicador: " + evaluador.optString("nombres")).setBorder(Border.NO_BORDER));
            tablaEvaluador.addCell(new Cell(1, 2).add("Fecha de Evaluacion: " + evaluador.optString("fecha")).setBorder(Border.NO_BORDER));
            
            JSONArray grupos = requestData.getJSONArray("grupos");
            for(int i = 0; i<grupos.length(); ++i){
                tablaCuerpo.addCell(new Cell(1, 5).add(grupos.getJSONObject(i).optString("gruNom")).setBackgroundColor(Color.LIGHT_GRAY));
                tablaCuerpo.addCell(new Cell(1, 1).add("Puntaje").setBackgroundColor(Color.LIGHT_GRAY));
                JSONArray indicadores = grupos.getJSONObject(i).getJSONArray("indicadores");
                for (int j = 0; j < indicadores.length(); j++) {
                    tablaCuerpo.addCell(new Cell(1, 5).add("   " + indicadores.getJSONObject(j).optString("indNom")));
                    tablaCuerpo.addCell(new Cell(1, 1).add(indicadores.getJSONObject(j).optString("punto")));
                }
            }
            
            tablaResultados.addCell(new Cell(1, 1).add("").setBorder(Border.NO_BORDER));
            tablaResultados.addCell(new Cell(1, 1).add("Suma Total: ").setBorder(Border.NO_BORDER));
            tablaResultados.addCell(new Cell(1, 1).add(plantilla.optString("total")).setBorder(Border.NO_BORDER));
            tablaResultados.addCell(new Cell(1, 3).add("").setBorder(Border.NO_BORDER));
            tablaResultados.addCell(new Cell(1, 6).add("").setBorder(Border.NO_BORDER));
            tablaResultados.addCell(new Cell(1, 2).add("Resultados por Indicadores:").setBorder(Border.NO_BORDER));
            tablaResultados.addCell(new Cell(1, 4).add("").setBorder(Border.NO_BORDER));
            
            double maxPunt = plantilla.getDouble("maxPunt");            
            for(int i = 0; i<grupos.length(); ++i){
                tablaResultados.addCell(new Cell(1, 2).add(grupos.getJSONObject(i).optString("gruNom")).setBorder(Border.NO_BORDER));
                double totalGrupo = grupos.getJSONObject(i).optDouble("total");               
                tablaResultados.addCell(new Cell(1, 1).add(toPercentage(totalGrupo*100/maxPunt) + " %").setBorder(Border.NO_BORDER));
                tablaResultados.addCell(new Cell(1, 3).add("").setBorder(Border.NO_BORDER));
            }
            
            double totalFicha = plantilla.optDouble("total");
            
            tablaResultados.addCell(new Cell(1, 1).add("").setBorder(Border.NO_BORDER));
            tablaResultados.addCell(new Cell(1, 1).add("Total(%): ").setBorder(Border.NO_BORDER));
            tablaResultados.addCell(new Cell(1, 1).add(toPercentage(totalFicha*100/maxPunt) + " %").setBorder(Border.NO_BORDER));
            tablaResultados.addCell(new Cell(1, 3).add("").setBorder(Border.NO_BORDER));
            
            
        }catch(Exception e){
            System.out.println(e);  
            return WebResponse.crearWebResponseError("No se pudo realizar el reporte", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Repuesta Correcta
        */
        
        JSONObject response = new JSONObject();
        try {
            Mitext r = new Mitext(Boolean.FALSE);
                        
            r.agregarTitulo("REPORTE DE FICHA DE EVALUACION");
            r.newLine(1);
            r.agregarTabla(tablaEvaluador);
            r.newLine(1);
            r.agregarTabla(tablaCuerpo);
            r.newLine(1);
            r.agregarTabla(tablaResultados);
            
            r.cerrarDocumento();
            response.append("reporte", r.encodeToBase64() );
        } catch (Exception ex) {
            Logger.getLogger(ReporteTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return WebResponse.crearWebResponseExito("el reporte se realizo",response);
        //Fin
    }
    
    public String toPercentage(double d){
        String result = String.valueOf(new BigDecimal(d).setScale(2, BigDecimal.ROUND_HALF_UP));
        return result;
    }
    
}
