/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.familiares.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ParientesDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Parientes;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONObject;

/**
 *
 * @author Alex
 */
public class BuscarPersonaParienteTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
        String dni = "";
        int perId;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            dni = requestData.getString("dni");
            perId = requestData.getInt("perId");
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("El dni ingresado es incorrecto" );
        }
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        Persona persona = null;
        Parientes pariente = null;
        PersonaDao personaDao = (PersonaDao)FactoryDao.buildDao("se.PersonaDao");
        ParientesDao parientesDao = (ParientesDao)FactoryDao.buildDao("se.ParientesDao");
        int caso=0;
        /*3 Casos: 
        *   0.- Es nueva persona, agregar persona y parentesco
        *   1.- Tiene persona, agregar parentesco
        *   2.- Ya es su pariente , no hacer nada
        */
        try{
           persona = personaDao.buscarPorDNI(dni);
           if(persona!=null)
           {
               pariente = parientesDao.buscarParientePorId(perId,persona.getPerId());
               if(pariente!=null)
               {
                   caso=2;
               }
               else{
                   caso=1;
               }
           }
           System.out.println("CASO: "+caso);
        }catch(Exception e){
            System.out.println("\n"+e);
            return WebResponse.crearWebResponseError("No se pudo buscar por dni", e.getMessage() );
        }
        
        if(caso==2){//Ya es su pariente , no hacer nada
            
            JSONObject oRes = new JSONObject();
            JSONObject oPersona = new JSONObject();

            String apePat = persona.getApePat();
            String apeMat = persona.getApeMat();
            String nom = persona.getNom();
            Character sex = persona.getSex();
            Date fecNac = persona.getFecNac();
            String fij = persona.getFij();
            String num1 = persona.getNum1();
            String num2 = persona.getNum2();
            String email = persona.getEmail();
            Integer tipoPariente = pariente.getParentesco().getTpaId();
            
            oPersona.put("perCod", persona.getPerCod());
            oPersona.put("apePat", apePat == null ? "" : apePat);
            oPersona.put("apeMat", apeMat == null ? "" : apeMat);
            oPersona.put("nom", nom == null ? "" : nom);
            oPersona.put("sex", sex == null ? "" : sex);
            oPersona.put("fecNac", fecNac == null ? "" : sdi.format(fecNac));
            oPersona.put("fij", fij == null ? "" : fij);
            oPersona.put("num1", num1 == null ? "" : num1);
            oPersona.put("num2", num2 == null ? "" : num2);
            oPersona.put("email", email == null ? "" : email);
            oPersona.put("tipoPariente", tipoPariente);
            oPersona.put("caso", caso);

            oRes.put("persona", oPersona);
            //System.out.println(oRes);
            return WebResponse.crearWebResponseExito("El pariente ya se encuentra registrado. Ingresar otro DNI", oRes);
            
        } else {
            if (caso == 1) {//Tiene persona, agregar parentesco
                JSONObject oRes = new JSONObject();
                JSONObject oPersona = new JSONObject();

                String apePat = persona.getApePat();
                String apeMat = persona.getApeMat();
                String nom = persona.getNom();
                Character sex = persona.getSex();
                Date fecNac = persona.getFecNac();
                String fij = persona.getFij();
                String num1 = persona.getNum1();
                String num2 = persona.getNum2();
                String email = persona.getEmail();
                
                oPersona.put("parId", persona.getPerId());
                oPersona.put("perCod", persona.getPerCod());
                oPersona.put("apePat", apePat == null ? "" : apePat);
                oPersona.put("apeMat", apeMat == null ? "" : apeMat);
                oPersona.put("nom", nom == null ? "" : nom);
                oPersona.put("sex", sex == null ? "" : sex);
                oPersona.put("fecNac", fecNac==null?"":sdi.format(fecNac));
                oPersona.put("fij", fij==null?"":fij);
                oPersona.put("num1", num1==null?"":num1);
                oPersona.put("num2", num2==null?"":num2);
                oPersona.put("email", email==null?"":email);
                oPersona.put("caso",caso);
            
                oRes.put("persona", oPersona);
                return WebResponse.crearWebResponseExito("Se encontró a la persona, debe agregar su Parentesco", oRes);
            } else {
                return WebResponse.crearWebResponseError("La persona no se encuentra registrado en el Sistema");
            }
        }
    }
    
}