/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.AscensoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ColegiaturaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DemeritoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DesplazamientoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DireccionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.EstudioComplementarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.EstudioPostgradoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ExposicionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FichaEscalafonariaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FormacionEducativaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.LegajoPersonalDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.PublicacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ReconocimientoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TipoParienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Ascenso;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Capacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Colegiatura;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Demerito;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Desplazamiento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Direccion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.EstudioComplementario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.EstudioPostgrado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Exposicion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FormacionEducativa;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.LegajoPersonal;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Parientes;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Publicacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Reconocimiento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoPariente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import java.io.File;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class run {
    public static void main(String[] args) {
 
        /*FichaEscalafonariaDaoHibernate dedao = new FichaEscalafonariaDaoHibernate();           
        FichaEscalafonaria de = null;
        de = dedao.obtenerDatosPersonales(6,53);
        System.err.println(de.getFicEscId());*/
        //JOptionPane.showMessageDialog(null,de.getTrabajador().getPersona().getApeMat());
        
        /*List<DatosEscalafon> datos = null;
        datos = dedao.ListarxOrganizacion(7);
        for(int i=0; i<datos.size();i++)
            JOptionPane.showMessageDialog(null,datos.get(i).getTrabajador().getPersona().getNom());  */  
        
        /*List<FichaEscalafonaria> datos = null;
        datos = dedao.ListarxOrganizacion(7);
        for(int i=0; i<datos.size();i++)
            JOptionPane.showMessageDialog(null,datos.get(i).getTrabajador().getPersona().getNom());*/
        
        /*Direccion direccion = new Direccion(new Persona(57), 'A', "a", "a", "a", "a", 4, new Date(), 'A');
        DireccionDao direccionDao = (DireccionDao) FactoryDao.buildDao("se.DireccionDao");
        direccionDao.insert(direccion);*/
        
       /* TrabajadorDao trabajadorDao = (TrabajadorDao) FactoryDao.buildDao("se.TrabajadorDao");
        trabajadorDao.insert(trabajador);*/
       
        /*FichaEscalafonaria fe = new FichaEscalafonaria(new Trabajador(1), 4, new Date(), 'A');
        FichaEscalafonariaDao feDao = (FichaEscalafonariaDao) FactoryDao.buildDao("se.FichaEscalafonariaDao");
        feDao.insert(fe);*/
        
        /*ParientesDaoHibernate paDao = new ParientesDaoHibernate();
        List<Parientes> parientes = null;
        parientes = paDao.listarxTrabajador(7);
        for(int i=0; i<parientes.size();i++)
           JOptionPane.showMessageDialog(null, parientes.get(i).getParId());*/
        
        /*TipoParienteDao tipoParienteDao = (TipoParienteDao)FactoryDao.buildDao("se.TipoParienteDao");
        List<TipoPariente> tipos = tipoParienteDao.listar(TipoPariente.class);
        for(int i=0; i<tipos.size();i++)
           JOptionPane.showMessageDialog(null, i+ ": "+ tipos.get(i).getTpaDes());*/
        
        
        /*FormacionEducativa fe = new FormacionEducativa(new FichaEscalafonaria(1), '1', "primaria","sistemas", "0001", false, new Date(), "dhdjhf", 1, new Date(), 'A');
        FormacionEducativaDao feDao = (FormacionEducativaDao) FactoryDao.buildDao("se.FormacionEducativaDao");
        feDao.insert(fe);*/
        
       /* FormacionEducativaDaoHibernate forEduDao = new FormacionEducativaDaoHibernate();           
        List<FormacionEducativa> forEdu = null;
        forEdu = forEduDao.listarxFichaEscalafonaria(6);
        for(int i=0; i<forEdu.size();i++)
           JOptionPane.showMessageDialog(null, i+ ": "+ forEdu.get(i).getCenEst());*/
       
        /*EstudioComplementario ec = new EstudioComplementario(new FichaEscalafonaria(1), '1', "primaria",'N', "INFOUNSA","Presencial", new Date(), new Date(),20, 1, new Date(), 'A');
        EstudioComplementarioDao ecDao = (EstudioComplementarioDao) FactoryDao.buildDao("se.EstudioComplementarioDao");
        ecDao.insert(ec);*/
        
        /*EstudioComplementarioDaoHibernate estComDao = new EstudioComplementarioDaoHibernate();           
        List<EstudioComplementario> estCom = null;
        estCom = estComDao.listarxFichaEscalafonaria(1);
        for(int i=0; i<estCom.size();i++)
           JOptionPane.showMessageDialog(null, i+ ": "+ estCom.get(i).getInsCer());*/
        
        /*Exposicion expPon = new Exposicion(new FichaEscalafonaria(1),"primaria","INFOUNSA","Presencial", new Date(), new Date(),20, 1, new Date(), 'A');
        ExposicionDao expPonDao = (ExposicionDao) FactoryDao.buildDao("se.ExposicionDao");
        expPonDao.insert(expPon);*/
        
        /*ExposicionDaoHibernate expPonDao = new ExposicionDaoHibernate();           
        List<Exposicion> expPon = null;
        expPon = expPonDao.listarxFichaEscalafonaria(1);
        for(int i=0; i<expPon.size();i++)
           JOptionPane.showMessageDialog(null, i+ ": "+ expPon.get(i).getInsOrg());*/
        
        /*Publicacion publicacion = new Publicacion(new FichaEscalafonaria(1),"p","IN","P", "O","C", new Date(), "55",1, new Date(), 'A');
        PublicacionDao publicacionDao = (PublicacionDao) FactoryDao.buildDao("se.PublicacionDao");
        publicacionDao.insert(publicacion);*/
        
        /* PublicacionDaoHibernate pubDao = new PublicacionDaoHibernate();           
        List<Publicacion> pub = null;
        pub = pubDao.listarxFichaEscalafonaria(1);
        for(int i=0; i<pub.size();i++)
           JOptionPane.showMessageDialog(null, i+ ": "+ pub.get(i).getGraPar());*/
      
        /*Desplazamiento des = new Desplazamiento(new FichaEscalafonaria(1),'g',"p",new Date(),"P", "O","C", new Date(), new Date(),1, new Date(), 'A');
        DesplazamientoDao desDao = (DesplazamientoDao) FactoryDao.buildDao("se.DesplazamientoDao");
        desDao.insert(des);*/
        
        /*DesplazamientoDaoHibernate pubDao = new DesplazamientoDaoHibernate();           
        List<Desplazamiento> pub = null;
        pub = pubDao.listarxFichaEscalafonaria(1);
        for(int i=0; i<pub.size();i++)
           JOptionPane.showMessageDialog(null, i+ ": "+ pub.get(i).getNumRes());*/
        
        /*Colegiatura colegiatura = new Colegiatura(new FichaEscalafonaria(1),"Colegio de ingenieros del Peru", "05421",true, 1, new Date(), 'A');
        ColegiaturaDao colegiaturaDao = (ColegiaturaDao) FactoryDao.buildDao("se.ColegiaturaDao");
        colegiaturaDao.insert(colegiatura);*/
        
        /*ColegiaturaDaoHibernate colegiaturaDao = new ColegiaturaDaoHibernate();
        List<Colegiatura> colegiatura = null;
        colegiatura = colegiaturaDao.listarxFichaEscalafonaria(1);
        for(int i=0; i<colegiatura.size();i++)
           JOptionPane.showMessageDialog(null, i+ ": "+ colegiatura.get(i).getNomColPro());*/
        
        /*Ascenso ascenso = new Ascenso(new FichaEscalafonaria(1),"55555", new Date(),new Date(),"VIII", 1, new Date(), 'A');
        AscensoDao ascensoDao = (AscensoDao) FactoryDao.buildDao("se.AscensoDao");
        ascensoDao.insert(ascenso);*/
        
       /*AscensoDaoHibernate ascensoDao = new AscensoDaoHibernate();
        List<Ascenso> ascensos = null;
        ascensos = ascensoDao.listarxFichaEscalafonaria(1);
        for(int i=0; i<ascensos.size();i++)
           JOptionPane.showMessageDialog(null, i+ ": "+ ascensos.get(i).getNumRes());*/
       
        /*Reconocimiento reconocimiento = new Reconocimiento(new FichaEscalafonaria(1),'1', "55555", new Date(),"hsudhshadhah", 1, new Date(), 'A');
        ReconocimientoDao reconocimientoDao = (ReconocimientoDao) FactoryDao.buildDao("se.ReconocimientoDao");
        reconocimientoDao.insert(reconocimiento);*/
        
        /*ReconocimientoDaoHibernate reconocimientoDao = new ReconocimientoDaoHibernate();
        List<Reconocimiento> reconocimientos = null;
        reconocimientos = reconocimientoDao.listarxFichaEscalafonaria(1);
        for(int i=0; i<reconocimientos.size();i++)
           JOptionPane.showMessageDialog(null, i+ ": "+ reconocimientos.get(i).getNumRes());*/
        
        /*EstudioPostgrado estudioPostgrado= new EstudioPostgrado(new FichaEscalafonaria(1), "55555", new Date(), new Date(), new Date(), "instituto", 1, new Date(), 'A');
        EstudioPostgradoDao estudioPostgradoDao = (EstudioPostgradoDao) FactoryDao.buildDao("se.EstudioPostgradoDao");
        estudioPostgradoDao.insert(estudioPostgrado);*/
        
       /* EstudioPostgradoDaoHibernate estudioPostgradoDao = new EstudioPostgradoDaoHibernate();
        List<EstudioPostgrado> estudiosPostgrado = null;
        estudiosPostgrado = estudioPostgradoDao.listarxFichaEscalafonaria(1);
        for(int i=0; i<estudiosPostgrado.size();i++)
           JOptionPane.showMessageDialog(null, i+ ": "+ estudiosPostgrado.get(i).getNumRes());*/
       
        /*Demerito demerito= new Demerito(new FichaEscalafonaria(1), "55555", "gggg", new Date(), true,  new Date(), new Date(), "controversia", 1, new Date(), 'A');
        DemeritoDao demeritoDao = (DemeritoDao) FactoryDao.buildDao("se.DemeritoDao");
        demeritoDao.insert(demerito);*/
        
        /*DemeritoDaoHibernate demeritoDao = new DemeritoDaoHibernate();
        List<Demerito> demeritos = null;
        demeritos = demeritoDao.listarxFichaEscalafonaria(1);
        for(int i=0; i<demeritos.size();i++)
           JOptionPane.showMessageDialog(null, i+ ": "+ demeritos.get(i).getFecIni());*/
        
        /*LegajoPersonal legajo = new LegajoPersonal(new FichaEscalafonaria(1));
        LegajoPersonalDao legajoDao = (LegajoPersonalDao) FactoryDao.buildDao("se.LegajoPersonalDao");
        legajoDao.insert(legajo);*/
        
        /*Capacitacion capacitacion = new Capacitacion(new FichaEscalafonaria(1), "nom", "tip", new Date(), 17, "lug", 1, new Date(), 'A');
        CapacitacionDao capacitacionDao = (CapacitacionDao) FactoryDao.buildDao("se.CapacitacionDao");
        capacitacionDao.insert(capacitacion);*/
        
        /*CapacitacionDaoHibernate capacitacionDao = new CapacitacionDaoHibernate();
        List<Capacitacion> capacitaciones = null;
        capacitaciones = capacitacionDao.listarxFichaEscalafonaria(1);
        for(int i=0; i<capacitaciones.size();i++)
           JOptionPane.showMessageDialog(null, i+ ": "+ capacitaciones.get(i).getNom());*/
        
        /*FichaEscalafonariaDaoHibernate dedao = new FichaEscalafonariaDaoHibernate();           
        FichaEscalafonaria de;
        de = dedao.buscarPorDNI("bbbbbbbb");
        JOptionPane.showMessageDialog(null,de);*/
        
        /*LegajoPersonalDaoHibernate legajoDao = new LegajoPersonalDaoHibernate();
        List<LegajoPersonal> legajos = null;
        legajos = legajoDao.listarxFichaEscalafonaria(10);
        for(int i=0; i<legajos.size();i++)
           JOptionPane.showMessageDialog(null, i+ ": "+ legajos.get(i).getNom());*/

        /*String pathInicial = System.getProperty("user.dir");
         JOptionPane.showMessageDialog(null, "path" + pathInicial);
         
        String path = pathInicial + File.separator + Sigesmed.COXTEXTO_PROYECTO_URL +  File.separator + Sigesmed.UBI_ARCHIVOS + "archivos/legajo_personal/" + "6_legajo_1.pdf";
        File archivo = new File(path);
        if (archivo.exists()){
            JOptionPane.showMessageDialog(null, "El archivo existe");
        } else {
            JOptionPane.showMessageDialog(null, "El archivo no existe");
        }*/
        
        /*DesplazamientoDaoHibernate desDao = new DesplazamientoDaoHibernate();           
        List<Desplazamiento> des = null;
        des = desDao.listarxOrganizacion(6);
        for(int i=0; i<des.size();i++)
           JOptionPane.showMessageDialog(null, i+ ": "+ des.get(i).getNumRes());*/
        
        /*ParientesDaoHibernate paDao = new ParientesDaoHibernate();
        JSONArray respuesta = null;
        respuesta = paDao.numeroHijosxTrabajador(6);*/
        
        /*DemeritoDaoHibernate demeritoDao = new DemeritoDaoHibernate();
        JSONObject respuesta = null;
        respuesta = demeritoDao.contarxOrganizacion(6);*/
        
        /*ReconocimientoDaoHibernate recDao = new ReconocimientoDaoHibernate();
        JSONObject respuesta = null;
        respuesta = recDao.contarxOrganizacion(6);*/
        
        /*TrabajadorDaoHibernate tradao = new TrabajadorDaoHibernate();           
        Trabajador t = null;
        t = tradao.buscarPorPerId(39);
        System.err.println(t.getFecIng());*/
        //JOptionPane.showMessageDialog(null,de.getTrabajador().getPersona().getApeMat())
        /*FichaEscalafonariaDao feDao = (FichaEscalafonariaDao) FactoryDao.buildDao("se.FichaEscalafonariaDao");
        List<FichaEscalafonaria> datos = null;
        datos = feDao.ListarxOrganizacion(6);
        for(int i=0; i<datos.size();i++){
            if (datos.get(i).getTrabajador().getTieServ() == null){
                JOptionPane.showMessageDialog(null, "años: NULL" + datos.get(i).getTrabajador().getTieServ() );

            }else{
                JOptionPane.showMessageDialog(null, "años: NO NULL"  + datos.get(i).getTrabajador().getTieServ());
            }
        }*/
        
        /*DireccionDao dirDao = (DireccionDao) FactoryDao.buildDao("se.DireccionDao");
        List<Direccion> datos = null;
        datos = dirDao.listarxPersona(1);
        for(int i=0; i<datos.size();i++){
                JOptionPane.showMessageDialog(null, datos.get(i));
            
        }*/
        
        FichaEscalafonariaDao fichaDao = (FichaEscalafonariaDao) FactoryDao.buildDao("se.FichaEscalafonariaDao");
        FichaEscalafonaria datos = null;
        datos = fichaDao.buscarPorUsuId(7);
        System.out.println(datos);
            
    }
}
      

