package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.matricula_individual.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.matricula_individual.v1.tx.BuscarEstudiantePropioTx;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.matricula_individual.v1.tx.BuscarEstudiantesMatriculadosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.matricula_individual.v1.tx.BuscarMatriculaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.matricula_individual.v1.tx.CambioSeccionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.matricula_individual.v1.tx.EliminarMatriculaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.matricula_individual.v1.tx.GenerarFichaMatriculaExcelTx;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.matricula_individual.v1.tx.GenerarGradosySeccionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.matricula_individual.v1.tx.GenerarMatriculaAutomaticaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.matricula_individual.v1.tx.ListarGradosySeccionesTx;

public class ComponentRegister implements IComponentRegister {

    @Override

    public WebComponent createComponent() {

        WebComponent UComponent = new WebComponent(Sigesmed.SUBMODULO_MATRICULA_INSTITUCIONAL);
        UComponent.setName("matriculaIndividual");
        UComponent.setVersion(1);
        UComponent.addTransactionGET("buscarEstudiantesMatriculados", BuscarEstudiantesMatriculadosTx.class);
        UComponent.addTransactionGET("buscarMatricula", BuscarMatriculaTx.class);
        UComponent.addTransactionGET("generarGradosySeccion", GenerarGradosySeccionTx.class);
        UComponent.addTransactionGET("listarGradosySecciones", ListarGradosySeccionesTx.class);
        UComponent.addTransactionPOST("generarMatriculaAutomatica", GenerarMatriculaAutomaticaTx.class);
        UComponent.addTransactionDELETE("eliminarMatricula", EliminarMatriculaTx.class);
        UComponent.addTransactionGET("generarFichaMatriculaExcel", GenerarFichaMatriculaExcelTx.class);
        UComponent.addTransactionPOST("cambioSeccion", CambioSeccionTx.class);
        UComponent.addTransactionGET("buscarEstudiantePropio", BuscarEstudiantePropioTx.class);

        return UComponent;
    }
}
