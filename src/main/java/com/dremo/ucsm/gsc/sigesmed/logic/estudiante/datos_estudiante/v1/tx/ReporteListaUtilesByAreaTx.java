/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.estudiante.datos_estudiante.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mpf.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Seccion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.ListaArticulo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.ListaUtiles;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import com.dremo.ucsm.gsc.sigesmed.util.GCell;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Carlos
 */
public class ReporteListaUtilesByAreaTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr) {        
        
        Integer org;
        JSONObject requestData = (JSONObject)wr.getData();
        String docente="";
        String nivel="";
        String grado_="";
        String seccion_="";
        Grado grado=null;
        Seccion seccion=null;
        AreaCurricular area=null;
        String area_="";
        try{
           org=requestData.getInt("organizacionID");
           docente=requestData.getString("docente");
           Integer areaId = requestData.getInt("areaID");
           area_=requestData.getString("area");
           JSONObject matricula=requestData.getJSONObject("matricula");
           Integer gradoId = matricula.getInt("gradoId");
           int seccionId = matricula.getInt("seccionId");
           grado_=matricula.getString("grado");
           seccion_=matricula.getString("seccion");
           nivel=matricula.getString("nivel");
           
           grado=new Grado(gradoId);
           seccion=new Seccion((char)seccionId);
           area=new AreaCurricular(areaId);
        }catch(Exception e){
            System.out.println("No se pudo verificar los datos \n"+e);
            return WebResponse.crearWebResponseError("No se pudo verificar los datos ", e.getMessage() );
        }
       
        OrganizacionDao organizacionDao= (OrganizacionDao)FactoryDao.buildDao("OrganizacionDao");
        Organizacion organizacion=null;
        ListaUtiles lista = null;
        EstudianteDao estudianteDao = (EstudianteDao)FactoryDao.buildDao("mpf.EstudianteDao");
        
        try{
            organizacion=organizacionDao.load(Organizacion.class, org);
            lista=estudianteDao.listarUtilesByArea(grado,seccion,area,organizacion);
        }catch(Exception e){
            System.out.println("No se pudo listar la Lista de Utiles \n"+e);
            return WebResponse.crearWebResponseError("No se pudo listar la Lista de Utiles ", e.getMessage() );
        }
        
        if(lista==null || lista.getListas().size()==0)
        {
             return WebResponse.crearWebResponseError("No hay lista de Articulos" );
        }
        
        //Creando el reporte....        
        Mitext m = null;        
        try {
            m = new Mitext(false);
            m.agregarTitulo("LISTA DE UTILES");
            m.agregarParrafo("");
            m.agregarParrafo("");
        } catch (Exception ex) {
            System.out.println("No se pudo crear el documento \n"+ex);
            Logger.getLogger(ReporteListaUtilesByAreaTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        float[] columnWidthsD={5,6,3};
        Table tabla = new Table(columnWidthsD);
        tabla.setWidthPercent(100);
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("INSTITUCION EDUCATIVA").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+organizacion.getNom()).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));

        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("COD").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+organizacion.getCod()).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));

        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("DIRECCION").setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+organizacion.getDir()).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));

        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("NIVEL").setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+nivel).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("GRADO").setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+grado_+" "+seccion_).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("AREA").setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+area_).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("DOCENTE").setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+docente).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
       
        
         
        float[] columnWidthsTit={2,8,1};
        Table tablaTit = new Table(columnWidthsTit);
        tablaTit.setWidthPercent(100);
        tablaTit.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).setBackgroundColor(Color.LIGHT_GRAY).add(new Paragraph("CANT.").setFontSize(10).setTextAlignment(TextAlignment.CENTER)));
        tablaTit.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).setBackgroundColor(Color.LIGHT_GRAY).add(new Paragraph("ARTICULO").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
        tablaTit.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        
        for(ListaArticulo la:lista.getListas())
        {
            tablaTit.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(la.getCanArt()+"").setFontSize(10).setTextAlignment(TextAlignment.CENTER)));
            tablaTit.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(la.getDesArt()).setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
            tablaTit.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        }
        
        float[] columnWidthsFoo ={1,8};
        Table tablafooter = new Table(columnWidthsFoo);
        tablafooter.setWidthPercent(100);
        tablafooter.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("Nota:").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
        tablafooter.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("Toda la lista de utiles puede ser adquirida en la libreria de la Institucion Educativa").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));


        m.agregarTabla(tabla);
        m.agregarParrafo("");
        m.agregarParrafo("");
        m.agregarTabla(tablaTit);
        m.agregarParrafo("");
        m.agregarParrafo("");
        m.agregarTabla(tablafooter);
        m.cerrarDocumento();
       
                 
        JSONArray miArray = new JSONArray();
        JSONObject oResponse = new JSONObject();        
        oResponse.put("datareporte",m.encodeToBase64());
        miArray.put(oResponse);                       
        
        return WebResponse.crearWebResponseExito("Se genero el reporte correctamente",miArray);        
        
    }
    
    
}