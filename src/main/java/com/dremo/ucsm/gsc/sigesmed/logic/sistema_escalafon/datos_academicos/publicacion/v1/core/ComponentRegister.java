/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.publicacion.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.publicacion.v1.tx.ActualizarPublicacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.publicacion.v1.tx.AgregarPublicacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.publicacion.v1.tx.EliminarPublicacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.publicacion.v1.tx.ListarPublicacionesTx;

/**
 *
 * @author Yemi
 */
public class ComponentRegister implements IComponentRegister{

    @Override
    public WebComponent createComponent() {
        WebComponent seComponent = new WebComponent(Sigesmed.MODULO_ESCALAFON);

        seComponent.setName("publicacion");
        seComponent.setVersion(1);
        
        seComponent.addTransactionGET("listarPublicaciones", ListarPublicacionesTx.class);
	seComponent.addTransactionPOST("agregarPublicacion", AgregarPublicacionTx.class);
        seComponent.addTransactionPOST("actualizarPublicacion", ActualizarPublicacionTx.class);
        seComponent.addTransactionDELETE("eliminarPublicacion", EliminarPublicacionTx.class);
        return seComponent;
    }
    
}
