/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.estudiante.datos_estudiante.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.PlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mpf.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Periodo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanNivel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.AreaModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.GradoIeEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.Matricula;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

/**
 *
 * @author HernanF
 */
public class ListarDetalleNotasTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        Integer org;
        Long matriculaId;
        JSONObject requestData = (JSONObject) wr.getData();

        try {
            org = requestData.getInt("organizacionID");
            matriculaId = requestData.getLong("matriculaID");

        } catch (Exception e) {
            System.out.println("No se pudo verificar los datos \n" + e);
            return WebResponse.crearWebResponseError("No se pudo verificar los datos ", e.getMessage());
        }
        EstudianteDao estudianteDao = (EstudianteDao) FactoryDao.buildDao("mpf.EstudianteDao");
        PlanEstudiosDao planDao = (PlanEstudiosDao)FactoryDao.buildDao("mech.PlanEstudiosDao");
        GradoIeEstudiante gradoIee = null;
        List<AreaModel> areas = null;
        List<PlanNivel> niveles = null;
        List<Periodo> periodos = null;
        Map<String, int[]> notasAreaPeriodo = new HashMap<String, int[]>();
        Matricula estudiante = null;
        int numeroPeriodos = 4;
        try {
            gradoIee = estudianteDao.getGradoIeeActual(new Matricula(matriculaId));
            estudiante = estudianteDao.getDatosMatriculaAndPlan(matriculaId);
            niveles = planDao.listarNivelesPorPlanEstudios(estudiante.getPlanNivel().getPlaEstId());
            periodos = planDao.listarPeriodos();
            for (Periodo periodo : periodos) {
                if (periodo.getPerId() == niveles.get(0).getPerId()) {
                    numeroPeriodos = 9/periodo.getFacPer();
                    break;
                }
            }
            areas = estudianteDao.buscarAreasByPlanEstudios(estudiante.getPlanNivel().getPlaEstId(), estudiante.getGradoId(),estudiante.getSeccionId());
            for (AreaModel area: areas) {
                int []notasPeriodo = new int[numeroPeriodos];
                for (int i = 1; i <= numeroPeriodos; i++) {
                    String nota = estudianteDao.getNotaAreaByPeriodo(gradoIee.getGraOrgEstId(), area.getAreaID(), i);
                    nota = nota == null ? "0" : nota;
                    notasPeriodo[i-1] = Integer.parseInt(nota.replaceAll("\\s+",""));
                }
                notasAreaPeriodo.put(area.getArea(), notasPeriodo);
            }
            
        } catch (Exception e) {
            System.out.println("No se pudo listar las Notas \n" + e);
            return WebResponse.crearWebResponseError("No se pudo listar las Notas ", e.getMessage());
        }
        
        JSONObject detalleNotas = new JSONObject();
        for (Map.Entry<String, int[]> entry : notasAreaPeriodo.entrySet()) {
            JSONObject temp = new JSONObject();
            for (int i = 0; i < entry.getValue().length; i++) {
                temp.put(i+"", entry.getValue()[i]);
            }
            detalleNotas.put(entry.getKey(), temp);
        }
        
        return WebResponse.crearWebResponseExito("Se listo correctamente", detalleNotas);
    }
    
}
