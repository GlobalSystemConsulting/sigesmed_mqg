/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.ConfiguracionNotaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.NotaEvaluacionIndicadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.RegistroAuxiliarCompetenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.RegistroAuxiliarDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.DocenteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.CompetenciaAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.TareaEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ConfiguracionNota;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.RegistroAuxiliar;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.RegistroAuxiliarCompetencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.GradoIEEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.PeriodosPlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadoresSesionAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.TareaIndicador;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.ma.notas.v1.tx.RegistrarPromedioCompetenciaTx;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author PC03
 */
public class RegistrarPromedioCompetenciasxEstudianteTx implements ITransaction {

    private List<ConfiguracionNota> config_docente = null;
    private static final Logger logger = Logger.getLogger(RegistrarPromedioCompetenciaTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();

        int idOrg = data.getInt("idOrg");
        int idArea = data.optInt("areCurId");
        int idSession = data.optInt("sesId");
        char tipPer = data.getString("tipPer").charAt(0);
        int numPer = data.getInt("numPer");
        char tipNot = data.optString("tip", "N").charAt(0);
        int idGra = data.getInt("graId");
        int idUsr = data.getInt("usuId");
        char idSeccion = data.optString("idSeccion").charAt(0);
        int idAlumno = data.getInt("idAlumno");
        int idPlaEst = data.getInt("idPlaEst");
        int tarIdMM = data.getInt("tarIdMM");

        return registrarNotas(idSession, tipPer, numPer, idUsr, idArea, tipNot, idGra, idOrg, idSeccion, idAlumno, idPlaEst, tarIdMM);
    }

    private WebResponse registrarNotas(int idSession, char tipPer, int numPer, int idUsr, int idArea, char tipNot, int idGra, int idOrg, char idSeccion, int idAlumno, int idPlaEst, int tarIdMM) {
        try {
            RegistroAuxiliarDao regisDao = (RegistroAuxiliarDao) FactoryDao.buildDao("ma.RegistroAuxiliarDao");
            RegistroAuxiliarCompetenciaDao regisAuxComDao = (RegistroAuxiliarCompetenciaDao) FactoryDao.buildDao("ma.RegistroAuxiliarCompetenciaDao");
            CompetenciaAprendizajeDao compDao = (CompetenciaAprendizajeDao) FactoryDao.buildDao("maestro.plan.CompetenciaAprendizajeDao");
            NotaEvaluacionIndicadorDao notaDao = (NotaEvaluacionIndicadorDao) FactoryDao.buildDao("ma.NotaEvaluacionIndicadorDao");
            DocenteDao docDao = (DocenteDao) FactoryDao.buildDao("maestro.DocenteDao");
            EstudianteDao estDao = (EstudianteDao) FactoryDao.buildDao("maestro.EstudianteDao");
            TareaEscolarDao tareaDao = (TareaEscolarDao) FactoryDao.buildDao("web.TareaEscolarDao");
            ConfiguracionNotaDao configNot = (ConfiguracionNotaDao) FactoryDao.buildDao("ma.ConfiguracionNotaDao");
            config_docente = configNot.listarConfiguracionDocente(idUsr, idOrg);
            List<TareaIndicador> indicadores = tareaDao.obtenerIndicadoresTarea(tarIdMM);
            GradoIEEstudiante graOrgEst = tareaDao.obtenerGradoIEEstudiante(idOrg, idGra, idSeccion, idAlumno);
            System.out.println(graOrgEst);
            
            String mensaje = "Se registro el promedio de las competencias exitosamente";
            Docente docente = docDao.buscarDocentePorUsuario(idUsr);
            PeriodosPlanEstudios perPlaEst = regisDao.buscarPeriodoPorAtributos(idPlaEst, tipPer, numPer);
            for (int j = 0; j < indicadores.size(); j++) {
                int idInd = indicadores.get(j).getInd_apr_id();
                IndicadoresSesionAprendizaje indSesApr = notaDao.obtenerCompetenciaDeIndicador(idInd);
                int comId = indSesApr.getCompetencia().getComId();
                List<IndicadoresSesionAprendizaje> indicadoresAsociados = notaDao.obtenerIndicadoresDeCompetencia(comId);

                Double promedio = 0.0;
                for (IndicadoresSesionAprendizaje indAso : indicadoresAsociados) {
                    RegistroAuxiliar notFinInd = regisDao.buscarNotaEstudiante(indAso.getIndicador().getIndAprId(), docente.getDoc_id(), perPlaEst.getPerPlaEstId(), idArea, graOrgEst.getGraOrgEstId());
                    if (notFinInd == null) {
                        continue; //Si no encuentra el registro auxiliar el docente aun no uso dicho indicador
                    }
                    promedio += Double.parseDouble(notFinInd.getNotInd());
                }

                promedio = promedio / indicadoresAsociados.size();
                promedio = (Math.round(promedio * 10.0) / 10.0);
                String nota = "";
                String nota_lit = obtenerNotaLiteral(promedio);
                nota = String.valueOf(promedio);

                RegistroAuxiliarCompetencia notComp = new RegistroAuxiliarCompetencia();
                if (!regisDao.buscarNotaFinalCompetencia(comId, idArea, perPlaEst.getPerPlaEstId(), graOrgEst.getGraOrgEstId())) {
                    notComp.setNota(nota);
                    notComp.setNot_com_lit(nota_lit);
                    notComp.setComp(compDao.buscarCompetenciPorCodigo(comId));
                    notComp.setPeriodoPlanEstudios(perPlaEst);
                    notComp.setAre_cur_id(docDao.buscarAreaPorId(idArea).getAreCurId());
                    notComp.setGrad_ie_est_id(graOrgEst.getGraOrgEstId());
                    notComp.setPer_eva_id(perPlaEst.getPerPlaEstId());
                    notComp.setUsuMod(idUsr);
                    regisAuxComDao.insert(notComp);
                } else {
                    notComp = regisDao.buscarNotaCompetenciaEsp(comId, idArea, perPlaEst.getPerPlaEstId(), graOrgEst.getGraOrgEstId());
                    notComp.setNota(nota);
                    regisAuxComDao.update(notComp);
                }
            }
            return WebResponse.crearWebResponseExito(mensaje);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "registrarPromedioCompetencia", e);
            return WebResponse.crearWebResponseError("No se pudo registrar el promedio de la competencia");
        }
    }

    public String obtenerNotaLiteral(Double not) {
        for (ConfiguracionNota conf : config_docente) {
            if (not >= conf.getConf_not_min() && not <= conf.getConf_not_max()) {
                return conf.getConf_cod();
            }
        }
        return "";
    }
}
