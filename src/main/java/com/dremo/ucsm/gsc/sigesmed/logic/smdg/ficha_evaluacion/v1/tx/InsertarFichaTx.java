/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.ficha_evaluacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.FichaDetalleDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.FichaEvaluacionDocumentosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.ItemFile;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.FichaDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.FichaEvaluacionDocumentos;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaGrupo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaIndicadores;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.item_file.v1.tx.RegistrarItemTx;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class InsertarFichaTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formatterOther= new SimpleDateFormat("dd/MMM/yy HH:mm:ss");
        String fechaNew  = formatterOther.format(new Date());
        
        JSONObject requestData = (JSONObject)wr.getData();
        JSONArray grupos = requestData.getJSONArray("grupos");
        
        int total = requestData.optInt("total");
        int iteide = requestData.optInt("iteide");        
        Date fevfec = null;
        Date fecvfeMod=null;
        try {
            fevfec = formatter.parse(requestData.optString("fevfec"));
            fecvfeMod= formatterOther.parse(fechaNew);
        } catch (ParseException ex) {
            Logger.getLogger(RegistrarItemTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        int feveva = requestData.optInt("feveva");
        int fevesp = requestData.optInt("fevesp");
        int plaid = requestData.optInt("plaide");
        
        FichaEvaluacionDocumentos ficha = new FichaEvaluacionDocumentos(fevfec, total, feveva, fevesp, new ItemFile(iteide), plaid);
        ficha.setFecMod(fecvfeMod);
        FichaEvaluacionDocumentosDao fichaDao = (FichaEvaluacionDocumentosDao)FactoryDao.buildDao("smdg.FichaEvaluacionDocumentosDao");
        
        fichaDao.insert(ficha);
        FichaDetalle detalle = null;        
        FichaDetalleDao detalleDao = (FichaDetalleDao)FactoryDao.buildDao("smdg.FichaDetalleDao");
//        PlantillaGrupoDao grupoDao = (PlantillaGrupoDao)FactoryDao.buildDao("smdg.PlantillaGrupoDao");
        
        for(int i = 0; i < grupos.length(); ++i){
            JSONArray indicadores = grupos.getJSONObject(i).getJSONArray("indicadores");
            for(int j = 0; j < indicadores.length(); ++j){
                detalle = new FichaDetalle(ficha.getFevDocId(), indicadores.getJSONObject(j).optInt("indId"), indicadores.getJSONObject(j).optInt("punto"));
                detalleDao.insert(detalle);
            }
        }
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();            
//        oResponse.put("plaId",ficha.getPfiInsId());
        
        return WebResponse.crearWebResponseExito("El registro la ficha", oResponse);
    }
}
