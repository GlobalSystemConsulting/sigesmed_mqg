/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Parientes;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public interface ParientesDao extends GenericDao<Parientes>{
    public List<Parientes> listarxTrabajador(int traId);
    public JSONArray numeroHijosxTrabajador(int orgId);
    public Parientes buscarParientePorId(int perId, int parId);
    
}
