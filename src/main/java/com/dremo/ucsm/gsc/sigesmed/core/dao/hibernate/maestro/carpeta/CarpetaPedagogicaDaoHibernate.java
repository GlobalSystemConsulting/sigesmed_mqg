package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.maestro.carpeta;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.carpeta.CarpetaPedagogicaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.CarpetaPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.SeccionCarpetaPedagogica;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by Administrador on 10/10/2016.
 */
public class CarpetaPedagogicaDaoHibernate extends GenericDaoHibernate<CarpetaPedagogica> implements CarpetaPedagogicaDao{
    @Override
    public CarpetaPedagogica buscarCarpetaPorEtapa(int eta) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(CarpetaPedagogica.class)
                    .add(Restrictions.eq("eta",eta));
            return (CarpetaPedagogica) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public CarpetaPedagogica buscarCarpetaPorId(int eta) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(CarpetaPedagogica.class)
                    .add(Restrictions.eq("carDigId",eta));
            return (CarpetaPedagogica) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public SeccionCarpetaPedagogica buscarSeccionPorId(int idSeccion) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(SeccionCarpetaPedagogica.class)
                    .add(Restrictions.eq("secCarPedId",idSeccion));
            return (SeccionCarpetaPedagogica) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public void registrarSeccionCarpeta(SeccionCarpetaPedagogica seccion) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            session.save(seccion);
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public void eliminarSeccionCarpeta(int idSeccion) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            String hql = "UPDATE SeccionCarpetaMaestro SET estReg='E' WHERE secCarPedId =:idSecc";
            Query query = session.createQuery(hql);
            query.setInteger("idSecc",idSeccion);
            query.executeUpdate();
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public void editarSeccionCarpeta(SeccionCarpetaPedagogica seccion) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            session.update(seccion);
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<SeccionCarpetaPedagogica> listarSeccionesCarpeta(int idCarpeta) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT s FROM SeccionCarpetaMaestro s LEFT JOIN FETCH s.contenidos c WHERE s.carpeta.carDigId =:idCar AND (c.estReg='A' OR c.estReg IS NULL) AND s.estReg='A'";
            Query query = session.createQuery(hql);
            query.setInteger("idCar",idCarpeta);
            query.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public CarpetaPedagogica buscarPorSeccionId(int seccionId) {
        CarpetaPedagogica carpeta = null;        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
                
        try{   
            String hql = "SELECT s.carpeta FROM SeccionCarpetaMaestro s "
                    + "WHERE s.estReg='A' AND s.secCarPedId=:secId";     
            Query query = session.createQuery(hql);  
            query.setInteger("secId",seccionId);
            query.setMaxResults(1);
            carpeta = (CarpetaPedagogica)query.uniqueResult();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo encontrar la carpeta pedagógica. Error: "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar la carpeta pedagógica. Error: "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return carpeta;
    }

    @Override
    public List<Organizacion> listarIEs() {
        List<Organizacion> insEdu = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            String hql = "SELECT o FROM Organizacion as o "
                    + "join fetch o.tipoOrganizacion to "
                    + "WHERE to.cod=:tipoOrg"; 
            Query query = session.createQuery(hql);
            query.setString("tipoOrg","IE");            
            insEdu = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudieron listar las instituciones educativas. Error: "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudieron listar las instituciones educativas. Error: "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return insEdu;
    }
}
