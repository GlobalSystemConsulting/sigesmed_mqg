package com.dremo.ucsm.gsc.sigesmed.logic.maestro.carpeta.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.DocenteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.carpeta.CarpetaPedagogicaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.carpeta.ContenidoSeccionCarpetaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.carpeta.DocenteCarpetaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.CarpetaPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.ContenidoSeccionCarpeta;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.DocenteCarpeta;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.SeccionCarpetaPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 29/12/2016.
 */
public class RegistrarContenidoCarpetaTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(RegistrarContenidoCarpetaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idSeccion  = data.getInt("id");
        String nom = data.getString("nom").toUpperCase();
        int tipUsu = data.getInt("tip");
        return registrarContenido(idSeccion, nom, tipUsu);
    }

    private WebResponse registrarContenido(int idSeccion, String nom, int tipUsu) {
        try{
            CarpetaPedagogicaDao carpetaDao = (CarpetaPedagogicaDao) FactoryDao.buildDao("maestro.carpeta.CarpetaPedagogicaDao");
            ContenidoSeccionCarpetaDao contDao = (ContenidoSeccionCarpetaDao) FactoryDao.buildDao("maestro.carpeta.ContenidoSeccionCarpetaDao");

            SeccionCarpetaPedagogica seccion = carpetaDao.buscarSeccionPorId(idSeccion);

            ContenidoSeccionCarpeta contenido = new ContenidoSeccionCarpeta(nom,tipUsu);
            contenido.setSeccion(seccion);

            contDao.insert(contenido);
            
            generarCarpetasPedagogicas(idSeccion, nom);
            return WebResponse.crearWebResponseExito("Se registro correctamente",new JSONObject().put("id",contenido.getConSecCarPedId()));
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarSeccion",e);
            return WebResponse.crearWebResponseError("No se puede registrar la seccion");
        }
    }
    
    private WebResponse generarCarpetasPedagogicas(int seccionId, String descripcion){
        System.out.println("Entra a generar carpetas pedagogicas");
        try{
            CarpetaPedagogicaDao carpetaDao = (CarpetaPedagogicaDao) FactoryDao.buildDao("maestro.carpeta.CarpetaPedagogicaDao");
            DocenteDao docenteDao = (DocenteDao) FactoryDao.buildDao("maestro.DocenteDao");
            DocenteCarpetaDao docenteCarDao = (DocenteCarpetaDao) FactoryDao.buildDao("maestro.carpeta.DocenteCarpetaDao");
            
            CarpetaPedagogica carpeta = carpetaDao.buscarPorSeccionId(seccionId);
            List<Organizacion> IEs = carpetaDao.listarIEs();
            
            
            for(Organizacion ie:IEs){
                System.out.println("Aqui");
                List<Docente> docentesIE = docenteDao.buscarDocentesIE(ie.getOrgId());
                System.out.println(docentesIE.size());
                for(Docente d:docentesIE){
                    System.out.println(d);
                }
                for(Docente docente:docentesIE){
                    DocenteCarpeta docCar = new DocenteCarpeta();
                    docCar.setDes(descripcion);
                    docCar.setOrganizacion(ie);
                    docCar.setDocente(docente);
                    docCar.setCarpeta(carpeta);
                    docCar.setEstAva(3);
                    docCar.setEstReg('A');
                    
                    System.out.println(docCar);
                    docenteCarDao.insert(docCar);
                }
            }
            return WebResponse.crearWebResponseExito("Se generaron correctamente las carpetas pedagógicas");
        }catch (Exception e){
            logger.log(Level.SEVERE,"generarCarpetaPedagogica",e);
            return WebResponse.crearWebResponseError("No se pueden generar las carpetas pedagógicas");
        }
    }

    
    
}
