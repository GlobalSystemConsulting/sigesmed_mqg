/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate;

import com.dremo.ucsm.gsc.sigesmed.core.dao.TipoOrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TipoOrganizacion;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
/**
 *
 * @author abel
 */
public class TipoOrganizacionDaoHibernate extends GenericDaoHibernate<TipoOrganizacion> implements TipoOrganizacionDao {
    @Override
    public List<TipoOrganizacion> listarConRoles() {
        List<TipoOrganizacion> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar Modulos con sus Submodulos
            String hql = "SELECT DISTINCT to FROM TipoOrganizacion to LEFT JOIN FETCH to.roles r WHERE to.estReg='A' and ( r.estReg!='E' or r IS NULL ) ORDER BY to.tipOrgId";
                
            Query query = session.createQuery(hql);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los tipos de Organizaciones \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los tipos de Organizacion \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    
    @Override
    public void eliminarRoles(int tipOrgID) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            String hql = "DELETE FROM tipo_organizacion_rol WHERE tip_org_id="+tipOrgID;
            SQLQuery query = session.createSQLQuery(hql);
            query.executeUpdate();
            miTx.commit();
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo Eliminar los roles del tipo de organizacion \n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Eliminar los roles del tipo de organizacion \n "+ e.getMessage());
        }
        finally{
            session.close();
        }
    }

}
