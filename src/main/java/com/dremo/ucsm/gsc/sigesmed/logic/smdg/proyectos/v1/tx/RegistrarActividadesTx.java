/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.proyectos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.ProyectoActividadesDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaFichaInstitucional;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.ProyectoActividades;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.Proyectos;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.TipoGrupo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class RegistrarActividadesTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formatter2= new SimpleDateFormat("dd/MMM/yy HH:mm:ss");
         
        JSONObject requestData = (JSONObject)wr.getData();
        
        int proid = requestData.getInt("proid");
        int pacres = requestData.getInt("pacres");
        String pacdes = requestData.optString("pacdes");
        int pacava = requestData.getInt("pacava");
        
        Date pacini = null;
        Date pacfin = null;
        Date pacfen=null;
        String fechaA  = formatter2.format(new Date());
                
        try {
            pacini = formatter.parse(requestData.optString("pacini"));
            pacfin = formatter.parse(requestData.optString("pacfin"));
            pacfen = formatter2.parse(fechaA);
            
        } catch (ParseException ex) {
            Logger.getLogger(RegistrarProyectosTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        ProyectoActividades actividad = new ProyectoActividades(pacdes, pacini, pacfin, pacres, pacava, new Proyectos(proid));;
        actividad.setFecMod(pacfen);       
        //Fin
                
        /*
        *  Parte para la operacion en la Base de Datos
        */
        ProyectoActividadesDao actividadDao = (ProyectoActividadesDao)FactoryDao.buildDao("smdg.ProyectoActividadesDao");
        
        try{
            actividadDao.insert(actividad);
        
        }catch(Exception e){
            System.out.println("No se pudo registrar\n"+e);
            return WebResponse.crearWebResponseError("No se pudo registrar", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("pacid",actividad.getPacId());
        //oResponse.put("fecha",actividad.getFecMod().toString());
        return WebResponse.crearWebResponseExito("El registro de la actividad se realizo correctamente", oResponse);
        //Fin
    }

}
