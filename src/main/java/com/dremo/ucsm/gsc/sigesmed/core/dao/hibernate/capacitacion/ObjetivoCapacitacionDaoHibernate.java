package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.ObjetivoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.ObjetivoCapacitacion;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;

public class ObjetivoCapacitacionDaoHibernate extends GenericDaoHibernate<ObjetivoCapacitacion> implements ObjetivoCapacitacionDao {
    private static final Logger logger = Logger.getLogger(ObjetivoCapacitacionDaoHibernate.class.getName());
    
    @Override
    public List<ObjetivoCapacitacion> listarObjetivosCursoCapacitacion(int curCapId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            CursoCapacitacion capacitacion = (CursoCapacitacion)session.get(CursoCapacitacion.class,curCapId);

            return new ArrayList<>(capacitacion.getObjetivosCapacitacion());
        } catch (Exception e){
            logger.log(Level.SEVERE,"listarObjetivosCursoCapacitacion",e);
            throw e;
        } finally {
            session.close();
        }
    } 

    @Override
    public ObjetivoCapacitacion buscarPorId(int idObj) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            ObjetivoCapacitacion objetivo = (ObjetivoCapacitacion)session.get(ObjetivoCapacitacion.class,idObj);

            return objetivo;
        } catch (Exception e){
            logger.log(Level.SEVERE,"buscarPorId",e);
            throw e;
        } finally {
            session.close();
        }
    }
}
