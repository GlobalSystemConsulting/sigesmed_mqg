/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.formacion_educativa.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FormacionEducativaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FormacionEducativa;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Yemi
 */
public class ActualizarFormacionEducativaTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ActualizarFormacionEducativaTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try{
     
            DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
            
            JSONObject requestData = (JSONObject)wr.getData();
        
            Integer forEduId = requestData.getInt("forEduId");
            Character tip = requestData.optString("tip").charAt(0);
            String niv = requestData.optString("niv");
            String numTit = requestData.optString("numTit");
            String esp = requestData.optString("esp");
            Boolean estCon = requestData.getBoolean("estCon");
            Date fecExp = requestData.optString("fecExp").equals("")?null:sdi.parse(requestData.optString("fecExp").substring(0, 10));
            String cenEst = requestData.optString("cenEst");       
            
            return actualizarForEdu(forEduId, niv, tip, numTit, esp, estCon, fecExp, cenEst);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar formacion educativa",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    
    private WebResponse actualizarForEdu(Integer forEduId, String niv, Character tip, String numTit, String esp, Boolean estCon, Date fecExp, String cenEst) {
        try{
            FormacionEducativaDao forEduDao = (FormacionEducativaDao)FactoryDao.buildDao("se.FormacionEducativaDao");        
            FormacionEducativa forEdu = forEduDao.buscarForEduPorId(forEduId);

            forEdu.setNivAca(niv);
            forEdu.setTipFor(tip);
            forEdu.setNumTit(numTit);
            forEdu.setEspAca(esp);
            forEdu.setEstCon(estCon);
            forEdu.setFecExp(fecExp);
            forEdu.setCenEst(cenEst);
            
            forEduDao.update(forEdu);

            JSONObject oResponse = new JSONObject();
            DateFormat sdo = new SimpleDateFormat("yyyy-MM-dd");
            oResponse.put("forEduId", forEdu.getForEduId());
            oResponse.put("tip", forEdu.getTipFor());
            oResponse.put("niv", forEdu.getNivAca());
            oResponse.put("numTit", forEdu.getNumTit()==null?"":forEdu.getNumTit());
            oResponse.put("esp", forEdu.getEspAca()==null?"":forEdu.getEspAca());
            oResponse.put("estCon", forEdu.getEstCon()==null?"":forEdu.getEstCon());
            oResponse.put("fecExp", forEdu.getFecExp()==null?"":sdo.format(forEdu.getFecExp()));
            oResponse.put("cenEst", forEdu.getCenEst()==null?"":forEdu.getCenEst());
            return WebResponse.crearWebResponseExito("Formacion Educativa actualizada exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarFormacionEducativa",e);
            return WebResponse.crearWebResponseError("Error, la foracion educativa no fue actualizada");
        }
    } 
    
}
