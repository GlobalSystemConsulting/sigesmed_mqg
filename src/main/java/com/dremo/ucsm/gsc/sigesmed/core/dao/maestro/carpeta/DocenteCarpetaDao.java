/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.carpeta;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.DocenteCarpeta;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.RutaContenidoCarpeta;
import java.util.List;

public interface DocenteCarpetaDao extends GenericDao<DocenteCarpeta>{
    DocenteCarpeta buscar(int carId, int orgId, int docId, String des);
    List<DocenteCarpeta> listarContenidosDocente(int orgId, int carId, int docId);
    RutaContenidoCarpeta buscarRutaConId(int docCarPedId);
    
}
