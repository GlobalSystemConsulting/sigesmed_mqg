/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.PlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanNivel;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Calendar;
import java.util.Date;
/**
 *
 * @author abel
 */
public class PersistenciaPlanNivelTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        PlanNivel nuevo = null;
        PlanEstudios PlanEstudiosAnterior = null;
        Date today = new Date();
        
        Calendar fecha = Calendar.getInstance();
        int año2 = fecha.get(Calendar.YEAR);
        año2 = año2 - 1;
        
        int orgID;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            
            int planNivelID = requestData.optInt("planNivelID");
            orgID = requestData.optInt("orgaID");
            int planID = requestData.getInt("planID");
            String descripcion = requestData.getString("descripcion");
            char periodoID = requestData.getString("periodoID").charAt(0);
            char turnoID = requestData.getString("turnoID").charAt(0);
            int jornadaID = requestData.getInt("jornadaID");
            
            nuevo = new PlanNivel(planNivelID,descripcion,planID,periodoID,turnoID,jornadaID,new Date(),wr.getIdUsuario(),'A');
            
            //nuevo.setMetas(metas);
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("datos incorrectos", e.getMessage() );
        }
        try{
         //   PlanEstudiosDao planEDao = (PlanEstudiosDao)FactoryDao.buildDao("mech.PlanEstudiosDao");
        //    PlanEstudiosAnterior = planEDao.buscarVigentePorOrganizacionFecha(orgID,año2);
         // PlanEstudiosAnterior.getNiveles();
            //PlanEstudiosAnterior 
                    
            PlanEstudiosDao planDao = (PlanEstudiosDao)FactoryDao.buildDao("mech.PlanEstudiosDao");
            planDao.mergeNivel(nuevo);
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo realizar la accion sobre el nivel", e.getMessage() );
        }
        //Fin
        
        JSONObject res = new JSONObject();
        res.put("planNivelID", ""+nuevo.getPlaNivId());
        
        return WebResponse.crearWebResponseExito("La accion sobre el nivel se realizo correctamente",res);
    }    
    
    
}

