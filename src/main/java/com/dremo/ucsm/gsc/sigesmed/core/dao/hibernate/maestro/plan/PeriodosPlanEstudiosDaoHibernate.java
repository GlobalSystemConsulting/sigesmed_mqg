package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.maestro.plan;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.PeriodosPlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.PeriodosPlanEstudios;

/**
 *
 * @author Jerson Herrera
 */
public class PeriodosPlanEstudiosDaoHibernate extends GenericDaoHibernate<PeriodosPlanEstudios> implements PeriodosPlanEstudiosDao{
    
}
