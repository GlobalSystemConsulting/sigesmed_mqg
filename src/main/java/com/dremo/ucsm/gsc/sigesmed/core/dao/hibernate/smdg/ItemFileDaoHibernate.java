/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.smdg;

import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.ItemFile;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Administrador
 */
public class ItemFileDaoHibernate extends GenericDaoHibernate<ItemFile> implements ItemFileDao{
    @Override
    public String buscarUltimoCodigo() {
        String codigo = null;
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        try{
//            //listar ultimo codigo de tramite
//            String hql = "SELECT a.apoId FROM Apoderado a ORDER BY a.apoId DESC ";
//            Query query = session.createQuery(hql);
//            query.setMaxResults(1);
//            codigo = ((String)query.uniqueResult());
//            
//            //solo cuando no hay ningun registro aun
//            if(codigo==null)
//                codigo = "0000";
//        
//        }catch(Exception e){
//            System.out.println("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());
//            throw new UnsupportedOperationException("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());            
//        }
//        finally{
//            session.close();
//        }
        return codigo;
    }
    @Override
    public List<ItemFile> ListarxOrganizacion(int org){
        List<ItemFile> itemfiles = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT i FROM com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.ItemFile i "
//                    + "join fetch i.trabajador t "
//                    + "join fetch t.persona p "
//                    + "join fetch t.traCar tc "
                    + "JOIN FETCH i.organizacion o "
                    + "WHERE (o.orgId = " + org + "  OR o.organizacionPadre.orgId="+org+") AND i.estReg='A' ORDER BY i.iteIde ";

            Query query = session.createQuery(hql);   
//            
//             
//            query.setParameter("p1", org);
            itemfiles = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo Listar los documentos o carpetas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los documentos o carpetas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return itemfiles;
    }
}
