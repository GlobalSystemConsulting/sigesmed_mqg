/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.Organizacion;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;


public class OrganizacionDaoHibernate extends GenericDaoHibernate<Organizacion> implements OrganizacionDao{

    @Override
    public List<Organizacion> listarInstitucionesxUgel(int ugelId) {
        List<Organizacion> ies = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT org from com.dremo.ucsm.gsm.sigesmed.core.entity.sma.Organizacion as org "
                    + "join fetch org.organizacionPadre as orgPad "
                    + "WHERE orgPad.orgId = " + ugelId;
            Query query = session.createQuery(hql);
            ies = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar las instituciones educativas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar las instituciones educativas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return ies;
    }

    @Override
    public Organizacion buscarPorId(int orgId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Organizacion org = (Organizacion)session.get(Organizacion.class, orgId);
        session.close();
        return org;
    }
    
}
