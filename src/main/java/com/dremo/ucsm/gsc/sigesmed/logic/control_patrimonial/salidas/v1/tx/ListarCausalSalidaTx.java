/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.salidas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.CausalSalidaDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.CausalSalida;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author harold
 */
public class ListarCausalSalidaTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        
        JSONArray responseData = new JSONArray();
        
        List<CausalSalida> cauSal = null;
        JSONObject requestData = (JSONObject)wr.getData();        
        
        try {
            
            CausalSalidaDAO cs_dao = (CausalSalidaDAO) FactoryDao.buildDao("scp.CausalSalidaDAO");
            cauSal = cs_dao.listarCausalSalida();

            for (CausalSalida causal : cauSal) {
                JSONObject oResponse = new JSONObject();
                oResponse.put("id_cau", causal.getCau_sal_id());
                oResponse.put("nom_cau", causal.getDes());
                responseData.put(oResponse);
            }
        
        } catch (Exception e) {
            
            System.out.println("No se pudo Listar las Causales de Salida\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar las Causales de Salida", e.getMessage() );
            
        }    
        
        return WebResponse.crearWebResponseExito("Se listo las causales correctamente",responseData); 
    }    
}
