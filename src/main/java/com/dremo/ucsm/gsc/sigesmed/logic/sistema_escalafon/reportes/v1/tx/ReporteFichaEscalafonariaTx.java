/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ParientesDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.AscensoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ColegiaturaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DemeritoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DesplazamientoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DireccionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.EstudioComplementarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.EstudioPostgradoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ExposicionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FichaEscalafonariaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FormacionEducativaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.PublicacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ReconocimientoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Ascenso;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Colegiatura;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Demerito;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Desplazamiento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Direccion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.EstudioComplementario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.EstudioPostgrado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Exposicion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FormacionEducativa;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Parientes;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Publicacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Reconocimiento;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import static com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx.Constantes.anios_estudio_reconocimiento;
import static com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx.Constantes.cargos_desempenados;
import static com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx.Constantes.conocimientos_informaticos;
import static com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx.Constantes.datos_familiares;
import static com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx.Constantes.estudios_especializacion;
import static com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx.Constantes.exposiciones_ponencias;
import static com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx.Constantes.formaciones_educativas;
import static com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx.Constantes.indice_datosPersonales;
import static com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx.Constantes.info_ascensos;
import static com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx.Constantes.info_bonificaciones;
import static com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx.Constantes.info_colegiaturas;
import static com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx.Constantes.info_demeritos;
import static com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx.Constantes.info_idiomas;
import static com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx.Constantes.info_publicaciones;
import static com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx.Constantes.meritos_felicitaciones_reconocimientos;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import static com.dremo.ucsm.gsc.sigesmed.util.JSONUtil.getUbigeoLocation;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.layout.element.Paragraph;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class ReporteFichaEscalafonariaTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ReporteFichaEscalafonariaTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {

        JSONObject data = (JSONObject) wr.getData();
        Integer ficEscId = data.optInt("ficEscId");
        Integer traId = data.optInt("traId");
        String perDni = data.optString("perDni");
        JSONArray seleccionados = data.optJSONArray("seleccionados");
        
        return crearReporte(ficEscId, traId, perDni, seleccionados);
    }

    private WebResponse crearReporte(Integer ficEscId, Integer traId, String perDni, JSONArray seleccionados) {
        try{
            String data = crearArchivo(ficEscId, traId, perDni, seleccionados);
            return WebResponse.crearWebResponseExito("", new JSONObject().put("file",data));
        }catch (Exception e){
            logger.log(Level.SEVERE,"crearReporte",e);
            return WebResponse.crearWebResponseError("No se puede crear el reporte");
        }
    }
    private String  crearArchivo(Integer ficEscId, Integer traId, String perDni, JSONArray seleccionados) throws Exception{
        System.out.println("Seleccionados: " + seleccionados); 
        
        FichaEscalafonariaDao  fichaEscalafonariaDao = (FichaEscalafonariaDao) FactoryDao.buildDao("se.FichaEscalafonariaDao");
        
        ParientesDao parientesDao = (ParientesDao) FactoryDao.buildDao("se.ParientesDao");
        FormacionEducativaDao formacionEducativaDao = (FormacionEducativaDao) FactoryDao.buildDao("se.FormacionEducativaDao");
        ColegiaturaDao colegiaturaDao = (ColegiaturaDao) FactoryDao.buildDao("se.ColegiaturaDao");
        EstudioComplementarioDao estudioComplementarioDao = (EstudioComplementarioDao) FactoryDao.buildDao("se.EstudioComplementarioDao");
        ExposicionDao exposicionDao = (ExposicionDao) FactoryDao.buildDao("se.ExposicionDao");
        PublicacionDao publicacionDao = (PublicacionDao) FactoryDao.buildDao("se.PublicacionDao");
        DesplazamientoDao desplazamientoDao = (DesplazamientoDao) FactoryDao.buildDao("se.DesplazamientoDao");
        ReconocimientoDao reconocimientoDao = (ReconocimientoDao) FactoryDao.buildDao("se.ReconocimientoDao");
        DemeritoDao demeritoDao = (DemeritoDao) FactoryDao.buildDao("se.DemeritoDao");
        EstudioPostgradoDao estudioPostgradoDao = (EstudioPostgradoDao) FactoryDao.buildDao("se.EstudioPostgradoDao");
        AscensoDao  ascensoDao = (AscensoDao) FactoryDao.buildDao("se.AscensoDao");
        
        
        FichaEscalafonaria datosGenerales = null;
        
        List<Parientes> parientes = new ArrayList<Parientes>();
        List<FormacionEducativa> formacionesEducativas = new ArrayList<FormacionEducativa>();
        List<Colegiatura> colegiaturas = new ArrayList<Colegiatura>();
        List<EstudioComplementario> estudiosComplementarios = new ArrayList<EstudioComplementario>();
        List<EstudioComplementario> estudiosEspecializacion = new ArrayList<EstudioComplementario>();
        List<EstudioComplementario> conocimientosInformaticos = new ArrayList<EstudioComplementario>();
        List<EstudioComplementario> idiomas = new ArrayList<EstudioComplementario>();
        List<Exposicion> exposiciones = new ArrayList<Exposicion>();
        List<Publicacion> publicaciones = new ArrayList<Publicacion>();
        List<Desplazamiento> desplazamientos = new ArrayList<Desplazamiento>();
        List<Reconocimiento> reconocimientos = new ArrayList<Reconocimiento>();
        List<Reconocimiento> meritos = new ArrayList<Reconocimiento>();
        List<Reconocimiento> bonificaciones = new ArrayList<Reconocimiento>();
        List<Demerito> demeritos = new ArrayList<Demerito>();
        List<EstudioPostgrado> estudiosPostgrado = new ArrayList<EstudioPostgrado>();
        List<Ascenso> ascensos = new ArrayList<Ascenso>();
        
        datosGenerales = fichaEscalafonariaDao.buscarPorDNI(perDni);
        
        parientes = parientesDao.listarxTrabajador(traId);
        formacionesEducativas = formacionEducativaDao.listarxFichaEscalafonaria(ficEscId);
        colegiaturas = colegiaturaDao.listarxFichaEscalafonaria(ficEscId);
        estudiosComplementarios = estudioComplementarioDao.listarxFichaEscalafonaria(ficEscId);
        exposiciones = exposicionDao.listarxFichaEscalafonaria(ficEscId);
        publicaciones = publicacionDao.listarxFichaEscalafonaria(ficEscId);
        desplazamientos = desplazamientoDao.listarxFichaEscalafonaria(ficEscId);
        reconocimientos = reconocimientoDao.listarxFichaEscalafonaria(ficEscId);
        demeritos = demeritoDao.listarxFichaEscalafonaria(ficEscId);
        estudiosPostgrado = estudioPostgradoDao.listarxFichaEscalafonaria(ficEscId);
        ascensos = ascensoDao.listarxFichaEscalafonaria(ficEscId);
        
        
        Mitext mitext = new Mitext(true,"SIGESMED");
        mitext.agregarTitulo("Ficha escalafonaria");

        PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
        
        //Datos personales
        if ((Boolean) seleccionados.get(indice_datosPersonales)) {
            Paragraph p = new Paragraph("I. DATOS PERSONALES");
            p.setKeepTogether(true);
            p.setFont(bold).setFontSize(12);
            mitext.getDocument().add(p);

            float[] columnsWidths = new float[]{5, 5, 5};//15
            GTabla gtabla = new GTabla(columnsWidths);
            gtabla.build(new String[]{"APELLIDO PATERNO", "APELLIDO MATERNO", "NOMBRES"});
            gtabla.setWidthPercent(100);
            gtabla.processLine(new String[]{datosGenerales.getTrabajador().getPersona().getApePat(), datosGenerales.getTrabajador().getPersona().getApeMat(),
                datosGenerales.getTrabajador().getPersona().getNom()});
            /*gtabla.build(new String[]{"EDAD","ESTADO CIVIL","LUGAR DE NACIMIENTO", "FECHA DE NACIMIENTO"});*/
            mitext.agregarTabla(gtabla);

            columnsWidths = new float[]{1, 1, 1, 1, 1, 0.5f, 0.5f, 0.5f};//15
            gtabla = new GTabla(columnsWidths);
            gtabla.build("EDAD", 8, 2, 1);
            gtabla.build("ESTADO CIVIL", 8, 2, 1);
            gtabla.build("LUGAR DE NACIMIENTO", 8, 1, 3);
            gtabla.build("FECHA DE NACIMIENTO", 8, 1, 3);
            //gtabla.build("PAIS", 8, 1, 1);
            gtabla.build("DEPARTAMENTO", 8, 1, 1);
            gtabla.build("PROVINCIA", 8, 1, 1);
            gtabla.build("DISTRITO", 8, 1, 1);
            gtabla.build("DIA", 8, 1, 1);
            gtabla.build("MES", 8, 1, 1);
            gtabla.build("AÑO", 8, 1, 1);
            gtabla.setWidthPercent(100);

            String edad = calcularEdad(datosGenerales.getTrabajador().getPersona().getFecNac()).toString();
            Date fecNac = datosGenerales.getTrabajador().getPersona().getFecNac();
            SimpleDateFormat diaFormat = new SimpleDateFormat("dd");
            SimpleDateFormat mesFormat = new SimpleDateFormat("MM");
            SimpleDateFormat anioFormat = new SimpleDateFormat("yyyy");

            datosGenerales.getTrabajador().getPersona().getSex().toString();

            String estCiv = datosGenerales.getTrabajador().getPersona().getEstCiv().toString();
            
            String dep = (datosGenerales.getTrabajador().getPersona().getDepNac() == null || datosGenerales.getTrabajador().getPersona().getDepNac().equals("  ")) ? "-" : datosGenerales.getTrabajador().getPersona().getDepNac();
            String prov = (datosGenerales.getTrabajador().getPersona().getProNac() == null || datosGenerales.getTrabajador().getPersona().getProNac().equals("  ")) ? "-" : datosGenerales.getTrabajador().getPersona().getProNac();
            String dis = (datosGenerales.getTrabajador().getPersona().getDisNac() == null || datosGenerales.getTrabajador().getPersona().getDisNac().equals("  ")) ? "-" : datosGenerales.getTrabajador().getPersona().getDisNac();

            String[] locationDir = getUbigeoLocation(dep + prov + dis);
            String depS = locationDir[0].equals("") ? dep : locationDir[0];
            String proS = locationDir[1].equals("") ? prov : locationDir[1];
            String disS = locationDir[2].equals("") ? dis : locationDir[2];

            String dia = diaFormat.format(fecNac);
            String mes = mesFormat.format(fecNac);
            String anio = anioFormat.format(fecNac);

            gtabla.processLine(new String[]{edad, estCiv, depS, proS, disS, dia, mes, anio});
            mitext.agregarTabla(gtabla);

            columnsWidths = new float[]{1, 1, 1};//15
            gtabla = new GTabla(columnsWidths);
            gtabla.build("SEXO", 8, 1, 1);
            gtabla.build("N° DNI", 8, 1, 1);
            //gtabla.build("N° PASAPORTE", 8, 1, 1);
            //gtabla.build("IDIOMA", 8, 1, 1);
            gtabla.build("AUTOGENERADO ESSALUD", 8, 1, 1);
            gtabla.setWidthPercent(100);

            String sex = datosGenerales.getTrabajador().getPersona().getSex().toString();
            String ndni = datosGenerales.getTrabajador().getPersona().getDni();
            String ess = datosGenerales.getAutEss();
            
            gtabla.processLine(new String[]{sex == null ? "" : sex, ndni, ess == null ? "" : ess});
            mitext.agregarTabla(gtabla);

            columnsWidths = new float[]{1, 1, 1, 1};//15
            gtabla = new GTabla(columnsWidths);
            gtabla.build("TELÉFONOS/CORREO ELECTRÓNICO", 8, 1, 4);
            gtabla.build("TELEFONO FIJO", 8, 1, 1);
            gtabla.build("CELULAR", 8, 1, 1);
            gtabla.build("CELULAR", 8, 1, 1);
            gtabla.build("DIRECCIÓN DE CORREO ELECTRÓNICO", 8, 1, 1);
            gtabla.setWidthPercent(100);

            String fijo = datosGenerales.getTrabajador().getPersona().getFij();
            String cel1 = datosGenerales.getTrabajador().getPersona().getNum1();
            String cel2 = datosGenerales.getTrabajador().getPersona().getNum2();
            String corr = datosGenerales.getTrabajador().getPersona().getEmail();

            gtabla.processLine(new String[]{(fijo == null || fijo.equals("")) ? "-" : fijo, (cel1 == null || cel1.equals("")) ? "-" : cel1, (cel2 == null || cel2.equals("")) ? "-" : cel2, (corr == null || corr.equals("")) ? "-" : corr});
            mitext.agregarTabla(gtabla);

            columnsWidths = new float[]{1, 1, 1, 0.5f, 0.5f, 0.5f};//15
            gtabla = new GTabla(columnsWidths);
            gtabla.build("DOMICILIO RENIEC", 8, 1, 6);
            gtabla.build("DIRECCIÓN", 8, 1, 1);
            gtabla.build("NOMBRE DE ZONA", 8, 1, 1);
            gtabla.build("REFERENCIA", 8, 1, 1);
            gtabla.build("DISTRITO", 8, 1, 1);
            gtabla.build("PROVINCIA", 8, 1, 1);
            gtabla.build("DEPARTAMENTO", 8, 1, 1);
            gtabla.setWidthPercent(100);

            DireccionDao direccionDao = (DireccionDao) FactoryDao.buildDao("se.DireccionDao");
            List<Direccion> direcciones = direccionDao.listarxPersona(datosGenerales.getTrabajador().getPersona().getPerId());
            String dir1 = "-", zon1 = "", ref1 = "", dis1 = "", pro1 = "", dep1 = "";
            String dir2 = "-", zon2 = "", ref2 = "", dis2 = "", pro2 = "", dep2 = "";

            if (!direcciones.isEmpty()) {
                for (Direccion dirs : direcciones) {

                    String dir0 = dirs.getNom() == null ? "-" : dirs.getNom();
                    String zon0 = dirs.getNomZon() == null ? "" : dirs.getNomZon();
                    String ref0 = dirs.getDesRef() == null ? "" : dirs.getDesRef();
                    /////////////
                    String dis0 = (dirs.getDis() == null || dirs.getDis().equals("  ")) ? "-" : dirs.getDis();
                    String pro0 = (dirs.getPro() == null || dirs.getPro().equals("  ")) ? "-" : dirs.getPro();
                    String dep0 = (dirs.getDep() == null || dirs.getDep().equals("  ")) ? "-" : dirs.getDep();

                    String[] locationDir0 = getUbigeoLocation(dep0 + pro0 + dis0);
                    String depS0 = locationDir0[0].equals("") ? dep0 : locationDir0[0];
                    String proS0 = locationDir0[1].equals("") ? pro0 : locationDir0[1];
                    String disS0 = locationDir0[2].equals("") ? dis0 : locationDir0[2];
                    //////////////

                    if (dirs.getTip().toString().equals("R")) {
                        dir1 = dir0;
                        zon1 = zon0;
                        ref1 = ref0;
                        dis1 = disS0;
                        pro1 = proS0;
                        dep1 = depS0;
                    }
                    if (dirs.getTip().toString().equals("A")) {
                        dir2 = dir0;
                        zon2 = zon0;
                        ref2 = ref0;
                        dis2 = disS0;
                        pro2 = proS0;
                        dep2 = depS0;
                    }
                }
            }
            gtabla.processLine(new String[]{dir1.equals("") ? "-" : dir1, zon1, ref1, dis1, pro1, dep1});
            mitext.agregarTabla(gtabla);

            columnsWidths = new float[]{1, 1, 1, 0.5f, 0.5f, 0.5f};//15
            gtabla = new GTabla(columnsWidths);
            gtabla.build("DOMICILIO ACTUAL(Distinto a RENIEC)", 8, 1, 6);
            gtabla.build("DIRECCIÓN", 8, 1, 1);
            gtabla.build("NOMBRE DE ZONA", 8, 1, 1);
            gtabla.build("REFERENCIA", 8, 1, 1);
            gtabla.build("DISTRITO", 8, 1, 1);
            gtabla.build("PROVINCIA", 8, 1, 1);
            gtabla.build("DEPARTAMENTO", 8, 1, 1);
            gtabla.setWidthPercent(100);
            gtabla.processLine(new String[]{dir2.equals("") ? "-" : dir2, zon2, ref2, dis2, pro2, dep2});
            mitext.agregarTabla(gtabla);

            String sisPen = datosGenerales.getSisPen() == null ? "ONP" : datosGenerales.getSisPen();

            if (sisPen.equals("AFP")) {
                columnsWidths = new float[]{1, 1, 1};//15
                gtabla = new GTabla(columnsWidths);
                gtabla.build("RÉGIMEN PENSIONARIO", 8, 1, 3);

                gtabla.build("NOMBRE DE LA AFP", 8, 1, 1);
                gtabla.build("COD. CUSPP", 8, 1, 1);
                gtabla.build("FECHA DE INGRESO", 8, 1, 1);
                gtabla.setWidthPercent(100);

                String nomAfp = datosGenerales.getNomAfp();
                String codcus = datosGenerales.getCodCuspp();
                Date fecIng = datosGenerales.getFecIngAfp();

                gtabla.processLine(new String[]{nomAfp == null ? "-" : nomAfp, codcus == null ? "-" : codcus, fecIng == null ? "" : fecIng.toString()});
                mitext.agregarTabla(gtabla);
            } else {
                columnsWidths = new float[]{1};//15
                gtabla = new GTabla(columnsWidths);
                gtabla.build("RÉGIMEN PENSIONARIO", 8, 1, 1);
                gtabla.setWidthPercent(100);
                gtabla.processLine(new String[]{sisPen});
                mitext.agregarTabla(gtabla);
            }

            columnsWidths = new float[]{1, 1};//15
            gtabla = new GTabla(columnsWidths);
            gtabla.build("PERSONA CON DISCAPACIDAD", 8, 1, 1);
            gtabla.build("REGISTRO CONADIS", 8, 1, 1);
            gtabla.setWidthPercent(100);

            Boolean perDis = datosGenerales.getPerDis();
            String valDis = perDis == null ? "No especificado" : (perDis ? "SI" : "NO");
            String regCon = datosGenerales.getRegCon();

            gtabla.processLine(new String[]{valDis, regCon == null ? "-" : regCon});
            mitext.agregarTabla(gtabla);

            mitext.newLine(2);
        }
        //Datos familiares
        if ((Boolean) seleccionados.get(datos_familiares)) {
            Paragraph p = new Paragraph("II. DATOS FAMILIARES");
            p.setKeepTogether(true);
            p.setFont(bold).setFontSize(12);
            mitext.getDocument().add(p);

            GTabla gtabla = new GTabla(new float[]{5.0f, 2.0f, 1.0f, 1.0f, 1.0f, 2.0f});
            gtabla.build(new String[]{"APELLIDOS Y NOMBRES", "PARENTESCO", "FECHA DE NACIMIENTO", "EDAD", "SEXO", "Nº DNI"});

            if (parientes != null) {
                for (Parientes pa : parientes) {
                    String apellidosYNombres = pa.getPariente().getApePat() + " " + pa.getPariente().getApeMat() + " " + pa.getPariente().getNom();
                    String edad = calcularEdad(pa.getPariente().getFecNac()).toString();
                    Character sexo = pa.getPariente().getSex();
                    if (sexo == null) {
                        sexo = ' ';
                    }
                    gtabla.processLine(new String[]{apellidosYNombres, pa.getParentesco().getTpaDes(), pa.getPariente().getFecNac().toString(), edad,
                        sexo.toString(), pa.getPariente().getDni()});
                }
            }
            mitext.agregarTabla(gtabla);
            mitext.newLine(2);
        }
        //Formaciones educativas
        if ((Boolean) seleccionados.get(formaciones_educativas)) {
            Paragraph p = new Paragraph("III. FORMACION EDUCATIVA");
            p.setKeepTogether(true);
            p.setFont(bold).setFontSize(12);
            mitext.getDocument().add(p);

            GTabla gtabla = new GTabla(new float[]{1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f});
            gtabla.build(new String[]{"TIPO FORMACION", "NIVEL ACADEMICO", "Nº DE TITULO", "ESPECIALIDAD O PROG. ACADEMICO", "ESTUDIOS CONCLUIDOS", "FECHA DE EXPEDICION", "CENTRO DE ESTUDIOS/LUGAR"});

            if (formacionesEducativas != null) {
                for (FormacionEducativa fe : formacionesEducativas) {
                    gtabla.processLine(new String[]{mostrarTipoFormacionEducativa(fe.getTipFor()), mostrarNivelFormacionEducativa(fe.getNivAca().charAt(0)), fe.getNumTit()==null?"":fe.getNumTit(), fe.getEspAca()==null?"":fe.getEspAca(), fe.getEstCon()==null?"":(fe.getEstCon()?"SI" : "NO"), fe.getFecExp()==null?"":fe.getFecExp().toString(), fe.getCenEst()==null?"":fe.getCenEst()});
                }
            }
            mitext.agregarTabla(gtabla);
            mitext.newLine(2);
        }
        //Colegiaturas
        if((Boolean)seleccionados.get(info_colegiaturas)){
        Paragraph p = new Paragraph("INFORMACION RESPECTO A COLEGIATURA");
        p.setKeepTogether(true);
        p.setFont(bold).setFontSize(12);
        mitext.getDocument().add(p);

        GTabla gtabla = new GTabla(new float[]{5.0f, 3.0f, 3.0f});
        gtabla.build(new String[]{"COLEGIO PROFESIONAL", "REG. Nº COLEGIATURA", "CONDICION A LA FECHA"});
        
        if (colegiaturas != null){
           for(Colegiatura c:colegiaturas){
                gtabla.processLine(new String[]{c.getNomColPro(), c.getNumRegCol(), c.getConReg()? "Habilitado": "No Habilitado"});
            } 
        }
        mitext.agregarTabla(gtabla);
        }
        //Reconocimientos
        
        for(EstudioComplementario ec:estudiosComplementarios){
            switch (ec.getTip()) {
                case '1':
                    conocimientosInformaticos.add(ec);
                    break;
                case '2':
                    idiomas.add(ec);
                    break;
                default:
                    estudiosEspecializacion.add(ec);
                    break;
            }
        }
        
        //Estudios de especializacion
        if ((Boolean) seleccionados.get(estudios_especializacion)) {
            Paragraph p = new Paragraph("ESTUDIOS DE ESPECIALIZACION");
            p.setKeepTogether(true);
            p.setFont(bold).setFontSize(12);
            mitext.getDocument().add(p);

            GTabla gtabla = new GTabla(new float[]{5.0f, 5.0f, 2.0f, 1.0f, 1.0f, 1.0f});
            gtabla.build(new String[]{"DESCRIPCION", "INSTITUCION CERTIFICADORA", "TIPO DE PARTICIPACION", "FECHA DE INICIO", "FECHA DE TERMINO", "HORAS LECTIVAS"});

            if (estudiosEspecializacion != null) {
                for (EstudioComplementario ec : estudiosEspecializacion) {
                    gtabla.processLine(new String[]{ec.getDes()==null?"":ec.getDes(), ec.getInsCer()==null?"":ec.getInsCer(), ec.getTipPar()==null?"":ec.getTipPar(), ec.getFecIni()==null?"":ec.getFecIni().toString(), ec.getFecTer()==null?"":ec.getFecTer().toString(),
                        ec.getHorLec()==null?"":ec.getHorLec().toString()});
                }
            }
            mitext.agregarTabla(gtabla);
        }
        //Conocimientos informaticos
        if ((Boolean) seleccionados.get(conocimientos_informaticos)) {
            Paragraph p = new Paragraph("CONOCIMIENTOS INFORMATICOS");
            p.setKeepTogether(true);
            p.setFont(bold).setFontSize(12);
            mitext.getDocument().add(p);

            GTabla gtabla = new GTabla(new float[]{5.0f, 3.0f});
            gtabla.build(new String[]{"CONOCIMIENTOS", "NIVEL"});

            if (conocimientosInformaticos != null) {
                for (EstudioComplementario ec : conocimientosInformaticos) {
                    gtabla.processLine(new String[]{ec.getDes() == null ? "" : ec.getDes(), mostrarNivel(ec.getNiv())});
                }
            }
            mitext.agregarTabla(gtabla);
            mitext.newLine(2);
        }
        //Idiomas
        if ((Boolean) seleccionados.get(info_idiomas)) {
            Paragraph p = new Paragraph("IDIOMAS");
            p.setKeepTogether(true);
            p.setFont(bold).setFontSize(12);
            mitext.getDocument().add(p);

            GTabla gtabla = new GTabla(new float[]{5.0f, 3.0f});
            gtabla.build(new String[]{"IDIOMAS", "NIVEL"});

            if (idiomas != null) {
                for (EstudioComplementario ec : idiomas) {
                    gtabla.processLine(new String[]{ec.getDes() == null ? "" : ec.getDes(), mostrarNivel(ec.getNiv())});
                }
            }
            mitext.agregarTabla(gtabla);
            mitext.newLine(2);
        }
       //Exposiciones y/o Ponencias
        if ((Boolean) seleccionados.get(exposiciones_ponencias)) {
            Paragraph p = new Paragraph("EXPOSICIONES Y/O PONENCIAS");
            p.setKeepTogether(true);
            p.setFont(bold).setFontSize(12);
            mitext.getDocument().add(p);

            GTabla gtabla = new GTabla(new float[]{5.0f, 5.0f, 3.0f, 1.5f, 1.5f, 1.5f});
            gtabla.build(new String[]{"DESCRIPCION", "INSTITUCION ORGANIZADORA", "TIPO DE PARTICIPACION", "FECHA DE INICIO", "FECHA DE TERMINO", "HORAS LECTIVAS"});

            if (exposiciones != null) {
                for (Exposicion eyp : exposiciones) {
                    gtabla.processLine(new String[]{eyp.getDes() == null ? "" : eyp.getDes(), eyp.getInsOrg() == null ? "" : eyp.getInsOrg(), eyp.getTipPar() == null ? "" : eyp.getTipPar(), eyp.getFecIni() == null ? "" : eyp.getFecIni().toString(), eyp.getFecTer() == null ? "" : eyp.getFecTer().toString(), eyp.getHorLec() == null ? "" : eyp.getHorLec().toString()});
                }
            }
            mitext.agregarTabla(gtabla);
            mitext.newLine(2);
        }
        //Publicaciones
        if ((Boolean) seleccionados.get(info_publicaciones)) {
            Paragraph p = new Paragraph("PUBLICACIONES");
            p.setKeepTogether(true);
            p.setFont(bold).setFontSize(12);
            mitext.getDocument().add(p);

            GTabla gtabla = new GTabla(new float[]{5.0f, 3.0f, 5.0f, 3.0f, 3.0f, 1.0f, 2.0f});
            gtabla.build(new String[]{"NOMBRE DE LA EDITORIAL, REVISTA O MEDIO DE DIFUSION", "TIPO DE PUBLICACION", "TITULO DE PUBLICACION", "GRADO DE PARTICIPACION",
                "LUGAR", "FECHA DE PUBLICACION", "Nº DE REGISTRO"});

            if (publicaciones != null) {
                for (Publicacion pu : publicaciones) {
                    gtabla.processLine(new String[]{pu.getNomEdi()==null?"":pu.getNomEdi(), pu.getTipPub()==null?"":pu.getTipPub(), pu.getTitPub()==null?"":pu.getTitPub(), pu.getGraPar()==null?"":pu.getGraPar(), pu.getLug()==null?"":pu.getLug(), pu.getFecPub()==null?"":pu.getFecPub().toString(), pu.getNumReg()==null?"":pu.getNumReg()});
                }
            }
            mitext.agregarTabla(gtabla);
            mitext.newLine(2);
        }
        //Cargos desempeñados
        if ((Boolean) seleccionados.get(cargos_desempenados)) {
            Paragraph p = new Paragraph("IV. CARGOS DESEMPEÑADOS");
            p.setKeepTogether(true);
            p.setFont(bold).setFontSize(12);
            mitext.getDocument().add(p);

            float[] colWidthsTitle1 = {5, 5, 5, 5, 5, 5, 5, 5};
            GTabla gtabla = new GTabla(colWidthsTitle1);
            gtabla.setWidthPercent(100);
            gtabla.build(new String[]{"DESPLAZAMIENTO O TIPO DE SERVIDOR", "Nº RESOLUCION", "FECHA DE RESOLUCION", "INSTITUCION EDUCATIVA", "CARGO",
                "HORAS", "FECHA INICIO", "FECHA TERMINO"});

            if (desplazamientos != null) {
                for (Desplazamiento d : desplazamientos) {
                    gtabla.processLine(new String[]{mostrarTipoDesplazamiento(d.getTip()), d.getNumRes()==null?"":d.getNumRes(), d.getFecRes()==null?"":d.getFecRes().toString(), d.getInsEdu()==null?"":d.getInsEdu(), d.getCar()==null?"":d.getCar(),
                        d.getJorLab()==null?"":d.getJorLab(), d.getFecIni()==null?"":d.getFecIni().toString(), d.getFecTer()==null?"":d.getFecTer().toString()});
                }
            }
            mitext.agregarTabla(gtabla);
            mitext.newLine(2);
        }
        
        for(Reconocimiento r:reconocimientos){
            if (r.getMot() == '3' || r.getMot() == '4' || r.getMot() == '5'){
                bonificaciones.add(r);
            }else{
                meritos.add(r);
            }
        }
        
        //Meritos, felicitaciones y reconocimientos
        if ((Boolean) seleccionados.get(meritos_felicitaciones_reconocimientos)) {
            Paragraph p = new Paragraph("V. MERITOS, FELICITACIONES Y RECONOCIMIENTOS");
            p.setKeepTogether(true);
            p.setFont(bold).setFontSize(12);
            mitext.getDocument().add(p);

            GTabla gtabla = new GTabla(new float[]{3.0f, 3.0f, 5.0f, 5.0f});
            gtabla.build(new String[]{"Nº RESOLUCION", "FECHA DE RESOLUCION", "MOTIVO DEL MERITO, FELICITACION Y/O RECONOCIMIENTO", "ENTIDAD QUE EMITE RESOLUCION"});

            if (meritos != null) {
                for (Reconocimiento m : meritos) {
                    gtabla.processLine(new String[]{m.getNumRes() == null ? "" : m.getNumRes(), m.getFecRes() == null ? "" : m.getFecRes().toString(), mostrarMotivo(m.getMot()), m.getEntEmi() == null ? "" : m.getEntEmi()});
                }
            }
            mitext.agregarTabla(gtabla);
            mitext.newLine(2);
        }
        //BONIFICACIONES POR CUMPLIR 25 Y 30 AÑOS DE SERVICIOS OFICIALES Y BONIFICACIONES POR LUTO Y SEPELIO
        if ((Boolean) seleccionados.get(info_bonificaciones)) {
            Paragraph p = new Paragraph("VI. BONIFICACIONES POR CUMPLIR 25 Y 30 AÑOS DE SERVICIOS OFICIALES Y BONIFICIACIONES POR LUTO Y SEPELIO");
            p.setKeepTogether(true);
            p.setFont(bold).setFontSize(12);
            mitext.getDocument().add(p);

            GTabla gtabla = new GTabla(new float[]{3.0f, 3.0f, 5.0f, 5.0f});
            gtabla.build(new String[]{"Nº RESOLUCION", "FECHA DE RESOLUCION", "25, 30 AÑOS DE SRVICIOS / LUTO Y SEPELIO", "ENTIDAD QUE EMITE RESOLUCION"});

            if (bonificaciones != null) {
                for (Reconocimiento b : bonificaciones) {
                    gtabla.processLine(new String[]{b.getNumRes()==null?"":b.getNumRes(), b.getFecRes()==null?"":b.getFecRes().toString(), mostrarMotivo(b.getMot()), b.getEntEmi()==null?"":b.getEntEmi()});
                }
            }
            mitext.agregarTabla(gtabla);
            mitext.newLine(2);
        }
        //Demeritos
        if ((Boolean) seleccionados.get(info_demeritos)) {
            Paragraph p = new Paragraph("VII. DEMERITOS");
            p.setKeepTogether(true);
            p.setFont(bold).setFontSize(12);
            mitext.getDocument().add(p);

            float[] colWidthsTitle = {10, 5, 5, 5, 5, 5, 5};
            GTabla gtabla = new GTabla(colWidthsTitle);
            gtabla.setWidthPercent(100);
            gtabla.build(new String[]{"ENTIDAD QUE EMITE LA RESOLUCION", "Nº RESOLUCION", "FECHA DE R.D",
                "RESUELVE SEPARACION DEFINITIVA Y/O TEMPORAL DEL CARGO", "FECHA INICIO", "FECHA FIN", "MOTIVO DE SEPARACION"});

            if (demeritos != null) {
                for (Demerito d : demeritos) {
                    gtabla.processLine(new String[]{d.getEntEmi()==null?"":d.getEntEmi(), d.getNumRes()==null?"":d.getNumRes(), d.getFecRes()==null?"":d.getFecRes().toString(), d.getSep()==null?"":(d.getSep()? "SI" : "NO") , d.getFecIni()==null?"":d.getFecIni().toString(),
                    d.getFecFin()==null?"":d.getFecFin().toString(), d.getMot()==null?"":d.getMot()});
                }
            }
            mitext.agregarTabla(gtabla);
            mitext.newLine(2);
        }
        //Acumulacion de años de estudios y reconocimientos por estudios de maestrias
        /*if ((Boolean) seleccionados.get(anios_estudio_reconocimiento)) {
            Paragraph p = new Paragraph("IX. ACUMULACION DE AÑOS DE ESTUDIOS Y RECONOCIMIENTOS POR ESTUDIOS DE MAESTRIAS");
            p.setKeepTogether(true);
            p.setFont(bold).setFontSize(12);
            mitext.getDocument().add(p);

            GTabla gtabla = new GTabla(new float[]{3.0f, 1.0f, 3.0f, 4.0f});
            gtabla.build(new String[]{"Nº RESOLUCION", "FECHA DE RESOLUCION", "FECHA DE INICIO DE ESTUDIOS Y TERMINO DE ESTUDIOS", "INSTITUTO Y/O UNIVERSIDAD"});

            if (estudiosPostgrado != null) {
                for (EstudioPostgrado ep : estudiosPostgrado) {
                    gtabla.processLine(new String[]{ep.getNumRes(), ep.getFecRes().toString(), ep.getFecIniEst().toString() + " a " + ep.getFecTerEst().toString(),
                        ep.getIns()});
                }
            }
            mitext.agregarTabla(gtabla);
        }*/
        //Ascensos
        if ((Boolean) seleccionados.get(info_ascensos)) {
            Paragraph p = new Paragraph("VIII. REGISTRO DE ASCENSOS");
            p.setKeepTogether(true);
            p.setFont(bold).setFontSize(12);
            mitext.getDocument().add(p);

            GTabla gtabla = new GTabla(new float[]{3.0f, 3.0f, 3.0f, 3.0f});
            gtabla.build(new String[]{"Nº RESOLUCION", "FECHA DE RESOLUCION", "FECHA EN LA QUE SURTE EFECTO LA RESOLUCION", "NIVEL, ESCALA AL QUE ASCIENDE"});

            if (ascensos != null) {
                for (Ascenso a : ascensos) {
                    gtabla.processLine(new String[]{a.getNumRes() == null ? "" : a.getNumRes(), a.getFecRes() == null ? "" : a.getFecRes().toString(), a.getFecEfe() == null ? "" : a.getFecEfe().toString(),
                        a.getEsc() == null ? "" : a.getEsc()});
                }
            }
            mitext.agregarTabla(gtabla);
            mitext.newLine(2);
        }
        mitext.cerrarDocumento();
        return mitext.encodeToBase64();
    }
    private String mostrarMotivo(Character m){
        switch (m){
            case '1': return "Mérito";
            case '2': return "Felicitación";
            case '3': return "25 años de servicios";
            case '4': return "30 años de servicios";
            case '5': return "Luto y sepelio";
            default: return "Otros";
        }
    }
    private String mostrarTipoDesplazamiento(Character t){
        switch (t){
            case '1': return "Designación";
            case '2': return "Rotación";
            case '3': return "Reasignación";
            case '4': return "Destaque";
            case '5': return "Permuta";
            case '6': return "Encargo";
            case '7': return "Comisión de servicio";
            case '8': return "Transferencia";
            default: return "Otros";
        }
    }
    
    private String mostrarTipoEstudioComplementario(Character ec){
        switch (ec){
            case '1': return "Informatica";
            case '2': return "Idiomas";
            case '3': return "Certificación";
            case '4': return "Diplomado";
            case '5': return "Especialización";
            default: return "Otros";
        }
    }
    
    private String mostrarNivel(Character n){
        switch (n){
            case 'B': return "Básico";
            case 'I': return "Intermedio";
            case 'A': return "Avanzado";
            default: return "Ninguno";
        }
    }  
    
    private String mostrarTipoFormacionEducativa(Character t){
        switch (t){
            case '1': return "Estudios Básico";
            case '2': return "Superior";
            case '3': return "Postgrado";
            default: return "Otros";
        }
    }
    
    private String mostrarNivelFormacionEducativa(Character t){
        switch (t){
            case '1': return "Primaria";
            case '2': return "Secundaria";
            case '3': return "Superior";
            case '4': return "Tecnica";
            case '5': return "Bachiller";
            case '6': return "Titulo";
            case '7': return "Magister";
            case '8': return "Doctorado";
            default: return "Otros";
        }
    }
    
    private Integer calcularEdad(Date fecNac) {
        Date fechaActual = new Date();
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        String fecha_nac = formato.format(fecNac);
        String hoy = formato.format(fechaActual);
        String[] dat1 = fecha_nac.split("/");
        String[] dat2 = hoy.split("/");
        int anos = Integer.parseInt(dat2[2]) - Integer.parseInt(dat1[2]);
        int mes = Integer.parseInt(dat2[1]) - Integer.parseInt(dat1[1]);
        if (mes < 0) {
            anos = anos - 1;
        } else if (mes == 0) {
            int dia = Integer.parseInt(dat2[0]) - Integer.parseInt(dat1[0]);
            if (dia > 0) {
                anos = anos - 1;
            }
        }
        return anos;
    }

}
