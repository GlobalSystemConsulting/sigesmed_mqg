/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.PlantillaFichaMonitoreoDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.CompromisoGestionPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.ConfiguracionPlantilla;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.IndicadorCompromisoGestionPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.InstruccionLlenadoIndicadores;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.monitoreo_sesion_aprendizaje.PlantillaFichaMonitoreo;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;

public class PlantillaFichaMonitoreoDaoHibernate extends GenericDaoHibernate<PlantillaFichaMonitoreo> implements PlantillaFichaMonitoreoDao{

    @Override
    public List<PlantillaFichaMonitoreo> listarPlantillasPorIE(int orgId) {
        List<PlantillaFichaMonitoreo> plantillas = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            String hql = "SELECT pfm FROM PlantillaFichaMonitoreo AS pfm "
                    + "WHERE pfm.organizacion.orgId=:orgId AND pfm.estReg='A'"; 
            Query query = session.createQuery(hql);
            query.setInteger("orgId",orgId);            
            plantillas = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudieron listar las plantillas de la I.E. Error: "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudieron listar las plantillas de la I.E. Error: "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return plantillas;
    }

    @Override
    public List<ConfiguracionPlantilla> listarConfiguracionCarpeta(int plaId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(IndicadorCompromisoGestionPedagogica.class, "ind")
                    .createCriteria("ind.instruccionLI", "ins", JoinType.INNER_JOIN)
                    .createCriteria("ins.compromisoGP", "com", JoinType.INNER_JOIN)
                    .createCriteria("com.plantillaFM", "pla", JoinType.INNER_JOIN)
                    .add(Restrictions.eq("pla.plaFicMonId", plaId))
                    .add(Restrictions.eq("com.estReg", "A"))
                    .add(Restrictions.eq("ins.estReg", "A"))
                    .add(Restrictions.eq("ind.estReg", "A"))
                    .setProjection(Projections.projectionList()
                            .add(Projections.property("pla.plaFicMonId"),"plaFicMonId")
                            .add(Projections.property("com.comGesPedId"),"comGesPedId")
                            .add(Projections.property("com.comGesPedDes"),"comDes")
                            .add(Projections.property("ins.insLleIndId"),"insLleIndId")
                            .add(Projections.property("ins.insLleIndDes"),"insDes")
                            .add(Projections.property("ind.indComGesPedId"),"indComGesPedId")
                            .add(Projections.property("ind.indComGesPedDes"),"indDes")
                    )
                    .setResultTransformer(Transformers.aliasToBean(ConfiguracionPlantilla.class));
            List<ConfiguracionPlantilla> esquema = criteria.list();
            return esquema;
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public PlantillaFichaMonitoreo buscarPorId(int plaFicMonId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        PlantillaFichaMonitoreo pfm = (PlantillaFichaMonitoreo)session.get(PlantillaFichaMonitoreo.class, plaFicMonId);
        session.close();
        return pfm;
    }

    @Override
    public List<CompromisoGestionPedagogica> listarCompromisos(int plaId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT cgp FROM CompromisoGestionPedagogica AS cgp "
                    + "LEFT JOIN FETCH cgp.instruccionesLI ili "
                    + "WHERE cgp.plantillaFM.plaFicMonId =:plaId "
                    + "AND (ili.estReg='A' OR ili.estReg IS NULL) "
                    + "AND cgp.estReg='A'";
            Query query = session.createQuery(hql);
            query.setInteger("plaId",plaId);
            query.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<IndicadorCompromisoGestionPedagogica> listarIndicadores(int insId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT icgp FROM IndicadorCompromisoGestionPedagogica AS icgp "
                    + "WHERE icgp.instruccionLI.insLleIndId =:insId "
                    + "AND (icgp.estReg='A' OR icgp.estReg IS NULL) "
                    + "AND icgp.instruccionLI.estReg='A'";
            Query query = session.createQuery(hql);
            query.setInteger("insId",insId);
            query.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
    
}
