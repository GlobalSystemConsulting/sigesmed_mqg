/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.maestro.carpeta;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.carpeta.DocenteCarpetaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.DocenteCarpeta;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.RutaContenidoCarpeta;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class DocenteCarpetaDaoHibernate extends GenericDaoHibernate<DocenteCarpeta> implements DocenteCarpetaDao{

    @Override
    public DocenteCarpeta buscar(int carId, int orgId, int docId, String des) {
        DocenteCarpeta docenteCarpeta = null;        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
                
        try{   
            String hql = "SELECT dc FROM DocenteCarpetaMaestro AS dc "
                    + "WHERE dc.estReg='A' AND dc.des=:des "
                    + "AND dc.carpeta.carDigId=:carId AND dc.organizacion.orgId=:orgId "
                    + "AND dc.docente.doc_id=:docId";     
            Query query = session.createQuery(hql);  
            query.setInteger("carId",carId);
            query.setInteger("orgId",orgId);
            query.setInteger("docId", docId);
            query.setString("des", des);
            query.setMaxResults(1);
            docenteCarpeta = (DocenteCarpeta)query.uniqueResult();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo encontrar la carpeta pedagógica del docente. Error: "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar la carpeta pedagógica del docente. Error: "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return docenteCarpeta;
    }
    
    @Override
    public List<DocenteCarpeta> listarContenidosDocente(int orgId, int carId, int docId) {
        List<DocenteCarpeta> contDoc = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            String hql = "SELECT dc FROM DocenteCarpetaMaestro AS dc "
                    + "INNER JOIN dc.organizacion o "
                    + "INNER JOIN dc.carpeta c "
                    + "INNER JOIN dc.docente d "
                    + "WHERE o.orgId=:orgId AND "
                    + "d.doc_id=:docId AND "
                    + "c.carDigId=:carId"; 
            Query query = session.createQuery(hql);
            query.setInteger("orgId",orgId); 
            query.setInteger("docId",docId);
            query.setInteger("carId",carId);
            contDoc = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudieron listar los contenidos del docente. Error: "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudieron listar los contenidos del docente. Error: "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return contDoc;
    }
    
    @Override
    public RutaContenidoCarpeta buscarRutaConId(int docCarPedId) {
        RutaContenidoCarpeta ruta = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            String hql = "SELECT dc.ruta FROM DocenteCarpetaMaestro AS dc "
                    + "WHERE dc.docCarPedId=:docCarPedId"; 
            Query query = session.createQuery(hql);
            query.setInteger("docCarPedId",docCarPedId);            
            ruta = (RutaContenidoCarpeta) query.uniqueResult();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar la ruta del contenido. Error: "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar la ruta del contenido. Error: "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        System.out.println(ruta);
        return ruta;
    }
    
}
