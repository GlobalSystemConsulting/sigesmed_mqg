package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.EvaluacionCursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.PreguntaEvaluacionCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.EvaluacionCursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.PreguntaEvaluacionCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class RegistrarPreguntaTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(RegistrarPreguntaTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();

            EvaluacionCursoCapacitacionDao evaluacionCursoCapacitacionDao = (EvaluacionCursoCapacitacionDao) FactoryDao.buildDao("capacitacion.EvaluacionCursoCapacitacionDao");
            PreguntaEvaluacionCapacitacionDao preguntaEvaluacionCapacitacionDao = (PreguntaEvaluacionCapacitacionDao) FactoryDao.buildDao("capacitacion.PreguntaEvaluacionCapacitacionDao");
            EvaluacionCursoCapacitacion evaluacion = evaluacionCursoCapacitacionDao.buscarPorId(data.getInt("eva"));
            JSONObject question = data.getJSONObject("question");
            String answer = "";
            String options = "";

            switch (question.getString("tip")) {
                case "S":
                    answer = question.getJSONObject("res").getString("s");
                    break;

                case "L":
                    answer = Boolean.toString(question.getJSONObject("res").getBoolean("l"));
                    break;

                case "M":
                    JSONArray opciones = question.getJSONObject("res").getJSONArray("m");

                    for (int i = 0; i < opciones.length(); i++) {
                        JSONObject option = opciones.getJSONObject(i);
                        options = options + option.getString("nom") + "#$";
                        if (option.getBoolean("sel")) {
                            answer = answer + i + "#$";
                        }
                    }

                    break;
            }
            
            PreguntaEvaluacionCapacitacion pregunta = new PreguntaEvaluacionCapacitacion(question.getString("tip").charAt(0),
                    answer, options, 'A', data.getInt("usuMod"), new Date(), question.getDouble("pun"), question.getString("nom"),
                    question.getString("des"), evaluacion
            );
            
            preguntaEvaluacionCapacitacionDao.insert(pregunta);
            
            JSONObject result = new JSONObject();
            result.put("id", pregunta.getPreEvaCapId());
            
            if(pregunta.getRes().isEmpty())
                result.put("emp", true);
            else
                result.put("emp", false);
            
            return WebResponse.crearWebResponseExito("La pregunta de la evaluación fue creada correctamente", WebResponse.OK_RESPONSE).setData(result);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "registrarEvaluacion", e);
            return WebResponse.crearWebResponseError("No se pudo registrar la pregunta de la evaluación", WebResponse.BAD_RESPONSE);
        }
    }
}
