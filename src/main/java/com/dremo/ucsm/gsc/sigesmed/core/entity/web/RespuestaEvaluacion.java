package com.dremo.ucsm.gsc.sigesmed.core.entity.web;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@IdClass(RespuestaEvaluacionId.class)
@Entity
@Table(name="respuesta_evaluacion" ,schema="pedagogico")
public class RespuestaEvaluacion  implements java.io.Serializable {

    @Id 
    @Column(name="res_eva_id", unique=true, nullable=false)
    private int resEvaId;
    @Id
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ban_eva_esc_id", nullable=false , insertable = false,updatable = false)
    private BandejaEvaluacion bandejaEvaluacion;
    
    @Column(name="ban_eva_esc_id")
    private int ban_eva_esc_id;
    
    
    @Column(name="res", length=64)
    private String respuesta;
    
    @Column(name="pun")
    private int puntos;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="tie_res")
    private Date tie_res;

    public RespuestaEvaluacion() {
    }

	
    public RespuestaEvaluacion(int resEvaId, BandejaEvaluacion bandejaEvaluacion) {
        this.resEvaId = resEvaId;
        this.bandejaEvaluacion = bandejaEvaluacion;
    }
    public RespuestaEvaluacion(int resEvaId, BandejaEvaluacion bandejaEvaluacion, String respuesta) {
       this.resEvaId = resEvaId;
       this.bandejaEvaluacion = bandejaEvaluacion;
       this.respuesta = respuesta;
    }

    public RespuestaEvaluacion(int resEvaId, int ban_eva_esc_id, String respuesta, int puntos, Date tie_res) {
        this.resEvaId = resEvaId;
        this.ban_eva_esc_id = ban_eva_esc_id;
        this.respuesta = respuesta;
        this.puntos = puntos;
        this.tie_res = tie_res;
    }
    
    

    public void setBan_eva_esc_id(int ban_eva_esc_id) {
        this.ban_eva_esc_id = ban_eva_esc_id;
    }

    public int getBan_eva_esc_id() {
        return ban_eva_esc_id;
    }
   
    
     
    public int getResEvaId() {
        return this.resEvaId;
    }
    public void setResEvaId(int resEvaId) {
        this.resEvaId = resEvaId;
    }

    public BandejaEvaluacion getBandejaEvaluacion() {
        return this.bandejaEvaluacion;
    }
    
    public void setBandejaEvaluacion(BandejaEvaluacion bandejaEvaluacion) {
        this.bandejaEvaluacion = bandejaEvaluacion;
    }

    
    public String getRespuesta() {
        return this.respuesta;
    }
    
    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }
    
    public int getPuntos(){
        return this.puntos;        
    }
    public void setPuntos(int puntos){
        this.puntos = puntos;
    }

    public void setTie_res(Date tie_res) {
        this.tie_res = tie_res;
    }

    public Date getTie_res() {
        return tie_res;
    }
    
    

}


