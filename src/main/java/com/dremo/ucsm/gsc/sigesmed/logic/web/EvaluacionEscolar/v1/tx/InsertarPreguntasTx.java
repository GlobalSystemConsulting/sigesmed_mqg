/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.EvaluacionEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.EvaluacionEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.PreguntaEvaluacion;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
/**
 *
 * @author abel
 */
public class InsertarPreguntasTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */        
        
        List<PreguntaEvaluacion> preguntas = new ArrayList<PreguntaEvaluacion>();
        int evaluacionID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            JSONArray listaPreguntas = requestData.getJSONArray("preguntas");
            evaluacionID = requestData.getInt("evaluacionID");
            
            EvaluacionEscolar evaluacion = new EvaluacionEscolar(evaluacionID);
            
            //leendo los mensajes
            if(listaPreguntas.length() > 0){
                for(int i = 0; i < listaPreguntas.length();i++){
                    JSONObject bo = listaPreguntas.getJSONObject(i);                    

                    //int preguntaID = bo.getInt("preguntaID");
                    String titulo = bo.getString("titulo");
                    String pregunta = bo.getString("pregunta");
                    String respuesta = bo.getString("respuesta");
                    String alternativa = bo.getString("alternativa");
                    String orden = bo.getString("orden");
                    int puntos = bo.getInt("puntos");
                    int tiempo = bo.optInt("tiempo");
                    char tipo = bo.getString("tipo").charAt(0);                    

                    preguntas.add( new PreguntaEvaluacion(i+1,evaluacion,titulo,tiempo,puntos,pregunta,respuesta,alternativa,orden,tipo ));
                }
            }         
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar las preguntas, datos incorrectos", e.getMessage() );
        }
        //Fin       
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        EvaluacionEscolarDao evalucionDao = (EvaluacionEscolarDao)FactoryDao.buildDao("web.EvaluacionEscolarDao");
        try{
            evalucionDao.eliminarPreguntas(evaluacionID);
            evalucionDao.insertarPreguntas(preguntas);
            
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar las preguntas", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        JSONArray aPre = new JSONArray();
        
        for(PreguntaEvaluacion p: preguntas){
            aPre.put(p.getPreEvaId());
        }
        
        oResponse.put("preguntas",aPre);
        return WebResponse.crearWebResponseExito("El registro de las preguntas se realizo correctamente", oResponse);
        //Fin
    }    
    
    
}
