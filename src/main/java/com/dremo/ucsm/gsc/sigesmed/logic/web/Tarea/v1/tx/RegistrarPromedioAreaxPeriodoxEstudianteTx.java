/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.ConfiguracionNotaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.HistoricoNotasEstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.RegistroAuxiliarDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.DocenteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.TareaEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ConfiguracionNota;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.HistoricoNotasEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.RegistroAuxiliarCompetencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.GradoIEEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.PeriodosPlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadoresSesionAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.ma.notas.v1.tx.RegistrarPromedioAreaPorPeriodoTx;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class RegistrarPromedioAreaxPeriodoxEstudianteTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(RegistrarPromedioAreaPorPeriodoTx.class.getName());
    private List<ConfiguracionNota> config_docente = null;

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();

        int idOrg = data.getInt("idOrg");
        int idArea = data.optInt("areCurId");
        int idSession = data.optInt("sesId");
        char tipPer = data.getString("tipPer").charAt(0);
        int numPer = data.getInt("numPer");
        char tipNot = data.optString("tip", "N").charAt(0);
        int idGra = data.getInt("graId");
        int idUsr = data.getInt("usuId");
        char idSeccion = data.optString("idSeccion").charAt(0);
        int idAlumno = data.getInt("idAlumno");
        int idPlaEst = data.getInt("idPlaEst");
        int tarIdMM = data.getInt("tarIdMM");

        return registrarNotas(idSession, tipPer, numPer, idUsr, idArea, tipNot, idOrg, idGra, idSeccion, idAlumno, idPlaEst, tarIdMM);
    }

    private WebResponse registrarNotas(int idSession, char tipPer, int numPer, int idUsr, int idArea, char tipNot, int idOrg, int idGra, char idSeccion, int idAlumno, int idPlaEst, int tarIdMM) {
        try {
            RegistroAuxiliarDao regisDao = (RegistroAuxiliarDao) FactoryDao.buildDao("ma.RegistroAuxiliarDao");
            DocenteDao docDao = (DocenteDao) FactoryDao.buildDao("maestro.DocenteDao");
            EstudianteDao estDao = (EstudianteDao) FactoryDao.buildDao("maestro.EstudianteDao");
            HistoricoNotasEstudianteDao hisNotEst = (HistoricoNotasEstudianteDao) FactoryDao.buildDao("ma.HistoricoNotasEstudianteDao");
            TareaEscolarDao tareaDao = (TareaEscolarDao) FactoryDao.buildDao("web.TareaEscolarDao");
            ConfiguracionNotaDao configNot = (ConfiguracionNotaDao) FactoryDao.buildDao("ma.ConfiguracionNotaDao");
            config_docente = configNot.listarConfiguracionDocente(idUsr, idOrg);
            GradoIEEstudiante graOrgEst = tareaDao.obtenerGradoIEEstudiante(idOrg, idGra, idSeccion, idAlumno);
            PeriodosPlanEstudios perPlaEst = regisDao.buscarPeriodoPorAtributos(idPlaEst, tipPer, numPer);
            String mensaje = "Se registro el promedio del area con exito";

            List<IndicadoresSesionAprendizaje> competencias = regisDao.obtenerCompetenciasDeSesion(idSession);
            Double promedio = 0.0;
            for (IndicadoresSesionAprendizaje indApr : competencias) {
                RegistroAuxiliarCompetencia regAuxCom = regisDao.buscarNotaCompetenciaEsp(indApr.getCompetencia().getComId(), idArea, numPer, graOrgEst.getGraOrgEstId());
                String nota = "0.0";
                if (regAuxCom != null) {
                    nota = regAuxCom.getNota();
                }
                promedio += Double.parseDouble(nota);

            }
            promedio = promedio / competencias.size();
            String nota = String.valueOf(Math.round(promedio));
            String nota_lit = obtenerNotaLiteral(promedio);

            HistoricoNotasEstudiante historico = new HistoricoNotasEstudiante();
            if (!regisDao.buscarNotaAreaEnPeriodo(idArea, numPer, graOrgEst.getGraOrgEstId())) {
                historico.setNota(nota);
                historico.setNot_are_lit(nota_lit);
                historico.setAreaCurricular(docDao.buscarAreaPorId(idArea));
                historico.setGradoEst(graOrgEst);
                historico.setPeriodos(perPlaEst);
                historico.setUsuMod(idUsr);
                hisNotEst.insert(historico);
            } else {
                historico = regisDao.buscaHistoricoNotasEstudiante(idArea, numPer, graOrgEst.getGraOrgEstId());
                historico.setNota(nota);
                historico.setNot_are_lit(nota_lit);
                hisNotEst.update(historico);
            }
            return WebResponse.crearWebResponseExito(mensaje);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "registrarPromedioArea", e);
            return WebResponse.crearWebResponseError("No se pudo registrar el promedio del area en el periodo actual");
        }
    }

    public String obtenerNotaLiteral(Double not) {
        for (ConfiguracionNota conf : config_docente) {
            if (not >= conf.getConf_not_min() && not <= conf.getConf_not_max()) {
                return conf.getConf_cod();
            }
        }
        return "";
    }
}
