app.controller("usuarioEmpresaCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){
    
    $scope.areas = [];
    $scope.roles = [];
    $scope.nuevoUsuario = {nombre:"",password:"",estado:'A',rolID:0,organizacionID:0,existe:true};
    $scope.nuevaPersona = {dni:"",nombre:"",materno:"",paterno:"",nacimiento:"",email:"",numero1:"",numero2:"",existe:true};
    $scope.usuarioSel = {};
    
    //tabla de usuarios
    var paramsUsuario = {count: 10};
    var settingUsuario = { counts: []};
    $scope.tablaUsuario = new NgTableParams(paramsUsuario, settingUsuario);
    
    //filtros
    $scope.estadosSelect = [{id:'A',title:"activo"},{id:'I',title:"inactivo"},{id:'E',title:"eliminado"}];
    $scope.areasSelect = [];
    $scope.rolesSelect = [];
    
    $scope.buscarPersona = function(){
        if($scope.nuevaPersona.dni==''){
            modal.mensaje("ALERTA","ingrese DNI");
            return;
        }
        //preparamos un objeto request
        var request = crud.crearRequest('usuarioSistema',1,'buscarPersona');
        request.setData({dni:$scope.nuevaPersona.dni});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las usuarios de exito y error
        crud.listar("/configuracionInicial",request,function(data){
            modal.mensaje("CONFIRMACION",data.responseMsg);
            if(data.responseSta){
                $scope.nuevaPersona = data.data.persona;
                if(data.data.usuario)
                    $scope.nuevoUsuario = data.data.usuario;
                else{
                    $scope.nuevoUsuario.nombre = $scope.nuevaPersona.dni;
                    $scope.nuevoUsuario.existe = false;
                }
            }
            else{
                $scope.nuevaPersona.existe = false;
                $scope.nuevoUsuario.nombre = $scope.nuevaPersona.dni;
                $scope.nuevoUsuario.existe = false;
            }
        },function(data){
            console.info(data);
        });
    };
    
    $scope.listarUsuarios = function(orgID){
        //preparamos un objeto request
        var request = crud.crearRequest('usuarioSistema',1,'listarUsuariosPorOrganizacion');
        request.setData({organizacionID:orgID});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las usuarios de exito y error
        crud.listar("/configuracionInicial",request,function(data){
            settingUsuario.dataset = data.data;
            iniciarPosiciones(settingUsuario.dataset);
            $scope.tablaUsuario.settings(settingUsuario);
        },function(data){
            console.info(data);
        });
    };
    $scope.reiniciarDatos = function(){
        var p = $scope.nuevaPersona;
        var u = $scope.nuevoUsuario;
        p.existe = true;
        p.nombre="";
        p.paterno="";
        p.materno="";
        p.nacimiento="";
        p.email="";
        p.numero1="";
        p.numero2="";                
        u.nombre="";
        u.password="";
        u.existe=true;
        u.rolID=0;
    };
    $scope.prepararAgregar = function(){
        $scope.nuevoUsuario = {nombre:"",password:"",estado:'A',rolID:0,organizacionID:0,existe:true};
        $scope.nuevaPersona = {dni:"",nombre:"",materno:"",paterno:"",nacimiento:"",email:"",numero1:"",numero2:"",existe:true};
        
        $("#modalNuevo").modal('show');
    };    
    $scope.agregarUsuario = function(orgID){
        
        $scope.nuevoUsuario.organizacionID = orgID;
        
        var request = crud.crearRequest('usuarioSistema',1,'insertarUsuario');
        request.setData({persona:$scope.nuevaPersona,usuario:$scope.nuevoUsuario});
        
        crud.insertar("/configuracionInicial",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                var u = {};
                //recuperamos las variables que nos envio el servidor
                
                u.usuarioID = response.data.usuarioID;                
                u.password = response.data.password;
                
                u.nombreUsuario = $scope.nuevoUsuario.nombre;
                u.estado = $scope.nuevoUsuario.estado;
                
                u.nombres = $scope.nuevaPersona.nombre+" "+$scope.nuevaPersona.paterno+" "+$scope.nuevaPersona.materno;
                u.DNI = $scope.nuevaPersona.dni;             
                
                u.sessiones = [];
                var session = {};
                session.organizacionID = $scope.nuevoUsuario.organizacionID
                session.rol = buscarContenido($scope.roles,"rolID","nombre",$scope.nuevoUsuario.rolID);
                session.rolID = $scope.nuevoUsuario.rolID;
                session.area = buscarContenido($scope.areas,"areaID","nombre",$scope.nuevoUsuario.areaID);
                session.areaID = $scope.nuevoUsuario.areaID;
                session.estado = 'A';
                session.sessionID = response.data.sessionID;
        
                u.sessiones.push(session);
                
                //insertamos el elemento a la lista
                insertarElemento(settingUsuario.dataset,u);
                $scope.tablaUsuario.reload();
                //reiniciamos las variables
                //$scope.nuevoUsuario = {nombre:"",password:"",estado:'A',rolID:0,organizacionID:0};
                //$scope.nuevaPersona = {dni:"",nombre:"",materno:"",paterno:"",nacimiento:"",email:"",numero1:"",numero2:"",existe:true};
                //cerramos la ventana modal
                $('#modalNuevo').modal('hide');
            }            
        },function(data){
            console.info(data);
        });
        
    };
    $scope.eliminarUsuario = function(i,idDato){
        
        modal.mensajeConfirmacion($scope,"seguro que desea eliminar este registro",function(){
            
            var request = crud.crearRequest('usuarioSistema',1,'eliminarUsuario');
            request.setData({usuarioID:idDato});

            crud.eliminar("/configuracionInicial",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    eliminarElemento(settingUsuario.dataset,i);
                    $scope.tablaUsuario.reload();
                }
            },function(data){
                console.info(data);
            });
        });
    };
    $scope.prepararEditar = function(t){
        $scope.usuarioSel = JSON.parse(JSON.stringify(t));
        $('#modalEditar').modal('show');
    };
    $scope.editarUsuario = function(orgID){
        
        $scope.usuarioSel.sessiones.forEach(function(item){
            item.organizacionID = orgID;
        });
        
        var request = crud.crearRequest('usuarioSistema',1,'actualizarUsuario');
        request.setData($scope.usuarioSel);
                
        crud.actualizar("/configuracionInicial",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){                
                //actualizando
                settingUsuario.dataset[$scope.usuarioSel.i] = $scope.usuarioSel;
                $scope.tablaUsuario.reload();
                $('#modalEditar').modal('hide');
            }
        },function(data){
            console.info(data);
        });
    };
    $scope.listarAreas = function(organizacionID){
        //preparamos un objeto request
        var request = crud.crearRequest('area',1,'listarAreasPorOrganizacion');
        request.setData({organizacionID:organizacionID});
        crud.listar("/configuracionInicial",request,function(data){
            $scope.areas.push({nombre:""});            
            //$scope.areas = $scope.areas.concat(data.data);
            
            data.data.forEach(function(item){
                $scope.areas.push(item);
                var o = {};
                o.id = item.nombre;
                o.title = item.nombre;
                $scope.areasSelect.push(o); 
            });
        },function(data){
            console.info(data);
        });
    };
    $scope.listarRoles = function(orgID){
        //preparamos un objeto request
        var request = crud.crearRequest('rol',1,'listarRolesPorOrganizacion');
        request.setData({organizacionID:orgID});
        crud.listar("/configuracionInicial",request,function(data){
            $scope.roles = data.data;
            data.data.forEach(function(item){
                var o = item;
                o.id = item.nombre;
                o.title = item.nombre;
                $scope.rolesSelect.push(o);                
            });
        },function(data){
            console.info(data);
        });
    };
    $scope.iniciarDatos = function(orgID){
        $scope.listarUsuarios(orgID);
        $scope.listarAreas(orgID);
        $scope.listarRoles(orgID);
    };
    
    $scope.agregarSession = function(){
        if(!$scope.session || !$scope.session.areaID || !$scope.session.rolID )
            return;
        
        $scope.session.area = buscarContenido($scope.areas,"areaID","nombre",$scope.session.areaID);
        $scope.session.rol = buscarContenido($scope.roles,"rolID","nombre",$scope.session.rolID);
        
        $scope.session.estado = 'A';
            
        $scope.usuarioSel.sessiones.push($scope.session);
        $scope.session = {};
    };
    
    $scope.editarSession = function(i,s){
        //si estamso editando
        if(s.edi){
            s.copia.area = buscarContenido($scope.areas,"areaID","nombre",s.copia.areaID);
            s.copia.rol = buscarContenido($scope.roles,"rolID","nombre",s.copia.rolID);            
            $scope.usuarioSel.sessiones[i] = s.copia;
        }
        //si queremos editar
        else{
            s.copia = JSON.parse(JSON.stringify(s));
            s.edi =true;            
        }
    };
    
    $scope.eliminarSession = function(i,r){
        //si estamso cancelando la edicion
        if(r.edi){
            r.edi = false;
            delete r.copia;
        }
        //si queremos eliminar el elemento
        else{
            $scope.usuarioSel.sessiones.splice(i,1);
        }
    };
    
    
    
    function validarDatos(persona, usuario){
        
    };
    
    
}]);