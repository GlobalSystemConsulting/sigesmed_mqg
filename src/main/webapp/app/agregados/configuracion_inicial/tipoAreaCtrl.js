app.controller("tipoAreaCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){
     
    $scope.nueTipoArea = {nombre:"",alias:"",estado:'A'};
    $scope.tipoAreaSel = {};
    
    //tabla de tipoAreas
    var paramsTipoArea = {count: 10};
    var settingTipoArea = { counts: []};
    $scope.tablaTipoArea = new NgTableParams(paramsTipoArea, settingTipoArea);
    
    //filtros
    $scope.estadosSelect = [{id:'A',title:"activo"},{id:'I',title:"inactivo"},{id:'E',title:"eliminado"}];
    
    $scope.listarTipoAreas = function(){
        //preparamos un objeto request
        var request = crud.crearRequest('area',1,'listarTipoAreas');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las tipoAreas de exito y error
        crud.listar("/configuracionInicial",request,function(data){
            settingTipoArea.dataset = data.data;
            iniciarPosiciones(settingTipoArea.dataset);
            $scope.tablaTipoArea.settings(settingTipoArea);
        },function(data){
            console.info(data);
        });
    };
    $scope.agregarTipoArea = function(){
        
        var request = crud.crearRequest('area',1,'insertarTipoArea');
        request.setData($scope.nueTipoArea);
        
        crud.insertar("/configuracionInicial",request,function(response){
            
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                //recuperamos las variables que nos envio el servidor
                $scope.nueTipoArea.tipoAreaID = response.data.tipoAreaID;
                //insertamos el elemento a la lista
                insertarElemento(settingTipoArea.dataset,$scope.nueTipoArea);
                $scope.tablaTipoArea.reload();
                //reiniciamos las variables
                $scope.nueTipoArea = {nombre:"",alias:"",estado:'A'};
                //cerramos la ventana modal
                $('#modalNuevo').modal('hide');
            }            
        },function(data){
            console.info(data);
        });
        
    };
    $scope.eliminarTipoArea = function(i,idDato){
        
        modal.mensajeConfirmacion($scope,"seguro que desea eliminar el Tipo de Area",function(){
            
            var request = crud.crearRequest('area',1,'eliminarTipoArea');
            request.setData({tipoAreaID:idDato});

            crud.eliminar("/configuracionInicial",request,function(response){

                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    eliminarElemento(settingTipoArea.dataset,i);
                    $scope.tablaTipoArea.reload();
                }

            },function(data){
                console.info(data);
            });
        });
    };
    $scope.prepararEditar = function(t){
        $scope.tipoAreaSel = JSON.parse(JSON.stringify(t));
        $('#modalEditar').modal('show');
    };
    $scope.editarTipoArea = function(){
        
        var request = crud.crearRequest('area',1,'actualizarTipoArea');
        request.setData($scope.tipoAreaSel);
                
        crud.actualizar("/configuracionInicial",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){                
                //actualizandoareaPadreID);
                settingTipoArea.dataset[$scope.tipoAreaSel.i] = $scope.tipoAreaSel;
                
                $scope.tablaTipoArea.reload();
                $('#modalEditar').modal('hide');
            }
        },function(data){
            console.info(data);
        });
    };
    
}]);