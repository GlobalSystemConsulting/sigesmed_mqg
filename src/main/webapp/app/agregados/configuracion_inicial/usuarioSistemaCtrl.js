app.controller("usuarioSistemaCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){
        
    $scope.roles = [];
    $scope.organizaciones = [];
    $scope.nuevoUsuario = {nombre:"",password:"",estado:'A',rolID:0,organizacionID:0,existe:true};
    $scope.nuevaPersona = {dni:"",nombre:"",materno:"",paterno:"",nacimiento:"",email:"",numero1:"",numero2:"",existe:true};
    $scope.usuarioSel = {};
    $scope.oSel = {};
    
    //tabla de usuarios
    var paramsUsuario = {count: 10};
    var settingUsuario = { counts: []};
    $scope.tablaUsuario = new NgTableParams(paramsUsuario, settingUsuario);
    
    //filtros
    $scope.estadosSelect = [{id:'A',title:"Activo"},{id:'I',title:"Inactivo"}];
    $scope.organizacionesSelect = [];
    $scope.rolesSelect = [];
    
    $scope.buscarPersona = function(){
        
        if($scope.nuevaPersona.dni==''){
            modal.mensaje("ALERTA","ingrese DNI");
            return;
        }
        //preparamos un objeto request
        var request = crud.crearRequest('usuarioSistema',1,'buscarPersona');
        request.setData({dni:$scope.nuevaPersona.dni});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las usuarios de exito y error
        crud.listar("/configuracionInicial",request,function(data){
            modal.mensaje("CONFIRMACION",data.responseMsg);
            if(data.responseSta){
                $scope.nuevaPersona = data.data.persona;
                if(data.data.usuario)
                    $scope.nuevoUsuario = data.data.usuario;
                else{
                    $scope.nuevoUsuario.nombre = $scope.nuevaPersona.dni;
                    $scope.nuevoUsuario.existe = false;
                }
            }
            else{
                $scope.nuevaPersona.existe = false;
                /*$scope.nuevaPersona.nombre="";
                $scope.nuevaPersona.paterno="";
                $scope.nuevaPersona.materno="";
                $scope.nuevaPersona.nacimiento="";
                $scope.nuevaPersona.email="";
                $scope.nuevaPersona.numero1="";
                $scope.nuevaPersona.numero2="";  */              
                $scope.nuevoUsuario.nombre = $scope.nuevaPersona.dni;
                $scope.nuevoUsuario.existe = false;
            }
        },function(data){
            console.info(data);
        });
    };
    
    $scope.listarUsuarios = function(){
        //preparamos un objeto request
        var request = crud.crearRequest('usuarioSistema',1,'listarUsuarios');
        crud.listar("/configuracionInicial",request,function(data){
            settingUsuario.dataset = data.data;
            iniciarPosiciones(settingUsuario.dataset);
            $scope.tablaUsuario.settings(settingUsuario);
            
            //El siguiente for agrega atributos para que funcione adecuadamente el filtro de la tabla de usuarios
            for (i in data.data) {                       
                for (j in data.data[i].sessiones) {
                    if(!data.data[i].primeraOrganizacion){
                        data.data[i].primeraOrganizacion = data.data[i].sessiones[j].organizacion;
                    }
                    else if(!data.data[i].primeraOrganizacion.includes(data.data[i].sessiones[j].organizacion)){
                        data.data[i].primeraOrganizacion += " - " + data.data[i].sessiones[j].organizacion;
                    }
                    if(!data.data[i].primerRol){
                        data.data[i].primerRol = data.data[i].sessiones[j].rol;
                    }
                    else {
                        data.data[i].primerRol += " - " + data.data[i].sessiones[j].rol;
                    }
                }
            }
        },function(data){
            console.info(data);
        });
    };
    $scope.reiniciarDatos = function(){
        var p = $scope.nuevaPersona;
        var u = $scope.nuevoUsuario;
        p.existe = true;
        p.nombre="";
        p.paterno="";
        p.materno="";
        p.nacimiento="";
        p.email="";
        p.numero1="";
        p.numero2="";                
        u.nombre="";
        u.password="";
        u.existe=true;
        $scope.oSel = {};
    };
    $scope.prepararAgregar = function(){
        $scope.nuevoUsuario = {nombre:"",password:"",estado:'A',rolID:0,organizacionID:0,existe:true};
        $scope.nuevaPersona = {dni:"",nombre:"",materno:"",paterno:"",nacimiento:"",email:"",numero1:"",numero2:"",existe:true};
        $scope.oSel = {};
        
        $("#modalNuevo").modal('show');
    };    
    $scope.agregarUsuario = function(){
        
        $scope.nuevoUsuario.organizacionID = $scope.oSel.organizacionID;
        
        var request = crud.crearRequest('usuarioSistema',1,'insertarUsuario');
        request.setData({persona:$scope.nuevaPersona,usuario:$scope.nuevoUsuario});
        
        crud.insertar("/configuracionInicial",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                //recuperamos las variables que nos envio el servidor
                var u = {};
                
                u.usuarioID = response.data.usuarioID;                
                u.password = response.data.password;
                
                u.nombreUsuario = $scope.nuevoUsuario.nombre;
                u.estado = $scope.nuevoUsuario.estado;
                
                u.nombres = $scope.nuevaPersona.nombre+" "+$scope.nuevaPersona.paterno+" "+$scope.nuevaPersona.materno;
                u.DNI = $scope.nuevaPersona.dni;             
                
                u.sessiones = [];
                var session = {};
                session.organizacionID = $scope.oSel.organizacionID; 
                session.organizacion = $scope.oSel.nombre; 
                session.rol = buscarContenido($scope.roles,"rolID","nombre",$scope.nuevoUsuario.rolID);
                session.rolID = $scope.nuevoUsuario.rolID;
                session.estado = 'A';
                session.sessionID = response.data.sessionID;
        
                u.sessiones.push(session);
                
                //insertamos el elemento a la lista
                insertarElemento(settingUsuario.dataset,u);
                $scope.tablaUsuario.reload();
                //reiniciamos las variables
                //$scope.nuevoUsuario = {nombre:"",password:"",estado:'A',rolID:0,organizacionID:0};
                //$scope.nuevaPersona = {dni:"",nombre:"",materno:"",paterno:"",nacimiento:"",email:"",numero1:"",numero2:"",existe:true};
                //cerramos la ventana modal
                $('#modalNuevo').modal('hide');
            }            
        },function(data){
            console.info(data);
        });
        
    };
    $scope.eliminarUsuario = function(i,idDato){
        
        modal.mensajeConfirmacion($scope,"Seguro que desea eliminar este registro?",function(){
            
            var request = crud.crearRequest('usuarioSistema',1,'eliminarUsuario');
            request.setData({usuarioID:idDato});

            crud.eliminar("/configuracionInicial",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    eliminarElemento(settingUsuario.dataset,i);
                    $scope.tablaUsuario.reload();
                }
            },function(data){
                console.info(data);
            });
        });
    };
    $scope.prepararEditar = function(t){
        $scope.usuarioSel = JSON.parse(JSON.stringify(t));
        $scope.usuarioSel.nombreUsuario = $scope.usuarioSel.nombreUsuario.replace(/ /g, "");
        //console.log($scope.usuarioSel);
        $('#modalEditar').modal('show');
    };
    $scope.editarUsuario = function(){
        
        var request = crud.crearRequest('usuarioSistema',1,'actualizarUsuario');
        request.setData($scope.usuarioSel);
        
        crud.actualizar("/configuracionInicial",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){                
                //actualizando
                //$scope.usuarioSel.organizacion = buscarContenido($scope.organizaciones,"organizacionID","nombre",$scope.usuarioSel.organizacionID);
                settingUsuario.dataset[$scope.usuarioSel.i] = $scope.usuarioSel;
                $scope.tablaUsuario.reload();
                $('#modalEditar').modal('hide');
            }
        },function(data){
            console.info(data);
        });
    };
    
    listarRoles();
    listarOrganizaciones();
    
    $scope.agregarSession = function(){
        if(!$scope.session || !$scope.session.org || !$scope.session.rolID )
            return;
        
        $scope.session.organizacion = $scope.session.org.nombre;
        $scope.session.organizacionID = $scope.session.org.organizacionID;
        $scope.session.rol = buscarContenido($scope.roles,"rolID","nombre",$scope.session.rolID);
        
        $scope.session.estado = 'A';
            
        $scope.usuarioSel.sessiones.push($scope.session);
        $scope.session = {};
    };
    
    $scope.editarSession = function(i,s){
        //si estamso editando
        if(s.edi){
            s.copia.organizacion = s.copia.org.nombre;
            s.copia.organizacionID = s.copia.org.organizacionID;
            s.copia.rol = buscarContenido($scope.roles,"rolID","nombre",s.copia.rolID);
            $scope.usuarioSel.sessiones[i] = s.copia;
        }
        //si queremos editar
        else{
            s.copia = JSON.parse(JSON.stringify(s));
            for(var i=0; i< $scope.organizaciones.length ;i++){
                if($scope.organizaciones[i].organizacionID == s.copia.organizacionID){
                    s.copia.org = $scope.organizaciones[i];
                    break;
                }
            }
            s.edi =true;            
        }
    };
    
    $scope.eliminarSession = function(i,r){
        //si estamso cancelando la edicion
        if(r.edi){
            r.edi = false;
            delete r.copia;
        }
        //si queremos eliminar el elemento
        else{
            $scope.usuarioSel.sessiones.splice(i,1);
        }
    };
    
    function listarRoles(){
        //preparamos un objeto request
        var request = crud.crearRequest('rol',1,'listarRoles');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las usuarios de exito y error
        crud.listar("/configuracionInicial",request,function(data){
            $scope.roles = data.data;
            data.data.forEach(function(item){
                var o = item;
                o.id = item.nombre;
                o.title = item.nombre;
                $scope.rolesSelect.push(o);                
            });
        },function(data){
            console.info(data);
        });
    };
    function listarOrganizaciones(){
        //preparamos un objeto request
        var request = crud.crearRequest('organizacion',1,'listarOrganizacionesConRoles');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las usuarios de exito y error
        crud.listar("/configuracionInicial",request,function(data){
            $scope.organizaciones.push({nombre:""});
            data.data.forEach(function(item){
                var o = item;
                o.id = item.nombre;
                o.title = item.nombre;
                $scope.organizacionesSelect.push(o);                
                $scope.organizaciones.push(o);
            });
        },function(data){
            console.info(data);
        });
    };
    
    function validarDatos(persona, usuario){
        
    }
    
    
}]);
