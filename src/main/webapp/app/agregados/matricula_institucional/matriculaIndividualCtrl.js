app.controller("matriculaIndividualCtrl", ["$scope", "$rootScope", "crud", "NgTableParams", "modal", function ($scope, $rootScope, crud, NgTableParams, modal) {

        $scope.myusuario = $rootScope.usuMaster.usuario.usuarioID;
        $scope.myOrganizacion = $rootScope.usuMaster.organizacion.organizacionID;

        $scope.busqueda = {
            codigoBusqueda: "",
            tipoBusqueda: "1"
        };

        $scope.matriculaLoad = {
            matId: -1,
            conMat: -1,
            conMatNom: "Desconocido",
            matSit: -1,
            sitMatNom: "Desconocido",
            matEst: -1,
            matEstNom: "Desconocido",
            apoId: -1,
            apoNom: "Desconocido",
            obs: "",
            matFec: new Date(),
            regId: -1,
            regNom: "Desconocido",
            estId: -1,
            estNom: "Desconocido",
            orgOriId: -1,
            orgOriNom: "Desconocido",
            orgDesId: -1,
            orgDesNom: "Desconocido",
            graId: -1,
            graNom: "Sin Antecedentes",
            secId: "-",
            planNivId: -1,
            planNivNom: "Desconocido"
        };

        $scope.matriculaCreate = {
            matEstId: -1,
            matEstDNI: "",
            matRegId: -1,
            matParId: -1,
            orgOriId: -1,
            orgDesId: -1,
            matEstNom: "EN PROCESO",
            matFec: new Date(),
            matSit: "1",
            conMat: "1",
            matObs: "",
            nivId: -1,
            jorEscId: -1,
            turId: -1,
            graId: -1,
            usuMod: $scope.myusuario
        };

        $scope.datosVacantes = {
            vacantesxGrado: 0,
            estIdSelect: -1,
            estOrgOriSelect: -1,
            ultGraCul: 0,
            sigGra: 1

        };

        $scope.arrays = {
            gradosySecciones: [],
            niveles: [],
            jornadas: [],
            turnos: [],
            grados: [],
            secciones: [],
            aulas: [],
            parientes: []
        };

        $scope.changeSeccion = {
            nivId: -1,
            nivNom: "",
            jorEscId: -1,
            jorEscNom: "",
            turId: 'T',
            turNom: "",
            graId: -1,
            graNom: "",
            secAct: 'A',
            secId: -1,
            planNivelId: -1,
            vacantesXSec: "",
            estId: -1,
            orgDesId: -1,
            usuMod: $scope.myusuario
        };

        $scope.flags = {
            flagModificarMatricula: true
        };

        var paramsMatriculaInd = {count: 10};
        var settingMatriculaInd = {counts: []};
        $scope.tablaPrincipal = new NgTableParams(paramsMatriculaInd, settingMatriculaInd);

        $scope.generarCambioSeccion = function () {
            var request = crud.crearRequest('matriculaIndividual', 1, 'cambioSeccion');
            request.setData($scope.changeSeccion);
            crud.insertar("/matriculaInstitucional", request, function (data) {
                if (data.responseSta) {
                    $('#modalCambiarSeccion').modal('hide');
                    modal.mensaje("Operacion Exitosa", "Cambio de Seccion Exitosa");
                } else {
                    $('#modalCambiarSeccion').modal('hide');
                    modal.mensaje("Error", "Error en el cambio de Seccion");
                }

            }, function (data) {
                console.info(data);
            });
        };

        $scope.cambiarSeccion = function (estId, estOrgOri) {
            //alert("estIDSS.- "+estId+"  "+"orgOriSS.- "+estOrgOri);
            var itmRequest = {
                estId: estId
            };
            var request = crud.crearRequest('matriculaIndividual', 1, 'buscarMatricula');
            request.setData(itmRequest);
            crud.listar("/matriculaInstitucional", request, function (data) {
                if (data.responseSta) {
                    $scope.cargarGradosySecciones();
                    $scope.changeSeccion.nivId = data.data.nivId;
                    $scope.changeSeccion.nivNom = data.data.nivNom;
                    $scope.changeSeccion.jorEscId = data.data.JornadaId;
                    $scope.changeSeccion.jorEscNom = data.data.JornadaNom;
                    $scope.changeSeccion.turId = data.data.turnoId;
                    $scope.changeSeccion.turNom = data.data.turnoNom;
                    $scope.changeSeccion.graId = data.data.graId;
                    $scope.changeSeccion.graNom = data.data.graNom;
                    $scope.changeSeccion.secAct = data.data.secId;
                    $scope.changeSeccion.secId = -1;
                    $scope.changeSeccion.planNivelId = data.data.planNivId;
                    $scope.changeSeccion.vacantesXSec = 0;

                    $scope.changeSeccion.estId = estId;
                    $scope.changeSeccion.orgDesId = estOrgOri;
                    $scope.cargarSecciones();
                    $('#modalCambiarSeccion').modal('toggle');

                } else {
                    modal.mensaje("Error", "El estudiante no esta Matriculado");
                }

            }, function (data) {
                console.info(data);
            });
        };

        $scope.cargarVacantesSeccion = function () {
            var size = $scope.arrays.gradosySecciones.length;
            var itm;
            for (var i = 0; i < size; i++) {
                itm = $scope.arrays.gradosySecciones[i];
                if (itm.seccId === $scope.changeSeccion.secId) {
                    $scope.changeSeccion.vacantesXSec = itm.gradoSecNumMax - itm.gradoSecMat;
                    break;
                }
            }
        };

        $scope.generarMatricula = function () {
            if ($scope.matriculaCreate.matParId === -1) {
                modal.mensaje("Error", "Seleccione un Pariente Responsable");
                return;
            }

            if ($scope.matriculaCreate.nivId === -1 ||
                    $scope.matriculaCreate.jorEscId === -1 ||
                    $scope.matriculaCreate.turId === -1 ||
                    $scope.matriculaCreate.graId === -1) {
                modal.mensaje("Error", "Seleccione un Grado correctamente!");
                return;
            }

            var request = crud.crearRequest('matriculaIndividual', 1, 'generarMatriculaAutomatica');
            $scope.matriculaCreate.matEstId = $scope.datosVacantes.estIdSelect;
          //  alert("mm  ",$scope.matriculaCreate.matEstId );
            
            $scope.matriculaCreate.orgOriId = $scope.datosVacantes.estOrgOriSelect;
            $scope.matriculaCreate.matRegId = $scope.myusuario;
            $scope.matriculaCreate.orgDesId = $scope.myOrganizacion;
            var flagt = false;
           // alert("estID2.- "+ $scope.matriculaCreate.matEstId+"  "+"orgOri2.- "+$scope.matriculaCreate.orgOriId ); 
            request.setData($scope.matriculaCreate);
            crud.insertar("/matriculaInstitucional", request, function (data) {
                if (data.responseSta) {
                    modal.mensaje("Operacion Exitosa", "Matricula Generada");
                    // alert("mm  ",$scope.matriculaCreate.matEstId );
                    $('#modalMatriculaCreate').modal('hide');
                    flagt =true;
                } else {
                    modal.mensaje("Error", "Error al Matricular al Estudiante");

                }
                if(flagt === true){
                     $scope.cambiarSeccion($scope.matriculaCreate.matEstId,$scope.matriculaCreate.orgOriId);
                 }
            }, function (data) {
                alert("exito");
                console.info(data);
            });
        };

        $scope.buscarEstudiante = function () {
            var request = crud.crearRequest('matriculaIndividual', 1, 'buscarEstudiantesMatriculados');
            request.setData($scope.busqueda);
            crud.listar("/matriculaInstitucional", request, function (data) {
                settingMatriculaInd.dataset = data.data;
                iniciarPosiciones(settingMatriculaInd.dataset);
                $scope.tablaPrincipal.settings(settingMatriculaInd);
                if (data.responseSta) {

                } else {
                }

            }, function (data) {
                console.info(data);
            });
        };

        $scope.buscarMatricula = function (estId, orgOri, ultGra, estDni) {
           // alert("estID.- "+estId+"  "+"orgOri.- "+orgOri);
            $scope.datosVacantes.estIdSelect = estId;
            $scope.datosVacantes.estOrgOriSelect = orgOri;
            var itmRequest = {
                estId: estId
            };
            var request = crud.crearRequest('matriculaIndividual', 1, 'buscarMatricula');
            request.setData(itmRequest);
            crud.listar("/matriculaInstitucional", request, function (data) {
                if (data.responseSta) {
                    $scope.matriculaLoad = data.data;
                    $('#modalMatriculaLoad').modal('toggle');
                } else {
                    $scope.cargarParientes(estId);
                    $scope.cargarGradosySecciones();
                    $scope.datosVacantes.vacantesxGrado = 0;
                    $scope.datosVacantes.ultGraCul = parseInt(ultGra);
                    $scope.datosVacantes.sigGra = parseInt(ultGra) + 1;
                    $scope.matriculaCreate.matEstDNI = estDni;
                    $scope.matriculaCreate.matParId = -1;
                    $scope.matriculaCreate.nivId = -1;
                    $scope.matriculaCreate.jorEscId = -1;
                    $scope.matriculaCreate.turId = -1;
                    $scope.matriculaCreate.graId = -1;
                    $scope.cargarNiveles();
                    $('#modalMatriculaCreate').modal('toggle');
                }

            }, function (data) {
                console.info(data);
            });

        };

        $scope.cargarParientes = function (perId) {
            var myEstudiante = {
                persona: {
                    perId: perId
                }
            };
            var request = crud.crearRequest('agregarEstudiante', 1, 'listarParientes');
            request.setData(myEstudiante);
            crud.listar("/matriculaInstitucional", request, function (data) {
                $scope.parientes = data.data;
                if (data.responseSta) {
                } else {
                }
            }, function (data) {
                console.info(data);
            });
        };

        $scope.generarAulas = function () {
            modal.mensajeConfirmacion($scope, "Esta seguro de Generar Grados y Secciones?", function () {
                var itmRequest = {
                    orgIdUser: $scope.myOrganizacion
                };
                var request = crud.crearRequest('matriculaIndividual', 1, 'generarGradosySeccion');
                request.setData(itmRequest);
                crud.listar("/matriculaInstitucional", request, function (data) {
                    if (data.responseSta) {
                        modal.mensaje("Operacion Exitosa", "Se generaro las Aulas en base al plan de estudios");
                    } else {
                        modal.mensaje("Error", "Error al Generar las Aulas");
                    }
                }, function (data) {
                    console.info(data);
                });
            });
        };

        $scope.eliminarMatricula = function (estId) {

            modal.mensajeConfirmacion($scope, "Esta seguro de eliminar la matricula?", function () {
                var rq = {
                    estId: estId
                };
                var request = crud.crearRequest('matriculaIndividual', 1, 'eliminarMatricula');
                request.setData(rq);
                crud.eliminar("/matriculaInstitucional", request, function (data) {
                    if (data.responseSta) {
                        modal.mensaje("Resultado", data.data.msj);
                    } else {
                        modal.mensaje("Error", "Error al Eliminar la Matricula");
                    }
                }, function (data) {
                    console.info(data);
                });
            });

        };

        $scope.cargarGradosySecciones = function () {
            var itmRequest = {
                orgIdUser: $scope.myOrganizacion
            };
            var request = crud.crearRequest('matriculaIndividual', 1, 'listarGradosySecciones');
            request.setData(itmRequest);
            crud.listar("/matriculaInstitucional", request, function (data) {
                $scope.arrays.gradosySecciones = data.data;
            }, function (data) {
                console.info(data);
            });
        };

        $scope.cargarNiveles = function () {
            var size = $scope.arrays.gradosySecciones.length;
            var tempID, tempNom, obj, itmRepeat, graDis;
            var flagRepeat = true;
            $scope.arrays.niveles = [];
            $scope.arrays.jornadas = [];
            $scope.arrays.turnos = [];
            $scope.arrays.grados = [];
            $scope.arrays.secciones = [];
            $scope.datosVacantes.vacantesxGrado = 0;
            for (var i = 0; i < size; i++) {
                tempID = $scope.arrays.gradosySecciones[i].nivelId;
                tempNom = $scope.arrays.gradosySecciones[i].nivelNom;
                graDis = $scope.arrays.gradosySecciones[i].gradoId;
                
                if (parseInt(graDis) !== $scope.datosVacantes.sigGra) {
                    //continue;
                }
                obj = {
                    nivelId: tempID,
                    nivelNom: tempNom
                };
                for (var j = 0; j < $scope.arrays.niveles.length; j++) {
                    itmRepeat = $scope.arrays.niveles[j];
                    if (obj.nivelId === itmRepeat.nivelId) {
                        flagRepeat = false;
                    }
                }
                if (flagRepeat) {
                    $scope.arrays.niveles.push(obj);
                }
                flagRepeat = true;
            }
            
            $scope.cargarJornada();
        };

        $scope.cargarJornada = function () {
            var size = $scope.arrays.gradosySecciones.length;
            var obj, itm, itmRepeat;
            var flagRepeat = true;
            $scope.arrays.jornadas = [];
            $scope.arrays.turnos = [];
            $scope.arrays.grados = [];
            $scope.arrays.secciones = [];

            $scope.datosVacantes.vacantesxGrado = 0;
            for (var i = 0; i < size; i++) {
                itm = $scope.arrays.gradosySecciones[i];
                if (parseInt(itm.gradoId) !== $scope.datosVacantes.sigGra) {
//                    continue;
                }
                if (itm.nivelId === $scope.matriculaCreate.nivId) {
                    obj = {
                        jornadaId: itm.jornadaId,
                        jornadaNom: itm.jornadaNom
                    };
                    for (var j = 0; j < $scope.arrays.jornadas.length; j++) {
                        itmRepeat = $scope.arrays.jornadas[j];
                        if (obj.jornadaId === itmRepeat.jornadaId) {
                            flagRepeat = false;
                        }
                    }
                    if (flagRepeat) {
                        $scope.arrays.jornadas.push(obj);
                    }
                    flagRepeat = true;
                }
            }
            $scope.cargarTurno();
        };

        $scope.cargarTurno = function () {
            var size = $scope.arrays.gradosySecciones.length;
            var obj, itm, itmRepeat;
            var flagRepeat = true;
            $scope.arrays.turnos = [];
            $scope.arrays.grados = [];
            $scope.arrays.secciones = [];

            $scope.datosVacantes.vacantesxGrado = 0;
            for (var i = 0; i < size; i++) {
                itm = $scope.arrays.gradosySecciones[i];
                if (parseInt(itm.gradoId) !== $scope.datosVacantes.sigGra) {
//                    continue;
                }
                if (itm.jornadaId === $scope.matriculaCreate.jorEscId) {
                    obj = {
                        turnoId: itm.turnoId,
                        turnoNom: itm.turnoNom
                    };
                    for (var j = 0; j < $scope.arrays.turnos.length; j++) {
                        itmRepeat = $scope.arrays.turnos[j];
                        if (obj.turnoId === itmRepeat.turnoId) {
                            flagRepeat = false;
                        }
                    }
                    if (flagRepeat) {
                        $scope.arrays.turnos.push(obj);
                    }
                    flagRepeat = true;
                }
            }
            $scope.cargarGrados();
        };

        $scope.cargarGrados = function () {
            var size = $scope.arrays.gradosySecciones.length;
            var obj, itm, itmRepeat;
            var flagRepeat = true;
            $scope.arrays.grados = [];
            $scope.arrays.secciones = [];

            for (var i = 0; i < size; i++) {
                itm = $scope.arrays.gradosySecciones[i];
                if (parseInt(itm.gradoId) !== $scope.datosVacantes.sigGra) {
//                    continue; 
                }
                if (itm.turnoId === $scope.matriculaCreate.turId) {
                    obj = {
                        gradoSecId: itm.gradoSecId,
                        planNivelId: itm.planNivelId,
                        gradoId: itm.gradoId,
                        gradoNom: itm.gradoNom
                    };
                    for (var j = 0; j < $scope.arrays.grados.length; j++) {
                        itmRepeat = $scope.arrays.grados[j];
                        if (obj.gradoId === itmRepeat.gradoId) {
                            flagRepeat = false;
                        }
                    }
                    if (flagRepeat) {
                        $scope.arrays.grados.push(obj);
                    }
                    flagRepeat = true;
                }
            }
            $scope.cargarVacantes();
        };

        //para cambio de matricula
        $scope.cargarSecciones = function () {
            var size = $scope.arrays.gradosySecciones.length;
            var obj, itm, itmRepeat;
            var flagRepeat = true;
            $scope.arrays.secciones = [];

            var nivId = $scope.changeSeccion.nivId;
            var jorEscId = $scope.changeSeccion.jorEscId;
            var turId = $scope.changeSeccion.turId;
            var graId = $scope.changeSeccion.graId;

            var tempNivId, tempJorEscId, tempTurId, tempGraId;

            for (var i = 0; i < size; i++) {
                itm = $scope.arrays.gradosySecciones[i];
                tempNivId = parseInt(itm.nivelId);
                tempJorEscId = parseInt(itm.jornadaId);
                tempTurId = itm.turnoId.toString().charCodeAt(0);
                tempGraId = parseInt(itm.gradoId);

                if (tempNivId === nivId && tempJorEscId === jorEscId &&
                        tempTurId === turId && tempGraId === graId) {
                    obj = {
                        secId: itm.seccId
                    };
                    for (var j = 0; j < $scope.arrays.secciones.length; j++) {
                        itmRepeat = $scope.arrays.secciones[j];
                        if (obj.secId === itmRepeat.secId) {
                            flagRepeat = false;
                        }
                    }
                    if (flagRepeat) {
                        $scope.arrays.secciones.push(obj);
                    }
                    flagRepeat = true;
                }
            }
            $scope.cargarVacantesSeccion();
        };

        $scope.cargarVacantes = function () {
            var size = $scope.arrays.gradosySecciones.length;
            var itm;
            var vacantesXGrado = 0;
            for (var i = 0; i < size; i++) {
                itm = $scope.arrays.gradosySecciones[i];
                if (itm.gradoId === $scope.matriculaCreate.graId &&
                        itm.turnoId === $scope.matriculaCreate.turId &&
                        itm.jornadaId === $scope.matriculaCreate.jorEscId &&
                        itm.nivelId === $scope.matriculaCreate.nivId) {
                    vacantesXGrado = vacantesXGrado + (itm.gradoSecNumMax - itm.gradoSecMat);
                }
            }
            $scope.datosVacantes.vacantesxGrado = vacantesXGrado;
        };

        $scope.descargarConstanciaMatricula = function (estId, estDni) {
            var itmRequest = {
                estId: estId
            };
            var request = crud.crearRequest('matriculaIndividual', 1, 'buscarMatricula');
            request.setData(itmRequest);
            crud.listar("/matriculaInstitucional", request, function (data) {
                if (data.responseSta) {
                    var path = "../archivos/matricula_institucional/constanciaMatricula/constanciaMatricula_" + estDni + ".pdf";
                    window.open(path);
                } else {
                    modal.mensaje("Error", "No se Encuentra Matriculado!");
                }
            }, function (data) {
                console.info(data);
            });

        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.altInputFormats = ['M!/d!/yyyy'];

        $scope.popup1 = {
            opened: false
        };
        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };

    }]);


