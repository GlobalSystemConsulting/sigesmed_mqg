var seApp = angular.module('app');
seApp.requires.push('angularModalService');
seApp.requires.push('ngAnimate');

seApp.config(['$routeProvider',function($routeProvider){
    $routeProvider.when('/ver_detalle_carpeta_docente/:data',{
            templateUrl:'pedagogico/sma/dre/detalleCarpetaDocenteToDRE.html',
            controller:'verCarpetaDocenteDetalleByDRECtrl',
            controllerAs:'verCarpetaDocenteDRECtrl'
        });
}]);
seApp.controller("monitoreoCarpetaPedagogicaDRECtrl", ["$location", "$rootScope", "$scope", "NgTableParams", "crud", "modal", "ModalService", function ($location, $rootScope, $scope, NgTableParams, crud, modal, ModalService) {

        //arreglo donde estan todas las carpetas
        $scope.carpetas = [];
        //variable que servira para seleccionar una carpeta
        $scope.carpetaSel = {};
        //Variables para manejo de la tabla
        $rootScope.paramsCarpetasNivelDRE = {count: 10};
        $rootScope.settingCarpetasNivelDRE = {counts: []};
        $rootScope.tablaCarpetasNivelDRE = new NgTableParams($rootScope.paramsCarpetasNivelDRE, $rootScope.settingCarpetasNivelDRE);
        $scope.contenidosDocentes = {};
        function buscarDocenteId(lista, docId){
            for(i=0;i<lista.length;i++){
                if(lista[i]===docId)
                    return true;
            }
            return false;
        }
        $scope.listarCarpetas = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('carpeta_pedagogica', 1, 'listarCarpetasPorDRE');
            request.setData({dre: $rootScope.usuMaster.organizacion.organizacionID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sma", request, function (data) {
                $scope.contenidosDocentes = data.data;
                var registrados = [];
                var carpetasDocentes = [];
                data.data.forEach(function(item){
                    if(!buscarDocenteId(registrados, item.docId)){
                        registrados.push(item.docId);
                        carpetasDocentes.push(item);
                    }
                });
                $rootScope.settingCarpetasNivelDRE.dataset = carpetasDocentes;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingCarpetasNivelDRE.dataset);
                $rootScope.tablaCarpetasNivelDRE.settings($rootScope.settingCarpetasNivelDRE);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.verCarpetaDetalle = function (docId) {
            var docenteContenidos = buscarObjetos($scope.contenidosDocentes,'docId', docId );
            $location.url('/ver_detalle_carpeta_docente/'+btoa(JSON.stringify(docenteContenidos)));
        };
        
    }]);

seApp.controller('verCarpetaDocenteDetalleByDRECtrl', ['$routeParams', '$scope', '$rootScope', '$http', 'NgTableParams', 'crud','modal', 'ModalService', function ($routeParams, $scope, $rootScope, $http, NgTableParams, crud, modal, ModalService) {
        $scope.estadoAvance = [{id:1,title:"Completo"}, {id:2,title:"En Proceso"}, {id:3,title:"Pendiente"}];
        $scope.carDocInfo = JSON.parse(atob($routeParams.data)); 
        $scope.docenteData = $scope.carDocInfo[0].docDat;
        $scope.ieData = $scope.carDocInfo[0].ieDat;
        $scope.ugelData = $scope.carDocInfo[0].ugelDat;
        
        cargarCarSecciones();
        function cargarCarSecciones(){
            var request = crud.crearRequest('carpeta_pedagogica',1,'verDetalleCarpeta');
            request.setData({car:$scope.carDocInfo[0].carId,org:$scope.carDocInfo[0].ieId, doc:$scope.carDocInfo[0].docId});
            crud.listar('/sma',request,function(response){
                if(response.responseSta){
                    var carDoc = [];
                    for(i=0; i<response.data.length; i++){
                        var objAux = buscarObjeto($scope.carDocInfo,'conDes', response.data[i].conNom);
                        carDoc.push({
                            carId:response.data[i].carId,
                            conId:response.data[i].conId,
                            conNom:response.data[i].conNom,
                            secId:response.data[i].secId,
                            secNom:response.data[i].secNom,
                            secOrd:response.data[i].secOrd,
                            docCarPedId:objAux.docCarPedId,
                            estAva:objAux.estAva, 
                            rutConExi:objAux.rutConExi, 
                            rutConMen:objAux.rutConMen, 
                            rutConPath:objAux.rutConPath,
                            rutConNomFil:objAux.rutConNomFil
                        });
                    }
                    
                    var auxData = _.groupBy(carDoc,function(obj){
                        return obj.secNom;
                    });
                    console.log(auxData);
                    $rootScope.secciones = [];
                    angular.forEach(auxData,function(obj,key){
                        $rootScope.secciones.push({nom:key,contenidos:new NgTableParams({count:15},{
                            counts: [],
                            paginationMaxBlocks: 13,
                            paginationMinBlocks: 2,
                            dataset:auxData[key]
                        })});
                    });
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
            });
        }
        
        $scope.verContenidoArchivo = function(conInf){
            ModalService.showModal({
                templateUrl: "pedagogico/sma/dre/verContenidoArchivoToDRE.html",
                controller: "verContenidoArchivoToDRECtrl",
                inputs: {
                    title: "Visualización de archivo",
                    conInf: conInf
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };
        
        $scope.verInformeCarPedDoc = function(){
            var informeData = {
                carId: $scope.carDocInfo[0].carId,
                docId: $scope.carDocInfo[0].docId,
                ieId: $scope.carDocInfo[0].ieId,
                dreId: $rootScope.usuMaster.organizacion.organizacionID,
                ugelDat: $scope.carDocInfo[0].ugelDat,
                ieDat: $scope.carDocInfo[0].ieDat,
                docDat: $scope.carDocInfo[0].docDat
            };
            ModalService.showModal({
                templateUrl: "pedagogico/sma/dre/verInformeCarpetaToDRE.html",
                controller: "verInformeCarpetaToDRECtrl",
                inputs: {
                    title: "Visualización de informe",
                    informeData: informeData
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };
}]);
seApp.controller('verContenidoArchivoToDRECtrl', ['$scope', '$rootScope', '$element','title', 'conInf', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, conInf, close, crud, modal, ModalService) {
        $scope.title = title;
        $scope.pathArchivo = "/SIGESMED/" + conInf.rutConPath + conInf.rutConNomFil;  
}]);

seApp.controller('verInformeCarpetaToDRECtrl', ['$sce','$scope', '$rootScope', '$element','title', 'informeData', 'close', 'crud', 'modal', 'ModalService', function ($sce,$scope, $rootScope, $element, title, informeData, close, crud, modal, ModalService) {
        $scope.title = title;
        $scope.pathInforme = "";
        verInforme();
        function verInforme() {
            var request = crud.crearRequest('carpeta_pedagogica',1,'generarInformeCompletitudCarPedDoc');
            request.setData({
                car:informeData.carId, 
                org:informeData.ieId, 
                doc:informeData.docId,
                dreId:informeData.dreId,
                ugelDat:informeData.ugelDat,
                ieDat:informeData.ieDat,
                docDat:informeData.docDat,
                opcion:3
            });
            crud.insertar('/sma',request,function(response){
                if(response.responseSta){
                    $scope.pathInforme =  $sce.trustAsResourceUrl(response.data.file);
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde');
            }); 
        }; 
 
}]);
