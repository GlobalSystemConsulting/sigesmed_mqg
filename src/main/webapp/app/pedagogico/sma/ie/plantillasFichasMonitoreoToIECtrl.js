var seApp = angular.module('app');
seApp.requires.push('angularModalService');
seApp.requires.push('ngAnimate');

seApp.config(['$routeProvider',function($routeProvider){
    $routeProvider.when('/configurar_plantilla_ficha_monitoreo/:data',{
            templateUrl:'pedagogico/sma/ie/configuracionPlantilla.html',
            controller:'configurarPlantillaCtrl',
            controllerAs:'confPlanCtrl'
        });
}]);
seApp.controller("plantillasFichasMonitoreoToIECtrl", ["$location", "$rootScope", "$scope", "NgTableParams", "crud", "modal", "ModalService", function ($location, $rootScope, $scope, NgTableParams, crud, modal, ModalService) {

    //arreglo donde estan todas las carpetas
    $scope.plantillas = [];
    //variable que servira para seleccionar una carpeta
    $scope.plantillaSel = {};
    //Variables para manejo de la tabla
    $rootScope.paramsPlantillasFMSA = {count: 10};
    $rootScope.settingPlantillasFMSA = {counts: []};
    $rootScope.tablaPlantillasFMSA = new NgTableParams($rootScope.paramsPlantillasFMSA, $rootScope.settingPlantillasFMSA);

    $scope.listarPlantillas = function () {
        //preparamos un objeto request
        var request = crud.crearRequest('plantilla_ficha_monitoreo', 1, 'listarPlantillasPorIE');
        request.setData({orgId: $rootScope.usuMaster.organizacion.organizacionID});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
        crud.listar("/sma", request, function (response) {
            if(response.responseSta){
                $rootScope.settingPlantillasFMSA.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingPlantillasFMSA.dataset);
                $rootScope.tablaPlantillasFMSA.settings($rootScope.settingPlantillasFMSA);
            } else{
                modal.mensaje("ERROR", response.responseMsg);
            }
        }, function (error) {
            console.info(error);
            modal.mensaje("ERROR","El servidor no responde");
        });
    };

    $scope.showNuevaPlantilla = function(){
        ModalService.showModal({
            templateUrl: "pedagogico/sma/ie/agregarEditarPlantilla.html",
            controller: "agregarPlantillaCtrl",
            inputs: {
                title: "Nueva Plantilla"
            }
        }).then(function (modal) {
            modal.element.modal();
        });
    };
    
    $scope.showEditarPlantilla = function(p){
        ModalService.showModal({
            templateUrl: "pedagogico/sma/ie/agregarEditarPlantilla.html",
            controller: "editarPlantillaCtrl",
            inputs: {
                title: "Edición de Plantilla",
                plaInfo: p
            }
        }).then(function (modal) {
            modal.element.modal();
        });
    };
    
    $scope.showDetallePlantilla = function(p){
        $location.url('/configurar_plantilla_ficha_monitoreo/'+btoa(JSON.stringify(p)));
    };
    
    $scope.eliminarPlantilla = function(p){   
        modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la plantilla?", function () {
            var request = crud.crearRequest('plantilla_ficha_monitoreo', 1, 'eliminarPlantilla');
            request.setData({plaId: p.plaId});

            crud.eliminar('/sma', request, function (response) {
                if (response.responseSta) {
                    _.remove($rootScope.tablaPlantillasFMSA.settings().dataset, function(item){
                        return p === item;
                    });
                    $rootScope.tablaPlantillasFMSA.reload();
                    modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                }else {
                    modal.mensaje("ERROR",response.responseMsg);
                }
            }, function (error) {
                console.info(error);
                modal.mensaje("ERROR","El servidor no responde");
            });
        }, '400'); 
    };
    
    $scope.showPlantillaPDF = function(p){
        var data = {
            plaId: p.plaId,
            plaCod: p.plaCod,
            plaNom: p.plaNom,
            plaDes: p.plaDes,
            opcion: true
        };
        ModalService.showModal({
            templateUrl: "pedagogico/sma/ie/verPlantillaPDF.html",
            controller: "verPlantillaPDFCtrl",
            inputs: {
                title: "Visualización de Plantilla",
                data: data
            }
        }).then(function (modal) {
            modal.element.modal();
        });
    };
}]);

seApp.controller("agregarPlantillaCtrl", ["$location", "$rootScope", "$scope", "NgTableParams", "crud", "modal", "ModalService", "title", function ($location, $rootScope, $scope, NgTableParams, crud, modal, ModalService, title) {
    $scope.title = title;
    $scope.accionPFM = true;
    $scope.nuevaPlantilla = {
        plaNom: "",
        plaCod: "",
        plaDes: "",
        orgId: $rootScope.usuMaster.organizacion.organizacionID,
        usuId: $rootScope.usuMaster.usuario.usuarioID
    };
    
    $scope.agregarPlantilla = function () {
        var request = crud.crearRequest('plantilla_ficha_monitoreo', 1, 'agregarPlantilla');
        request.setData($scope.nuevaPlantilla);
        crud.insertar("/sma", request, function (response) {
            if (response.responseSta) {
                $('#modalAgregarPlantilla').modal('hide');
                if ($('.modal-backdrop').is(':visible')) {
                    $('body').removeClass('modal-open'); 
                    $('.modal-backdrop').remove(); 
                };
                insertarElemento($rootScope.settingPlantillasFMSA.dataset, response.data);
                $rootScope.tablaPlantillasFMSA.reload();
                
                $location.url('/configurar_plantilla_ficha_monitoreo/'+btoa(JSON.stringify(response.data)));
                modal.mensaje("CONFIRMACION", response.responseMsg);
            } else {
                modal.mensaje("ERROR", response.responseMsg);
            }
        }, function (error) {
            console.info(error);
            modal.mensaje("ERROR","El servidor no responde");
        });
    };
}]);

seApp.controller("editarPlantillaCtrl", ["$rootScope", "$scope", "NgTableParams", "crud", "modal", "ModalService", "title","plaInfo", function ($rootScope, $scope, NgTableParams, crud, modal, ModalService, title, plaInfo) {
    $scope.title = title;
    $scope.accionPFM = false;
    $scope.plantillaSel = {
        plaId: plaInfo.plaId,
        plaNom: plaInfo.plaNom,
        plaCod: plaInfo.plaCod,
        plaDes: plaInfo.plaDes,
        i: plaInfo.i
    };
    
    $scope.actualizarPlantilla = function(){
        var request = crud.crearRequest('plantilla_ficha_monitoreo', 1, 'actualizarDatosBasicosPlantilla');
        request.setData($scope.plantillaSel);
        crud.actualizar("/sma", request, function (response) {
            if (response.responseSta) {
                response.data.i = $scope.plantillaSel.i;
                $rootScope.settingPlantillasFMSA.dataset[$scope.plantillaSel.i] = response.data;
                $rootScope.tablaPlantillasFMSA.reload();
                modal.mensaje("CONFIRMACION", response.responseMsg);
            } else{
               modal.mensaje("ERROR", response.responseMsg); 
            }
        }, function (error) {
            console.info(error);
            modal.mensaje("ERROR","El servidor no responde");
        });
    };
    
}]);

seApp.controller("configurarPlantillaCtrl", ["$routeParams", "$rootScope", "$scope", "NgTableParams", "crud", "modal", "ModalService", function ($routeParams, $rootScope, $scope, NgTableParams, crud, modal, ModalService) {
    var plantillaInfo = JSON.parse(atob($routeParams.data));
    $scope.plantillaNom = plantillaInfo.plaNom;
    //Variables para manejo de tabla de niveles de avance
    $rootScope.paramsValoracionesInd = {count: 10};
    $rootScope.settingValoracionesInd  = {counts: []};
    $rootScope.tablaValoracionesInd  = new NgTableParams($rootScope.paramsValoracionesInd , $rootScope.settingValoracionesInd );
    
    $scope.accionValoracionInd = true;
    $scope.advertenciaCompromiso = false;
    $scope.tipoCompromisos = [{id:'4', des:"Rubrica 4"},{id:'5', des:"Rubrica 5"},{id:'6', des:"Rubrica 6"},{id:'7', des:"Rubrica 7"}];
    
    $scope.nuevaValoracionInd = {
        valIndNom: "",
        valIndDes: "",
        valIndPun: 0,
        plaId: plantillaInfo.plaId,
        usuId: $rootScope.usuMaster.usuario.usuarioID
    };
    
    $scope.nuevoCompromiso = {};
    $scope.nuevaInstruccion = {};
    $scope.nuevoIndicador = {};
    
    $scope.compromisos = [];
    
    //Niveles de avance
    $scope.listarValoracionesInd = function () {
        //preparamos un objeto request
        var request = crud.crearRequest('plantilla_ficha_monitoreo', 1, 'listarValoracionesInd');
        request.setData({plaId: plantillaInfo.plaId});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
        crud.listar("/sma", request, function (response) {
            if(response.responseSta){
                $rootScope.settingValoracionesInd.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingValoracionesInd.dataset);
                $rootScope.tablaValoracionesInd.settings($rootScope.settingValoracionesInd);
            } else{
                modal.mensaje("ERROR", response.responseMsg);
            }
        }, function (error) {
            console.info(error);
            modal.mensaje("ERROR","El servidor no responde");
        });
    };
    
    $scope.agregarValoracionInd = function(){
        var request = crud.crearRequest('plantilla_ficha_monitoreo', 1, 'agregarValoracionInd');
        request.setData($scope.nuevaValoracionInd);
        crud.insertar("/sma", request, function (response) {
            if (response.responseSta) {
                $scope.nuevaValoracionInd.valIndNom = "";
                $scope.nuevaValoracionInd.valIndDes = "";
                $scope.nuevaValoracionInd.valIndPun = 0;
                insertarElemento($rootScope.settingValoracionesInd.dataset, response.data);
                $rootScope.tablaValoracionesInd.reload();
                modal.mensaje("CONFIRMACION", response.responseMsg);
            } else{
                modal.mensaje("ERROR", response.responseMsg);
            }
        }, function (error) {
            console.info(error);
            modal.mensaje("ERROR","El servidor no responde");
        });
    };
    
    $scope.valoracionIndSel = {};
    $scope.prepararEditarValoracionInd = function(vi){
        $scope.accionValoracionInd = false;
        $scope.valoracionIndSel.valIndId = vi.valIndId;
        $scope.valoracionIndSel.valIndNom = vi.valIndNom;
        $scope.valoracionIndSel.valIndDes = vi.valIndDes;
        $scope.valoracionIndSel.valIndPun = vi.valIndPun;
        $scope.valoracionIndSel.plaId = vi.plaId;
        $scope.valoracionIndSel.i = vi.i;
    };
    
    $scope.actualizarAccionValInd = function(){
        $scope.accionValoracionInd = true;
        $scope.nuevaValoracionInd.valIndNom = "";
        $scope.nuevaValoracionInd.valIndDes = "";
        $scope.nuevaValoracionInd.valIndPun = 0;
    };
    
    $scope.actualizarValoracionInd = function(){
        var request = crud.crearRequest('plantilla_ficha_monitoreo', 1, 'actualizarValoracionInd');
        request.setData($scope.valoracionIndSel);
        crud.actualizar("/sma", request, function (response) {
            if (response.responseSta) {
                $scope.accionValoracionInd = true;
                $scope.nuevaValoracionInd.valIndNom = "";
                $scope.nuevaValoracionInd.valIndDes = "";
                $scope.nuevaValoracionInd.valIndPun = 0;
                response.data.i = $scope.valoracionIndSel.i;
                response.data.plaId = $scope.valoracionIndSel.plaId;
                $rootScope.settingValoracionesInd.dataset[$scope.valoracionIndSel.i] = response.data;
                $rootScope.tablaValoracionesInd.reload();
                modal.mensaje("CONFIRMACION", response.responseMsg);
            } else{
                modal.mensaje("ERROR", response.responseMsg);
            }
        }, function (error) {
            console.info(error);
            modal.mensaje("ERROR","El servidor no responde");
        });
    };
    
    $scope.eliminarValoracionInd = function(vi){   
        modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el nivel de avance?", function () {
            var request = crud.crearRequest('plantilla_ficha_monitoreo', 1, 'eliminarValoracionInd');
            request.setData({valIndId: vi.valIndId});

            crud.eliminar('/sma', request, function (response) {
                if (response.responseSta) {
                    _.remove($rootScope.tablaValoracionesInd.settings().dataset, function(item){
                        return vi === item;
                    });
                    $rootScope.tablaValoracionesInd.reload();
                    modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                }else {
                        modal.mensaje("ERROR",response.responseMsg);
                }
            }, function(error) {
                console.info(error);
                modal.mensaje("ERROR","El servidor no responde");
            });
        }, '400'); 
    };
        
    listarContenidoPlantilla();
    function listarContenidoPlantilla(){
        var request = crud.crearRequest('plantilla_ficha_monitoreo',1,'listarContenidoPlantilla');
        request.setData({plaId: plantillaInfo.plaId});
        crud.listar('/sma',request,function(response){
            if(response.responseSta){
                $scope.compromisos = response.data;
            }else{
                modal.mensaje('ERROR',response.responseMsg);
            }
        },function(error){
            console.info(error);
            modal.mensaje("ERROR","El servidor no responde");
        });
    };
    
    //Compromisos Pedagogicos
    var nuevoCompromisoGP = {
        edi: false,
        plaId: plantillaInfo.plaId,
        usuId: $rootScope.usuMaster.usuario.usuarioID
    };
        
    $scope.agregarCompromiso = function() {
        $scope.nuevoCompromiso.instrucciones = [];
        $scope.compromisos.push($scope.nuevoCompromiso);

        nuevoCompromisoGP.comDes = $scope.nuevoCompromiso.comDes;
        nuevoCompromisoGP.comTip = $scope.nuevoCompromiso.comTip;
        $scope.nuevoCompromiso = {};
        
        var request = crud.crearRequest('plantilla_ficha_monitoreo', 1, 'agregarCompromisoGP');
        request.setData(nuevoCompromisoGP);
        crud.insertar("/sma", request, function (response) {
            if (response.responseSta) {
                angular.extend($scope.compromisos[$scope.compromisos.length-1], response.data);
                modal.mensaje("CONFIRMACION", response.responseMsg);
            } else{
                modal.mensaje("ERROR", response.responseMsg);
            }
        }, function (error) {
            console.info(error);
            modal.mensaje("ERROR","El servidor no responde");
        });
    };
    
    var compromisoGPSel = {
        plaId: plantillaInfo.plaId,
        usuId: $rootScope.usuMaster.usuario.usuarioID
    };
    $scope.editarCompromiso = function(c) {
        if(c.edi){                        
            c.comDes = c.copia.comDes;
            compromisoGPSel.comId = c.comId;
            compromisoGPSel.comDes = c.comDes;
            compromisoGPSel.comTip = c.comTip;
            delete c.copia;
            
            var request = crud.crearRequest('plantilla_ficha_monitoreo', 1, 'actualizarCompromisoGP');
            request.setData(compromisoGPSel);
            crud.actualizar("/sma", request, function (response) {
                if (response.responseSta) {
                    angular.extend(c, response.data);
                    c.edi = false; 
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                } else{
                    modal.mensaje("ERROR",response.responseMsg);
                }
            }, function (error) {
                console.info(error);
                modal.mensaje("ERROR","El servidor no responde");
            });
        }
        else{
            c.copia = JSON.parse(JSON.stringify(c));            
            c.edi = true;
        }
    };
    
    $scope.eliminarCompromiso = function(index) {  
        modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el compromiso de gestión pedagógica?", function () {
            var request = crud.crearRequest('plantilla_ficha_monitoreo', 1, 'eliminarCompromisoGP');
            request.setData({comId: $scope.compromisos[index].comId});
            crud.eliminar('/sma', request, function (response) {
                if (response.responseSta) {
                    $scope.compromisos.splice(index, 1);
                    modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                }else {
                    modal.mensaje("ERROR",response.responseMsg);
                }
            }, function (error) {
                console.info(error);
                modal.mensaje("ERROR","El servidor no responde");
            });
        }, '400'); 
    };
    
    //Instrucciones
    var nuevaInstruccionLI = {
        edi: false,
        insDes: "",
        comId: "",
        usuId: $rootScope.usuMaster.usuario.usuarioID
    };
    
    $scope.agregarInstruccion = function(compromiso) {
        compromiso.nuevaInstruccion.indicadores = [];
        compromiso.instrucciones.push(compromiso.nuevaInstruccion);
        
        nuevaInstruccionLI.insDes = compromiso.nuevaInstruccion.insDes;
        nuevaInstruccionLI.comId = compromiso.comId;
        compromiso.nuevaInstruccion = {};
        
        var request = crud.crearRequest('plantilla_ficha_monitoreo', 1, 'agregarInstruccionLI');
        request.setData(nuevaInstruccionLI);
        crud.insertar("/sma", request, function (response) {
            if (response.responseSta) {
                angular.extend(compromiso.instrucciones[compromiso.instrucciones.length-1], response.data);
                modal.mensaje("CONFIRMACION", response.responseMsg);
            } else{
                modal.mensaje("ERROR", response.responseMsg);
            }
        }, function (error) {
            console.info(error);
            modal.mensaje("ERROR","El servidor no responde");
        });
    };
    
    var instruccionLISel = {
        usuId: $rootScope.usuMaster.usuario.usuarioID
    };
    $scope.editarInstruccion = function(i) {
        if(i.edi){            
            i.insDes = i.copia.insDes;
            
            instruccionLISel.insId = i.insId;
            instruccionLISel.insDes = i.insDes;
            
            delete i.copia;
            
            var request = crud.crearRequest('plantilla_ficha_monitoreo', 1, 'actualizarInstruccionLI');
            request.setData(instruccionLISel);
            crud.actualizar("/sma", request, function (response) {
                if (response.responseSta) {
                    angular.extend(i, response.data);
                    i.edi = false; 
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                } else{
                    modal.mensaje("ERROR",response.responseMsg);
                }
            }, function (error) {
                console.info(error);
                modal.mensaje("ERROR","El servidor no responde");
            });
        }
        else{
            i.copia = JSON.parse(JSON.stringify(i));            
            i.edi = true;
        }
    };
    
    $scope.eliminarInstruccion = function(index, compromiso) {
        modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la instrucción de llenado de indicador(es)?", function () {
            var request = crud.crearRequest('plantilla_ficha_monitoreo', 1, 'eliminarInstruccionLI');
            request.setData({insId: compromiso.instrucciones[index].insId});
            crud.eliminar('/sma', request, function (response) {
                if (response.responseSta) {
                    compromiso.instrucciones.splice(index, 1);
                    modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                }else {
                    modal.mensaje("ERROR",response.responseMsg);
                }
            }, function (error) {
                console.info(error);
                modal.mensaje("ERROR","El servidor no responde");
            });
        }, '400');
    };
    
    //Indicadores
    var nuevoIndicadorCGP = {
        edi: false,
        indDes: "",
        insId: "",
        usuId: $rootScope.usuMaster.usuario.usuarioID
    };
    $scope.agregarIndicador = function(instruccion) { 
        instruccion.indicadores.push(instruccion.nuevoIndicador);
        
        nuevoIndicadorCGP.indDes = instruccion.nuevoIndicador.indDes;
        nuevoIndicadorCGP.insId = instruccion.insId;
        
        instruccion.nuevoIndicador = {};
        
        var request = crud.crearRequest('plantilla_ficha_monitoreo', 1, 'agregarIndicadorCGP');
        request.setData(nuevoIndicadorCGP);
        crud.insertar("/sma", request, function (response) {
            if (response.responseSta) {
                angular.extend(instruccion.indicadores[instruccion.indicadores.length-1], response.data);
                modal.mensaje("CONFIRMACION", response.responseMsg);
            } else{
                modal.mensaje("ERROR", response.responseMsg);
            }
        }, function (error) {
            console.info(error);
            modal.mensaje("ERROR","El servidor no responde");
        });
    };
    
    var indicadorCGPSel = {
        usuId: $rootScope.usuMaster.usuario.usuarioID
    };
    $scope.editarIndicador = function(ind) {
        if(ind.edi){            
            ind.indDes = ind.copia.indDes;
            
            indicadorCGPSel.indId = ind.indId;
            indicadorCGPSel.indDes = ind.indDes;
            
            delete ind.copia;
            
            var request = crud.crearRequest('plantilla_ficha_monitoreo', 1, 'actualizarIndicadorCGP');
            request.setData(indicadorCGPSel);
            crud.actualizar("/sma", request, function (response) {
                if (response.responseSta) {
                    angular.extend(ind, response.data);
                    ind.edi = false; 
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                } else{
                    modal.mensaje("ERROR",response.responseMsg);
                }
            }, function (error) {
                console.info(error);
                modal.mensaje("ERROR","El servidor no responde");
            });
        }
        else{
            ind.copia = JSON.parse(JSON.stringify(ind));            
            ind.edi = true;
        }
    };
    
    $scope.eliminarIndicador = function(index, instruccion) {
        modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el indicador de compromiso de gestión pedagógica?", function () {
            var request = crud.crearRequest('plantilla_ficha_monitoreo', 1, 'eliminarIndicadorCGP');
            request.setData({indId: instruccion.indicadores[index].indId});
            crud.eliminar('/sma', request, function (response) {
                if (response.responseSta) {
                    instruccion.indicadores.splice(index, 1);
                    modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                }else {
                    modal.mensaje("ERROR",response.responseMsg);
                }
            }, function (error) {
                console.info(error);
                modal.mensaje("ERROR","El servidor no responde");
            });
        }, '400');
    };
    
    /*$scope.visualizarPlantillaSC = function(){
        console.log(plantillaInfo);
        console.log($rootScope.tablaValoracionesInd.data);
        console.log($scope.compromisos);
    };*/
    
    $scope.prepararVisualizacionPlantilla = function(){
        var totInd = hallarTotalIndicadores($scope.compromisos);
        var data = {
            plaInfBas: plantillaInfo,
            plaNivAva: $rootScope.tablaValoracionesInd.data,
            plaCon:$scope.compromisos,
            totInd: totInd,
            opcion: false
        };
        ModalService.showModal({
            templateUrl: "pedagogico/sma/ie/verPlantillaPDF.html",
            controller: "verPlantillaPDFCtrl",
            inputs: {
                title: "Visualización de plantilla",
                data: data
            }
        }).then(function (modal) {
            modal.element.modal();
        });
    };
    
    function hallarTotalIndicadores(arrayCompromisos){
        var totInd = 0;
        for(i=0;i<arrayCompromisos.length;i++){
            var totPar = 0;
            for(j=0;j<arrayCompromisos[i].instrucciones.length; j++){
                totPar+=arrayCompromisos[i].instrucciones[j].indicadores.length;
            }
            totInd+=totPar;
        }
        return totInd;
    }
}]);
seApp.controller('verPlantillaPDFCtrl', ['$sce','$scope', '$rootScope', '$element','title', 'data', 'close', 'crud', 'modal', 'ModalService', function ($sce,$scope, $rootScope, $element, title, data, close, crud, modal, ModalService) {
        $scope.title = title;
        $scope.pathPlantilla = "";
        
        inicio();
        function inicio(){
            console.log(data);
            if(data.opcion){ 
                console.log("con consulta");
                var request = crud.crearRequest('plantilla_ficha_monitoreo',1,'verPlantillaPDFConConsulta');
                request.setData({
                    plaId: data.plaId,
                    plaCod: data.plaCod,
                    plaNom: data.plaNom,
                    plaDes: data.plaDes
                });
                crud.insertar('/sma',request,function(response){
                    if(response.responseSta){
                        $scope.pathPlantilla =  $sce.trustAsResourceUrl(response.data.file);
                    }else{
                        modal.mensaje('ERROR',response.responseMsg);
                    }
                },function(error){
                    console.info(error);
                    modal.mensaje('ERROR','El servidor no responde');
                });
            }else{
                console.log("sin consulta");
                var request = crud.crearRequest('plantilla_ficha_monitoreo',1,'verPlantillaPDFSinConsulta');
                request.setData({
                    plaInfBas: data.plaInfBas,
                    plaNivAva: data.plaNivAva,
                    plaCon: data.plaCon,
                    totInd: data.totInd
                });
                crud.insertar('/sma',request,function(response){
                    if(response.responseSta){
                        $scope.pathPlantilla =  $sce.trustAsResourceUrl(response.data.file);
                    }else{
                        modal.mensaje('ERROR',response.responseMsg);
                    }
                },function(error){
                    console.info(error);
                    modal.mensaje('ERROR','El servidor no responde');
                });
            }
        }
}]);

