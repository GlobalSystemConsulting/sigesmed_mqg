var seApp = angular.module('app');
seApp.requires.push('angularModalService');
seApp.requires.push('ngAnimate');

seApp.config(['$routeProvider',function($routeProvider){
    $routeProvider.when('/ver_detalle_carpeta_docente/:data',{
            templateUrl:'pedagogico/sma/ie/detalleCarpetaDocenteToIE.html',
            controller:'verCarpetaDocenteDetalleByIECtrl',
            controllerAs:'verCarpetaDocenteIECtrl'
        });
}]);
seApp.controller("monitoreoCarpetaPedagogicaCtrl", ["$location", "$rootScope", "$scope", "NgTableParams", "crud", "modal", "ModalService", function ($location, $rootScope, $scope, NgTableParams, crud, modal, ModalService) {

        //arreglo donde estan todas las carpetas
        $scope.carpetas = [];
        //variable que servira para seleccionar una carpeta
        $scope.carpetaSel = {};
        //Variables para manejo de la tabla
        $rootScope.paramsCarpetas = {count: 10};
        $rootScope.settingCarpetas = {counts: []};
        $rootScope.tablaCarpetas = new NgTableParams($rootScope.paramsCarpetas, $rootScope.settingCarpetas);
        $scope.contenidosDocentes = {};
        function buscarDocenteId(lista, docId){
            for(i=0;i<lista.length;i++){
                if(lista[i]===docId)
                    return true;
            }
            return false;
        }
        $scope.listarCarpetas = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('carpeta_pedagogica', 1, 'listarCarpetasPorIE');
            request.setData({orgId: $rootScope.usuMaster.organizacion.organizacionID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sma", request, function (response) {
                if(response.responseSta){
                    $scope.contenidosDocentes = response.data;
                    var registrados = [];
                    var carpetasDocentes = [];
                    response.data.forEach(function(item){
                        if(!buscarDocenteId(registrados, item.docId)){
                            registrados.push(item.docId);
                            carpetasDocentes.push(item);
                        }
                    });
                    $rootScope.settingCarpetas.dataset = carpetasDocentes;
                    //asignando la posicion en el arreglo a cada objeto
                    iniciarPosiciones($rootScope.settingCarpetas.dataset);
                    $rootScope.tablaCarpetas.settings($rootScope.settingCarpetas);
                } else{
                    modal.mensaje("ERROR", response.responseMsg);
                }
            }, function (error) {
                console.info(error);
                modal.mensaje("ERROR","El servidor no responde");
            });
        };
        
        $scope.verCarpetaDetalle = function (docId) {
            var docenteContenidos = buscarObjetos($scope.contenidosDocentes,'docId', docId );
            $location.url('/ver_detalle_carpeta_docente/'+btoa(JSON.stringify(docenteContenidos)));
        };
        
    }]);

seApp.controller('verCarpetaDocenteDetalleByIECtrl', ['$routeParams', '$scope', '$rootScope', '$http', 'NgTableParams', 'crud','modal', 'ModalService', function ($routeParams, $scope, $rootScope, $http, NgTableParams, crud, modal, ModalService) {
        $scope.estadoAvance = [{id:1,title:"Completo"}, {id:2,title:"En Proceso"}, {id:3,title:"Pendiente"}];
        $scope.carDocInfo = JSON.parse(atob($routeParams.data)); 
        $scope.docenteData = $scope.carDocInfo[0].docDat;
        cargarCarSecciones();
        function cargarCarSecciones(){
            var request = crud.crearRequest('carpeta_pedagogica',1,'verDetalleCarpeta');
            request.setData({car:$scope.carDocInfo[0].carId,org:$rootScope.usuMaster.organizacion.organizacionID, doc:$scope.carDocInfo[0].docId});
            crud.listar('/sma',request,function(response){
                if(response.responseSta){
                    var carDoc = [];
                    for(i=0; i<response.data.length; i++){
                        var objAux = buscarObjeto($scope.carDocInfo,'conDes', response.data[i].conNom);
                        carDoc.push({
                            carId:response.data[i].carId,
                            conId:response.data[i].conId,
                            conNom:response.data[i].conNom,
                            secId:response.data[i].secId,
                            secNom:response.data[i].secNom,
                            secOrd:response.data[i].secOrd,
                            docCarPedId:objAux.docCarPedId,
                            estAva:objAux.estAva, 
                            rutConExi:objAux.rutConExi, 
                            rutConMen:objAux.rutConMen, 
                            rutConPath:objAux.rutConPath,
                            rutConNomFil:objAux.rutConNomFil
                        });
                    }
                    
                    var auxData = _.groupBy(carDoc,function(obj){
                        return obj.secNom;
                    });
                    $rootScope.secciones = [];
                    angular.forEach(auxData,function(obj,key){
                        $rootScope.secciones.push({nom:key,contenidos:new NgTableParams({count:15},{
                            counts: [],
                            paginationMaxBlocks: 13,
                            paginationMinBlocks: 2,
                            dataset:auxData[key]
                        })});
                    });
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            }, function(error){
                console.info(error);
                modal.mensaje("ERROR","El servidor no responde");
            });
        }
        
        $scope.verContenidoArchivo = function(conInf){
            ModalService.showModal({
                templateUrl: "pedagogico/sma/ie/verContenidoArchivo.html",
                controller: "verContenidoArchivoCtrl",
                inputs: {
                    title: "Visualización de archivo",
                    conInf: conInf
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.verInformeCarPedDoc = function(){
            var informeData = {
                carId: $scope.carDocInfo[0].carId,
                docId: $scope.carDocInfo[0].docId,
                docDat: $scope.carDocInfo[0].docDat
            };
            ModalService.showModal({
                templateUrl: "pedagogico/sma/ie/verInformeCarpeta.html",
                controller: "verInformeCarpetaCtrl",
                inputs: {
                    title: "Visualización de informe",
                    informeData: informeData
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
}]);
seApp.controller('verContenidoArchivoCtrl', ['$scope', '$rootScope', '$element','title', 'conInf', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, conInf, close, crud, modal, ModalService) {
        $scope.estadoAvance = [{id:1,title:"Completo"}, {id:2,title:"En Proceso"}, {id:3,title:"Pendiente"}];
        $scope.pathArchivo = "/SIGESMED/" + conInf.rutConPath + conInf.rutConNomFil;  
        $scope.docenteContenidoArchivo = {
            docCarPedId: conInf.docCarPedId,
            estAva: conInf.estAva
        };
        
        $scope.actualizarEstadoAvance = function(){
            var request = crud.crearRequest('carpeta_pedagogica', 1, 'actualizarEstadoAvance');
            request.setData($scope.docenteContenidoArchivo);
            crud.actualizar("/sma", request, function (response) {
                if (response.responseSta) {
                    response.data.carId = conInf.carId;
                    response.data.conId = conInf.conId;
                    response.data.conNom = conInf.conNom;
                    response.data.secId = conInf.secId;
                    response.data.secNom = conInf.secNom;
                    response.data.secOrd = conInf.secOrd;
                    response.data.rutConExi = conInf.rutConExi;
                    response.data.rutConMen = conInf.rutConMen;
                    response.data.rutConPath = conInf.rutConPath;
                    response.data.rutConNomFil = conInf.rutConNomFil;
                    response.data.i = conInf.i;
                    
                    angular.extend(conInf,response.data);
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                } else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            }, function (error) {
                console.info(error);
                modal.mensaje("ERROR","El servidor no responde");
            });
        };
}]);

seApp.controller('verInformeCarpetaCtrl', ['$sce','$scope', '$rootScope', '$element','title', 'informeData', 'close', 'crud', 'modal', 'ModalService', function ($sce,$scope, $rootScope, $element, title, informeData, close, crud, modal, ModalService) {
        $scope.pathInforme = "";
        verInforme();
        function verInforme() {
            var request = crud.crearRequest('carpeta_pedagogica',1,'generarInformeCompletitudCarPedDoc');
            request.setData({
                car:informeData.carId, 
                org:$rootScope.usuMaster.organizacion.organizacionID, 
                doc:informeData.docId,
                docDat:informeData.docDat,
                opcion:1
            });
            crud.insertar('/sma',request,function(response){
                if(response.responseSta){
                    $scope.pathInforme =  $sce.trustAsResourceUrl(response.data.file);
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(error){
                console.info(error);
                modal.mensaje('ERROR','El servidor no responde');
            }); 
        }; 
}]);
