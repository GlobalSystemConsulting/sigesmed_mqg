
app.controller("tareasCtrl", ["$scope", "NgTableParams", "crud", "modal", "$rootScope", function ($scope, NgTableParams, crud, modal, $rootScope) {

        $scope.plan = {};
        $scope.gradoSel = {};
        $scope.seccionSel = '';
        $scope.areaSel = '';
        $scope.tarea = {};
        $scope.numSel = true; // (Nota numerica o Literal)

        $scope.configuracion_nota_docente = []; // Cuadro de Equivalencia Docente

        // nota tarea actual seleccionada
        $scope.nota_act_sel = {
            nota: ""
        };


        ///TAREA ACTUAL SELECCIONADA
        $scope.tarea_act_sel;

        //DATOS DE TAREA CREADO EN EL MODULO MAESTRO
        $scope.tarea_maestro;

        var tareas = [];


        //


        //tabla de tareas
        var paramsTarea = {count: 10};
        var settingTarea = {counts: []};
        $scope.tablaTarea = new NgTableParams(paramsTarea, settingTarea);

        $scope.estados = [{id: 'N', title: "nuevo"}, {id: 'E', title: "enviado"}, {id: 'C', title: "calificado"}];

        //tabla de alumnos
        var paramsAlumno = {count: 10};
        var settingAlumno = {counts: []};
        $scope.tablaAlumno = new NgTableParams(paramsAlumno, settingAlumno);

        $scope.prepararAgregar = function (seccionID, areaID) {
            $scope.tarea = {seccionID: seccionID, areaID: areaID};
            $('#modalNuevaTarea').modal('show');
        };
        $scope.agregarTarea = function (usuarioID) {

            if (!$scope.tarea.tareaID) {
                $scope.tarea.planID = $scope.plan.planID;
                $scope.tarea.gradoID = $scope.gradoSel.gradoID;
                $scope.tarea.seccionID = $scope.seccionSel.seccionID;
                $scope.tarea.areaID = $scope.areaSel;
                $scope.tarea.docenteID = usuarioID;

                var request = crud.crearRequest('tarea', 1, 'insertarTarea');
                request.setData($scope.tarea);
                crud.insertar("/web", request, function (res) {
                    modal.mensaje("CONFIRMACION", res.responseMsg);
                    if (res.responseSta) {
                        //recuperamos las variables que nos envio el servidor
                        $scope.tarea.adjunto = res.data.adjunto;
                        $scope.tarea.tareaID = res.data.tareaID;
                        $scope.tarea.estado = res.data.estado;

                        $scope.tarea.grado = buscarContenido($scope.plan.grados, "gradoID", "grado", $scope.tarea.gradoID);
                        $scope.tarea.area = buscarContenido($scope.plan.areas, "areaID", "area", $scope.tarea.areaID);

                        //insertamos el elemento a la calificarTareaa            
                        insertarElemento(settingTarea.dataset, $scope.tarea);
                        $scope.tablaTarea.reload();
                        //reiniciamos las variables
                        $scope.tarea = {seccionID: $scope.tarea.seccionID, areaID: $scope.tarea.areaID};
                        //$scope.tablaGrupo.reload();
                        //cerramos la ventana modal
                        $('#modalNuevaTarea').modal('hide');
                    }
                }, function (data) {
                    console.info(data);
                });
            } else {
                var request = crud.crearRequest('tarea', 1, 'actualizarTarea');
                request.setData($scope.tarea);
                console.log($scope.tarea);
                crud.actualizar("/web", request, function (res) {
                    modal.mensaje("CONFIRMACION", res.responseMsg);
                    if (res.responseSta) {
                        $scope.tarea.adjunto = res.data.adjunto;
                        settingTarea.dataset[$scope.tarea.i] = $scope.tarea;
                        $scope.tablaTarea.reload();
                        //cerramos la ventana modal
                        $('#modalNuevaTarea').modal('hide');
                    }
                }, function (data) {
                    console.info(data);
                });
            }


        };
        $scope.prepararEditar = function (o) {
            $scope.tarea = JSON.parse(JSON.stringify(o));
            $('#modalNuevaTarea').modal('show');
        };
        $scope.eliminarTarea = function (i, idDato) {

            modal.mensajeConfirmacion($scope, "seguro que desea eliminar la tarea", function () {

                var request = crud.crearRequest('tarea', 1, 'eliminarTarea');
                request.setData({tareaID: idDato});

                crud.eliminar("/web", request, function (response) {

                    modal.mensaje("CONFIRMACION", response.responseMsg);
                    if (response.responseSta) {
                        eliminarElemento(settingTarea.dataset, i);
                        $scope.tablaTarea.reload();
                    }

                }, function (data) {
                    console.info(data);
                });

            }, "400");
        };
        $scope.dateOptions = {
            formatYear: 'yy',
            maxDate: new Date(),
            minDate: new Date(),
            startingDay: 1
        };
        $scope.prepararEnviar = function (o) {
            $scope.tarea = JSON.parse(JSON.stringify(o));
            $scope.fechaMinima = new Date();
            $scope.dateOptions.minDate = $scope.fechaMinima;

            var fechaMax = new Date();
            fechaMax.setMonth(11);
            fechaMax.setDate(31);
            fechaMax.setHours(23);
            $scope.dateOptions.maxDate = fechaMax;

            $scope.tarea.fecha = new Date();
            $scope.tarea.fecha.setSeconds(0);
            $('#modalEnvioTarea').modal('show');
        };
        $scope.enviarTarea = function (orgID) {

            //$scope.tarea.fechaEnvio = $scope.tarea.fecha.toLocaleString();
            $scope.tarea.fechaEntrega = convertirFecha2($scope.tarea.fecha) + " " + convertirHora($scope.tarea.fecha);
            $scope.tarea.organizacionID = orgID;

            var request = crud.crearRequest('tarea', 1, 'enviarTarea');
            request.setData($scope.tarea);
            crud.insertar("/web", request, function (res) {
                modal.mensaje("CONFIRMACION", res.responseMsg);
                if (res.responseSta) {
                    //recuperamos las variables que nos envio el servidor
                    $scope.tarea.estado = res.data.estado;
                    settingTarea.dataset[$scope.tarea.i] = $scope.tarea;
                    $scope.tablaTarea.reload();
                    //cerramos la ventana modal
                    $('#modalEnvioTarea').modal('hide');
                }
            }, function (data) {
                console.info(data);
            });
        };

        $scope.prepararTarea = function (tarea) {

            $scope.tarea = JSON.parse(JSON.stringify(tarea));
            $scope.tarea_act_sel = tarea.tareaID;
            //preparamos un objeto request
            var request = crud.crearRequest('tarea', 1, 'listarAlumnosCumplieronTarea');
            request.setData({tareaID: tarea.tareaID});
            crud.listar("/web", request, function (res) {

                if (res.responseSta) {

                    settingAlumno.dataset = res.data;
                    $scope.tablaAlumno.settings(settingAlumno);

                    $scope.tareaSel = tarea;
                    $scope.active = 1;
                }

            }, function (data) {
                console.info(data);
            });

        };

        $scope.prepararCalificar = function (alumno) {

            $scope.alumno = JSON.parse(JSON.stringify(alumno));
            $scope.alumno.nombres = alumno.nombres + " " + alumno.apellidos;

            //preparamos un objeto request
            var request = crud.crearRequest('tarea', 1, 'verDocumentosDeTarea');
            request.setData({bandejaTareaID: alumno.bandejaTareaID});
            crud.listar("/web", request, function (res) {

                if (res.responseSta) {

                    $scope.documentos = res.data;
                    $scope.active = 2;
                }

            }, function (data) {
                console.info(data);
            });

        };
        $scope.calcularNota = function () {
            var nota = 0;
            $scope.documentos.forEach(function (item) {
                nota += item.nota ? item.nota : 0;
            });
            $scope.alumno.nota = nota / $scope.documentos.length;
            $scope.alumno.nota = $scope.alumno.nota.toFixed(0);//solo se permite numero sin decimales

        };

        $scope.obtener_nota_literal = function () {
            //
            var size = $scope.configuracion_nota_docente.length;
            var nota_alu = $scope.alumno.nota;
            for (var i = 0; i < size; i++) {
                var conf_act = $scope.configuracion_nota_docente[i];
                if (parseInt(nota_alu) >= conf_act.not_min && parseInt(nota_alu) <= conf_act.not_max) {
                    $scope.alumno.nota_lit = conf_act.cod;
                    break;
                }
            }
        }
        $scope.calificarTarea = function () {

            if ($scope.numSel == false) {
                $scope.alumno.nota = parseInt($scope.nota_act_sel.nota.not_max); // Por defecto se tomara la nota Mayor (OJO)
                $scope.alumno.nota_lit = $scope.nota_act_sel.nota.cod;
            } else {
                if (!$scope.alumno.nota || isNaN($scope.alumno.nota)) {
                    modal.mensaje("CONFIRMACION", "La nota no es un valor valido");
                    return;
                } else {
                    $scope.obtener_nota_literal(); // Pasar de numeral a literal

                }
            }

            modal.mensajeConfirmacion($scope, "¿Esta seguro que la calificacion es correcta?", function () {

                var request = crud.crearRequest('tarea', 1, 'calificarTarea');
                request.setData({bandejaTareaID: $scope.alumno.bandejaTareaID, nota: $scope.alumno.nota, nota_lit: $scope.alumno.nota_lit});
                crud.actualizar("/web", request, function (res) {
                    if (res.responseSta) {
                        modal.mensaje("CONFIRMACION", res.responseMsg);
                        //recuperamos las variables que nos envio el servidor
                        $scope.alumno.estado = res.data.estado;
                        $scope.alumno.nota = res.data.nota;
                        $scope.alumno.nota_lit = res.data.not_lit;
                        settingAlumno.dataset[$scope.alumno.i] = $scope.alumno;
                        $scope.tablaAlumno.reload();

                        $scope.active = 1;
                        console.log($scope.tarea);
                        console.log($scope.alumno);
                        
                        var nota = res.data.nota;
                        var notLit = res.data.notLit;
                        var request = crud.crearRequest('tarea', 1, 'registrarNotaIndicadorTar');
                        request.setData({
                            sesId: $scope.tarea.sesionId,
                            tipPer: $scope.tarea.tipPeriodo,
                            numPer: $scope.tarea.idPer_dt,
                            graId: $scope.tarea.gradoID,
                            usuId: $rootScope.usuMaster.usuario.usuarioID,
                            nota: nota,
                            notLit: notLit,
                            tarIdMM: $scope.tarea.idTareaMM,
                            idOrg: $rootScope.usuMaster.organizacion.organizacionID,
                            idSeccion: $scope.tarea.seccionID,
                            idAlumno: $scope.alumno.alu_id
                        });
                        crud.actualizar("/web", request, function (res) {
                            if (res.responseSta) {
                                modal.mensaje("CONFIRMACION", res.responseMsg);
                                var request = crud.crearRequest('tarea', 1, 'registrarPromedioIndicadoresTar');
                                request.setData({
                                    areCurId: $scope.tarea.areaID,
                                    tipPer: $scope.tarea.tipPeriodo,
                                    numPer: $scope.tarea.idPer_dt,
                                    graId: $scope.tarea.gradoID,
                                    usuId: $rootScope.usuMaster.usuario.usuarioID,
                                    nota: nota,
                                    notLit: notLit,
                                    tarIdMM: $scope.tarea.idTareaMM,
                                    idOrg: $rootScope.usuMaster.organizacion.organizacionID,
                                    idSeccion: $scope.tarea.seccionID,
                                    idAlumno: $scope.alumno.alu_id,
                                    idPlaEst: $scope.tarea.planID
                                });
                                crud.actualizar("/web", request, function (res) {
                                    if (res.responseSta) {
                                        modal.mensaje("CONFIRMACION", res.responseMsg);
                                        var request = crud.crearRequest('tarea', 1, 'registrarPromedioCompetenciasTar');
                                        request.setData({
                                            areCurId: $scope.tarea.areaID,
                                            idSesion: $scope.tarea.sesionID,
                                            tipPer: $scope.tarea.tipPeriodo,
                                            numPer: $scope.tarea.idPer_dt,
                                            graId: $scope.tarea.gradoID,
                                            usuId: $rootScope.usuMaster.usuario.usuarioID,
                                            nota: nota,
                                            notLit: notLit,
                                            tarIdMM: $scope.tarea.idTareaMM,
                                            idOrg: $rootScope.usuMaster.organizacion.organizacionID,
                                            idSeccion: $scope.tarea.seccionID,
                                            idAlumno: $scope.alumno.alu_id,
                                            idPlaEst: $scope.tarea.planID
                                        });
                                        crud.actualizar("/web", request, function (res) {
                                            if (res.responseSta) {
                                                modal.mensaje("CONFIRMACION", res.responseMsg);
                                                var request = crud.crearRequest('tarea', 1, 'registrarPromedioAreaPorPeriodoTar');
                                                request.setData({
                                                    areCurId: $scope.tarea.areaID,
                                                    idSesion: $scope.tarea.sesionID,
                                                    tipPer: $scope.tarea.tipPeriodo,
                                                    numPer: $scope.tarea.idPer_dt,
                                                    graId: $scope.tarea.gradoID,
                                                    usuId: $rootScope.usuMaster.usuario.usuarioID,
                                                    nota: nota,
                                                    notLit: notLit,
                                                    tarIdMM: $scope.tarea.idTareaMM,
                                                    idOrg: $rootScope.usuMaster.organizacion.organizacionID,
                                                    idSeccion: $scope.tarea.seccionID,
                                                    idAlumno: $scope.alumno.alu_id,
                                                    idPlaEst: $scope.tarea.planID
                                                });
                                                crud.actualizar("/web", request, function (res) {
                                                    if (res.responseSta) {
                                                        modal.mensaje("CONFIRMACION", res.responseMsg);
                                                    }
                                                }, function (data) {
                                                    console.info(data);
                                                });
                                            }
                                        }, function (data) {
                                            console.info(data);
                                        });
                                    }
                                }, function (data) {
                                    console.info(data);
                                });
                            }
                        }, function (data) {
                            console.info(data);
                        });

                    }
                }, function (data) {
                    console.info(data);
                });
            }, "400");
        };

        $scope.actualizar_notas = function () {


            modal.mensajeConfirmacion($scope, "¿Esta Seguro que desea Actualizar el Registro Auxiliar de Notas?", function () {

                //OBTENEMOS LOS INDICADORES DE LA TAREA
                $scope.ind_tar;
                console.log($scope.ind_tar);

                //ESTUDIANTES EN GENERAL 
                $scope.est_general = new Object();

                //NOTAS
                $scope.notas_indicador = new Object();

                var request = crud.crearRequest('tarea', 1, 'obtener_indicadores_tarea');
                request.setData({tareaID: $scope.tarea_act_sel});
                crud.listar("/web", request, function (res) {
                    if (res.responseSta) {

                        console.log(res);
                        $scope.ses_id = res.data[0].ses_id;
                        $scope.gra_id = res.data[0].gra_id;
                        $scope.sec_id = res.data[0].sec_id;

                        // CREAMOS EL ARRAY CON LOS INDICADORES Y NOTAS DE LOS ALUMNOS
                        $scope.notas_indicador.ses = $scope.ses_id;
                        $scope.notas_indicador.per = settingAlumno.dataset[0].tip_per;

                        $scope.notas_indicador.numper = settingAlumno.dataset[0].id_per;

                        $scope.notas_indicador.ses = 2;

                        $scope.notas_indicador.per = 'B';

                        $scope.notas_indicador.numper = 1;



                        $scope.notas_indicador.tip = "N";
                        $scope.notas_indicador.estudiantes = new Array();

                        $scope.obtener_alumnos($scope.gra_id, $scope.sec_id, $scope.notas_indicador.ses, $scope.notas_indicador.per, $scope.notas_indicador.numper);
                    }
                });

            }, "400");
        };


        $scope.obtener_alumnos = function (grad_id, sec_id, ses_id, per_, num_per) {
            var request = crud.crearRequest('notas_estudiante', 1, 'listarNotasIndicadoresAlumnos');

            $scope.data_alu = new Object();
            $scope.data_alu.org = $rootScope.usuMaster.organizacion.organizacionID;
            $scope.data_alu.gra = grad_id;
            $scope.data_alu.sec = sec_id;
            $scope.data_alu.ses = ses_id;
            $scope.data_alu.per = per_;
            $scope.data_alu.numper = num_per;

            request.setData($scope.data_alu);
            crud.listar('/submodulo_academico', request, function (response) {

                if (response.responseSta) {

                    $scope.est_general = response.data.estudiantes;

                    $scope.alumnoTmp = {nota: 0.0, nota_lit: ""};

                    //PASAMOS LAS NOTAS PARA ENVIARLAS AL REGISTRO DE NOTAS
                    for (var i = 0; i < settingAlumno.total; i++) {
                        console.log("est_general");
                        console.log($scope.est_general);
                        for (j = 0; j < $scope.est_general.length; j++) {
                            console.log("settingAlumno.dataset");
                            console.log(settingAlumno.dataset);
                            if (settingAlumno.dataset[i].alu_id == $scope.est_general[j].id) {
                                // Actualizamos la Nota de la Tarea en cada Indicador por Sesion de Aprendizaje

                                for (k = 0; k < $scope.est_general[j].notas.length; k++) {

                                    //if($scope.est_general[j].notas[k].not != "0" && $scope.est_general[j].notas[k].not != "0.0" && $scope.est_general[j].notas[k].not != " " && $scope.est_general[j].notas[k].not != "NaN"){
                                    if ($scope.est_general[j].notas[k].not === " " && $scope.est_general[j].notas[k].not === "NaN") {
                                        if (settingAlumno.dataset[i].nota.toString() === " " && settingAlumno.dataset[i].nota.toString() === "NaN") {
                                            $scope.alumnoTmp.nota = 0;
                                            $scope.est_general[j].notas[k].not = ($scope.alumnoTmp.nota).toString();
                                        } else {
                                            $scope.alumnoTmp.nota = settingAlumno.dataset[i].nota;
                                            $scope.est_general[j].notas[k].not = ($scope.alumnoTmp.nota).toString();
                                        }
                                        console.log("entra a if");

                                    } else {
                                        $scope.est_general[j].notas[k].not = ((parseFloat($scope.est_general[j].notas[k].not) + settingAlumno.dataset[i].nota) / 2).toString();
                                        console.log("entra a else");
                                    }
                                    $scope.alumnoTmp.nota = parseFloat($scope.est_general[j].notas[k].not);

                                    $scope.obtener_nota_literal();
                                    $scope.est_general[j].notas[k].not_lit = $scope.alumnoTmp.nota_lit;

                                    console.log("nota literal");
                                    console.log($scope.alumnoTmp.nota_lit);
                                    console.log($scope.est_general[j].notas[k].not_lit);
                                }



                            }
                        }
                    }
                    /*$scope.notas_indicador.estudiantes = $scope.est_general;
                     console.log("$scope.notas_indicador", $scope.notas_indicador);    
                     var request = crud.crearRequest('notas_estudiante',1,'registrarNotasIndicador');
                     
                     request.setData($scope.notas_indicador);
                     
                     console.log($scope.notas_indicador); 
                     crud.insertar('/submodulo_academico',request,function(response){
                     if(response.responseSta){
                     modal.mensaje("CONFIRMACION",response.responseMsg);
                     }
                     },function(error){
                     console.info(error);
                     });*/

                    //Añadido
                    /*var request = crud.crearRequest('notas_estudiante', 1, 'registrarPromedioIndicadores');
                     request.setData({
                     ses: self.sesion.id,
                     per: self.periodo.id,
                     numper: self.periodo.num,
                     are: self.area.id,
                     usr: $rootScope.usuMaster.usuario.usuarioID,
                     org: $rootScope.usuMaster.organizacion.organizacionID,
                     estudiantes: self.estudiantes
                     });
                     crud.insertar('/submodulo_academico', request, function (response) {
                     if (response.responseSta) {
                     console.log('data response', response.data);
                     modal.mensaje('CONFIRMACION', response.responseMsg);
                     }
                     });*/

                } else {
                    console.log("Lista con error->listarNotasIndicadoresAlumnos");
                    modal.mensaje('ERROR', response.responseMsg);
                }
            }, function (errResponse) {});
        }




        $scope.finalizarCalificacion = function () {

            if (!$scope.tarea.tareaID || isNaN($scope.tarea.tareaID)) {
                modal.mensaje("CONFIRMACION", "Tarea no valida");
                $scope.active = 0;
                return;
            }

            modal.mensajeConfirmacion($scope, "Esta seguro que finalizara la calificacion de la tarea, y actualizar el registro auxilar", function () {

                var request = crud.crearRequest('tarea', 1, 'finalizarTarea');
                request.setData({tareaID: $scope.tarea.tareaID});
                crud.actualizar("/web", request, function (res) {
                    modal.mensaje("CONFIRMACION", res.responseMsg);
                    if (res.responseSta) {
                        //recuperamos las variables que nos envio el servidor
                        $scope.tarea.estado = res.data.estado;
                        settingTarea.dataset[$scope.tarea.i] = $scope.tarea;
                        $scope.tablaTarea.reload();

                        $scope.active = 0;
                    }
                }, function (data) {
                    console.info(data);
                });
            }, "400");
        };

        $scope.calcularSecciones = function (grado) {

            if (grado.grado.indexOf("secundaria") > -1) {
                $scope.numSel = true;
            } else if (grado.grado.indexOf("primaria") > -1) {
                $scope.numSel = false;
            }


            $scope.seccionSel = '';
            $scope.areaSel = '';
            if (grado)
                $scope.tablaTarea.filter({'gradoID': grado.gradoID});
            else
                $scope.tablaTarea.filter({});
        };
        $scope.filtrarSecciones = function () {
            $scope.areaSel = '';
            if ($scope.seccionSel)
                $scope.tablaTarea.filter({'gradoID': $scope.gradoSel.gradoID, 'seccionID': $scope.seccionSel.seccionID});
            else
                $scope.tablaTarea.filter({'gradoID': grado.gradoID});
        };
        $scope.filtrarAreas = function () {
            if ($scope.areaSel)
                $scope.tablaTarea.filter({'gradoID': $scope.gradoSel.gradoID, 'seccionID': $scope.seccionSel.seccionID, 'areaID': $scope.areaSel.areaID});
            else
                $scope.tablaTarea.filter({'gradoID': $scope.gradoSel.gradoID, 'seccionID': $scope.seccionSel.seccionID});
        };
        $scope.filtrarTareaporNombre = function (nomTarea) {
            $scope.tablaTarea.filter({'gradoID': $scope.gradoSel.gradoID, 'seccionID': $scope.seccionSel.seccionID, 'areaID': $scope.areaSel.areaID, 'nombre': nomTarea});
        }

        $scope.buscarPlanEstudios = function (orgID, usuarioID) {
            //preparamos un objeto request
            var request = crud.crearRequest('planEstudios', 1, 'buscarPlanEstudios');
            request.setData({organizacionID: orgID});
            crud.listar("/cuadroHoras", request, function (response) {

                if (response.responseSta) {
                    $scope.plan = response.data;

                    request = crud.crearRequest('cuadroHoras', 1, 'buscarDistribucionPorDocente');
                    request.setData({planID: $scope.plan.planID, docenteID: usuarioID});
                    crud.listar("/cuadroHoras", request, function (response) {

                        if (response.responseSta) {
                            $scope.grados = response.data;
                            $scope.grados.forEach(function (item) {
                                var g = buscarObjeto($scope.plan.grados, "gradoID", item.gradoID);
                                item.grado = g.grado;
                                item.meta = g.meta;
                                var areas = buscarObjetos($scope.plan.areas, "gradoID", item.gradoID);
                                item.secciones.forEach(function (item2) {
                                    //si no tiene distribucion de areas, es docente de primaria se leasigna todas las areas segun el nivel
                                    if (item2.areas.length == 0)
                                        item2.areas = areas;
                                    else {
                                        item2.areas.forEach(function (item3) {
                                            item3.area = buscarContenido(areas, "areaID", "area", item3.areaID);
                                        });
                                    }
                                });
                            });
                            $scope.obtener_tarea();

                        }
                    }, function (data) {
                        console.info(data);
                    });
                    $scope.buscarTareas(usuarioID, orgID);
                }
            }, function (data) {
                console.info(data);
            });
        };
        $scope.buscarTareas = function (usuarioID, orgID) {
            var request = crud.crearRequest('tarea', 1, 'listarTareasPorDocente');
            request.setData({planID: $scope.plan.planID, docenteID: usuarioID});
            crud.listar("/web", request, function (response) {

                if (response.responseSta) {
                    tareas = response.data;
                    console.log(response.data);
                    tareas.forEach(function (item) {
                        item.grado = buscarContenido($scope.plan.grados, "gradoID", "grado", item.gradoID);

                        item.area = buscarContenido($scope.plan.areas, "areaID", "area", item.areaID);
                    });

                    settingTarea.dataset = tareas;

                    $scope.tablaTarea.settings(settingTarea);
                    $scope.buscarEquivalenciaNota(usuarioID, orgID);

                }
            }, function (data) {
                console.info(data);
            });

        };
        $scope.buscarEquivalenciaNota = function (usuarioID, orgID) {
            var request = crud.crearRequest('notas_estudiante', 1, 'listarConfiguracionNota');
            request.setData({organizacionID: orgID, docenteID: usuarioID});
            crud.listar("/submodulo_academico", request, function (response) {
                if (response.responseSta) {
                    $scope.configuracion_nota_docente = response.data;
                }
            }, function (data) {
                console.info(data);
            });
        }




        $scope.obtener_tarea = function () {
            //Verifica si se ha llegado al módulo Web desde el Módulo Maestro al seleccionar una tarea
            if (!localStorage.getItem('tarea_maestro')) {
                return;
            }
            $scope.tarea_maestro = JSON.parse(window.atob(localStorage.getItem('tarea_maestro')));
            //ACTUALIZAMOS LOS COMBOS DE LA INTERFAZ
            // GRADO
            var size_grados = $scope.grados.length;
            for (var i = 0; i < size_grados; i++) {
                if ($scope.grados[i].gradoID === $scope.tarea_maestro.grad_id) {
                    $scope.gradoSel = $scope.grados[i];
                    $scope.calcularSecciones($scope.gradoSel);
                    break;
                }
            }
            // SECCION
            var size_seccion = $scope.gradoSel.secciones.length;
            for (var j = 0; j < size_seccion; j++) {
                if ($scope.gradoSel.secciones[j].seccionID === $scope.tarea_maestro.sec_id) {
                    $scope.seccionSel = $scope.gradoSel.secciones[j];
                    //  $scope.filtrarSecciones();
                    break;
                }
            }

            //AREA
            var size_areas = $scope.seccionSel.areas.length;
            for (k = 0; k < size_areas; k++) {
                if ($scope.seccionSel.areas[k].areaID === $scope.tarea_maestro.are_id) {
                    $scope.areaSel = $scope.seccionSel.areas[k];
                    // $scope.filtrarAreas();
                    break;
                }
            }

            //Filtro valido para la busqueda de la tarea desde el Modulo Maestro
            $scope.filtrarTareaporNombre($scope.tarea_maestro.nom);

            localStorage.removeItem('tarea_maestro');
        };

    }]);
