var cciApp = angular.module('app');

app.controller('calendarioCtrl', ['$scope', 'crud', 'modal', '$uibModal', '$rootScope', function ($scope, crud, modal, $uibModal, $rootScope) {
    $rootScope.mostrarMenuDerecha();
    var calendar;

    $scope.addActivity = function (start, end) {
        var aux_1 = new Date(start._d);
        var aux_2 = new Date();
        var iniDate = new Date(aux_1.getFullYear(), aux_1.getMonth(), aux_1.getDate() + 1, aux_2.getHours(), aux_2.getMinutes(), aux_2.getSeconds(), aux_2.getMilliseconds());

        if (iniDate < new Date())
            modal.mensaje("ADVERTENCIA", "No se puede añadir una actividad este dia");
        else {
            aux_1 = new Date(end._d);

            var modalInstance = $uibModal.open({
                animation: true,
                keyboard: true,
                backdrop: true,
                templateUrl: 'calendar.html',
                controller: 'calendarCtrl',
                controllerAs: 'calendar',
                resolve: {
                    data: function () {
                        return {
                            est: true,
                            ini: iniDate,
                            fin: new Date(aux_1.getFullYear(), aux_1.getMonth(), aux_1.getDate(), 23, 59, 59, 999)
                        };
                    }
                }
            });

            modalInstance.result.then(function (activity) {
                calendar.fullCalendar('renderEvent', activity, true);
            }, function () {
            });
        }
    };

    $scope.editActivity = function (activity) {
        var goOn = false;

        switch (activity.tip) {
            case 'P':
                goOn = true;
                break;
            case 'D':
                goOn = $rootScope.usuMaster.rol.rolID > 4 && $rootScope.usuMaster.rol.rolID < 8;
                break;
            case 'U':
                goOn = $rootScope.usuMaster.rol.rolID > 4 && $rootScope.usuMaster.rol.rolID < 8;
                break;
            case 'I':
                goOn = $rootScope.usuMaster.rol.rolID > 4 && $rootScope.usuMaster.rol.rolID < 8;
                break;
        }

        if (!goOn)
            modal.mensaje("MENSAJE", "La actividad no puede ser modificada. No posee permisos de edición");
        else if (activity.est !== "A") {
            modal.mensaje("MENSAJE", "La actividad ya no puede ser modificada, ya ha concluido");
            if(activity.est === "F") {
                modal.mensajeConfirmacion($scope, "¿Le gustaría solicitar la reactivación de la actividad seleccionada a la autoridad de la organización jerárquica superior?", function () {
                    var request = crud.crearRequest('calendario', 1, 'reactivarActividad');
                    request.setData({usu: $rootScope.usuMaster.usuario.ID, tit: activity.title, tip: activity.tip});
                    crud.insertar('/web', request, function (success) {
                        if (success.response === 'OK')
                            modal.mensaje("CONFIRMACION", "El mensaje ha sido enviado correctamente");
                        else if (success.response === 'BAD')
                            modal.mensaje("ERROR", success.responseMsg);
                    }, function (error) {
                        modal.mensaje("MENSAJE", error.responseMsg);
                    });
                }, '400');
            }
        } else {
            var modalInstance = $uibModal.open({
                animation: true,
                keyboard: true,
                backdrop: true,
                templateUrl: 'calendar.html',
                controller: 'calendarCtrl',
                controllerAs: 'calendar',
                resolve: {
                    data: function () {
                        return {
                            est: false,
                            ini: activity.start._d,
                            fin: activity.end._d,
                            des: activity.des,
                            tit: activity.title,
                            tip: activity.tip,
                            cod: activity.cod,
                            ide: activity._id,
                            col: activity.color
                        };
                    }
                }
            });

            modalInstance.result.then(function (response) {
                var activity = response.act;
                if (response.est) {
                    calendar.fullCalendar('removeEvents', [activity.ide]);
                    calendar.fullCalendar('renderEvent', activity, true);
                } else
                    calendar.fullCalendar('removeEvents', [activity.ide]);
            }, function () {
            });
        }
    };

    $scope.showCalendar = function () {
        calendar = $('#calendar').fullCalendar({
            header: {
                
                left: 'title',
                center: 'prev,next today',
                right: 'month,agendaDay,listMonth'
            },
            timezone: 'local',
            defaultDate: new Date(),
            navLinks: true,
            businessHours: false,
            selectable: true,
            selectHelper: true,
            select: $scope.addActivity,
            eventLimit: true,
            editable: false,
            events: [],
            eventRender: function (event, element) {
                element.bind("click", function () {
                    $scope.editActivity(event);
                });
            }
        });

        var request = crud.crearRequest('calendario', 1, 'listarActividades');
        crud.listar('/web', request, function (success) {
            if (success.response === 'OK') {
                success.data.forEach(function (item) {
                    calendar.fullCalendar('renderEvent', item, true);
                });
            } else if (success.response === 'BAD')
                modal.mensaje("ERROR", success.responseMsg);
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar las actividades " + error.responseMsg);
        });
    };
}]);

cciApp.controller('calendarCtrl', ['$uibModalInstance', 'crud', 'modal', 'data', '$rootScope', '$scope', '$location', function ($uibModalInstance, crud, modal, data, $rootScope, $scope, $location) {
    var self = this;
    self.est = data.est;
    self.ini = data.ini;
    self.fin = data.fin;
    self.rol = $rootScope.usuMaster.rol.rolID > 4 && $rootScope.usuMaster.rol.rolID < 8;

    if (self.est) {
        self.tip = 'P';
    } else {
        self.tit = data.tit;
        self.tip = data.tip === 'P' ? 'P' : 'I';
        self.des = data.des;
        self.cod = data.cod;
        self.ide = data.ide;
        self.col = data.col;
        self.realTip = data.tip;
        if (self.tip === 'P')
            self.del = false;
        else {
            switch (self.realTip) {
                case "D":
                    self.del = !($rootScope.usuMaster.rol.rolID == 5);
                    break;
                case "U":
                    self.del = !($rootScope.usuMaster.rol.rolID == 6);
                    break;
                case "I":
                    self.del = !($rootScope.usuMaster.rol.rolID == 7);
                    break;
            }
        }
    }

    $scope.$watch('calendar.tip', function (newValue, oldValue) {
        switch (newValue) {
            case 'P':
                self.dateOptions = {
                    minDate: new Date(),
                    initDate: self.ini,
                    showWeeks: false,
                    startingDay: 0
                };
                break;

            case 'I':
                var current = new Date();
                var tomorrow = new Date(current.getFullYear(), current.getMonth(), current.getDate() + 1, current.getHours(), current.getMinutes(), current.getSeconds(), current.getMilliseconds());
                self.dateOptions = {
                    minDate: tomorrow,
                    initDate: self.ini,
                    showWeeks: false,
                    startingDay: 0
                };

                if (self.ini < tomorrow)
                    self.ini = tomorrow;
                if (self.fin < tomorrow)
                    self.fin = tomorrow;
                if (self.est)
                    self.loadModules();
                break;

            case null:
                self.tip = oldValue;
                break;
        }
    });

    self.loadModules = function () {
        self.clear('M');
        var request = crud.crearRequest('calendario', 1, 'listarFuncionalidades');
        request.setData({opt: 0});
        crud.listar('/web', request, function (success) {
            if (success.response === 'OK') {
                if (success.data.length > 0)
                    self.modules = success.data;
                else
                    modal.mensaje("MENSAJE", "No existen módulos");
            } else if (success.response === 'BAD')
                modal.mensaje("ERROR", success.responseMsg);
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar los módulos " + error.responseMsg);
        });
    };

    self.loadSubModules = function () {
        self.clear('S');
        var request = crud.crearRequest('calendario', 1, 'listarFuncionalidades');
        request.setData({opt: 1, cod: self.mod});
        crud.listar('/web', request, function (success) {
            if (success.response === 'OK') {
                if (success.data.length > 0) {
                    self.subModules = success.data;
                    self.modEst = false;
                } else
                    modal.mensaje("MENSAJE", "No existen submódulos");
            } else if (success.response === 'BAD')
                modal.mensaje("ERROR", success.responseMsg);
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar los submódulos " + error.responseMsg);
        });
    };

    self.loadFunctions = function () {
        self.clear('F');
        var request = crud.crearRequest('calendario', 1, 'listarFuncionalidades');
        request.setData({opt: 2, cod: self.sub, rol: $rootScope.usuMaster.rol.rolID});
        crud.listar('/web', request, function (success) {
            if (success.response === 'OK') {
                if (success.data.length > 0) {
                    self.functions = success.data;
                    self.subEst = false;
                } else
                    modal.mensaje("MENSAJE", "No existen funciones principales asignadas");
            } else if (success.response === 'BAD')
                modal.mensaje("ERROR", success.responseMsg);
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar las funciones " + error.responseMsg);
        });
    };

    self.accept = function () {
        if (self.ini < new Date())
            modal.mensaje("MENSAJE", "No se puede registrar esta actividad en la fecha de inicio registrada");
        else if (self.fin < self.ini)
            modal.mensaje("MENSAJE", "Las fechas de inicio y de fin no poseen coherencia");
        else {
            if (self.est) {
                var request = crud.crearRequest('calendario', 1, 'registrarActividad');
                var rol = $rootScope.usuMaster.rol.rolID;
                rol = (rol === '5') ? "D" : (rol === '6') ? "U" : (rol === '7') ? "I" : "N";
                request.setData({tit: self.tit,
                    des: self.des, ini: self.ini,
                    fin: self.fin, tip: self.tip,
                    rol: rol, fun: self.fun,
                    usu: $rootScope.usuMaster.usuario.ID,
                    fun: self.fun,
                            org: $rootScope.usuMaster.organizacion.organizacionID});

                crud.insertar('/web', request, function (success) {
                    if (success.response === 'OK') {
                        modal.mensaje("CONFIRMACION", "Los registros han sido insertados correctamente");
                        $uibModalInstance.close(success.data);
                    } else if (success.response === 'BAD')
                        modal.mensaje("ERROR", success.responseMsg);
                }, function (error) {
                    modal.mensaje("MENSAJE", error.responseMsg);
                });
            } else {
                var request = crud.crearRequest('calendario', 1, 'editarActividad');
                request.setData({tit: self.tit,
                    des: self.des, ini: self.ini,
                    fin: self.fin, cod: self.cod});

                crud.actualizar('/web', request, function (success) {
                    if (success.response === 'OK') {
                        modal.mensaje("CONFIRMACION", "Los registros han sido modificados correctamente");
                        var est = (self.fin < new Date()) ? "F" : "A";
                        $uibModalInstance.close({act: {title: self.tit, des: self.des, tip: self.realTip, start: self.ini, end: self.fin, est: est, cod: self.cod, color: self.col, ide: self.ide}, est: true});
                    } else if (success.response === 'BAD')
                        modal.mensaje("ERROR", success.responseMsg);
                }, function (error) {
                    modal.mensaje("MENSAJE", error.responseMsg);
                });
            }
        }
    };

    self.remove = function () {
        modal.mensajeConfirmacion($scope, "¿Estás seguro de eliminar la actividad seleccionada?", function () {
            var request = crud.crearRequest('calendario', 1, 'eliminarActividad');
            request.setData({cod: self.cod});
            crud.eliminar('/web', request, function (success) {
                if (success.response === 'OK') {
                    modal.mensaje("CONFIRMACION", "Los registros han sido eliminados correctamente");
                    $uibModalInstance.close({act: {ide: self.ide}, est: false});
                } else if (success.response === 'BAD')
                    modal.mensaje("ERROR", success.responseMsg);
            }, function (error) {
                modal.mensaje("MENSAJE", error.responseMsg);
            });
        }, '400');
    };

    self.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    self.clear = function (state) {
        switch (state) {
            case 'M':
                self.mod = "";
                self.modEst = true;
                self.sub = "";
                self.subEst = true;
                self.fun = "";
                self.funEst = true;
            case 'S':
                self.modEst = true;
                self.sub = "";
                self.subEst = true;
                self.fun = "";
                self.funEst = true;
            case 'F':
                self.subEst = true;
                self.fun = "";
                self.funEst = true;
        }
    };
}]);
