angular.module('app')
    .controller('reporte',['$log','$location','$rootScope','NgTableParams','modal','UtilAppServices','crud',function($log,$location,$rootScope,NgTableParams,modal,util,crud){
        var self = this;
        self.orgs = [];
        
        self.tablaComisiones = new NgTableParams({count:8},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });
        
        listarOrganizaciones();
        function listarOrganizaciones(){
            self.showProgress = true;
            var request = crud.crearRequest('reportes',1,'listarOrganizaciones');
            request.setData({org:$rootScope.usuMaster.organizacion.organizacionID});
            crud.listar('/conformacion_comisiones',request,function (response) {
                self.showProgress = false;
                if(response.responseSta){
                    self.orgs = response.data;
                }else{
                    modal.mensaje('Error',response.responseMsg)
                }
            },function (errResponse) {
                self.showProgress = false;
                modal.mensaje('ERROR','El servidor no responde')
            });
        }
        self.mostrarComisiones = function (org) {
            var request = crud.crearRequest('comisiones',1,'listarComisiones');
            var data = {mine:"num", perID:$rootScope.usuMaster.usuario.usuarioID};
            request.setMetadataValue('org',''+org.cod);
            request.setData(data);
            crud.listar('/conformacion_comisiones',request,function (response) {
                if(response.responseSta){
                    self.tablaComisiones.settings().dataset = response.data;
                    self.tablaComisiones.reload();
                }else {
                    modal.mensaje('Error',response.responseMsg)
                }
            },function (errResponse) {
                modal.mensaje('ERROR',"El servidor no responde");
            });
        }
        self.estActa = function (row) {
            var request = crud.crearRequest('reportes',1,'estadisticaGlobal');
            request.setData({com:row.cod,tip:'acta'});
            crud.listar('/conformacion_comisiones',request,function (response) {
                if(response.responseSta){
                    $log.log('actas',response);
                    var resolve = {
                        data : function () {
                            return {
                                acta: true,
                                obj: response.data
                               
                            }
                             console.info(response.data);
                        }
                    }
                    if(response.data.labels==='undefined'||response.data.labels.length==0 ){
                        modal.mensaje('ALERTA',"La comision aun no registo actas");
                    }
                    else{
                        var modalInstance = util.openModal('estadisticas.html','estadisticaCtrl','lg','ctrl',resolve);
                    }                    
                }else {
                    modal.mensaje('Error',response.responseMsg)
                }
            },function (errResponse) {
                modal.mensaje('ERROR',"El servidor no responde");
            });
        }
        self.estAsistencia = function (row) {
            var request = crud.crearRequest('reportes',1,'estadisticaGlobal');
            request.setData({com:row.cod,tip:'asistencia'});
            crud.listar('/conformacion_comisiones',request,function (response) {
                if(response.responseSta){
                    $log.log('actas',response);
                    var resolve = {
                        data : function () {
                            return {
                                acta: false,
                                obj: response.data
                            }
                        }
                    }
                    if(response.data.labels==='undefined'||response.data.labels.length==0 ){
                        modal.mensaje('ALERTA',"La comision aun no registo asistencias, reuniones");
                    }
                    else{
                        var modalInstance = util.openModal('estadisticas.html','estadisticaCtrl','lg','ctrl',resolve);
                    }
                }else {
                    modal.mensaje('Error',response.responseMsg)
                }
            },function (errResponse) {
                modal.mensaje('ERROR',"El servidor no responde");
            });
        }
    }])
    .controller('estadisticaCtrl',['$log','$uibModalInstance','data',function($log,$uibModalInstance,data){
        var self = this;
        self.titulo = data.acta ? 'Estadisticas General de Acuerdos' : 'Estadisticas General de Asistencia'
        self.colors = ['#46BFBD', '#FDB45C', '#3366cc'];   
        self.labels = data.obj.labels;
        self.data= data.obj.data;
        console.info(self.labels);
        self.series = data.acta ? ['Cumplido', 'No cumplido']:['Asistio', 'No Asistio'] ;
        self.cancelar = function () {
            $uibModalInstance.dismiss('cancel');
        }

    }]);;