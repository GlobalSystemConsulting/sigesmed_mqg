/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module('app')
    .controller('registroAuxiliar',['$log','$location','modal','UtilAppServices','NgTableParams','$rootScope','crud',function($log,$location,modal,util,NgTableParams,$rootScope,crud){
        var self = this;
        self.numSel = true; // Nota numerica seleccionada
        
        self.abrirCalendar = false;
        self.openCalendar = function () {
            self.abrirCalendar = true;
        }
        self.dateOptions = {
            formatYear: 'yyyy',
            maxDate: new Date(),
            minDate: new Date(1900,1,1),
            startingDay: 1
        };

        self.tablaAsistenciaEst = new NgTableParams({count:8},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });
        self.numSel = true; // Nota numerica seleccionada
        self.patron = /^\d+$/;
        self.configuracion_nota_docente = [];
        listarNiveles();
        function listarNiveles(){
            var request = crud.crearRequest('anecdotario',1,'listarGrados');
            request.setData({org:$rootScope.usuMaster.organizacion.organizacionID, doc:$rootScope.usuMaster.usuario.usuarioID});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.niveles = response.data.niveles;
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
            });
        }
        function listarPeriodos(nivel){
            var request = crud.crearRequest('unidades',1,'listarPeriodosPlanEstudios');
            request.setData({org:$rootScope.usuMaster.organizacion.organizacionID, niv:nivel.id});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.periodos = response.data;

                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
            });
        }
        self.mostrarGrados =  function(nivel){
            self.grados = nivel.grados;
            listarPeriodos(nivel);
        }
        self.mostrarSecciones =  function(grado){
            self.secciones = grado.secciones;

        }
        self.mostrarAlumnos = function(){
            var request = crud.crearRequest('anecdotario',1,'listarEstudiantes');
            request.setData({org:$rootScope.usuMaster.organizacion.organizacionID, gra:self.grado.id,sec:self.seccion});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.estudiantes = response.data;
                    angular.forEach(self.estudiantes,function(objest,keyest){
                        objest.notas = [];
                        angular.forEach(self.indicadores,function(objind,keyind){
                            objest.notas.push({ind:objind.id,not:""});
                        });
                    });
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
            });
        }
        self.mostrarAreas = function(seccion){
            var request = crud.crearRequest('acomp',1,'listarCursosDocente');
            request.setData({doc:$rootScope.usuMaster.usuario.usuarioID,org:$rootScope.usuMaster.organizacion.organizacionID,gra:self.grado.id,secc:seccion});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.areas = response.data;
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){});
        }
        self.mostrarCompetencias = function(area){
            var request = crud.crearRequest('notas_estudiante',1,'listarCompetenciasPeriodo');
            request.setData({
                per: self.periodo.id,
                usr:$rootScope.usuMaster.usuario.usuarioID,
                org:$rootScope.usuMaster.organizacion.organizacionID,
                gra:self.grado.id,
                are:area.id
            });
            crud.listar('/submodulo_academico',request,function(response){
                if(response.responseSta){
                    self.competencias = response.data;
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){});
        }
        self.mostrarFicha = function(competencia){
            var request = crud.crearRequest('notas_estudiante',1,'listarNotasRegistroAuxiliar');
            request.setData({
                per: self.periodo.id,
                usr:$rootScope.usuMaster.usuario.usuarioID,
                org:$rootScope.usuMaster.organizacion.organizacionID,
                gra:self.grado.id,
                are:self.area.id,
                secc: self.seccion,
                comp:competencia.id
            });
            crud.listar('/submodulo_academico',request,function(response){
                if(response.responseSta){
                    console.log(response);
                    self.indicadores = response.data.indicadores;
                    self.estudiantes = response.data.estudiantes;
                    self.listar_configuracion_nota($rootScope.usuMaster.organizacion.organizacionID,$rootScope.usuMaster.usuario.usuarioID);
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){});
        }
        self.listar_configuracion_nota = function(orgID,usuarioID){
            var request = crud.crearRequest('notas_estudiante',1,'listarConfiguracionNota');
            request.setData({organizacionID:orgID,docenteID:usuarioID});
            crud.listar("/submodulo_academico",request,function(response){
               if(response.responseSta){
                    self.configuracion_nota_docente = response.data;
               }  
            },function(data){
                console.info(data);
            });
        }
        
        self.registrarNotaEstudiante = function(estudiante){
            var est = [];
            est.push(angular.copy(estudiante));
            var resolve = {
                data : function () {
                    return {
                        indicadores: self.indicadores,
                        estudiantes: est,
                        numSel:  self.numSel,
                        configuracion_nota : self.configuracion_nota_docente
                    }
                }
            }
            var modalInstance = util.openModal('notas_alumno.html','registrarNotaEstudiante','lg','ctrl',resolve);
            modalInstance.result.then(function(estRes){
                angular.extend(estudiante,estRes);
            },function(cdata){});
        }

        self.registrarNotasCompetencia = function(){
            var request = crud.crearRequest('notas_estudiante',1,'registrarNotasCompetencia');
            request.setData({
                comp: self.competencia.id,
                per: self.periodo.id,
                are: self.area.id,
                usr: $rootScope.usuMaster.usuario.usuarioID,
                estudiantes: self.estudiantes
            });
            crud.insertar('/submodulo_academico',request,function(response){
                if(response.responseSta){
                   modal.mensaje("EXITO",response.responseMsg);
                    var request = crud.crearRequest('notas_estudiante',1,'listarNotasCompetencias');
                    request.setData({
                        per: self.periodo.id,
                        usr:$rootScope.usuMaster.usuario.usuarioID,
                        org:$rootScope.usuMaster.organizacion.organizacionID,
                        gra:self.grado.id,
                        are:self.area.id,
                        secc: self.seccion
                    });
                    crud.listar('/submodulo_academico',request,function(response){
                        if(response.responseSta){
                            self.estudiantes_resumen = response.data.estudiantes;
                            var request = crud.crearRequest('notas_estudiante',1,'registrarNotasArea');
                            request.setData({
                                per: self.periodo.id,
                                are: self.area.id,
                                usr: $rootScope.usuMaster.usuario.usuarioID,
                                estudiantes: self.estudiantes_resumen
                            });
                            crud.insertar('/submodulo_academico',request,function(response){
                                if(response.responseSta){
                                    modal.mensaje("EXITO",response.responseMsg);

                                }else{
                                    modal.mensaje('ERROR',response.responseMsg);
                                }
                            },function(errResponse){});
                            
                        }else{
                            modal.mensaje('ERROR',response.responseMsg);
                        }
                    },function(errResponse){});
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){});
        };
        
        //Obtenemos el Promedio Final de Area por Periodo
        self.obtener_promedios_finales_periodo = function(){
            
            
        }
        
        
        
        self.registrarNotasArea = function(){
            var request = crud.crearRequest('notas_estudiante',1,'registrarNotasArea');
            request.setData({
                per: self.periodo.id,
                are: self.area.id,
                usr: $rootScope.usuMaster.usuario.usuarioID,
                estudiantes: self.estudiantes_resumen
            });
            crud.insertar('/submodulo_academico',request,function(response){
                if(response.responseSta){
                    modal.mensaje("EXITO",response.responseMsg);

                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){});
        };
        self.mostrarResumenNotas = function(){
            if(self.grado == undefined || self.grado == null){
                modal.mensaje("ERROR","DEBE SELECCIONAR UN GRADO");
                return;
            }else if(self.seccion == undefined || self.seccion == null){
                modal.mensaje("ERROR","DEBE SELECCIONAR UNA SECCION");
                return;
            } else if(self.periodo == undefined || self.periodo == null){
                modal.mensaje("ERROR","DEBE SELECCIONAR UN PERIODO");
                return;
            } else if(self.area == undefined || self.area == null){
                modal.mensaje("ERROR","DEBE SELECCIONAR UN AREA");
                return;
            }
            self.showResumen = true;
            var request = crud.crearRequest('notas_estudiante',1,'listarNotasCompetencias');
            request.setData({
                per: self.periodo.id,
                usr:$rootScope.usuMaster.usuario.usuarioID,
                org:$rootScope.usuMaster.organizacion.organizacionID,
                gra:self.grado.id,
                are:self.area.id,
                secc: self.seccion
            });
            crud.listar('/submodulo_academico',request,function(response){
                if(response.responseSta){
                    self.competencias_resumen = response.data.indicadores;
                    self.estudiantes_resumen = response.data.estudiantes;
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){});
        }
        self.registrarNotaEstudiantesArea = function(estudiante){
            var est = [];
            est.push(angular.copy(estudiante));
            var resolve = {
                data : function () {
                    return {
                        competencias: self.competencias_resumen,
                        estudiantes: est
                    }
                }
            }
            var modalInstance = util.openModal('notas_area_alumno.html','registrarNotaEstudianteArea','lg','ctrl',resolve);
            modalInstance.result.then(function(estRes){
                angular.extend(estudiante,estRes);
            },function(cdata){});
        }
        self.volverDeResumen = function(){
            self.showResumen = false;
        }
        
        self.pre_edi = function(nota){
            nota.preedi = true;
            nota.edi = false;
        }
        self.nota_ant = "";
        self.prepeditarNota = function(nota,tipo){
            if(tipo == 'N'){self.nota_ant = nota.notIndDoc;}
            else if(tipo == 'L'){self.nota_ant = nota.notLitDoc;}
            
            nota.edi = true;
            nota.preedi = false;
        }
        
        self.editarNota = function(alumno,nota,tipo){
            nota.preedi = false;
            nota.edi = false;
            if(tipo == 'N'){self.updPromNum(alumno);nota.notLitDoc = self.obtenerNumToLit(nota.notIndDoc).toString()}
            else if(tipo == 'L'){self.updPromLit(alumno);nota.notIndDoc = self.obtenerLitToNum(nota.notLitDoc).toString()}
        }
        self.cancelEditNot = function(nota,tipo){
            
            if(tipo == 'N'){nota.notIndDoc = self.nota_ant; }
            else if(tipo == 'L'){nota.notLitDoc = self.nota_ant;}    
            
            nota.preedi = false;
            nota.edi = false;
            
        }
        
        self.cancelAction = function(nota){
            nota.preedi = false;
            nota.edi = false;
        }
        
        self.updPromLit = function(alumno){
            var size = alumno.notas.length;
            var promedio = 0;
            for(var i = 0 ; i<size ; i++){
                promedio += self.obtenerLitToNum(alumno.notas[i].notLitDoc)
            }
            promedio = promedio/size;
            alumno.notComp = promedio.toString();
            alumno.notCompLit = self.obtenerNumToLit(promedio);
        }
        self.updPromNum = function(alumno){
            var size = alumno.notas.length;
            var promedio = 0;
            for(var i = 0 ; i< size ;i++){
                promedio += parseInt(alumno.notas[i].notIndDoc);
            }
            promedio = promedio/size;
            alumno.notComp = promedio.toString();
            alumno.notCompLit = self.obtenerNumToLit(promedio);
            
        }
        self.obtenerLitToNum = function(notaLit){
            var size = self.configuracion_nota_docente.length;
            for(i = 0 ; i< size ; i++){
                if(self.configuracion_nota_docente[i].cod == notaLit){
                    return self.configuracion_nota_docente[i].not_max;
                }
            }
        }
        self.obtenerNumToLit = function(notaNum){
            var size = self.configuracion_nota_docente.length;
            
            for(var i = 0 ;i<size ; i++){
                var conf_act = self.configuracion_nota_docente[i];
                if(parseInt(notaNum) >= conf_act.not_min && parseInt(notaNum) <= conf_act.not_max){
                    return conf_act.cod;
                }
            }
        }
        
        
    }]).controller('registrarNotaEstudiante',['$log','NgTableParams','$uibModalInstance','$rootScope','modal','data','crud',function($log,NgTableParams,$uibModalInstance,$rootScope,modal,data,crud){
        
        var self = this;
        self.numSel = data.numSel;
        self.indicadores  = data.indicadores;
        self.estudiantes = data.estudiantes;
        self.configuracion_nota = data.configuracion_nota;
        self.current_nota_lit = {};
        
        self.test = {nombre:""};
        
        self.obtenerNota = function(codNot,notas){
            var size = self.configuracion_nota.length;
            for(var i = 0 ; i< size ; i++){
                if(self.configuracion_nota[i].cod == codNot){
                    notas.nota_literal = self.configuracion_nota[i];
                    break;
                }
            }
        }

        
        for(var j=0;j<self.estudiantes[0].notas.length;j++){
                
              //  self.obtenerNota(self.estudiantes[0].notas[j].notLitDoc,self.estudiantes[0].notas[j]);
              //  self.estudiantes[0].notas[j].nota_literal = self.current_nota_lit;
        }
        
       self.updateNotaFinalLit = function(){
           
           var a = 1;
       }
       
       self.update = function(){
          
           var b = 0;
       }
       
       self.updateNotaFinalNum = function(){
           
           var b = 1;
           
       }

       self.obtener_nota_literal = function(NotaNum){
           var size = self.configuracion_nota.length;
           
           
       }
       self.obtener_nota_numeral = function(NotaLit){
           
       }

        self.cancelar = function () {
            /*angular.forEach(self.estudiantes[0].notas,function(obj){
                obj.not = '';
            });*/
            $uibModalInstance.dismiss('cancel');
        }
        self.guardar = function(){
            $uibModalInstance.close(self.estudiantes[0]);
        }
    }]).controller('registrarNotaEstudianteArea',['$log','NgTableParams','$uibModalInstance','$rootScope','modal','data','crud',function($log,NgTableParams,$uibModalInstance,$rootScope,modal,data,crud){
        var self = this;
        self.competencias  = data.competencias;
        $log.log('competencias',self.competencias);
        self.estudiantes = data.estudiantes;
        self.cancelar = function () {
            $uibModalInstance.dismiss('cancel');
        }
        self.guardar = function(){
            $uibModalInstance.close(self.estudiantes[0]);
        }
    }]);

