app.controller("config_equivalencia",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){




    $scope.listar_tipos_notas = function(){
        var request = crud.crearRequest('notas_estudiante',1,'configuracion_nota');
        
        crud.listar("/submodulo_academico",request,function(data){
            if(data.data){
                $scope.tipos_notas = data.data;
            }
        });     
    }
    
    $scope.editar_tipo_nota = function(tipo_nota){
        $scope.tit_config = "EDITAR CONFIGURACION NOTA";
        $scope.editar_nota = tipo_nota;
        $('#modaleditarequivalencia').modal('show');
        
    }
    $scope.agregar_tipo_nota = function(){
        $scope.tit_config = "AGREGAR CONFIGURACION NOTA";
        $scope.editar_nota = "";
        $('#modalregistrarequivalencia').modal('show');
    }

    $scope.actualizar_equivalencia_nota = function(usu_id,tipo){
         var request = crud.crearRequest('notas_estudiante',1,'registrar_configuracion_nota');
        $scope.editar_nota.usu_mod = usu_id;
        $scope.editar_nota.act = tipo;
        request.setData($scope.editar_nota);
        crud.insertar("/submodulo_academico",request,function(response){
        modal.mensaje("CONFIRMACION",response.responseMsg);
        if(response.responseSta){
           $scope.editar_nota = "";
           $('#modaleditarequivalencia').modal('hide');   
        }
        }); 
    }
    
    
    
    
    









}]);