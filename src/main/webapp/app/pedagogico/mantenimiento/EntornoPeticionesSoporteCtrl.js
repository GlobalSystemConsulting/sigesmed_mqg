app.config(["ngTableFilterConfigProvider",function(ngTableFilterConfigProvider){
    ngTableFilterConfigProvider.setConfig({
      aliasUrls: {"checkbox": "app/ng-table/filters/checkbox.html"}
    });
}]);

app.controller("EntornoPeticionesSoporteCtrl",["$rootScope","$scope","NgTableParams","crud","modal", function ($rootScope,$scope,NgTableParams,crud,modal) {
    $scope.mod_selec={};
    $scope.sub_mod_selec={};
        $scope.mod_selec_men={};
    $scope.sub_mod_selec_men={};
    $scope.func_selec={};
    $scope.fechEnvio= new Date();
    $scope.mensajeEnviar={
      destinatario:"",
      destinatarioID:0,
      remitente:"",
      remitenteID:0,
      asunto:"",
      tipoMensaje:"",//Peticion o Soporte
      subTipoMensaje:"",//Guia de pasos o reporte de error
      fechaEnvioMostrar:"",
      fechaEnvio:"",
      flagimg:"",
      descripcion:"",
      funcionID:0,
      nombreReal:"",
      imagen:""
    };
    $scope.mensajeVer={
      destinatario:"",
      destinatarioID:0,
      remitente:"",
      remitenteID:0,
      asunto:"",
      tipoMensaje:"",//Peticion o Soporte
      subTipoMensaje:"",//Guia de pasos o reporte de error
      fechaEnvioMostrar:"",
      fechaEnvio:"",
      flagimg:"",
      descripcion:"",
      funcionID:0,
      nombreReal:"",
      imagen:""
    };
    $scope.mensajesEnviados=[];
    $scope.mensajesRecibidos=[];
    $scope.active=0;
    $scope.active2=0;
    
    var paramsFuncion = {count: 10};
    var settingEnviados = { counts: []};
    $scope.tablaEnviados = new NgTableParams(paramsFuncion, settingEnviados);
    
   var settingRecibidos = { counts: []};
    $scope.tablaRecibidos = new NgTableParams(paramsFuncion, settingRecibidos);
 
  $scope.subTipoMensaje="GuiaPasos";//esto cambia con el valor de los radiobutton
     
        
    $scope.funcionalidadRuta={
        id_mod: 0,
        id_submod: 0,
        id_func: 0
    }
    
    $scope.hayEnviados=false;
    $scope.hayRecibidos=false;


    
  $scope.tabs = [
    { title:'Dynamic Title 1', content:'Dynamic content 1' },
    { title:'Dynamic Title 2', content:'Dynamic content 2', disabled: true }
  ];
    //Temporal, carga lo necesario para escribir un mensaje.
        iniciarRedactarMensaje();
        
  $scope.cambiarVistaUsuario=function(){
      //TEMPORAL, SOLO PARA PRUEBAS
      if($rootScope.usuMaster.rol.rolID != 1){//Usuario
        $scope.mostrarAdministrador=false;
      }else if($rootScope.usuMaster.rol.rolID ==1){//Administrador
        $scope.mostrarAdministrador=true;
      }
  };

    $scope.cambioModulo=function(mod_selec, sub_mod_selec, func_selec){
      sub_mod_selec={};
      func_selec={};
      pasos={};
      $scope.mensajeEnviar.moduloNom=mod_selec.nombre;
    };
    $scope.cambioSubMod=function(sub_mod_selec, func_selec){
        func_selec={};
        $scope.mensajeEnviar.subModNom=sub_mod_selec.nombre;
    };
    
    function iniciarRedactarMensaje(){
        var f=new Date();
        $scope.mensajeEnviar.fechaEnvio=f;
        $scope.mensajeEnviar.fechaEnvioMostrar=f.getDate()+"/"+(f.getMonth()+1)+"/"+f.getFullYear(); 
       
        //Obteniendo remitente
        var request=crud.crearRequest('mantenimiento',1,'buscarPersonaPorId');
        request.setData({perID: $rootScope.usuMaster.usuario.usuarioID});
        crud.listar("/mantenimiento",request,function(data){
            $scope.mensajeEnviar.remitente=data.data.parNom + " " + data.data.parPat+" " + data.data.parMat; 
//            $scope.mensajeEnviar.remitenteID=data.data.parId;
        },function(response){
            console.info(response);
        });
        $scope.mensajeEnviar.remitenteID=$rootScope.usuMaster.usuario.ID;
        
        //Obteniendo destinatario
        //Es el codigo de usuari que busque el de session.
//        request=crud.crearRequest('mantenimiento',1,'buscarPersonaPorId');
//        request.setData({perID:53});
//        crud.listar("/mantenimiento",request,function(data){
//            $scope.mensajeEnviar.destinatario=data.data.parNom+ " " + data.data.parPat+" " + data.data.parMat ;
//            console.log(data.data);
//            //$scope.mensajeEnviar.destinatarioID=data.data.parId;
//            $scope.mensajeEnviar.destinatarioID=data.data.parId;
//        },function(response){
//            console.info(response)
//        }); 
        $scope.mensajeEnviar.destinatario="Administrador";
        $scope.mensajeEnviar.destinatarioID=1;
    };
    $scope.cambiarSubTipoMensaje=function(){
      if($scope.subTipoMensaje=="GuiaPasos") {
          $scope.subTipoMensaje="ReporteError";         
      }          
      else if($scope.subTipoMensaje=="ReporteError"){
          $scope.subTipoMensaje="GuiaPasos";        
      }
    };    
    $scope.listarMensajesEnviados=function(){
        var request=crud.crearRequest('mantenimiento',1,'listarMensajesEnviadosPeticion');
        request.setData({usuarioID:$rootScope.usuMaster.usuario.ID})
        crud.listar("/mantenimiento",request,function(data){
            if(data.data.length!=0){
                settingEnviados.dataset=data.data;            
                iniciarPosiciones(settingEnviados.dataset);
                $scope.tablaEnviados.settings(settingEnviados);
                $scope.hayEnviados=true;
            }else{
                $scope.hayEnviados=false;                
            }            
        },function(data){
            console.info(data);
        });
    };
    $scope.archivarMensaje=function(mensaje,flag){
        if(mensaje.estado!='Archivado'){
            var request=crud.crearRequest('mantenimiento',1,'archivarMensaje');
            if(flag){//Autor
                request.setData({menID: mensaje.mensajeID, flag:true});
            }else{//Destinatario
                request.setData({menID: mensaje.mensajeID, flag:false});
            }                        
            crud.actualizar("/mantenimiento", request, function(data){
                mensaje.estado='Archivado';
                modal.mensaje("CONFIRMACION",data.responseMsg);
            },function(data){            
                modal.mensaje("MENSAJE","No se pudo archivar el mensaje");
            });
        }else{
            modal.mensaje("MENSAJE","Mensaje ya  archivado");
        }        
    };
    $scope.eliminarMensaje=function(mensaje, tipo){
        modal.mensajeConfirmacion($scope, "Esta seguro de eliminar el mensaje", function () {
            var request=crud.crearRequest('mantenimiento',1,'eliminarMensaje');
            if(tipo=='Enviados'){
                request.setData({menID: mensaje.mensajeID, flag: true});                
            }else if(tipo=='Recibidos'){
                request.setData({menID: mensaje.mensajeID, flag: false});
            }
            
            crud.eliminar("/mantenimiento", request, function(data){
                if(tipo=='Enviados')//tabla enviados
                {
                    eliminarElemento(settingEnviados.dataset,mensaje.i);
                    $scope.tablaEnviados.reload();                        
                }else if(tipo=='Recibidos')//tabla recibidos
                {
                    eliminarElemento(settingRecibidos.dataset,mensaje.i);
                    $scope.tablaRecibidos.reload();
                }
                modal.mensaje("CONFIRMACION", data.responseMsg);
            }, function(data){
                modal.mensaje("MENSAJE", "No se pudo eliminar el mensaje");
            });
            
        });        
    };
    $scope.reenviarMensaje=function(mensaje){                       
        $scope.active=2;
        $scope.active2=2;
        for(var i=0; i<$rootScope.menuPrincipal.length;i++){
                if( $rootScope.menuPrincipal[i].moduloID==mensaje.mod){
                    $scope.sub_mod_selec_men=$rootScope.menuPrincipal[i].subModulos;
                    $scope.mod_selec_men=$rootScope.menuPrincipal[i];                    
                    break;
                }
            }
            for(var i=0; i<$scope.sub_mod_selec_men.length;i++){
                if( $scope.sub_mod_selec_men[i].subModuloID==mensaje.submod){
                    $scope.func_selec_men=$scope.sub_mod_selec_men[i].funciones;
                    $scope.sub_mod_selec_men=$scope.sub_mod_selec_men[i];                    
                    break;
                }                
            }
            for(var i=0; i<$scope.func_selec_men.length;i++){
                if( $scope.func_selec_men[i].funcionID==mensaje.func){
                    $scope.func_selec_men=$scope.func_selec_men[i];                
                    $scope.mensajeEnviar.funcionID=$scope.func_selec_men.funcionID;
                    break;
                }                
            }
            if(mensaje.tipoSoporte=='Guia Pasos'){
                $scope.subTipoMensaje="GuiaPasos";
            }else if(mensaje.tipoSoporte=='Reporte Error'){
                $scope.subTipoMensaje="ReporteError";
            }            
            $scope.mensajeEnviar.descripcion=mensaje.mensajeSoporte;
             $scope.mensajeEnviar.asunto=mensaje.asunto;          
             
             if($scope.mostrarAdministrador){
                 $scope.mensajeEnviar.destinatario=mensaje.destinatario;
                 $scope.mensajeEnviar.destinatarioID=mensaje.destinatarioIDSession;
             }else{
                 $scope.mensajeEnviar.destinatario="Administrador";
                $scope.mensajeEnviar.destinatarioID=0;
             }
             
             
    };
    $scope.responderMensaje=function(mensaje){
        $scope.active2=2;
        for(var i=0; i<$rootScope.menuPrincipal.length;i++){
            if( $rootScope.menuPrincipal[i].moduloID==mensaje.mod){
                $scope.sub_mod_selec_men=$rootScope.menuPrincipal[i].subModulos;
                $scope.mod_selec_men=$rootScope.menuPrincipal[i];                    
                break;
            }
        }
        for(var i=0; i<$scope.sub_mod_selec_men.length;i++){
            if( $scope.sub_mod_selec_men[i].subModuloID==mensaje.submod){
                $scope.func_selec_men=$scope.sub_mod_selec_men[i].funciones;
                $scope.sub_mod_selec_men=$scope.sub_mod_selec_men[i];                    
                break;
            }                
        }
        for(var i=0; i<$scope.func_selec_men.length;i++){
            if( $scope.func_selec_men[i].funcionID==mensaje.func){
                $scope.func_selec_men=$scope.func_selec_men[i];  
                $scope.mensajeEnviar.funcionID=$scope.func_selec_men.funcionID;
                break;
            }                
        }
        if(mensaje.tipoSoporte=='Guia Pasos'){
            $scope.subTipoMensaje="GuiaPasos";
        }else if(mensaje.tipoSoporte=='Reporte Error'){
            $scope.subTipoMensaje="ReporteError";
        }            
        $scope.mensajeEnviar.asunto=mensaje.asunto;
        $scope.mensajeEnviar.destinatarioID=mensaje.autorIDSession;
        $scope.mensajeEnviar.destinatario=mensaje.autor;                     
        $scope.mensajeEnviar.tipoMensaje="S";
    };
    
    
    $scope.listarMensajesRecibidos=function(){
        var request=crud.crearRequest('mantenimiento',1,'listarMensajesRecibidos');
        request.setData({usuarioID:$rootScope.usuMaster.usuario.ID})
        crud.listar("/mantenimiento",request,function(data){
            if(data.data.length!=0){
                settingRecibidos.dataset=data.data;
                console.info(data.data);
                iniciarPosiciones(settingRecibidos.dataset);
                $scope.tablaRecibidos.settings(settingRecibidos);
                $scope.hayRecibidos=true;
            }else{
                $scope.hayRecibidos=false;
            }
        },function(data){
            console.info(data);
        });
        
    };
    $scope.confirmarMensaje=function(){
        if($scope.mostrarAdministrador){
            $scope.mensajeEnviar.flagOcurrencia=false;
            $scope.enviarMensaje($scope.mensajeEnviar);
        }            
        else{
            $scope.mensajeEnviar.flagOcurrencia=true;
            $('#modalConfirmacionMensaje').modal('show');
        }
            
        
    }
    $scope.vistaPrevia=function(){
        $('#modalVistaPrevia').modal('show');
    };
    $scope.mostrarMensaje=function(mensaje){
        $scope.mensajeVer.asunto=mensaje.asunto;
        $scope.mensajeVer.destinatario=mensaje.destinatario;
        $scope.mensajeVer.descripcion=mensaje.mensajeSoporte;
        $scope.mensajeVer.remitente=mensaje.autor;
        $scope.mensajeVer.url=mensaje.url;
        $scope.mensajeVer.nombreReal=mensaje.nombreReal;
        $scope.mensajeVer.moduloNom=mensaje.modnom;
        $scope.mensajeVer.subModNom=mensaje.submodnom;
        $scope.mensajeVer.funcNom=mensaje.funcnom;
        $scope.mensajeVer.fechaEnvio=mensaje.fechaEnvio;
        $scope.mensajeVer.fechaOcurrencia=mensaje.fechaOcurrencia;
        console.info($scope.mensajeVer.url);
        
      $('#modalVisualizarMensaje').modal('show');
    };
    $scope.enviarMensaje=function(mensajeEnviar){
        //console.log(mensajeEnviar);
        //mensajeEnviar.funcionID=func_selec.funcionID;
       // console.log(func_selec);
       
            if(mensajeEnviar.asunto != ""){
                 if(mensajeEnviar.descripcion != ""){
                     if(mensajeEnviar.funcionID){
                         var request=crud.crearRequest('mantenimiento',1,'insertarMensaje');
                         if(!mensajeEnviar.tipoMensaje)//Si no existe quiere decir que no es eesponder mensaje, por lo que es peticion y no soporte
                             mensajeEnviar.tipoMensaje="P";
                         mensajeEnviar.subTipoMensaje=$scope.subTipoMensaje.charAt(0);
     //                    mensajeEnviar.funcionID=func_selec.funcionID;                                       
                        //Se recibe del remitente su id de usuario, del destinatario el de session.                   
                        if(!mensajeEnviar.destinatarioID)
                            mensajeEnviar.destinatarioID=1;
                        console.log(mensajeEnviar);
                        if(mensajeEnviar.nombreReal)
                            mensajeEnviar.flagimg=true;
                        else
                            mensajeEnviar.flagimg=false;
                        mensajeEnviar.fechaOcurrencia=convertirFecha($scope.fechEnvio);
                         request.setData(mensajeEnviar);

                         crud.insertar("/mantenimiento",request,function(data){    

                                $('#modalConfirmacionMensaje').modal('hide');
                             modal.mensaje("CONFIRMACION","Mensaje enviado");


                             $scope.listarMensajesEnviados();
                             if($scope.mostrarAdministrador){
                                 $scope.active2=1;
                             }else{
                                 $scope.active=1;
                             }       
                             $scope.mensajeEnviar.asunto="";
                             $scope.mensajeEnviar.descripcion="";
                             $scope.mensajeEnviar.img="";
                             $scope.mensajeEnviar.destinatarioID=0;//Se pone para que envie el mensaje al admin.
                             $scope.mensajeEnviar.nombreReal="";
                             $scope.mensajeEnviar.archivo="";
                             $scope.func_selec_men=undefined;
                             $scope.sub_mod_selec_men=undefined;
                             $scope.mod_selec_men=undefined;
                         },function(response){
                             console.log(response)
                         });  
                     }else{
                         modal.mensaje("MENSAJE","No se selecciono la funcionalidad");
                     }                
                 }else{
                     modal.mensaje("MENSAJE","No se ingreso una descripcion del mensaje")
                 }
             }else{
                 modal.mensaje("MENSAJE","No se ingreso el asunto del mensaje")
             }          
                   
    };

    $scope.prepararFuncionParaMensaje=function(func_selec,mensajeEnviar){
        mensajeEnviar.funcionID=func_selec.funcionID;
        mensajeEnviar.funcNom=func_selec.nombre;
    };
    
    $scope.limpiarEscribirMensaje=function(){
        $scope.mensajeEnviar.asunto="";
        $scope.mensajeEnviar.descripcion="";
        $scope.mensajeEnviar.destinatario="";
        $scope.mensajeEnviar.destinatarioID=0;//Se pone para que envie el mensaje al admin.
        $scope.mensajeEnviar.url="";
        $scope.mensajeEnviar.nombreReal="";
        $scope.mensajeEnviar.moduloNom="";
        $scope.mensajeEnviar.subModNom="";
        $scope.mensajeEnviar.funcNom="";
        //$scope.mensajeEnviar.flagEditDestinatario=flag;
        $scope.subTipoMensaje="GuiaPasos";
        $scope.mod_selec_men=undefined;
        $scope.sub_mod_selec_men=undefined;
        $scope.func_selec_men=undefined;
        $scope.mensajeEnviar.funcionID=0;
    };
    $scope.limpiarEscribirPeticion=function(){
        $scope.mensajeEnviar.asunto="";
        $scope.mensajeEnviar.descripcion="";
        $scope.mensajeEnviar.url="";
        $scope.mensajeEnviar.nombreReal="";
        $scope.mensajeEnviar.moduloNom="";
        $scope.mensajeEnviar.subModNom="";
        $scope.mensajeEnviar.funcNom="";
        //$scope.mensajeEnviar.flagEditDestinatario=flag;
        $scope.subTipoMensaje="GuiaPasos";
        $scope.mod_selec_men=undefined;
        $scope.sub_mod_selec_men=undefined;
        $scope.func_selec_men=undefined;
        $scope.mensajeEnviar.funcionID=0;
    };
    $scope.alertMe = function() {
        setTimeout(function() {
          $window.alert('You\'ve selected the alert tab!');
        });
    };
  
//  $scope.preparaFuncionEnMensaje=function(func_selec){
//      $scope.mensajeEnviar.funcionID=func_selec.funcionID;
//  }

   $scope.dt=new Date();
  $scope.clear = function() {
    $scope.dt = null;
  };

  $scope.dateOptions = {
    dateDisabled: disabled,
    formatYear: 'yy',
    maxDate: new Date(),
    minDate: new Date(2016, 1, 1),
    startingDay: 1
  };

  

    // Disable weekend selection
  function disabled(data) {
    var date = data.date,
      mode = data.mode;
    return mode === 'day' && (date.getDay() === 0 );
  }

  $scope.open2 = function() {
    $scope.popup2.opened = true;
  };
  $scope.popup2 = {
    opened: false
  };

 $scope.model = {
    name: 'Tabs'
  };
}]);
