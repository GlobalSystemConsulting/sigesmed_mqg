
app.controller("configuracionSdcCtrl", ["$rootScope","$scope", "NgTableParams", '$window', "crud", "modal", function ($rootScope,$scope, NgTableParams, $window, crud, modal) {

        //Implenetacion del controlador


        var params = {count: 10};
        var setting = {counts: []};
        $scope.miTabla = new NgTableParams(params, setting);
        $scope.posicionEditado = {};
        $scope.estados = [{id: 1, title: "Registrado"}, {id: 2, title: "Pendiente"}];
        $scope.tipoPlantilla = {};
        
        $scope.descripcionPlantilla = {};
        $scope.contenidos = [{id: "", plantillaId: "", contenido: ""}];
        $scope.letras = [];
        $scope.alineacion=[{id:1, title:'Centrado'},{id:2, title:'Justificado'},{id:3, title:'Izquierda'},{id:4, title:'Derecha'}];
        $scope.plantillaEditada = {
            planId: 0,
            tipoDoc: {},
            descripcion: "",
            version: 0,
            contenidoPlantilla: [{nombreLetra:null,size:0, contenido:"", alineacion: {}, isBold:false,isCursiva:false,isSubrayado:false}],
            personaID:$rootScope.usuMaster.usuario.ID,
            marcaAgua:true,
            documento:{nombreArchivo:"",archivo:{},url:""},
            verImagen:false,
            imagenes :[{descripcion:"",nombreArchivo:"",archivo:{},edi:false,url:""}],
            isSaved:false
        };
        $scope.imagen = {descripcion:"",nombreArchivo:"",archivo:{},edi:false,url:""};
        $scope.imagenes = [{descripcion:"",nombreArchivo:"",archivo:{},edi:false,url:""}];
        $scope.verImagen={};
        $scope.isThereArchivo=false;
        $scope.dataBase64 = "";
        $scope.tipoDocumentos = [{tipoDocumentoID:0,nombre:"",descripcion:"",fecha:"",estado:""}];
      
        $scope.listarPlantillas = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('configuracion', 1, 'listarPlantillas');
            request.setData({organizacionID:$rootScope.usuMaster.organizacion.organizacionID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las funciones de exito y error
            crud.listar("/documentosComunicacion", request, function (data) {
                setting.dataset = data.data;
                iniciarPosiciones(setting.dataset);
                $scope.miTabla.settings(setting);
            }, function (data) {
                console.info(data);
            });
        };



        $scope.listarTipoDocumentos = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('tramite_datos', 1, 'listarTipoDocumentos');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las organizaciones de exito y error
            crud.listar("/tramiteDocumentario", request, function (data) {
                $scope.tipoDocumentos = data.data;
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.listarNombresLetra = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('generarPlantilla', 1, 'listarPropLetras');
           
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las organizaciones de exito y error
            crud.listar("/documentosComunicacion", request, function (data) {
                $scope.letras = data.data;
                
//                $scope.iniciarDatos();
            }, function (data) {
                console.info(data);
            });
            
        };


        $scope.registrarPlantilla = function (t) {
            modal.mensajeConfirmacion($scope, "Esta seguro que desea registrar la plantilla?", function () {
                $scope.plantillaSel = JSON.parse(JSON.stringify(t));

                var request = crud.crearRequest('configuracion', 1, 'registrarPlantilla');
                request.setData({plantillaID: $scope.plantillaSel.plantillaId});

                crud.actualizar("/documentosComunicacion", request, function (data) {
                    modal.mensaje("CONFIRMACION", data.responseMsg);
                    if (data.responseSta) {
                        t.estado = 1;

                    }
                }, function (data) {
                    console.info(data);
                });
            });
        };

        $scope.eliminarPlantilla = function (t) {
            modal.mensajeConfirmacion($scope, "Esta seguro que desea eliminar la plantilla?", function () {
                $scope.plantillaSel = JSON.parse(JSON.stringify(t));

                var request = crud.crearRequest('configuracion', 1, 'eliminarPlantilla');
                request.setData({plantillaID: $scope.plantillaSel.plantillaId});

                crud.actualizar("/documentosComunicacion", request, function (data) {
                    modal.mensaje("CONFIRMACION", data.responseMsg);
                    if (data.responseSta) {
                        eliminarElemento(setting.dataset, t.i);
                        $scope.miTabla.reload();
                    }
                }, function (data) {
                    console.info(data);
                });
            });
        };

        $scope.listarImagenes = function (t) {
            //preparamos un objeto request
            var request = crud.crearRequest('configuracion', 1, 'listarImagenes');
            request.setData({plantillaID: t});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las organizaciones de exito y error
            crud.listar("/documentosComunicacion", request, function (data) {
                $scope.imagenes = data.data;
                
//                $scope.iniciarDatos();
            }, function (data) {
                console.info(data);
            });
            
        };
        $scope.editarPlantilla = function (t) {
            
            modal.mensajeConfirmacion($scope, "Esta seguro que desea editar la plantilla?", function () {
                $scope.plantillaEditada.documento.nombreArchivo="";
                $scope.plantillaEditada.documento.url="";
                $scope.verImagen={};
                $scope.plantillaEditada.documento.archivo={};
                $scope.imagen = {descripcion: "", nombreArchivo: "", archivo: {}, edi: false};
                $scope.imagenes =[];
                $scope.plantillaSel = JSON.parse(JSON.stringify(t));
                $scope.plantillaEditada.descripcion = t.descripcion;
                $scope.plantillaEditada.planId = t.plantillaId;
                $scope.descripcionPlantilla = t.descripcion;
                $scope.plantillaEditada.version = t.version;
                $scope.plantillaEditada.tipoDoc=t.tipoDocumento;
                $scope.plantillaEditada.documento.nombreArchivo=t.archivo;
                $scope.plantillaEditada.documento.url=t.url;
                var request = crud.crearRequest('generarDocumento', 1, 'traerPlantilla2Editar');
                request.setData({plantillaID: $scope.plantillaSel.plantillaId});

                crud.listar("/documentosComunicacion", request, function (response) {
                    if (response)
                    {
                        $scope.listarImagenes($scope.plantillaEditada.planId);
                        $scope.plantillaSel = response.data;
                        $scope.posicionEditado = t.i;
                        
                        $scope.plantillaEditada.tipoDoc=$scope.tipoDocumentos[t.tipoDocumentoID-1];
                       
                        $scope.plantillaEditada.contenidoPlantilla[0].alineacion=$scope.alineacion[$scope.plantillaSel[0].alineacion-1];
                        $scope.plantillaEditada.contenidoPlantilla[1].alineacion=$scope.alineacion[$scope.plantillaSel[1].alineacion-1];
                        $scope.plantillaEditada.contenidoPlantilla[2].alineacion=$scope.alineacion[$scope.plantillaSel[2].alineacion-1];
                        $scope.plantillaEditada.contenidoPlantilla[0].nombreLetra=$scope.letras[$scope.plantillaSel[0].letraId-1];
                        $scope.plantillaEditada.contenidoPlantilla[1].nombreLetra=$scope.letras[$scope.plantillaSel[1].letraId-1];
                        $scope.plantillaEditada.contenidoPlantilla[2].nombreLetra=$scope.letras[$scope.plantillaSel[2].letraId-1];
                        $scope.plantillaEditada.contenidoPlantilla[0].size=$scope.plantillaSel[0].tamanho;
                        $scope.plantillaEditada.contenidoPlantilla[1].size=$scope.plantillaSel[1].tamanho;
                        $scope.plantillaEditada.contenidoPlantilla[2].size=$scope.plantillaSel[2].tamanho;
                        $scope.plantillaEditada.contenidoPlantilla[0].isCursiva=$scope.plantillaSel[0].isCursiva;
                        $scope.plantillaEditada.contenidoPlantilla[1].isCursiva=$scope.plantillaSel[1].isCursiva;
                        $scope.plantillaEditada.contenidoPlantilla[2].isCursiva=$scope.plantillaSel[2].isCursiva;
                        $scope.plantillaEditada.contenidoPlantilla[0].isBold=$scope.plantillaSel[0].isBold;
                        $scope.plantillaEditada.contenidoPlantilla[1].isBold=$scope.plantillaSel[1].isBold;
                        $scope.plantillaEditada.contenidoPlantilla[2].isBold=$scope.plantillaSel[2].isBold;
                        $scope.plantillaEditada.contenidoPlantilla[0].isSubrayado=$scope.plantillaSel[0].isSubrayado;
                        $scope.plantillaEditada.contenidoPlantilla[1].isSubrayado=$scope.plantillaSel[1].isSubrayado;
                        $scope.plantillaEditada.contenidoPlantilla[2].isSubrayado=$scope.plantillaSel[2].isSubrayado;
                        $scope.plantillaEditada.contenidoPlantilla[0].contenido=$scope.plantillaSel[0].contenido;
                        $scope.plantillaEditada.contenidoPlantilla[1].contenido=$scope.plantillaSel[1].contenido;
                        $scope.plantillaEditada.contenidoPlantilla[2].contenido=$scope.plantillaSel[2].contenido;
                        $scope.plantillaEditada.marcaAgua=true;
                        
                        $('#modalEditarPlantilla').modal('show');
                        
                    }

                }, function (data) {
                    console.info(data);
                });
            });


        };

        $scope.generar = function ()
        {
            $scope.plantillaEditada.imagenes=$scope.imagenes;
            var request = crud.crearRequest('configuracion', 1, 'editarPlantilla');
            request.setData($scope.plantillaEditada);

            crud.insertar("/documentosComunicacion", request, function (data) {
                if (data.responseSta)
                {
                    modal.mensaje("CONFIRMACION", data.responseMsg);

                    
                    $('#modalEditarPlantilla').modal('hide');
                    $window.location.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.editarImagen = function (i, d) {
            if (d.edi) {
                $scope.imagenes[i] = d.copia;

            }
            else {
                d.copia = JSON.parse(JSON.stringify(d));
                d.edi = true;
            }
        };
        
        $scope.agregarImagen = function () {
           
            if ($scope.imagen.descripcion == "") {
                modal.mensaje("CONFIRMACION", "Ingrese descripcion de la Imagen");
                return;
            }
            if ($scope.imagen.nombreArchivo=="") {
                modal.mensaje("CONFIRMACION", "Ingrese Imagen");
                return;
            }
            $scope.imagen.url="";
            $scope.imagenes.push($scope.imagen);
            $scope.imagen = {descripcion: "", nombreArchivo: "", archivo: {}, edi: false,url:""};
        };
        
        $scope.eliminarImagen = function (i, d) {
            //si estamso cancelando la edicion
            if (d.edi) {
                d.edi = false;
                //delete d.copia;
                d.copia = null;
            }
            //si queremos eliminar el elemento
            else {
                $scope.imagenes.splice(i, 1);
            }
        };
        
        $scope.ver = function ()
        {
            if (typeof $scope.verImagen.verImagen == "undefined") {
                $scope.plantillaEditada.verImagen = false;
                $scope.vistaPreliminar();
                return;
            }
//            if (!angular.equals($scope.verImagen.verImagen,{descripcion:"",nombreArchivo:"",archivo:{},edi:false,url:""}))
            if (typeof $scope.verImagen.verImagen.archivo != "undefined")
            {
                var request = crud.crearRequest('generarPlantilla', 1, 'ingresarImagenTemporal');
                request.setData($scope.verImagen.verImagen);

                crud.insertar("/documentosComunicacion", request, function (data) {
                    modal.mensaje("CONFIRMACION", data.responseMsg);
                    $scope.plantillaEditada.verImagen = true;
                    $scope.plantillaEditada.isSaved = true;
                    $scope.vistaPreliminar();
                }, function (data) {
                    console.info(data);
                });
            }
            else
            {
                $scope.plantillaEditada.verImagen = true;
                $scope.plantillaEditada.isSaved = false;
                $scope.plantillaEditada.nombreArchivo=$scope.verImagen.verImagen.nombreArchivo;
                $scope.vistaPreliminar();
            }

        };


        $scope.vistaPreliminar = function ()
        {
            var request = crud.crearRequest('generarPlantilla', 1, 'vistaPreliminar');
            request.setData($scope.plantillaEditada);

            crud.listar("/documentosComunicacion", request, function (data) {
                modal.mensaje("CONFIRMACION", data.responseMsg);
                
                
                verDocumentoPestana(data.data[0].datareporte);
                    
                var link = document.createElement("a");
                link.href = data.data[0].datareporte;
                //set the visibility hidden so it will not effect on your web-layout
                link.style = "visibility:hidden";
                link.download = "documento.pdf";
                //this part will append the anchor tag and remove it after automatic click
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
                
                
                
                
            }, function (data) {
                console.info(data);
            });

        };

        //Encabezado
        $scope.botonAlineacionIzqEnc = function () {
            $scope.plantillaEditada.contenidoPlantilla[0].alineacion = $scope.alineacion[2];
        };
        $scope.botonAlineacionCenEnc = function () {
            $scope.plantillaEditada.contenidoPlantilla[0].alineacion = $scope.alineacion[0];
        };
        $scope.botonAlineacionDerEnc = function () {
            $scope.plantillaEditada.contenidoPlantilla[0].alineacion = $scope.alineacion[3];
        };
        $scope.botonAlineacionJusEnc = function () {
            $scope.plantillaEditada.contenidoPlantilla[0].alineacion = $scope.alineacion[1];
        };

        $scope.botonBoldEnc = function () {
            if(!$scope.plantillaEditada.contenidoPlantilla[0].isBold)
            $scope.plantillaEditada.contenidoPlantilla[0].isBold = true;
            else
                $scope.plantillaEditada.contenidoPlantilla[0].isBold = false;
        };
        $scope.botonCursivaEnc = function () {
            if(!$scope.plantillaEditada.contenidoPlantilla[0].isCursiva)
            $scope.plantillaEditada.contenidoPlantilla[0].isCursiva = true;
            else
                $scope.plantillaEditada.contenidoPlantilla[0].isCursiva = false;
        };
        $scope.botonSubrayadoEnc = function () {
            if(!$scope.plantillaEditada.contenidoPlantilla[0].isSubrayado)
            $scope.plantillaEditada.contenidoPlantilla[0].isSubrayado = true;
            else
                $scope.plantillaEditada.contenidoPlantilla[0].isSubrayado = false;
        };
        //Cuerpo
        $scope.botonAlineacionIzqCue = function () {
            $scope.plantillaEditada.contenidoPlantilla[1].alineacion = $scope.alineacion[2];
        };
        $scope.botonAlineacionCenCue = function () {
            $scope.plantillaEditada.contenidoPlantilla[1].alineacion = $scope.alineacion[0];
        };
        $scope.botonAlineacionDerCue = function () {
            $scope.plantillaEditada.contenidoPlantilla[1].alineacion = $scope.alineacion[3];
        };
        $scope.botonAlineacionJusCue = function () {
            $scope.plantillaEditada.contenidoPlantilla[1].alineacion = $scope.alineacion[1];
        };

        $scope.botonBoldCue = function () {
            if(!$scope.plantillaEditada.contenidoPlantilla[1].isBold)
            $scope.plantillaEditada.contenidoPlantilla[1].isBold = true;
            else
                $scope.plantillaEditada.contenidoPlantilla[1].isBold = false;
        };
        $scope.botonCursivaCue = function () {
            if(!$scope.plantillaEditada.contenidoPlantilla[1].isCursiva)
            $scope.plantillaEditada.contenidoPlantilla[1].isCursiva = true;
            else
                $scope.plantillaEditada.contenidoPlantilla[1].isCursiva = false;
        };
        $scope.botonSubrayadoCue = function () {
            if(!$scope.plantillaEditada.contenidoPlantilla[1].isSubrayado)
            $scope.plantillaEditada.contenidoPlantilla[1].isSubrayado = true;
            else
                $scope.plantillaEditada.contenidoPlantilla[1].isSubrayado = false;
        };
        //Despedida
        $scope.botonAlineacionIzqDes = function () {
            $scope.plantillaEditada.contenidoPlantilla[2].alineacion = $scope.alineacion[2];
        };
        $scope.botonAlineacionCenDes = function () {
            $scope.plantillaEditada.contenidoPlantilla[2].alineacion = $scope.alineacion[0];
        };
        $scope.botonAlineacionDerDes = function () {
            $scope.plantillaEditada.contenidoPlantilla[2].alineacion = $scope.alineacion[3];
        };
        $scope.botonAlineacionJusDes = function () {
            $scope.plantillaEditada.contenidoPlantilla[2].alineacion = $scope.alineacion[1];
        };

        $scope.botonBoldDes = function () {
            if(!$scope.plantillaEditada.contenidoPlantilla[2].isBold)
            $scope.plantillaEditada.contenidoPlantilla[2].isBold = true;
            else
                $scope.plantillaEditada.contenidoPlantilla[2].isBold = false;
        };
        $scope.botonCursivaDes = function () {
            if(!$scope.plantillaEditada.contenidoPlantilla[2].isCursiva)
            $scope.plantillaEditada.contenidoPlantilla[2].isCursiva = true;
            else
                $scope.plantillaEditada.contenidoPlantilla[2].isCursiva = false;
        };
        $scope.botonSubrayadoDes = function () {
            if(!$scope.plantillaEditada.contenidoPlantilla[2].isSubrayado)
            $scope.plantillaEditada.contenidoPlantilla[2].isSubrayado = true;
            else
                $scope.plantillaEditada.contenidoPlantilla[2].isSubrayado = false;
        };
        $scope.iniciarDatos = function () {
            $scope.listarPlantillas();
            $scope.listarTipoDocumentos();
            $scope.listarNombresLetra();
            $scope.plantillaEditada.contenidoPlantilla = [{}, {}, {}];
            $scope.imagen = {descripcion: "", nombreArchivo: "", archivo: {}, edi: false,url:""};
            
        };
    }]);
