
app.controller("registroTrabajadorCtrl", ["$rootScope", "$scope", "NgTableParams", '$window', "crud", "modal", "ModalService", function ($rootScope, $scope, NgTableParams, $window, crud, modal, ModalService) {

        //Implenetacion del controlador


        var params = {count: 10};
        var setting = {counts: []};
        $scope.miTabla = new NgTableParams(params, setting);
        $scope.nuevaPersona = {perId:-1,dni:"",nombre:"",materno:"",paterno:"",nacimiento:"",email:"",numero1:"",numero2:"",fijo:"",direccion:"",existe:true};
        $scope.nuevoTrabajador={fechaIngreso:"",salario:"",cargo:"",usuario:"",org:"",perId:-1,tipo:"",existe:true};
        $scope.tipoTrabajadores=[{id:"Di",title:"Directivo"},{id:"Ad",title:"Administrativo"},{id:"Do",title:"Docente"}];
        $scope.cargoTrabajadores=[{id:"",nombre:""}];
        $scope.tipoTrabajadorSel={};

        $rootScope.showLoading();
        $scope.trabajador = [];
        $scope.trabajadorSel = {};
        $scope.trabajadorId = {};
       
        $scope.listarTrabajador = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('registroTrabajador', 1, 'listarTrabajadorDetallado');
            request.setData({organizacionID: $rootScope.usuMaster.organizacion.organizacionID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las funciones de exito y error
            crud.listar("/controlPersonal", request, function (data) {
                                
                if (data.responseSta)
                {
                    setting.dataset = data.data;
                    //console.log("prueba1",  data.data);
                    iniciarPosiciones(setting.dataset);
                    $scope.miTabla.settings(setting);
                    $rootScope.hideLoading();
                }
                else{
                    modal.mensaje("MENSAJE","No se encontro ningun trabajador");
                }
                
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.listarCargoTrabajador = function () {
            //preparamos un objeto request
            if ($scope.nuevoTrabajador.tipo == null)
            {
                $scope.cargoTrabajadores=[{id:"",nombre:""}];
                return ;
            }               
            else
            {
                var request = crud.crearRequest('registroTrabajador', 1, 'listarCargoTrabajador');
                request.setData({tipoTrab: $scope.nuevoTrabajador.tipo.id});
                //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
                //y las funciones de exito y error
                crud.listar("/controlPersonal", request, function (data) {
                    $scope.cargoTrabajadores = data.data;

                }, function (data) {
                    console.info(data);
                });
            }

        };

        $scope.buscarPersona = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('registroTrabajador', 1, 'buscarPersona');
            request.setData({dni: $scope.nuevaPersona.dni,organizacionID: $rootScope.usuMaster.organizacion.organizacionID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/controlPersonal", request, function (data) {
                modal.mensaje("CONFIRMACION", data.responseMsg);
                if (data.responseSta) {
                    $scope.nuevaPersona = data.data.persona;
                    $scope.nuevaPersona.nacimiento = stringToDate($scope.nuevaPersona.nacimiento,"yyyy-mm-dd","-");
                    if (data.data.trabajador)
                    {
                        
                        $scope.nuevoTrabajador = data.data.trabajador;
                        if($scope.nuevoTrabajador.tipo==$scope.tipoTrabajadores[0].id)
                        {
                            $scope.nuevoTrabajador.tipo=$scope.tipoTrabajadores[0];
                        }else if($scope.nuevoTrabajador.tipo==$scope.tipoTrabajadores[1].id)
                        {
                            $scope.nuevoTrabajador.tipo=$scope.tipoTrabajadores[1];
                        }else if($scope.nuevoTrabajador.tipo==$scope.tipoTrabajadores[2].id)
                        {
                            $scope.nuevoTrabajador.tipo=$scope.tipoTrabajadores[2];
                        }
                        $scope.listarCargoTrabajador();
//                        $scope.nuevoTrabajador.cargo=buscarObjeto($scope.cargoTrabajadores,"id",$scope.nuevoTrabajador.cargo.id);
                        $scope.nuevoTrabajador.fechaIngreso= stringToDate($scope.nuevoTrabajador.fechaIngreso,"yyyy-mm-dd","-");
                    }
                    else {
//                        $scope.nuevoTrabajador.nombre = $scope.nuevaPersona.dni;
                          $scope.nuevoTrabajador.existe = false;
                          $scope.nuevoTrabajador.tipo="";
                          $scope.nuevoTrabajador.cargo="";
                          $scope.nuevoTrabajador.salario=0.0;
                          $scope.cargoTrabajadores=[{id:"",nombre:""}];
                    }
                }
                else {
                    
                    $('#registrarUsuario').modal('show');
            
                    $scope.nuevaPersona.existe = false;
                    /*$scope.nuevaPersona.nombre="";
                     $scope.nuevaPersona.paterno="";
                     $scope.nuevaPersona.materno="";
                     $scope.nuevaPersona.nacimiento="";
                     $scope.nuevaPersona.email="";
                     $scope.nuevaPersona.numero1="";
                     $scope.nuevaPersona.numero2="";  */
//                    $scope.nuevoUsuario.nombre = $scope.nuevaPersona.dni;
                    $scope.nuevoTrabajador.tipo="";
                    $scope.nuevoTrabajador.cargo="";
                    $scope.nuevoTrabajador.salario=0.0;
                    $scope.nuevoTrabajador.existe = false;
                }
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.cancelarUsuarioRegistro = function () {
            
            $("#registrarUsuario, #modalNuevo").modal("hide");
        };
        
        $scope.registroUsuario = function () {
            
            $scope.cancelarUsuarioRegistro();
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            
            $window.location.href = '/SIGESMED/app/#/usuarioEmpresa';
        };

        $scope.prepararAgregar = function () {
            
            $scope.nuevaPersona = {perId:-1,dni:"",nombre:"",materno:"",paterno:"",nacimiento:"",email:"",numero1:"",numero2:"",fijo:"",direccion:"",existe:true};
            $scope.nuevoTrabajador={fechaIngreso:"",salario:"",cargo:"",usuario:"",org:"",perId:-1,tipo:"",existe:true};
            $scope.cargoTrabajadores=[{id:"",nombre:""}];

            $("#modalNuevo").modal('show');
        };

        $scope.reiniciarDatos = function () {
            var p = $scope.nuevaPersona;
            var t = $scope.nuevoTrabajador;
            p.existe = true;
            p.perId = -1;
            p.nombre = "";
            p.paterno = "";
            p.materno = "";
            p.nacimiento = "";
            p.email = "";
            p.numero1 = "";
            p.numero2 = "";
            p.direccion = "";
            p.fijo = "";
            t.cargo = "";
            t.existe = true;
            t.fechaIngreso = "";
            t.perId = -1;
            t.salario = "";
            t.tipo="";
//            t.org="";
//            t.usuario="";
           
        };

        $scope.iniciarDatos = function () {
            $scope.listarTrabajador();
            
        };
        $scope.agregarUsuario = function () {
            $scope.nuevoTrabajador.org =$rootScope.usuMaster.organizacion.organizacionID;
            $scope.nuevoTrabajador.tipo=$scope.nuevoTrabajador.tipo.id;
            $scope.nuevoTrabajador.salario=$scope.nuevoTrabajador.salario;
            var request = crud.crearRequest('registroTrabajador', 1, 'nuevoTrabajador');
            request.setData({persona: $scope.nuevaPersona, trabajador: $scope.nuevoTrabajador});

            crud.insertar("/controlPersonal", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {  
                    $scope.Trabajador = response.data;
                    //$scope.nuevoTrabajador.nombreCompleto = response.data.nombreCompleto;
                    insertarElemento(setting.dataset, response.data[0]);
                    $scope.miTabla.reload();
                    $('#modalNuevo').modal('hide');
                    console.log("este es nuevo trabajador", response.data[0]);
                    console.log("este es nuevo trabajador", $scope.Trabajador);
                }
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.prepararEditarTrabajador = function (t, i){
                       
            $scope.trabajadorSel = JSON.parse(JSON.stringify(t));
            $scope.trabajadorSel.i = i;
            console.log("esto tengo", $scope.trabajadorSel);
            //preparamos un objeto request
            var request = crud.crearRequest('registroTrabajador', 1, 'buscarPersona');
            request.setData({dni: $scope.trabajadorSel.dni,organizacionID: $rootScope.usuMaster.organizacion.organizacionID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/controlPersonal", request, function (data) {
                modal.mensaje("CONFIRMACION", data.responseMsg);
                if (data.responseSta) {
                    $scope.nuevaPersona = data.data.persona;
                    $scope.nuevaPersona.nacimiento = stringToDate($scope.nuevaPersona.nacimiento,"yyyy-mm-dd","-");
                    console.log("prueba1", data.data.persona);
                    console.log("prueba2", data.data.trabajador);
                    
                    if (data.data.trabajador)
                    {
                        
                        $scope.nuevoTrabajador = data.data.trabajador;
                        if($scope.nuevoTrabajador.tipo==$scope.tipoTrabajadores[0].id)
                        {
                            $scope.nuevoTrabajador.tipo=$scope.tipoTrabajadores[0];
                        }else if($scope.nuevoTrabajador.tipo==$scope.tipoTrabajadores[1].id)
                        {
                            $scope.nuevoTrabajador.tipo=$scope.tipoTrabajadores[1];
                        }else if($scope.nuevoTrabajador.tipo==$scope.tipoTrabajadores[2].id)
                        {
                            $scope.nuevoTrabajador.tipo=$scope.tipoTrabajadores[2];
                        }
                        $scope.listarCargoTrabajador();
//                        $scope.nuevoTrabajador.cargo=buscarObjeto($scope.cargoTrabajadores,"id",$scope.nuevoTrabajador.cargo.id);
                        $scope.nuevoTrabajador.fechaIngreso= stringToDate($scope.nuevoTrabajador.fechaIngreso,"yyyy-mm-dd","-");
                        $scope.nuevoTrabajador.traId = $scope.trabajadorSel.traId;
                        $scope.nuevoTrabajador.salario = $scope.trabajadorSel.salario;
                        $scope.trabajadorId.traId = $scope.trabajadorSel.traId;
                    }
                    else {
//                        $scope.nuevoTrabajador.nombre = $scope.nuevaPersona.dni;
                          $scope.nuevoTrabajador.existe = false;
                          $scope.nuevoTrabajador.tipo="";
                          $scope.nuevoTrabajador.cargo="";
                          $scope.nuevoTrabajador.salario = "";
                          $scope.nuevoTrabajador.traId = -1;
                          $scope.trabajadorId.traId = -1;
                          $scope.cargoTrabajadores=[{id:"",nombre:""}];
                    }
                    $('#modalEditarTrabajador').modal('show');
                }                
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.editarUsuario = function (orgID){
            $scope.nuevoTrabajador.org = orgID;
            $scope.nuevoTrabajador.tipo=$scope.nuevoTrabajador.tipo.id;
            $scope.nuevoTrabajador.salario=$scope.nuevoTrabajador.salario;
            $scope.trabajadorId.traId = $scope.trabajadorSel.traId;
            var request = crud.crearRequest('registroTrabajador', 1, 'editarTrabajador');
            request.setData({persona: $scope.nuevaPersona, trabajador: $scope.nuevoTrabajador, trabajadorId: $scope.trabajadorId});
            crud.actualizar("/controlPersonal",request, function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    //actualizando
                    var index = $scope.trabajadorSel.i;
                    console.log("este es index", index);
                    setting.dataset[index] = response.data[0];
                    $scope.miTabla.reload();
                    $('#modalEditarTrabajador').modal('hide');
                }
            },function(data){
                console.info(data);
            });
            
        }
    }]);
