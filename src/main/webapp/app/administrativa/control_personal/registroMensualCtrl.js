
app.controller("registroMensualCtrl", ["$rootScope", "$scope", "NgTableParams", '$window', "crud", "modal", "ModalService", function ($rootScope, $scope, NgTableParams, $window, crud, modal, ModalService) {

        //Implenetacion del controlador


        var params = {count: 10};
        var setting = {counts: []};
        $scope.miTabla = new NgTableParams(params, setting);
        $scope.codigoOrg="";

       
        $scope.listarRegistrosMensuales = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('configuracionControl', 1, 'listarRegistrosMensuales');
            request.setData({organizacionID: $rootScope.usuMaster.organizacion.organizacionID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las funciones de exito y error
            crud.listar("/controlPersonal", request, function (data) {
                if (data.responseSta)
                {
                    setting.dataset = data.data.lista;
                    iniciarPosiciones(setting.dataset);
                    $scope.miTabla.settings(setting);
                    $scope.codigoOrg=data.data.nroOrg;
                }

            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.verConsolidado=function(o)
        {
            var request = crud.crearRequest('reportes',1,'reporteConsolidadoMensual');       
     
            request.setData({fecha:o.fecha,organizacionID: $rootScope.usuMaster.organizacion.organizacionID,nro:$scope.codigoOrg});        
            crud.listar("/controlPersonal",request,function(data){
                verDocumentoPestana(data.data[0].datareporte);
            },function(data){
                console.info(data);
            });            
        };
        
        $scope.verCuadroAsistencia=function(o)
        {
             var request = crud.crearRequest('reportes',1,'reporteCuadroAsistencia');       
     
            request.setData({fecha:o.fecha,organizacionID: $rootScope.usuMaster.organizacion.organizacionID,nombreOrg:$rootScope.usuMaster.organizacion.nombre});        
            crud.listar("/controlPersonal",request,function(data){            
                $scope.dataBase64 = data.data[0].datareporte;
                window.open($scope.dataBase64);
            },function(data){
                console.info(data);
            });            
        };
        
        
    }]);
