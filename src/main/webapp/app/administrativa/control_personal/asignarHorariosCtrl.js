app.requires.push('angularModalService');
app.requires.push('ngAnimate');

app.controller("asignarHorariosCtrl", ["$rootScope", "$scope", "NgTableParams", '$window', "crud", "modal", "ModalService", function ($rootScope, $scope, NgTableParams, $window, crud, modal, ModalService) {

        //Implenetacion del controlador


        var params = {count: 10};
        var setting = {counts: []};
        $scope.miTabla = new NgTableParams(params, setting);

        var paramsTrabajadoresPersonalizado = {count: 10};
        var settingTrabajadoresPersonalizado = {counts: []};
        $scope.tablaTrabajadoresPersonalizado = new NgTableParams(paramsTrabajadoresPersonalizado, settingTrabajadoresPersonalizado);



        //$scope.configuracion = {anho:"",documento:"", descripcion: "", responsable: "",idResponsable:"", fechaInicio: "", fechaFin: "", estado: "",org:"",horaTol:0,minTol:0}; 
        $scope.trabajador = {personaID: '', dni: "", datos: "", idCargo: "", cargo: ""};
        $scope.trabajadorPersonalizado = {personaID: '', dni: "", datos: "", idCargo: "", cargo: "", sel: false};
        $scope.personalizado = {personaID: "", horarioId: ""};
        $scope.horario = [];
        $scope.listaHorario = [];
        $scope.nuevoHorario = {idHorario: "", des: "", idCargo: ""};
        $scope.asignados = {id: "", des: "", idCargo: ""};
        $scope.asignadosActual = {id: "", des: "", idCargo: ""};

        $scope.trabajadoresSeleccionados = [];


        $scope.dia =
                {
                    id: "",
                    lunDesde: "",
                    lunHasta: "",
                    marDesde: "",
                    marHasta: "",
                    mieDesde: "",
                    mieHasta: "",
                    jueDesde: "",
                    jueHasta: "",
                    vieDesde: "",
                    vieHasta: "",
                    sabDesde: "",
                    sabHasta: "",
                    domDesde: "",
                    domHasta: ""
                };

        $scope.listarCargos = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('registroHorario', 1, 'listarCargos');
            request.setData({organizacionID: $rootScope.usuMaster.organizacion.organizacionID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las funciones de exito y error
            crud.listar("/controlPersonal", request, function (data) {
                if (data.responseSta) {
                    setting.dataset = data.data;
                    iniciarPosiciones(setting.dataset);
                    $scope.miTabla.settings(setting);
                }

            }, function (data) {
                console.info(data);
            });
        };



        $scope.listarHorarios = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('registroHorario', 1, 'listarHorarios');
            request.setData({organizacionID: $rootScope.usuMaster.organizacion.organizacionID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las funciones de exito y error
            crud.listar("/controlPersonal", request, function (data) {
                if (data.responseSta) {
                    setting.dataset = data.data;
                    $scope.listaHorario = data.data;
                    iniciarPosiciones(setting.dataset);
                    $scope.miTabla.settings(setting);
//                    $scope.horarios=[];
//                   
                }

            }, function (data) {
                console.info(data);
            });
        };
        $scope.verDetalladoSeleccionado = function ()
        {
            $scope.horarioTemp = {
                id: $scope.personalizado.horarioId,
                //function buscarContenido(lista, labelClave, labelContenido, idBuscado){        
                des: buscarContenido($scope.listaHorario, "id", "des", $scope.personalizado.horarioId)
            }

            $scope.verDetallado($scope.horarioTemp);

        };

        $scope.verDetalleCargoConsulta = function ()
        {
            $scope.verDetallado($scope.asignados);

        };
        $scope.verDetalleActualConsulta = function ()
        {
            $scope.verDetallado($scope.asignadosActual);

        };

        $scope.verDetallado = function (o) {
            //preparamos un objeto request
            $scope.horario = [];
            var request = crud.crearRequest('registroHorario', 1, 'listarHorarioById');
            request.setData({horCabId: o.id});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las funciones de exito y error
            crud.listar("/controlPersonal", request, function (data) {
                if (data.responseSta) {
                    for (var m = 0; m < data.data.length; m++)
                    {

                        $scope.diaDet = data.data[m];

                        if ($scope.diaDet.nom == "L")
                        {
                            $scope.dia.lunDesde = $scope.diaDet.lunDesde;
                            $scope.dia.lunHasta = $scope.diaDet.lunHasta;
                            $scope.dia.id = $scope.diaDet.id;
                            var i = 0;

                            if ($scope.horario.length > 0)
                            {
                                for (i = 0; i < $scope.horario.length; i++)
                                {
                                    if (!$scope.horario[i].lunDesde)
                                    {
                                        break;
                                    }

                                }
                                if (i == $scope.horario.length)
                                {
                                    $scope.horario.push($scope.dia);
                                } else
                                {
                                    $scope.horario[i].lunDesde = $scope.dia.lunDesde;
                                    $scope.horario[i].lunHasta = $scope.dia.lunHasta;
                                }

                            } else
                            {
                                $scope.horario.push($scope.dia);
                            }


                        } else if ($scope.diaDet.nom == "M")
                        {

                            $scope.dia.marDesde = $scope.diaDet.marDesde;
                            $scope.dia.marHasta = $scope.diaDet.marHasta;
                            $scope.dia.id = $scope.diaDet.id;

                            var i = 0;
                            if ($scope.horario.length > 0)
                            {
                                for (i = 0; i < $scope.horario.length; i++)
                                {
                                    if (!$scope.horario[i].marDesde)
                                    {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                                        break;
                                    }
                                }
                                if (i == $scope.horario.length)
                                {
                                    $scope.horario.push($scope.dia);
                                } else
                                {
                                    $scope.horario[i].marDesde = $scope.dia.marDesde;
                                    $scope.horario[i].marHasta = $scope.dia.marHasta;
                                }

                            } else
                            {
                                $scope.horario.push($scope.dia);
                            }
                        } else if ($scope.diaDet.nom == "W")
                        {
                            $scope.dia.mieDesde = $scope.diaDet.mieDesde;
                            $scope.dia.mieHasta = $scope.diaDet.mieHasta;
                            $scope.dia.id = $scope.diaDet.id;

                            var i = 0;
                            if ($scope.horario.length > 0)
                            {
                                for (i = 0; i < $scope.horario.length; i++)
                                {
                                    if (!$scope.horario[i].mieDesde)
                                    {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                                        break;
                                    }
                                }
                                if (i == $scope.horario.length)
                                {
                                    $scope.horario.push($scope.dia);
                                } else
                                {
                                    $scope.horario[i].mieDesde = $scope.dia.mieDesde;
                                    $scope.horario[i].mieHasta = $scope.dia.mieHasta;
                                }

                            } else
                            {
                                $scope.horario.push($scope.dia);
                            }
                        } else if ($scope.diaDet.nom == "J")
                        {
                            $scope.dia.jueDesde = $scope.diaDet.jueDesde;
                            $scope.dia.jueHasta = $scope.diaDet.jueHasta;
                            $scope.dia.id = $scope.diaDet.id;

                            var i = 0;
                            if ($scope.horario.length > 0)
                            {
                                for (i = 0; i < $scope.horario.length; i++)
                                {
                                    if (!$scope.horario[i].jueDesde)
                                    {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                                        break;
                                    }

                                }
                                if (i == $scope.horario.length)
                                {
                                    $scope.horario.push($scope.dia);
                                } else
                                {
                                    $scope.horario[i].jueDesde = $scope.dia.jueDesde;
                                    $scope.horario[i].jueHasta = $scope.dia.jueHasta;
                                }

                            } else
                            {
                                $scope.horario.push($scope.dia);
                            }
                        } else if ($scope.diaDet.nom == "V")
                        {
                            $scope.dia.vieDesde = $scope.diaDet.vieDesde;
                            $scope.dia.vieHasta = $scope.diaDet.vieHasta;
                            $scope.dia.id = $scope.diaDet.id;

                            var i = 0;
                            if ($scope.horario.length > 0)
                            {
                                for (i = 0; i < $scope.horario.length; i++)
                                {
                                    if (!$scope.horario[i].vieDesde)
                                    {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                                        break;
                                    }
                                }
                                if (i == $scope.horario.length)
                                {
                                    $scope.horario.push($scope.dia);
                                } else
                                {
                                    $scope.horario[i].vieDesde = $scope.dia.vieDesde;
                                    $scope.horario[i].vieHasta = $scope.dia.vieHasta;
                                }

                            } else
                            {
                                $scope.horario.push($scope.dia);
                            }
                        } else if ($scope.diaDet.nom == "S")
                        {
                            $scope.dia.sabDesde = $scope.diaDet.sabDesde;
                            $scope.dia.sabHasta = $scope.diaDet.sabHasta;
                            $scope.dia.id = $scope.diaDet.id;

                            var i = 0;
                            if ($scope.horario.length > 0)
                            {
                                for (i = 0; i < $scope.horario.length; i++)
                                {
                                    if (!$scope.horario[i].sabDesde)
                                    {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                                        break;
                                    }
                                }
                                if (i == $scope.horario.length)
                                {
                                    $scope.horario.push($scope.dia);
                                } else
                                {
                                    $scope.horario[i].sabDesde = $scope.dia.sabDesde;
                                    $scope.horario[i].sabHasta = $scope.dia.sabHasta;
                                }

                            } else
                            {
                                $scope.horario.push($scope.dia);
                            }
                        } else if ($scope.diaDet.nom == "D")
                        {
                            $scope.dia.domDesde = $scope.diaDet.domDesde;
                            $scope.dia.domHasta = $scope.diaDet.domHasta;
                            $scope.dia.id = $scope.diaDet.id;

                            var i = 0;
                            if ($scope.horario.length > 0)
                            {
                                for (i = 0; i < $scope.horario.length; i++)
                                {
                                    if (!$scope.horario[i].domDesde)
                                    {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                                        break;
                                    }
                                }
                                if (i == $scope.horario.length)
                                {
                                    $scope.horario.push($scope.dia);
                                } else
                                {
                                    $scope.horario[i].domDesde = $scope.dia.domDesde;
                                    $scope.horario[i].domHasta = $scope.dia.domHasta;
                                }

                            } else
                            {
                                $scope.horario.push($scope.dia);
                            }
                        }

                        $scope.dia = {
                            id: "",
                            lunDesde: "",
                            lunHasta: "",
                            marDesde: "",
                            marHasta: "",
                            mieDesde: "",
                            mieHasta: "",
                            jueDesde: "",
                            jueHasta: "",
                            vieDesde: "",
                            vieHasta: "",
                            sabDesde: "",
                            sabHasta: "",
                            domDesde: "",
                            domHasta: ""
                        };

                    }

                    $scope.showHorarioDetalle(o);



                }
                // $scope.horario = data.data;




            }, function (data) {
                console.info(data);
            });
        };

        $scope.showHorarioDetalle = function (o) {

            ModalService.showModal({
                templateUrl: "administrativa/control_personal/agregarHorario.html",
                controller: "verHorarioCtrl",
                inputs: {
                    title: "Ver Horario",
                    semana: $scope.diaSemana,
                    horario: $scope.horario,
                    cabecera: o
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                        //$scope.listarHorarios();
                        $scope.horario = [];
                        //$scope.miTabla.reload();
                    }
                });
            });
        };
//liberarHorario
        $scope.liberarHorario = function (o) {
            modal.mensajeConfirmacion($scope, "Esta seguro que desea Liberar el Horario?", function () {
                $scope.horarioSel = JSON.parse(JSON.stringify(o));

                var request = crud.crearRequest('registroHorario', 1, 'liberarHorarioByCargo');
                request.setData({id: $scope.horarioSel.id, organizacionID: $rootScope.usuMaster.organizacion.organizacionID});

                crud.actualizar("/controlPersonal", request, function (data) {
                    modal.mensaje("CONFIRMACION", data.responseMsg);
                    if (data.responseSta) {
                        $scope.listarHorarios();
                        $scope.buscarHorarioByTrabajadorId();
                        $scope.buscarHorarioActualByTrabajadorId();
                        $scope.miTabla.reload();
                    }
                }, function (data) {
                    console.info(data);
                });
            });
        };
        $scope.asignarHorario = function (o) {
            //preparamos un objeto request
            $scope.horario = [];
            var request = crud.crearRequest('registroHorario', 1, 'listarHorarioById');
            request.setData({horCabId: o.id});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las funciones de exito y error
            crud.listar("/controlPersonal", request, function (data) {
                if (data.responseSta) {
                    for (var m = 0; m < data.data.length; m++)
                    {

                        $scope.diaDet = data.data[m];

                        if ($scope.diaDet.nom == "L")
                        {
                            $scope.dia.lunDesde = $scope.diaDet.lunDesde;
                            $scope.dia.lunHasta = $scope.diaDet.lunHasta;
                            $scope.dia.id = $scope.diaDet.id;
                            var i = 0;

                            if ($scope.horario.length > 0)
                            {
                                for (i = 0; i < $scope.horario.length; i++)
                                {
                                    if (!$scope.horario[i].lunDesde)
                                    {
                                        break;
                                    }

                                }
                                if (i == $scope.horario.length)
                                {
                                    $scope.horario.push($scope.dia);
                                } else
                                {
                                    $scope.horario[i].lunDesde = $scope.dia.lunDesde;
                                    $scope.horario[i].lunHasta = $scope.dia.lunHasta;
                                }

                            } else
                            {
                                $scope.horario.push($scope.dia);
                            }


                        } else if ($scope.diaDet.nom == "M")
                        {

                            $scope.dia.marDesde = $scope.diaDet.marDesde;
                            $scope.dia.marHasta = $scope.diaDet.marHasta;
                            $scope.dia.id = $scope.diaDet.id;

                            var i = 0;
                            if ($scope.horario.length > 0)
                            {
                                for (i = 0; i < $scope.horario.length; i++)
                                {
                                    if (!$scope.horario[i].marDesde)
                                    {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                                        break;
                                    }
                                }
                                if (i == $scope.horario.length)
                                {
                                    $scope.horario.push($scope.dia);
                                } else
                                {
                                    $scope.horario[i].marDesde = $scope.dia.marDesde;
                                    $scope.horario[i].marHasta = $scope.dia.marHasta;
                                }

                            } else
                            {
                                $scope.horario.push($scope.dia);
                            }
                        } else if ($scope.diaDet.nom == "W")
                        {
                            $scope.dia.mieDesde = $scope.diaDet.mieDesde;
                            $scope.dia.mieHasta = $scope.diaDet.mieHasta;
                            $scope.dia.id = $scope.diaDet.id;

                            var i = 0;
                            if ($scope.horario.length > 0)
                            {
                                for (i = 0; i < $scope.horario.length; i++)
                                {
                                    if (!$scope.horario[i].mieDesde)
                                    {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                                        break;
                                    }
                                }
                                if (i == $scope.horario.length)
                                {
                                    $scope.horario.push($scope.dia);
                                } else
                                {
                                    $scope.horario[i].mieDesde = $scope.dia.mieDesde;
                                    $scope.horario[i].mieHasta = $scope.dia.mieHasta;
                                }

                            } else
                            {
                                $scope.horario.push($scope.dia);
                            }
                        } else if ($scope.diaDet.nom == "J")
                        {
                            $scope.dia.jueDesde = $scope.diaDet.jueDesde;
                            $scope.dia.jueHasta = $scope.diaDet.jueHasta;
                            $scope.dia.id = $scope.diaDet.id;

                            var i = 0;
                            if ($scope.horario.length > 0)
                            {
                                for (i = 0; i < $scope.horario.length; i++)
                                {
                                    if (!$scope.horario[i].jueDesde)
                                    {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                                        break;
                                    }

                                }
                                if (i == $scope.horario.length)
                                {
                                    $scope.horario.push($scope.dia);
                                } else
                                {
                                    $scope.horario[i].jueDesde = $scope.dia.jueDesde;
                                    $scope.horario[i].jueHasta = $scope.dia.jueHasta;
                                }

                            } else
                            {
                                $scope.horario.push($scope.dia);
                            }
                        } else if ($scope.diaDet.nom == "V")
                        {
                            $scope.dia.vieDesde = $scope.diaDet.vieDesde;
                            $scope.dia.vieHasta = $scope.diaDet.vieHasta;
                            $scope.dia.id = $scope.diaDet.id;

                            var i = 0;
                            if ($scope.horario.length > 0)
                            {
                                for (i = 0; i < $scope.horario.length; i++)
                                {
                                    if (!$scope.horario[i].vieDesde)
                                    {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                                        break;
                                    }
                                }
                                if (i == $scope.horario.length)
                                {
                                    $scope.horario.push($scope.dia);
                                } else
                                {
                                    $scope.horario[i].vieDesde = $scope.dia.vieDesde;
                                    $scope.horario[i].vieHasta = $scope.dia.vieHasta;
                                }

                            } else
                            {
                                $scope.horario.push($scope.dia);
                            }
                        } else if ($scope.diaDet.nom == "S")
                        {
                            $scope.dia.sabDesde = $scope.diaDet.sabDesde;
                            $scope.dia.sabHasta = $scope.diaDet.sabHasta;
                            $scope.dia.id = $scope.diaDet.id;

                            var i = 0;
                            if ($scope.horario.length > 0)
                            {
                                for (i = 0; i < $scope.horario.length; i++)
                                {
                                    if (!$scope.horario[i].sabDesde)
                                    {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                                        break;
                                    }
                                }
                                if (i == $scope.horario.length)
                                {
                                    $scope.horario.push($scope.dia);
                                } else
                                {
                                    $scope.horario[i].sabDesde = $scope.dia.sabDesde;
                                    $scope.horario[i].sabHasta = $scope.dia.sabHasta;
                                }

                            } else
                            {
                                $scope.horario.push($scope.dia);
                            }
                        } else if ($scope.diaDet.nom == "D")
                        {
                            $scope.dia.domDesde = $scope.diaDet.domDesde;
                            $scope.dia.domHasta = $scope.diaDet.domHasta;
                            $scope.dia.id = $scope.diaDet.id;

                            var i = 0;
                            if ($scope.horario.length > 0)
                            {
                                for (i = 0; i < $scope.horario.length; i++)
                                {
                                    if (!$scope.horario[i].domDesde)
                                    {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                                        break;
                                    }
                                }
                                if (i == $scope.horario.length)
                                {
                                    $scope.horario.push($scope.dia);
                                } else
                                {
                                    $scope.horario[i].domDesde = $scope.dia.domDesde;
                                    $scope.horario[i].domHasta = $scope.dia.domHasta;
                                }

                            } else
                            {
                                $scope.horario.push($scope.dia);
                            }
                        }

                        $scope.dia = {
                            id: "",
                            lunDesde: "",
                            lunHasta: "",
                            marDesde: "",
                            marHasta: "",
                            mieDesde: "",
                            mieHasta: "",
                            jueDesde: "",
                            jueHasta: "",
                            vieDesde: "",
                            vieHasta: "",
                            sabDesde: "",
                            sabHasta: "",
                            domDesde: "",
                            domHasta: ""
                        };

                    }

                    $scope.showAsignarHorario(o);

                }
                // $scope.horario = data.data;

            }, function (data) {
                console.info(data);
            });
        };

        $scope.showAsignarHorario = function (o) {
            ModalService.showModal({
                templateUrl: "administrativa/control_personal/agregarHorario.html",
                controller: "asignarHorarioByCargoCtrl",
                inputs: {
                    title: "Asignar Horario",
                    horario: $scope.horario,
                    listaHorario: $scope.listaHorario,
                    cabecera: o
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                        $scope.listarHorarios();
                        $scope.buscarHorarioByTrabajadorId();
                        $scope.buscarHorarioActualByTrabajadorId();
                        $scope.horario = [];
                        $scope.miTabla.reload();
                    }
                });
            });
        };

        $scope.showBuscarTrabajador = function () {

            //Variables para manejo de la tabla
            var paramsTrabajadores = {count: 10};
            var settingTrabajadores = {counts: []};
            $scope.tablaTrabajadores = new NgTableParams(paramsTrabajadores, settingTrabajadores);

            listarTablaTrabajadores();

            function  listarTablaTrabajadores() {
                //preparamos un objeto request
                var request = crud.crearRequest('configuracionControl', 1, 'listarTrabajador');
                request.setData({organizacionID: $rootScope.usuMaster.organizacion.organizacionID});
                //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
                crud.listar("/controlPersonal", request, function (response) {
                    if (response.responseSta)
                    {
                        console.log(response.toString());
                        settingTrabajadores.dataset = response.data;
                        //asignando la posicion en el arreglo a cada objeto
                        iniciarPosiciones(settingTrabajadores.dataset);
                        $scope.tablaTrabajadores.settings(settingTrabajadores);
                        console.log($scope.tablaTrabajadores);
                    }

                }, function (data) {
                    console.info(data);
                });
            }
            ;

            ModalService.showModal({
                templateUrl: "administrativa/control_personal/buscarTrabajador.html",
                controller: "buscarTrabajadorCtrl",
                inputs: {
                    title: "Seleccionar Trabajador",
                    tabla: $scope.tablaTrabajadores
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {

                        $scope.trabajador.id = result.data.id;
                        $scope.trabajador.personaID = result.data.personaID;
                        $scope.trabajador.dni = result.data.dni;
                        $scope.trabajador.datos = result.data.datos;
                        $scope.trabajador.idCargo = result.data.idCargo;
                        $scope.buscarHorarioByTrabajadorId();
                        $scope.buscarHorarioActualByTrabajadorId();
                    }

                });
            });
            console.log($scope.trabajador);
        };

        $scope.buscarHorarioByTrabajadorId = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('registroHorario', 1, 'buscarHorarioByTrabajadorId');
            request.setData({idCargo: $scope.trabajador.idCargo});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las funciones de exito y error
            crud.listar("/controlPersonal", request, function (data) {
                if (data.responseSta) {
                    $scope.asignados = data.data;
                }

            }, function (data) {
                console.info(data);
            });
        };

        $scope.buscarHorarioActualByTrabajadorId = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('registroHorario', 1, 'buscarHorarioActualByTrabajadorId');
            request.setData({idTrabajador: $scope.trabajador.id});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las funciones de exito y error
            crud.listar("/controlPersonal", request, function (data) {
                if (data.responseSta) {
                    $scope.asignadosActual = data.data;
                }

            }, function (data) {
                console.info(data);
            });
        };

        $scope.iniciarDatos = function () {
            $scope.listarHorarios();
        };

        $scope.listarTablaTrabajadorPersonalizado = function () {

            //preparamos un objeto request
            var request = crud.crearRequest('configuracionControl', 1, 'listarTrabajador');
            request.setData({organizacionID: $rootScope.usuMaster.organizacion.organizacionID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/controlPersonal", request, function (response) {
                if (response.responseSta)
                {
                    console.log(response.toString());
                    settingTrabajadoresPersonalizado.dataset = response.data;
                    //asignando la posicion en el arreglo a cada objeto
                    iniciarPosiciones(settingTrabajadoresPersonalizado.dataset);
                    $scope.tablaTrabajadoresPersonalizado.settings(settingTrabajadoresPersonalizado);
                    console.log($scope.tablaTrabajadoresPersonalizado);
                }

            }, function (data) {
                console.info(data);
            });
        };

        $scope.setClickedRowPersonalizado = function (d) {
            $scope.trabajador = JSON.parse(JSON.stringify(d));

            $scope.buscarHorarioByTrabajadorId();
            $scope.buscarHorarioActualByTrabajadorId();
        };

        $scope.liberarHorarioPersonalizadoGrupo = function () {
            $scope.trabajadoresSeleccionados = [];

            for (var i = 0; i < $scope.tablaTrabajadoresPersonalizado.data.length; i++)
            {
                if ($scope.tablaTrabajadoresPersonalizado.data[i].sel == true)
                {
                    $scope.trabajadoresSeleccionados.push($scope.tablaTrabajadoresPersonalizado.data[i]);
                }
            }
            if ($scope.trabajadoresSeleccionados.length < 1)
            {
                modal.mensaje("CONFIRMACION", "Seleccione Trabajadores");
                return;
            }

            modal.mensajeConfirmacion($scope, "Esta seguro que desea liberar el Horario de los Trabajadores seleccionados?", function () {

                var request = crud.crearRequest('registroHorario', 1, 'liberarHorarioPersonalizadoGrupo');
                request.setData({trabajadores: $scope.trabajadoresSeleccionados});
                //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
                //y las funciones de exito y error
                crud.insertar("/controlPersonal", request, function (data) {
                    if (data.responseSta) {
                        modal.mensaje("CONFIRMACION", data.responseMsg);
                        for (var i = 0; i < $scope.tablaTrabajadoresPersonalizado.data.length; i++)
                        {
                            $scope.tablaTrabajadoresPersonalizado.data[i].sel = false;
                        }
                        $scope.tablaTrabajadoresPersonalizado.reload();
                        $scope.personalizado = {personaID: "", horarioId: ""};
                        $scope.asignados = {id: "", des: "", idCargo: ""};
                        $scope.asignadosActual = {id: "", des: "", idCargo: ""};
                        $scope.trabajador = {personaID: '', dni: "", datos: "", idCargo: "", cargo: ""};
                    }

                }, function (data) {
                    console.info(data);
                });
            });
        };

        $scope.asignarHorarioPersonalizado = function () {
            $scope.trabajadoresSeleccionados = [];
            if ($scope.personalizado.horarioId == "")
            {
                modal.mensaje("CONFIRMACION", "Seleccione Horario");
                return;
            }

            for (var i = 0; i < $scope.tablaTrabajadoresPersonalizado.data.length; i++)
            {
                if ($scope.tablaTrabajadoresPersonalizado.data[i].sel == true)
                {
                    $scope.trabajadoresSeleccionados.push($scope.tablaTrabajadoresPersonalizado.data[i]);
                }
            }
            if ($scope.trabajadoresSeleccionados.length < 1)
            {
                modal.mensaje("CONFIRMACION", "Seleccione Trabajadores");
                return;
            }

            modal.mensajeConfirmacion($scope, "Esta seguro que desea asignar el Horario?", function () {

                var request = crud.crearRequest('registroHorario', 1, 'asignarHorarioPersonalizado');
                request.setData({idHorario: $scope.personalizado.horarioId, trabajadores: $scope.trabajadoresSeleccionados});
                //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
                //y las funciones de exito y error
                crud.insertar("/controlPersonal", request, function (data) {
                    if (data.responseSta) {
                        modal.mensaje("CONFIRMACION", data.responseMsg);
                        for (var i = 0; i < $scope.tablaTrabajadoresPersonalizado.data.length; i++)
                        {
                            $scope.tablaTrabajadoresPersonalizado.data[i].sel = false;
                        }
                        $scope.tablaTrabajadoresPersonalizado.reload();
                        $scope.personalizado = {personaID: "", horarioId: ""};
                        $scope.asignados = {id: "", des: "", idCargo: ""};
                        $scope.asignadosActual = {id: "", des: "", idCargo: ""};
                        $scope.trabajador = {personaID: '', dni: "", datos: "", idCargo: "", cargo: ""};
                    }

                }, function (data) {
                    console.info(data);
                });
            });
        };
    }]);
app.controller('buscarTrabajadorCtrl', [
    '$scope', '$element', 'title', 'tabla', 'close', 'crud', 'modal', 'NgTableParams',
    function ($scope, $element, title, tabla, close, crud, modal, NgTableParams) {


        $scope.title = title;
        $scope.tablaTrabajadores = tabla;
        $scope.search = true;
        $scope.setClickedRow = function (d) {

            //  Manually hide the modal.
            $element.modal('hide');

            //  Now call close, returning control to the caller.
            close({
                data: d,
                flag: true
            }, 500); // close, but give 500ms for bootstrap to animate

            console.log(d);
        };

        $scope.close = function () {
            close({
                flag: false
            }, 500); // close, but give 500ms for bootstrap to animate
        };

        //  This cancel function must use the bootstrap, 'modal' function because
        //  the doesn't have the 'data-dismiss' attribute.
        $scope.cancel = function () {

            //  Manually hide the modal.
            $element.modal('hide');

            //  Now call close, returning control to the caller.
            close({
                flag: false
            }, 500); // close, but give 500ms for bootstrap to animate
        };



    }]);

app.controller('asignarHorarioByCargoCtrl', [
    '$rootScope', '$scope', '$element', 'title', 'horario', 'listaHorario', 'cabecera', 'close', 'crud', 'modal', 'ModalService', 'NgTableParams',
    function ($rootScope, $scope, $element, title, horario, listaHorario, cabecera, close, crud, modal, ModalService, NgTableParams) {

        $scope.title = title;
        $scope.cabecera = cabecera;
        $scope.listaHorario = listaHorario;
        $scope.hora = {tipo: ""};
        $scope.horarioGeneral = {
            id: $scope.cabecera.id,
            idCargo: ""

        };        
                
        $scope.observacion = {obs: ""};
        $scope.trabajador = {personaID: '', dni: "", datos: "", cargo: ""};
        $scope.tipoTrabajadores = [{id: "Di", title: "Directivo"}, {id: "Ad", title: "Administrativo"}, {id: "Do", title: "Docente"}];
        $scope.cargoTrabajadores = [{id: "", nombre: ""}];
        $scope.horario = horario;
        //verificarCargoSeleccionado
        $scope.verificarCargoSeleccionado = function () {
            $scope.observacion.obs = buscarContenido($scope.listaHorario, "idCargo", "des", $scope.horarioGeneral.idCargo);

        };
        $scope.asignarHorarioByCargo = function () {

            if ($scope.horarioGeneral.idCargo == "" || $scope.horarioGeneral.idCargo == null)
            {
                modal.mensaje("CONFIRMACION", "Seleccione Cargo");
                return;
            }
            if ($scope.observacion.obs)
            {
                modal.mensaje("CONFIRMACION", "El Cargo Seleccionado ya esta Asignado");
                return;
            }


            var request = crud.crearRequest('registroHorario', 1, 'asignarHorarioByCargo');
            request.setData({idHorario: $scope.horarioGeneral.id, idCargo: $scope.horarioGeneral.idCargo, organizacionID: $rootScope.usuMaster.organizacion.organizacionID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las funciones de exito y error
            crud.insertar("/controlPersonal", request, function (data) {
                if (data.responseSta) {
                    $scope.horario = [];
                    $scope.horarioGeneral = {
                        id: "",
                        idCargo: ""
                    };
                    var a = 2;
                    var b;
                    modal.mensaje("CONFIRMACION", data.responseMsg);
                    $element.modal('hide');
                    close({
                        flag: true
                    }, 500); // close, but give 500ms for bootstrap to animate
                }

            }, function (data) {
                console.info(data);
            });

        };

        $scope.listarCargoTrabajador = function () {
            //preparamos un objeto request
            if ($scope.hora.tipo == null)
            {
                $scope.cargoTrabajadores = [{id: "", nombre: ""}];
                return;
            } else
            {
                var request = crud.crearRequest('registroTrabajador', 1, 'listarCargoTrabajador');
                request.setData({tipoTrab: $scope.hora.tipo.id});
                //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
                //y las funciones de exito y error
                crud.listar("/controlPersonal", request, function (data) {
                    $scope.cargoTrabajadores = data.data;

                }, function (data) {
                    console.info(data);
                });
            }

        };


        $scope.close = function () {
            close({
                flag: false
            }, 500); // close, but give 500ms for bootstrap to animate
        };
        $scope.cancel = function () {

            //  Manually hide the modal.
            $element.modal('hide');

            //  Now call close, returning control to the caller.
            close({
                flag: false
            }, 500); // close, but give 500ms for bootstrap to animate
        };
    }]);
