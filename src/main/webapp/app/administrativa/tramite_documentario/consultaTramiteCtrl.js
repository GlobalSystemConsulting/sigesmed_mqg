app.controller("consultaTramiteCtrl",["$scope","NgTableParams","crud", function ($scope,NgTableParams,crud){
    
    var params = {count: 10};
    var setting = { counts: []};
    $scope.miTabla = new NgTableParams(params, setting);
    
    $scope.tupas = [{id:true,title:"tupa"},{id:false,title:"no tupa"}];
    $scope.tipos = [{id:true,title:"externo"},{id:false,title:"interno"}];
    
    //variable temporal, para la seleccion de un tramite
    $scope.tipoTramiteSel = {};
    
    //lista de requisitos del tramite
    $scope.requisitos = [];        
    //lista de rutas del tramite
    $scope.rutas = [];    
    
    $scope.listarTiposTramites = function(){
        //preparamos un objeto request
        var request = crud.crearRequest('tipoTramite',1,'listarTipoTramite');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/tramiteDocumentario",request,function(data){
            setting.dataset = data.data;
            iniciarPosiciones(setting.dataset);
            $scope.miTabla.settings(setting);
        },function(data){
            console.info(data);
        });
    };
    $scope.prepararEditar = function(t){
        
        $scope.rutaSel = {areaOriID:{},areaDes:{},descripcion:""};
        $scope.requisitoSel = {descripcion:"",nombreArchivo:"",archivo:{},edi:false};
        $scope.tipoTramiteSel = JSON.parse(JSON.stringify(t));
        
        var request = crud.crearRequest('tipoTramite',1,'listarRequisito');
        request.setData({tipoTramiteID:$scope.tipoTramiteSel.tipoTramiteID,tupa:$scope.tipoTramiteSel.tupa});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las usuarios de exito y error
        crud.listar("/tramiteDocumentario",request,function(data){
            $scope.tipoTramiteSel.requisitos = data.data.requisitos;
            $scope.tipoTramiteSel.rutas = data.data.rutas;
            $('#modalEditarTramite').modal('show');
        },function(data){
            console.info(data);
        });
    };
    
    $scope.imprimirTramite = function(){
        
        var request = crud.crearRequest('tipoTramite',1,'exportarTramite');
        request.setData($scope.tipoTramiteSel);
        crud.insertar("/tramiteDocumentario",request,function(response){
            if(response.responseSta){                
                verDocumentoPestana( response.data.tramite );
            } 
        },function(data){
            console.info(data);
        });
    };
    
    function listarTipoOrganizaciones (){
        //preparamos un objeto request
        var request = crud.crearRequest('tipoOrganizacion',1,'listarTipoOrganizaciones');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las organizaciones de exito y error
        crud.listar("/configuracionInicial",request,function(data){
            $scope.tipoOrganizaciones  = data.data;            
        },function(data){
            console.info(data);
        });
    };
    function listarOrganizaciones (){
        //preparamos un objeto request
        var request = crud.crearRequest('organizacion',1,'listarOrganizaciones');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las organizaciones de exito y error
        crud.listar("/configuracionInicial",request,function(data){
            $scope.organizaciones  = data.data;            
        },function(data){
            console.info(data);
        });
    };
    $scope.iniciarDatos = function(){
        $scope.listarTiposTramites();
    };
}]);
