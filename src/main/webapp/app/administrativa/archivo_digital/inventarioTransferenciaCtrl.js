app.requires.push('angularModalService');
app.controller("inventarioTransferenciaCtrl", ["$scope", "NgTableParams", "crud", "modal","ModalService", function ($scope, NgTableParams, crud, modal,ModalService) {

        $scope.inventarios_transferencia = [];
        $scope.prestamos_serie = [];
        $scope.areas = [];
        $scope.unidades_organicas = [];
        $scope.series_documentales = [];
        $scope.archivos = [];
        $scope.inventario_transferencia = {fec:new Date("dd/mm/aaaa"),fec_prestamo:new Date("dd/mm/aaaa"),fec_dev:new Date("dd/mm/aaaa")};
       // $scope.inventario_transferencia.fec = new Date();
        $scope.persona = {dni:"",existe:true};

        //Datos de la tabla Iventario de Series Documentales
        var params = {count: 20};
        var setting = {counts: []};
        $scope.tabla_inventarios = new NgTableParams(params, setting);

        var params2 = {count: 20};
        var setting2 = {counts: []};
        $scope.tabla_archivos = new NgTableParams(params2, setting2);

        var editar_archivo = false;

        //INICIALIZAMOS NUESTRO INVENTARIO DETALLE DE TRANSFERENCIA ACTUAL
        /*
         $scope.inventario_transferencia.asu = 0;
         $scope.inventario_transferencia.met_lin=0;
         $scope.inventario_transferencia.res_con="";
         $scope.inventario_transferencia.loc=0;
         $scope.inventario_transferencia.alm=0;
         $scope.inventario_transferencia.est=0;
         $scope.inventario_transferencia.bal=0;
         $scope.inventario_transferencia.fil=0;
         $scope.inventario_transferencia.cue=0;
         $scope.inventario_transferencia.caj=0;
         */


        $scope.listarInventarios = function () {

            var request = crud.crearRequest('inventario_transferencia', 1, 'listar_inventario_transferencia');
            request.setData({serie_id: 1});
            crud.listar("/archivoDigital", request, function (data) {
                $scope.inventarios_transferencia = data.data;
                 setting.dataset = data.data;
                 console.log(data);
                $scope.tabla_inventarios.settings(setting);

                //Listamos los Prestamos de serie documental
                var request2 = crud.crearRequest('inventario_transferencia', 1, 'listar_prestamos_serie');

                crud.listar("/archivoDigital", request2, function (data) {

                    $scope.prestamos_serie = data.data;
                    //  $scope.CurrentDate = new Date();
                    //Verificamos si por cada inventario existe un prestamo
                    for (var i = 0; i < $scope.inventarios_transferencia.length; i++) {
                        for (var j = 0; j < $scope.prestamos_serie.length; j++) {
                            if ($scope.inventarios_transferencia[i].inv_tra_id === $scope.prestamos_serie[j].inv_tra_id) {
                                $scope.inventarios_transferencia[i].pre_serie_id = $scope.prestamos_serie[j].pre_serie_id;
                                $scope.inventarios_transferencia[i].prestamo = true;
                                $scope.inventarios_transferencia[i].fec_dev = $scope.prestamos_serie[j].fec_dev;
                                $scope.inventarios_transferencia[i].fec_pre = $scope.prestamos_serie[j].fec_pre;
                                $scope.inventarios_transferencia[i].etiqueta = "Inventario Prestado!";
                                /*
                                 if($scope.prestamos_serie[j].fec_dev > $scope.CurrentDate){
                                 $scope.inventarios_transferencia[i].fecha = 'A';
                                 }
                                 else{
                                 $scope.inventarios_transferencia[i].fecha = 'I';
                                 }
                                 */

                            }
                        }
                    }

                }, function (data) {
                    console.info(data);
                });


            }, function (data) {
                console.info(data);
            });


        };

      /*
        $scope.listarAreas = function () {

            var request = crud.crearRequest('serie_documental', 1, 'listarArea');
            crud.listar("/archivoDigital", request, function (data) {
                $scope.areas = data.data;
            }, function (data) {
                console.info(data);
            });

        };*/
        $scope.buscarPersona = function(){
            //preparamos un objeto request
            var request = crud.crearRequest('usuarioSistema',1,'buscarPersona');

        //        if(!$scope.tipoPersona)
        //            request.setData({dni:0,usuarioID:$scope.persona.dni});
        //        else
                    request.setData({dni:$scope.persona.dni});
                //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
                //y las usuarios de exito y error
                crud.listar("/configuracionInicial",request,function(data){
                    modal.mensaje("CONFIRMACION",data.responseMsg);
                    if(data.responseSta){
                        $scope.persona = data.data.persona;
                          $scope.inventario_transferencia.dni=$scope.persona.dni;
                          $scope.inventario_transferencia.nom = $scope.persona.nombre +" "+ $scope.persona.paterno +" "+ $scope.persona.materno;
                          
                      }
                    else
                        $scope.persona.existe = false;
                },function(data){
                    console.info(data);
                });
        };
        $scope.listarAreas = function(organizacionID){
            //preparamos un objeto request
            var request = crud.crearRequest('area',1,'listarAreasPorOrganizacion');
            request.setData({organizacionID:organizacionID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las areas de exito y error
            crud.listar("/configuracionInicial",request,function(data){
                $scope.areas = data.data;
              //  iniciarPosiciones(settingArea.dataset);            

              //  $scope.areasSelect.splice(0);
              //  $scope.areasSelect.push({id:'',title:""});
                //console.log("Areas: %o :", data);
                data.data.forEach(function(item){
               //     var o = item;
                //    o.id = item.nombre;
                //    o.title = item.nombre;
                //    $scope.areasSelect.push(o);
                });
             //   $scope.tablaArea.settings(settingArea);
            },function(data){
                console.info(data);
            });
        };  
        
        $scope.elegirUnidadOrganica = function (AreaID) {

            var tipoArea = null;
            for (var i = 0; i < $scope.areas.length; i++)
                if ($scope.areas[i].areaID == AreaID) {
                    tipoArea = $scope.areas[i];
                    break;
                }
            if (tipoArea) {
                var request = crud.crearRequest('gestion_area_archivo_central', 1, 'listar_unidad_organica');
                request.setData({codigo_area: tipoArea.areaID});
                crud.listar("/archivoDigital", request, function (data) {
                    $scope.unidades_organicas = data.data;
                }, function (data) {
                    console.info(data);
                });

            } else {
                $scope.requisitos = [];
                $scope.rutas = [];
            }

        };
        $scope.listarSeries = function () {
            var request = crud.crearRequest('gestion_series_documentales', 1, 'listar_series_documentales');
            var uniID = $scope.inventario_transferencia.unID;
            request.setData({uni_org_id: uniID});
            crud.listar("/archivoDigital", request, function (data) {
                // setting.dataset = data.data;
                $scope.series_documentales = data.data;
            }, function (data) {
                console.info(data);
            });

        };
        $scope.elegir_codigo_serie = function (SerieID){

            for (var i = 0; i < $scope.series_documentales.length; i++)
                if ($scope.series_documentales[i].serie_id == SerieID) {
                    $scope.inventario_transferencia.cod_serie = $scope.series_documentales[i].cod_serie;
                    break;
                }
        };


        $scope.registrar_inventario = function () {
            
           if($scope.inventario_transferencia.areaID === undefined ){
               
                    modal.mensaje("ERROR","Seleccione un area");
                    return;           
            }
            if($scope.inventario_transferencia.unID === undefined ){
               
                    modal.mensaje("ERROR","Seleccione una Unidad Organica");
                    return;           
            }
             if($scope.inventario_transferencia.serie_id === undefined){
               
                    modal.mensaje("ERROR","Seleccione Titulo de la serie");
                    return;           
            }
            if($scope.inventario_transferencia.alm === undefined || $scope.inventario_transferencia.est === undefined || $scope.inventario_transferencia.bal === undefined || $scope.inventario_transferencia.fil === undefined || $scope.inventario_transferencia.cue === undefined || $scope.inventario_transferencia.caj === undefined){
               
                    modal.mensaje("ERROR","Falto Ingresar Almacen o Estante o Balda o Fila o Cuerpo o Caja . VERIFICAR");
                    return;           
            }
            
            console.log("fecha antes ",$scope.inventario_transferencia.fec);
            $scope.inventario_transferencia.fec = convertirFecha($scope.inventario_transferencia.fec);
            
            console.log("fecha despues ",$scope.inventario_transferencia.fec);
            var request = crud.crearRequest('inventario_transferencia', 1, 'registrar_inventario_transferencia');
            request.setData($scope.inventario_transferencia);
            crud.insertar("/archivoDigital", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {

                    $scope.buscar_serie($scope.inventario_transferencia);
                    $scope.buscar_unidad($scope.inventario_transferencia);
                    $scope.buscar_area($scope.inventario_transferencia);
                    $scope.inventario_transferencia.inv_tra_id = response.data.det_inv_trans;
                    
                    
                    
                    insertarElemento(setting.dataset, $scope.inventario_transferencia);
                    $('#modalNuevoInventario').modal('hide');
                    //Ademas insertarmos a nuestro arreglo de inventarios de transferencia              
                                    
                  //  $scope.inventarios_transferencia.push($scope.inventario_transferencia);
                  //  insertarElemento($scope.inventarios_transferencia.dataset, $scope.inventario_transferencia);

                    $scope.tabla_inventarios.reload();
                    $scope.inventario_transferencia = {};
                    $scope.inventario_transferencia = {fec:new Date("dd/mm/aaaa"),fec_prestamo:new Date("dd/mm/aaaa"),fec_dev:new Date("dd/mm/aaaa")};
                }

            }, function (data) {
                console.info(data);
            });

        };
        $scope.buscar_area = function (area) {
            for (var i = 0; i < $scope.areas.length; i++) {
                if ($scope.areas[i].areaID == area.areaID) {
                    $scope.inventario_transferencia.area = $scope.areas[i].nombre;
                    break;
                }
            }
        };

        $scope.buscar_serie = function (serie) {
            for (var i = 0; i < $scope.series_documentales.length; i++) {
                if ($scope.series_documentales[i].serie_id == serie.serie_id) {
                    $scope.inventario_transferencia.serie = $scope.series_documentales[i].nom_serie;
                    break;
                }
            }
        };

        $scope.buscar_unidad = function (unidad) {

            for (var i = 0; i < $scope.unidades_organicas.length; i++) {
                if ($scope.unidades_organicas[i].uni_org_id == unidad.unID) {
                    $scope.inventario_transferencia.un_org = $scope.unidades_organicas[i].nombre;
                    break;
                }
            }
        };

        $scope.PrepararPrestamo = function (Inventario) {

            $scope.inventario_transferencia.inv_tra_id = Inventario.inv_tra_id;
            $scope.inventario_transferencia.un_org = Inventario.un_org;
            $scope.inventario_transferencia.serie = Inventario.serie;
            $scope.inventario_transferencia.ser_doc_id = Inventario.ser_doc_id;

            $('#modalNuevoPrestamo').modal('show');
        };

        $scope.registrar_prestamo = function () {

            $scope.inventario_transferencia.fec_prestamo = convertirFecha($scope.inventario_transferencia.fec_prestamo);
            $scope.inventario_transferencia.fec_dev = convertirFecha($scope.inventario_transferencia.fec_dev);

            var request = crud.crearRequest('inventario_transferencia', 1, 'registrar_prestamo_inventario');
            request.setData($scope.inventario_transferencia);
            crud.insertar("/archivoDigital", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {


                    for (var i = 0; i < $scope.inventarios_transferencia.length; i++) {
                        if ($scope.inventarios_transferencia[i].inv_tra_id == $scope.inventario_transferencia.inv_tra_id) {
                            $scope.inventarios_transferencia[i].prestamo = true;
                            $scope.inventarios_transferencia[i].fec_dev = $scope.inventario_transferencia.fec_dev;
                            $scope.inventarios_transferencia[i].fec_pre = $scope.inventario_transferencia.fec_pre;
                            $scope.inventarios_transferencia[i].etiqueta = "Inventario Prestado!";
                            break;
                        }
                    }
                    $scope.persona= {};
                    $scope.tabla_inventarios.reload();
                    $('#modalNuevoPrestamo').modal('hide');
                }

            }, function (data) {
                console.info(data);
            });
        };
        $scope.devolverSerie = function(preSerieId , invTraId){
            var invId = invTraId;
            var presSerId = preSerieId;
            var request = crud.crearRequest('inventario_transferencia', 1, 'eliminar_presatamo_inventario');
            request.setData({inv_tra_id:invId , pre_serie_id :presSerId });
            crud.eliminar("/archivoDigital", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {

                    $scope.listarInventarios();
                    $scope.tabla_inventarios.reload();
                    $('#modalNuevoPrestamo').modal('hide');
                }

            }, function (data) {
                console.info(data);
            });
        };
        $scope.Registrar_Archivo = function () {


            $scope.inventario_transferencia.fecha = convertirFecha($scope.inventario_transferencia.fecha);

            if (editar_archivo == false) {
                var request = crud.crearRequest('inventario_transferencia', 1, 'registrar_archivo');
                request.setData($scope.inventario_transferencia);
                crud.insertar("/archivoDigital", request, function (response) {
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                    if (response.responseSta) {
                        $('#modalNuevoArchivo').modal('hide');

                        $scope.nuevo_archivo = {};
                        $scope.nuevo_archivo.arc_inv_tra_id = response.data.arc_inv_tra_id;
                        $scope.nuevo_archivo.det_inv_trans = response.data.det_inv_trans;
                        $scope.nuevo_archivo.num_fol = response.data.num_fol;
                        $scope.nuevo_archivo.titulo = response.data.titulo;
                        $scope.nuevo_archivo.fecha = response.data.fecha;
                        $scope.nuevo_archivo.cod_expediente = response.data.cod_expediente;
                        $scope.nuevo_archivo.nombre_archivo = response.data.nombre_archivo;

                        insertarElemento(setting2.dataset, $scope.nuevo_archivo);
                        $scope.tabla_archivos.reload();
                    } else {
                        var request = crud.crearRequest('inventario_transferencia', 1, 'editar_archivo');
                        request.setData($scope.inventario_transferencia);
                        crud.actualizar("/archivoDigital", request, function (response) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                            if (response.responseSta) {
                                $('#modalNuevoArchivo').modal('hide');
                            }
                        });

                    }
                }, function (data) {
                console.info(data);
            });
            } else {

                var request = crud.crearRequest('inventario_transferencia', 1, 'editar_archivo');
                request.setData($scope.inventario_transferencia);
                crud.actualizar("/archivoDigital", request, function (response) {
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                    if (response.responseSta) {
                        editar_archivo = false;
                    }
                });

            }


        };


        $scope.Listar_Archivos = function (Inventario) {
            $scope.inventario_transferencia = Inventario;
            var request = crud.crearRequest('inventario_transferencia', 1, 'listar_archivos');
            request.setData(Inventario);
            crud.listar("/archivoDigital", request, function (data) {
                setting2.dataset = data.data;
                $scope.archivos = data.data;
            }, function (data) {
                console.info(data);
            });
            $('#modalListarArchivo').modal('show');

        };

        $scope.nuevo_archivo = function () {
            $scope.inventario_transferencia.titulo = "";
            $scope.inventario_transferencia.cod_expediente = "";
            $scope.inventario_transferencia.num_folio = "";

            $('#modalNuevoArchivo').modal('show');
        };

        $scope.EliminarInventario = function (o,Inventario) {
           //  console.log("splice o :",o);
            modal.mensajeConfirmacion($scope, "Seguro que desea eliminar este Inventario ?", function () {
                var request = crud.crearRequest('inventario_transferencia', 1, 'eliminar_inventario_transferencia');
                request.setData(Inventario);

                crud.eliminar("/archivoDigital", request,
                        function (response) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                            if (response.responseSta) {
                                // eliminarElemento(setting.dataset,i);
                                // $scope.tabla_series.reload();
                                  eliminarElemento(setting.dataset,o);
                                $scope.tabla_inventarios.reload();
                               // $scope.tabla_inventarios.splice(o,1);
                            }

                        }, function (data) {
                    console.info(data);
                });

            });

        };
        $scope.EditarInventario = function (inventario) {
            console.log("posibel edicion  ", inventario);
            /*Buscamos el Inventario de Transferencia*/
            for (var i = 0; i < $scope.inventarios_transferencia.length; i++) {
                if ($scope.inventarios_transferencia[i].inv_tra_id === inventario.inv_tra_id) {
                    $scope.inventario_transferencia = $scope.inventarios_transferencia[i];
                    $scope.date1 = new Date($scope.inventario_transferencia.fec);
                    $scope.date1.setDate(  $scope.date1.getDate() + 1);
                    $scope.inventario_transferencia.fec=$scope.date1;
                    break;
                }
            }
           /* console.log("fecha " , $scope.inventario_transferencia.fec);
            $scope.date1 = new Date($scope.inventario_transferencia.fec);
              $scope.date1.setDate(  $scope.date1.getDate() + 1);
            console.log("Fehca vocnvert  " ,$scope.date1);*/
            $('#modalEditarInventario').modal('show');
        };


        $scope.actualizar_inventario = function () {

            $scope.inventario_transferencia.fec = convertirFecha($scope.inventario_transferencia.fec);
            var request = crud.crearRequest('inventario_transferencia', 1, 'editar_inventario_transferencia');
            request.setData($scope.inventario_transferencia);
            crud.actualizar("/archivoDigital", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    $scope.inventario_transferencia = [];
                    $('#modalEditarInventario').modal('hide');
                }
            }, function (data) {
                console.info(data);
            });

        };

        $scope.limpiar_inventario = function () {


            $('#modalNuevoInventario').modal('show');
        };


        $scope.cancelar_archivo = function () {

            $('#modalNuevoArchivo').modal('hide');

        };
        $scope.cancelar_inventario = function () {

            $scope.inventario_transferencia = null;
            $('#modalNuevoInventario').modal('hide');
        };

        $scope.EditarArchivo = function (archivo) {

            for (var i = 0; i < $scope.archivos.length; i++) {
                if (archivo.arc_inv_tra_id == $scope.archivos[i].arc_inv_tra_id) {

                    $scope.inventario_transferencia.titulo = archivo.titulo;
                    $scope.inventario_transferencia.cod_expediente = archivo.cod_expediente;
                    $scope.inventario_transferencia.fecha = archivo.fecha;
                    $scope.inventario_transferencia.num_folio = archivo.num_fol;
                    $scope.inventario_transferencia.archivo = archivo.archivo;
                    editar_archivo = true;
                    $('#modalNuevoArchivo').modal('show');
                    break;
                }
            }
        };

        $scope.EliminarArchivo = function (archivo) {

            var pos;
            for (var i = 0; i < $scope.archivos.length; i++) {
                if ($scope.archivos[i].arc_inv_tra_id == archivo.arc_inv_tra_id) {
                    pos = i;
                    break;
                }
            }

            modal.mensajeConfirmacion($scope, "Seguro que desea eliminar este Archivo ?", function () {
                var request = crud.crearRequest('inventario_transferencia', 1, 'eliminar_archivo');
                request.setData(archivo);

                crud.eliminar("/archivoDigital", request,
                        function (response) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                            if (response.responseSta) {
                                eliminarElemento(setting2.dataset, pos);
                                $scope.tabla_archivos.reload();
                            }

                        }, function (data) {
                        console.info(data);
                    });

            });
        };
        
        $scope.showRegistro = function () {

            ModalService.showModal({
                templateUrl: "administrativa/archivo_digital/registroDni.html",
                controller: "registrarDniCtrl",
                inputs: {
                    title: "Seleccionar Operación",
                    tablaOperacion: $scope.tablaOperaciones,
                    persona : $scope.persona
                    
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {

                        
                        //$scope.asiento.fecha=new Date($scope.asiento.fecha);             
                        //insertamos el elemento a la lista
                                         

                        
                    }


                });
            });

        };
        
         $scope.open3 = function () {
            $scope.popup3.opened = true;
        };

        $scope.open4 = function () {
            $scope.popup4.opened = true;
        };

        $scope.popup3 = {
            opened: false
        };

        $scope.popup4 = {
            opened: false
        };
        $scope.dateOptions = {
            minDate: new Date(),
            maxDate: new Date() 
         };
         var fecha = new Date();
         var addTime = 86400 * 30;
         fecha.setSeconds(addTime);
         
         $scope.dateOptions2 = {
             
            minDate: new Date(),
            maxDate: fecha
         };
         
         
    }]);

app.controller('registrarDniCtrl', [
    '$scope', "$rootScope", '$element', 'title',  'close', 'crud', 'modal', "ModalService",'persona',
    function ($scope, $rootScope, $element, title,  close, crud, modal, ModalService,persona) {
        //variable de la fecha actual para el calendaario
        var fecha = new Date();
        $scope.persona=persona;

        //variable
       

       $scope.registrarPersona = function(){
        
        var request = crud.crearRequest('usuarioSistema',1,'insertarPersona');
        
//        if(!$scope.tipoPersona){
//            $scope.persona.usuarioID = $scope.persona.dni;
//            $scope.persona.dni = 0;
//        }
        
        request.setData($scope.persona);
        
        crud.insertar("/configuracionInicial",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                $scope.persona.personaID = response.data.personaID;
                $scope.persona.existe = true;
                $scope.persona={};
                $('#modalNuevo').modal('hide');
               
            }            
        },function(data){
            console.info(data);
        });
        
    };

       
    
      

        //  This close function doesn't need to use jQuery or bootstrap, because
        //  the button has the 'data-dismiss' attribute.
        $scope.close = function () {
            //  Manually hide the modal.
            $element.modal('hide');

            close({
                cuenta: $scope.cuentaContable,
                flag: false
            }, 500); // close, but give 500ms for bootstrap to animate
        };

        //  This cancel function must use the bootstrap, 'modal' function because
        //  the doesn't have the 'data-dismiss' attribute.
        $scope.cancel = function () {

            //  Manually hide the modal.
            $element.modal('hide');

            //  Now call close, returning control to the caller.
            close({
                cuenta: $scope.cuentaContable,
                flag: false
            }, 500); // close, but give 500ms for bootstrap to animate
        };


        $scope.open2 = function () {
            $scope.popup2.opened = true;
        };

        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };

        $scope.popup2 = {
            opened: false
        };

        $scope.popup1 = {
            opened: false
        };

        $scope.getFecha = function (fecha, dato) {
            var f = dato;
            if (f) {
                fecha.d = f.getDate();
                fecha.m = f.getMonth();
                fecha.y = f.getFullYear();
            }

        };


    }]);

