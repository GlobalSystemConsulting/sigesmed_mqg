 app.controller("reporteSeriesCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){
      
      $scope.areas = [];
      $scope.area = {};

      $scope.verReporte = function(){
          
         var tipoArea = null;
         
         for(var i=0;i<$scope.areas.length;i++ )
             if($scope.areas[i].areaID == $scope.area){
                 tipoArea = $scope.areas[i];
                 break;
         }
          
          console.log("valor area :", $scope.area);
            var request = crud.crearRequest('gestion_series_documentales',1,'reporte_series');
            request.setData({area_id:tipoArea.areaID,area_nombre:tipoArea.nombre});        
            crud.listar("/archivoDigital",request,function(data){            
                 $scope.dataBase64 = data.data[0].datareporte;
                    window.open($scope.dataBase64);
                    },function(data){
                    console.info(data);
            });   
      }  
      
      /*
    $scope.listarAreas = function(){
       
       var request = crud.crearRequest('serie_documental',1,'listarArea');
       crud.listar("/archivoDigital",request,function(data){
       $scope.areas = data.data;
       },function(data){
           console.info(data);
       });    
    };*/
     $scope.listarAreas = function(organizacionID){
        //preparamos un objeto request
        var request = crud.crearRequest('area',1,'listarAreasPorOrganizacion');
        request.setData({organizacionID:organizacionID});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las areas de exito y error
        crud.listar("/configuracionInicial",request,function(data){
            $scope.areas = data.data;
          //  iniciarPosiciones(settingArea.dataset);            
            
          //  $scope.areasSelect.splice(0);
          //  $scope.areasSelect.push({id:'',title:""});
            //console.log("Areas: %o :", data);
            data.data.forEach(function(item){
           //     var o = item;
            //    o.id = item.nombre;
            //    o.title = item.nombre;
            //    $scope.areasSelect.push(o);
            });
         //   $scope.tablaArea.settings(settingArea);
        },function(data){
            console.info(data);
        });
    };  
    
 }]);