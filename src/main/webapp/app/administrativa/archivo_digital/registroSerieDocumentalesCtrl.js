app.controller("registroSerieDocumentalesCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){
      
    $scope.areas = [];
    $scope.tipoAreas = [];
    $scope.nuevaArea = {codigo:"",nombre:"",estado:'A',organizacionID:"0",areaPadreID:"0"};

     //tabla de areas 
    var paramsArea = {count: 10};
    var settingArea = { counts: []};
    $scope.tablaArea = new NgTableParams(paramsArea, settingArea);
    $scope.areasSelect = [];
    $scope.areaSel = {};

    $scope.bandera =false ;
    $scope.unidadesOrg =[];
    $scope.unidadOrg = {nombre:"", abreviacion:"", descripcion:"",estado:''};
    $scope.unidadOrgSel = {nombre:"", abreviacion:"", descripcion:"",estado:''};



    $scope.unidades_organicas = [];
    $scope.series_documentales = [];
    $scope.serie_documental = {};
    $scope.area={}; // obtiene datos del area a registrar
    $scope.area_id={};
    $scope.serie_select={};

    $scope.codigo_area={};

    //Datos de la tabla Series Documentales
    var params = {count: 20};
    var setting = { counts: []};
    $scope.tabla_series = new NgTableParams(params, setting);

    //Posicion de la Serie a Editar se usa en : editar_serie();
    $scope.pos_serie = {};

    $scope.show_modal = false;






     listarTipoAreas ();
    //Registramos un Area


   function listarTipoAreas (){
        //preparamos un objeto request
        var request = crud.crearRequest('area',1,'listarTipoAreas');
        crud.listar("/configuracionInicial",request,function(data){
            $scope.tipoAreas  = data.data;
        },function(data){
            console.info(data);
        });
    };
    

    //Listamos las Areas
/*
    $scope.listarUnidadesOrganicas = function(){

        $scope.series_documentales = null;
        var request = crud.crearRequest('gestion_area_archivo_central',1,'listar_unidad_organica');
      //  request.setData({tipoTramiteID:tipoTramite.tipoTramiteID,tupa:tipoTramite.tupa});
     //   request.setData({codigo_area:serie_documental.areaID});
        crud.listar("/archivoDigital",request,function(data){
             $scope.unidades_organicas = data.data;
         },function(data){
             console.info(data);
         });
    }*/

    $scope.elegirUnidadOrganica = function(AreaID){
         $scope.series_documentales = [];
        var tipoArea = null;
         for(var i=0;i<$scope.areas.length;i++ )
             if($scope.areas[i].areaID == AreaID){
                 tipoArea = $scope.areas[i];
                 break;
             }
         
         if(tipoArea){
             var request = crud.crearRequest('gestion_area_archivo_central',1,'listar_unidad_organica');
             request.setData({codigo_area:tipoArea.areaID});
             crud.listar("/archivoDigital",request,function(data){
                  $scope.unidades_organicas = data.data;
                 },function(data){
                     console.info(data);
                 });

         }
         else{
             $scope.requisitos = [];
             $scope.rutas = [];
         }

    };
    $scope.elegirSerieDocumental = function(UniID){

        var tipoUniOrg = null;
        for(var i = 0 ;i<$scope.unidades_organicas.length;i++)
            if($scope.unidades_organicas[i].uni_org_id == UniID){
                tipoUniOrg = $scope.unidades_organicas[i];
                break;
            }
        if(tipoUniOrg){
            $scope.serie_documental.uni_org= tipoUniOrg.nombre;
            $scope.serie_documental.uni_org_id = UniID;
             var request = crud.crearRequest('gestion_series_documentales',1,'listar_series_documentales');
             request.setData({uni_org_id:tipoUniOrg.uni_org_id});
             crud.listar("/archivoDigital",request,function(data){
                 setting.dataset = data.data;
                 $scope.series_documentales = data.data;
                 },function(data){
                     console.info(data);
                 });
        }
       $scope.bandera = true ;

    };

    $scope.obtener_serie_actual = function(areID){
      //  alert(areID);

        $scope.serie_documental.cod_serie = "";
        $scope.serie_documental.nom_serie = "";
        $scope.serie_documental.val_serie = "";

        var tipoSerie = null;
        for(var i=0 ;i<$scope.areas.length;i++){
            if($scope.areas[i].areaID == areID){
             //   console.log($scope.areas[i]);
                tipoSerie = $scope.areas[i];
              //  $scope.serie_documental.abr = tipoSerie.abreviacion;
                $scope.serie_documental.nombre_area = tipoSerie.nombre;
                $scope.serie_documental.uni_org = $scope.serie_documental.uni_org;
                break;
            }
        }

    };
    function registrar_retencion (){
         var request = crud.crearRequest('gestion_series_documentales',1,'registrar_tabla_retencion');
            console.log($scope.serie_documental);
             request.setData($scope.serie_documental);
             crud.insertar("/archivoDigital",request,function(response){
                    modal.mensaje("CONFIRMACION",response.responseMsg);
                    if(response.responseSta){

                       $('#modalTablaRetencion').modal('hide');

                    }
                   },function(response){
                       console.info(response);
              });

    };


    $scope.registrar_serie = function(){


        var request = crud.crearRequest('gestion_series_documentales',1,'registrar_serie_documental');
        request.setData($scope.serie_documental);

        crud.insertar("/archivoDigital",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){

            //     $scope.serie_documental.serie_id = response.data.serie_id;
            //     $scope.serie_documental.cod_serie = response.data.ser_doc_cod;
            //     $scope.serie_documental.nom_serie = response.data.nom;
                    $scope.nueva_serie={}; 

                   $scope.nueva_serie.serie_id = response.data.serie_id;
                   $scope.nueva_serie.cod_serie = response.data.ser_doc_cod;
                   $scope.nueva_serie.nom_serie = response.data.nom;
                   $scope.nueva_serie.val_serie = $scope.serie_documental.val_serie;
                 //insertamos el elemento a la lista
                   $scope.serie_documental.serie_id = $scope.nueva_serie.serie_id;
                   $scope.serie_documental.cod_serie =  $scope.nueva_serie.cod_serie;
                   $scope.serie_documental.nom_serie = $scope.nueva_serie.nom_serie ;


                 //PENDIENTE DE VERIFICACION (Actualizar la lista de series documentales)
                 insertarElemento($scope.series_documentales,$scope.nueva_serie);
                 $scope.tabla_series.reload();



                 //Limpiamos los campos obtenidos
             //    $scope.serie_documental = {cod_serie:"",nom_serie:"",val_Serie:""};

               $('#modalNuevaSerie').modal('hide');
                registrar_retencion();

            }

        },function(data){
             console.info(data);
         });



    };
    $scope.preparar_retencion = function(SerieCod){
        var serie = null;


        for(var i = 0 ;i<$scope.series_documentales.length;i++){
            if($scope.series_documentales[i].cod_serie == SerieCod){
                serie = $scope.series_documentales[i];
                break;
            }
        }
         var request = crud.crearRequest('gestion_series_documentales',1,'obtener_tabla_retencion');
            request.setData({serie_id:serie.serie_id});
             crud.listar("/archivoDigital",request,function(response){
                 //modal.mensaje("CONFIRMACION",response.responseMsg);
                  if(response.responseSta){
                      $scope.serie_documental.ag = response.data[0].ag;
                      $scope.serie_documental.ap = response.data[0].ap;
                      $scope.serie_documental.oaa = response.data[0].oaa;

                     $scope.serie_documental.serie_id  = serie.serie_id;
                     $scope.serie_documental.cod_serie = serie.cod_serie;
                     $scope.serie_documental.nom_serie = serie.nom_serie;
                     $scope.serie_documental.val_serie = serie.val_serie;

                      $('#modalTablaRetencion').modal('show');
                  }


                 },function(response){
                       console.info(response);
              });
            
     //   $scope.serie_documental.serie_id  = serie.serie_id;
     //   $scope.serie_documental.cod_serie = serie.cod_serie;
     //   $scope.serie_documental.nom_serie = serie.nom_serie;
     //   $scope.serie_documental.val_serie = serie.val_serie;
     //   $('#modalTablaRetencion').modal('show');
    };

    $scope.actualizar_retencion = function(){
         var request = crud.crearRequest('gestion_series_documentales',1,'editar_tabla_retencion');
             request.setData($scope.serie_documental);
            // console.log("datos ag...",$scope.serie_documental)
             crud.actualizar("/archivoDigital",request,function(response){
                  modal.mensaje("CONFIRMACION",response.responseMsg);
                  if(response.responseSta){

                     $('#modalTablaRetencion').modal('hide');
                      $scope.serie_documental.ag = "";
                      $scope.serie_documental.ap = "";
                      $scope.serie_documental.oaa ="";  
                  }
                 },function(response){
                       console.info(response);
              });

            

    };

    $scope.preparar_editar_serie = function(SerieCod){
        var serie = null;

        for(var i = 0 ;i<$scope.series_documentales.length;i++){
            if($scope.series_documentales[i].cod_serie == SerieCod){
                serie = $scope.series_documentales[i];
                $scope.pos_serie = i;
                break;
            }
        }
        $scope.serie_documental.serie_id  = serie.serie_id;
        $scope.serie_documental.cod_serie = serie.cod_serie;
        $scope.serie_documental.nom_serie = serie.nom_serie;
        $scope.serie_documental.val_serie = serie.val_serie;
        $('#modalEditarSerie').modal('show');
    };

    $scope.editar_serie = function(){

             var request = crud.crearRequest('gestion_series_documentales',1,'editar_serie_documental');
             request.setData($scope.serie_documental);
             
             crud.actualizar("/archivoDigital",request,function(response){
                  modal.mensaje("CONFIRMACION",response.responseMsg);
                  if(response.responseSta){

                     setting.dataset[$scope.pos_serie] = $scope.serie_documental;
                     $scope.tabla_series.reload();
                     $('#modalEditarSerie').modal('hide');
                  }
                 },function(response){
                       console.info(response);
              });

     };

    $scope.eliminar_serie = function(SerieCod){

       var serie = null;
       var num_serie = 0; // numero de serie de la tabla a eliminar
       for(var i = 0 ;i<$scope.series_documentales.length;i++){
           if($scope.series_documentales[i].cod_serie == SerieCod){
               serie = $scope.series_documentales[i];
               nun_serie = i+1;
               break;
           }
       }
       if(serie){
           var id_serie = serie.serie_id;

           modal.mensajeConfirmacion($scope,"seguro que desea eliminar este registro",function(){
           var request = null;
           
            request = crud.crearRequest('gestion_series_documentales',1,'eliminar_tabla_retencion');
                request.setData({serie_id:id_serie});
                crud.eliminar("/archivoDigital",request,
                    function(response){
                        modal.mensaje("CONFIRMACION",response.responseMsg);
                        if(response.responseSta){

                        }
                    },function(data){
               });
               
               
           request = crud.crearRequest('gestion_series_documentales',1,'eliminar_serie_documental');
           request.setData({serie_id:id_serie});

           crud.eliminar("/archivoDigital",request,
            function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    eliminarElemento(setting.dataset,i);
                    $scope.tabla_series.reload();
                    
                }

            },function(data){
            });

           
               
            });
          
       }

    };

  
 //CRUD PARA UNIDAD ORGANICA
    $scope.agregarUnidaO = function(tipo){
          //si estamos editando
          if(tipo){
              if($scope.unidadOrgSel.descripcion =="" ){
                  modal.mensaje("CONFIRMACION","ingrese descripcion del requisito");
                  return;
              }
              $scope.areasSelect.unidadesOrg.push($scope.unidadOrg);
              $scope.unidadOrgSel = {nombre:"", abreviacion:"", descripcion:""};
          }
          //si estamos agregando
          else{
              if($scope.unidadOrg.descripcion =="" ){
                  modal.mensaje("CONFIRMACION","ingrese descripcion del requisito");
                  return;
                  
              }
              $scope.unidadOrg.estado = 'A';
              $scope.unidadesOrg.push($scope.unidadOrg);
              $scope.unidadOrg = {nombre:"", abreviacion:"", descripcion:""};
          }        
      };

    $scope.editarUnidaO = function(i,r,modo){
        //si estamso editando
          if(r.edi){
              if(modo)
                  $scope.areasSelect.unidadesOrg[i] = r.copia;                
              else
                  $scope.unidadesOrg[i] = r.copia;
          }
          //si queremos editar
          else{
              r.copia = JSON.parse(JSON.stringify(r));
              r.edi =true;            
          }
      };
    $scope.eliminarUnidaO = function(i,r,modo){
         //si estamso cancelando la edicion
        if(r.edi){
            r.edi = false;
            delete r.copia;
        }
        //si queremos eliminar el elemento
        else{
            if(modo)
                $scope.areasSelect.unidadesOrg.splice(i,1);            
            else
                $scope.unidadesOrg.splice(i,1);
        }
    };     
    
    
 //CRUD PARA AREA
    $scope.agregarArea = function(organizacionID){

            $scope.nuevaArea.organizacionID = organizacionID;

            $scope.nuevaArea.nombre = buscarContenido($scope.tipoAreas,"tipoAreaID","nombre",$scope.nuevaArea.tipoAreaID);

            var request = crud.crearRequest('area',1,'insertarArea');
            request.setData($scope.nuevaArea);

            crud.insertar("/configuracionInicial",request,function(response){

                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    //recuperamos las variables que nos envio el servidor
                    $scope.nuevaArea.areaID = response.data.areaID;
                    $scope.nuevaArea.areaPadre = buscarContenido(settingArea.dataset,"areaID","nombre",$scope.nuevaArea.areaPadreID);

                    //insertamos el elemento a la lista
                    insertarElemento(settingArea.dataset,$scope.nuevaArea);

                    var o = {};
                    o.id = $scope.nuevaArea.nombre;
                    o.title = $scope.nuevaArea.nombre;
                    $scope.areasSelect.push(o);

                    $scope.tablaArea.reload();
                    //reiniciamos las variables
                    $scope.nuevaArea = {codigo:"",nombre:"",estado:'A',organizacionID:"0",areaPadreID:"0"};
                    //cerramos la ventana modal
                    $('#modalNuevo').modal('hide');
                }            
            },function(data){
                console.info(data);
            });

        };
    $scope.eliminarArea = function(i,idDato){

        modal.mensajeConfirmacion($scope,"seguro que desea eliminar la area",function(){

            var request = crud.crearRequest('area',1,'eliminarArea');
            request.setData({areaID:idDato});

            crud.eliminar("/configuracionInicial",request,function(response){

                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    eliminarElemento(settingArea.dataset,i);
                    $scope.tablaArea.reload();
                }

            },function(data){
                console.info(data);
            });
        });
    };
    $scope.prepararEditar = function(t){
        $scope.areaSel = JSON.parse(JSON.stringify(t));
        $('#modalEditarArea').modal('show');
    };
    $scope.editarArea = function(){

            $scope.areaSel.nombre = buscarContenido($scope.tipoAreas,"tipoAreaID","nombre",$scope.areaSel.tipoAreaID);

            var request = crud.crearRequest('area',1,'actualizarArea');
            request.setData($scope.areaSel);

            crud.actualizar("/configuracionInicial",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){                
                    //actualizando
                    $scope.areaSel.areaPadre = buscarContenido(settingArea.dataset,"areaID","nombre",$scope.areaSel.areaPadreID);
                    settingArea.dataset[$scope.areaSel.i] = $scope.areaSel;

                    var o = $scope.areasSelect[$scope.areaSel.i];
                    o.id = o.title = $scope.areaSel.nombre;

                    $scope.tablaArea.reload();
                    $('#modalEditarArea').modal('hide');
                }
            },function(data){
                console.info(data);
            });
        };

    $scope.listarAreas = function(organizacionID){
        //preparamos un objeto request
        var request = crud.crearRequest('area',1,'listarAreasPorOrganizacion');
        request.setData({organizacionID:organizacionID});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las areas de exito y error
        crud.listar("/configuracionInicial",request,function(data){
            $scope.areas = settingArea.dataset = data.data;
            iniciarPosiciones(settingArea.dataset);
            
            $scope.areasSelect.splice(0);
            $scope.areasSelect.push({id:'',title:""});
            //console.log("Areas: %o :", data);
            data.data.forEach(function(item){
                var o = item;
                o.id = item.nombre;
                o.title = item.nombre;
                $scope.areasSelect.push(o);
            });
            $scope.tablaArea.settings(settingArea);
        },function(data){
            console.info(data);
        });
    };  
    
    
    $scope.tempAreaID = null;
    $scope.verUnidadesOrganicas = function(area){
        $scope.areaSel = area;
        $scope.unidadesOrg = [];
        $scope.tempAreaID = area.areaID;
        //preparamos un objeto request
        
        var request = crud.crearRequest('gestion_area_archivo_central',1,'listar_unidad_organica');
         request.setData({codigo_area:area.areaID});
        
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las usuarios de exito y error
        crud.listar("/archivoDigital",request,function(data){
            $scope.unidadesOrg = data.data;
            $('#modalVerUnOr').modal('show');
        },function(data){
            console.info(data);
        });     
    };
    
    $scope.editarUnidadesOrganicas = function(){
      //  alert("area Temporal id : "+ $scope.tempAreaID);
       
      // console.log("Unidades Organicas %O:" , $scope.unidadesOrg);
       
        //  registrar_unidades_organicas
        var request = crud.crearRequest('gestion_area_archivo_central',1,'registrar_unidades_organicas');
       
        request.setData({areaID:$scope.tempAreaID,unidadesOrg:$scope.unidadesOrg});
        
        crud.insertar("/archivoDigital",request,function(response){
         
          modal.mensaje("CONFIRMACION",response.responseMsg);
           if(response.responseSta){
            
             $scope.unidadesOrg=[];
            }
         },function(data){
             console.info(data);
         });
      
         $scope.tempAreaID = null;
        $('#modalVerUnOr').modal('hide');
       // alert("area Temporal id : "+ $scope.tempAreaID);
    };
  
}]);