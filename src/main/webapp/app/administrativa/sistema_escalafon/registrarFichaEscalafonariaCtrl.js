app.requires.push('angularModalService');
app.requires.push('ngAnimate');
app.controller('registrarFichaEscalafonariaCtrl', ['$scope', '$rootScope', '$http','NgTableParams','crud', 'modal', 'ModalService', function ($scope, $rootScope, $http, NgTableParams, crud, modal, ModalService) {
        //Variables y funciones para controlar la vista
        $scope.optionsDatosPersonales = [true, false, false, false, false];
        $scope.optionsDatosAcademicos = [true, false, false, false, false, false];
        $scope.optionsExperienciaLaboral = [true, false, false, false];
        
        $scope.updateOptions = function (numOption, optionsSet) {
            for (var i = 0; i < optionsSet.length; i += 1) {
               optionsSet[i] = false;
            }
           optionsSet[numOption] = true;
        };
        
        $scope.status_1 = {close: true, statusGD: false, statusLD: false};
        
        $scope.statusDA = false;
        $scope.changeDASi = function () {
            $scope.statusDA = false;
        };
        $scope.changeDANo = function () {
            $scope.statusDA = true;
        };
        
        $rootScope.parsearBooleano = function (i) {
            var o = "";
            if (i) {
                o = "Si";
            } else{
                o = "No";
            }
            return o;
        };
        
        $rootScope.parsearBooleano2 = function (i) {
            var o = "";
            if (i) {
                o = "Habilitado";
            } else{
                o = "No Habilitado";
            }
            return o;
        };
        
        
        /*$scope.guardarLegajo = function () {
            if ($scope.nuevoLegajoPersonal.nom !== null){
                $scope.legajoPersonal.push()
            } else {
                console.log("No se adjunto archivo :P");
            }
        };*/
        $scope.habPestana = true;
        $scope.boolBoton = false;
        $scope.boolCampos = false;
        
        $rootScope.paramsModParientes = {count: 10};
        $rootScope.settingModParientes = {counts: []};
        $rootScope.settingModParientes.dataset = [];
        $rootScope.tablaModParientes = new NgTableParams($rootScope.paramsModParientes, $rootScope.settingModParientes);
        
        $rootScope.paramsModForEdu = {count: 10};
        $rootScope.settingModForEdu  = {counts: []};
        $rootScope.settingModForEdu.dataset = [];
        $rootScope.tablaModForEdu  = new NgTableParams($rootScope.paramsModForEdu, $rootScope.settingModForEdu);
        
        $rootScope.paramsModEstCom = {count: 10};
        $rootScope.settingModEstCom  = {counts: []};
        $rootScope.settingModEstCom.dataset = []; 
        $rootScope.tablaEstCom  = new NgTableParams($rootScope.paramsModEstCom, $rootScope.settingModEstCom);
        
        $rootScope.paramsModExpPon = {count: 10};
        $rootScope.settingModExpPon  = {counts: []};
        $rootScope.settingModExpPon.dataset = [];
        $rootScope.tablaExpPon  = new NgTableParams($rootScope.paramsModExpPon, $rootScope.settingModExpPon);
        
        $rootScope.paramsModPublicaciones = {count: 10};
        $rootScope.settingModPublicaciones  = {counts: []};
        $rootScope.settingModPublicaciones.dataset = []; 
        $rootScope.tablaPublicaciones = new NgTableParams($rootScope.paramsModPublicaciones, $rootScope.settingModPublicaciones);
        
        $rootScope.paramsModDesplazamientos = {count: 10};
        $rootScope.settingModDesplazamientos  = {counts: []};
        $rootScope.settingModDesplazamientos.dataset = [];
        $rootScope.tablaDesplazamientos= new NgTableParams($rootScope.paramsModDesplazamientos, $rootScope.settingModDesplazamientos);

        $rootScope.paramsModColegiaturas = {count: 10};
        $rootScope.settingModColegiaturas  = {counts: []};
        $rootScope.settingModColegiaturas.dataset = [];
        $rootScope.tablaColegiaturas = new NgTableParams($rootScope.paramsModColegiaturas, $rootScope.settingModColegiaturas);
        
        $rootScope.paramsModAscensos = {count: 10};
        $rootScope.settingModAscensos  = {counts: []};
        $rootScope.settingModAscensos.dataset = [];
        $rootScope.tablaAscensos = new NgTableParams($rootScope.paramsModAscensos, $rootScope.settingModAscensos);
        
        $rootScope.paramsModCapacitaciones = {count: 10};
        $rootScope.settingModCapacitaciones  = {counts: []};
        $rootScope.settingModCapacitaciones.dataset = [];
        $rootScope.tablaCapacitaciones = new NgTableParams($rootScope.paramsModCapacitaciones, $rootScope.settingModCapacitaciones);
        
        $rootScope.paramsModReconocimientos= {count: 10};
        $rootScope.settingModReconocimientos  = {counts: []};
        $rootScope.settingModReconocimientos.dataset = [];
        $rootScope.tablaReconocimientos = new NgTableParams($rootScope.paramsModReconocimientos, $rootScope.settingModReconocimientos);
        
        $rootScope.paramsModEstPos = {count: 10};
        $rootScope.settingModEstPos  = {counts: []};
        $rootScope.settingModEstPos.dataset = [];
        $rootScope.tablaEstPos = new NgTableParams($rootScope.paramsModEstPos, $rootScope.settingModEstPos);
        
        $rootScope.paramsModDemeritos = {count: 10};
        $rootScope.settingModDemeritos  = {counts: []};
        $rootScope.settingModDemeritos.dataset = [];
        $rootScope.tablaDemeritos = new NgTableParams($rootScope.paramsModDemeritos, $rootScope.settingModDemeritos);
        
        //Variables para el registro de datos
        $scope.nuevaPersona = {perId:0, apePat: "", apeMat: "", nom: "", dni: "", fecNac: "", num1: "", num2: "", fij: "", email: "", sex: "", estCiv: "", depNac: "", proNac: "", disNac: "", estado: 'A'};
        $scope.nuevaDireccionDNI = {tipDir: "R", nomDir: "", depDir: "", proDir: "", disDir: "", num: "", dpto: "", int: "", mz: "", lote: "", km: "", bloque: "", etapa: "", nomZon: "", desRef: ""};
        $scope.nuevaDireccionDA = {tipDir: "A", nomDir: "", depDir: "", proDir: "", disDir: "", num: "", dpto: "", int: "", mz: "", lote: "", km: "", bloque: "", etapa: "", nomZon: "", desRef: ""};
        $scope.nuevoTrabajador = {traId:0, traCon:""};
        $scope.nuevaFichaEscalafonaria = {ficEscId:0, autEss:"", sisPen:"", nomAfp:"", codCuspp:"", fecIngAfp:"", perDis:false, regCon:"", gruOcu:""};
        $scope.legajoDatosPersonales = [
            {ficEscId:"", nom:"", archivo:"", url:"", des:"DNI", catLeg:"1", subCat:"1", codAspOri:0}, 
            {ficEscId:"", nom:"", archivo:"", url:"", des:"Partida de Nacimiento", catLeg:"1", subCat:"2", codAspOri:0},
            {ficEscId:"", nom:"", archivo:"", url:"", des:"Registro CONADIS", catLeg:"1", subCat:"4", codAspOri:0}
        ];
        $scope.direcciones = [];
        $scope.sexo = [{id: "F", title: "Femenino"}, {id: "M", title: "Masculino"}];
        $scope.estadoCivil = [{id: "S", title: "Soltero(a)"}, {id: "C", title: "Casado(a)"}];
        $scope.tipoTrabajador = [{id: "N", title: "Nombrado"}, {id: "V", title: "Contratado"}];
        $scope.grupoOcupacional = [{id: "A", title: "Administrativo"}, {id: "D", title: "Docente"}];
        
        $scope.showNuevoPariente = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarPariente.html",
                controller: "agregarNuevoParienteCtrl",
                inputs: {
                    title: "Nuevo Pariente",
                    personaId: $scope.nuevaPersona.perId,
                    sexo: $scope.sexo,
                    fichaEscalafonariaId: $scope.nuevaFichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };
        
        $rootScope.legajoPersonal = [];
        $scope.agregarDatosGenerales = function () {
            
            //if (!$scope.statusDA) {
            //    $scope.direcciones = [$scope.nuevaDireccionDNI];
            //} else {
            $scope.direcciones = [$scope.nuevaDireccionDNI, $scope.nuevaDireccionDA];
            //}
            var request = crud.crearRequest('datos_personales', 1, 'agregarDatosPersonales');
            request.setData({persona: $scope.nuevaPersona, trabajador: $scope.nuevoTrabajador, fichaEscalafonaria:$scope.nuevaFichaEscalafonaria, direcciones: $scope.direcciones, orgId: $rootScope.usuMaster.organizacion.organizacionID});
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                
                if (response.responseSta) {//Si registro con exito
                    
                    $scope.nuevaPersona.perId = response.data.perId;
                    $scope.nuevoTrabajador.traId = response.data.traId;
                    $scope.nuevaFichaEscalafonaria.ficEscId = response.data.ficEscId;
                    $scope.habPestana = false;
                    $scope.boolBoton = true;
                    $scope.boolCampos = true;
                    
                    var legajos = [];
                    for (var i = 0; i < $scope.legajoDatosPersonales.length; i++) {
                        if ($scope.legajoDatosPersonales[i].archivo) {
                            $scope.legajoDatosPersonales[i].ficEscId = $scope.nuevaFichaEscalafonaria.ficEscId;
                            legajos.push($scope.legajoDatosPersonales[i]);
                        }
                    }

                    console.log(legajos);
                    if (legajos.length > 0) {
                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajos');
                        request.setData({legajos: legajos});
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                }
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.buscarPersona = function () {
            if ($scope.nuevaPersona.perDni === '') {
                modal.mensaje("ALERTA", "Ingrese DNI");
                return;
            }
            var request = crud.crearRequest('datos_personales', 1, 'buscarPersonaTrabajador');
            request.setData({perDni: $scope.nuevaPersona.perDni});
            crud.listar("/sistema_escalafon", request, function (data) {
                modal.mensaje("CONFIRMACION", data.responseMsg);
                $scope.boolRegistro=true;
                if (!data.responseSta) {//Caso 0: No existe la Persona
                    $scope.boolCampos=false;
                    $scope.limpiarCampos();
                    $scope.nuevaPersona.perDni=request.data.perDni;
                    $scope.nuevaPersona.dni=request.data.perDni;
                    $scope.nuevaPersona.caso=0;
                    $scope.boolBoton=false;
                    $scope.habDir = false;
                }
                else{
                    if(data.data.persona.caso===1)//Si existe la persona, trabajador, ficha solo ver los campos y habilitar boton de trabajador
                    {
                        
                        $scope.boolCampos=true;
                        $scope.nuevaPersona = {
                            
                            perId: data.data.persona.perId,
                            apePat: data.data.persona.apePat,
                            apeMat: data.data.persona.apeMat,
                            nom: data.data.persona.nom,
                            sex: data.data.persona.sex,
                            estCiv: data.data.persona.estCiv,
                            dni: data.data.persona.dni,
                            fij: data.data.persona.fij,
                            num1: data.data.persona.num1,
                            num2: data.data.persona.num2,
                            email: data.data.persona.email,
                            depNac: data.data.persona.depNac,
                            proNac: data.data.persona.proNac,
                            disNac: data.data.persona.disNac,
                            fecNac: convertirFechaStr(data.data.persona.fecNac),
                            caso: 1
                        };
                        
                        $scope.nuevoTrabajador = {
                            traId: data.data.trabajador.traId,
                            traCon: data.data.trabajador.traCon
                        };
                        
                        $scope.asegurado2 = data.data.ficha.autEss!=="               "?true:false;
                        
                        $scope.nuevaFichaEscalafonaria = {
                            ficEscId: data.data.ficha.ficEscId,
                            autEss: $scope.asegurado2 ? data.data.ficha.autEss : "               ",
                            sisPen: data.data.ficha.sisPen,
                            nomAfp: data.data.ficha.nomAfp,
                            codCuspp: data.data.ficha.codCuspp,
                            fecIngAfp: convertirFechaStr(data.data.ficha.fecIngAfp),
                            perDis: data.data.ficha.perDis,
                            regCon: data.data.ficha.regCon,
                            gruOcu: data.data.ficha.gruOcu
                        };
                        
                        $scope.boolSalud = {
                            valS: ($scope.asegurado2) === true ? 'SI' : 'NO'
                        };
                        $scope.boolDis = {
                            val: ($scope.nuevaPersona.perDis) === true ? 'SI' : 'NO'
                        };
                        
                        $scope.boolBoton=true;
                        $scope.habDir = true;
                        $scope.loadProLugNac();
                        $scope.loadDisLugNac();
                        listarDirecciones($scope.nuevaPersona.perId);
                        $scope.loadProDirDNI();
                        $scope.loadDisDirDNI();
                        $scope.loadProDirDA();
                        $scope.loadDisDirDA();
                        
                        $scope.nuevaPersona.perDni=request.data.perDni;
                    }
                    if(data.data.persona.caso===2)//Caso 2
                    {
                        $scope.boolCampos=false;
                        $scope.limpiarCampos();
                        $scope.nuevaPersona.perId=data.data.persona.perId;
                        $scope.nuevaPersona.apePat=data.data.persona.apePat;
                        $scope.nuevaPersona.apeMat=data.data.persona.apeMat;
                        $scope.nuevaPersona.dni=data.data.persona.dni;
                        $scope.nuevaPersona.nom=data.data.persona.nom;
                        $scope.nuevaPersona.sex=data.data.persona.sex;
                        $scope.nuevaPersona.fecNac=convertirFechaStr(data.data.persona.fecNac);
                        $scope.nuevaPersona.fij=data.data.persona.fij;
                        $scope.nuevaPersona.num1=data.data.persona.num1;
                        $scope.nuevaPersona.num2=data.data.persona.num2;
                        $scope.nuevaPersona.email=data.data.persona.email;
                        $scope.nuevaPersona.caso=2;
                        $scope.boolBoton=false;
                        $scope.habDir = false;
                        $scope.nuevaPersona.perDni=request.data.perDni;
                    }
                }
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.reiniciarDatos = function () {
            $scope.boolRegistro = false;
            $scope.boolBoton = true;
            $scope.tabPrincipal=0;
            $scope.habPestana = true;
        };
        $scope.limpiarCampos = function () {
            
            $scope.nuevaPersona = {
                perId: 0,
                apePat: "",
                apeMat: "",
                nom: "",
                sex: "",
                estCiv: "",
                dni:" ",
                fij: "",
                num1: "",
                num2: "",
                email: "",
                depNac: "",
                proNac: "",
                disNac: "",
                fecNac: "",
                estado: 'A'
            };
            
            $scope.nuevoTrabajador = {
                traId: 0,
                traCon: ""
            };
            
            $scope.nuevaFichaEscalafonaria = {
                ficEscId: 0,
                autEss: "",
                sisPen: "",
                nomAfp: "",
                codCuspp: "",
                fecIngAfp: "",
                perDis: "",
                regCon: "",
                gruOcu: ""
            };
        
            $scope.boolSalud = {
                valS: ""
            };
            $scope.boolDis = {
                val: ""
            };
            $scope.nuevaDireccionDNI = {
                tipDir: "R", 
                nomDir: "", 
                depDir: "", 
                proDir: "", 
                disDir: "", 
                num: "", 
                dpto: "", 
                int: "", 
                mz: "", 
                lote: "", 
                km: "", 
                bloque: "", 
                etapa: "", 
                nomZon: "", 
                desRef: ""
            };
            $scope.nuevaDireccionDA = {
                tipDir: "A", 
                nomDir: "", 
                depDir: "", 
                proDir: "", 
                disDir: "", 
                num: "", 
                dpto: "", 
                int: "", 
                mz: "", 
                lote: "", 
                km: "", 
                bloque: "", 
                etapa: "", 
                nomZon: "", 
                desRef: ""
            };
            $scope.direcciones=[];
            $scope.optionsD = "";
            $scope.status_1.statusGD = false;
            $scope.status_1.statusLD = false;
            $scope.statusDA = false;
            
        };
        
        function listarDirecciones(perId){
            var request = crud.crearRequest('datos_personales',1,'listarDirecciones');
            request.setData({perId:perId});
            crud.listar('/sistema_escalafon',request,function(response){
                $scope.direcciones = response.data;
                if($scope.direcciones.length!==0)
                {
                    nuevaDireccionSel=[];
                    nuevaDireccionAct=[];
                    for (var dir in $scope.direcciones)
                    {
                        if ($scope.direcciones[dir].tipDir === "R")
                        {
                            nuevaDireccionSel.dirId = $scope.direcciones[dir].dirId;
                            nuevaDireccionSel.tipDir = $scope.direcciones[dir].tipDir;
                            nuevaDireccionSel.nomDir = $scope.direcciones[dir].nomDir;
                            nuevaDireccionSel.depDir = $scope.direcciones[dir].depDir;
                            nuevaDireccionSel.proDir = $scope.direcciones[dir].proDir;
                            nuevaDireccionSel.disDir = $scope.direcciones[dir].disDir;
                            nuevaDireccionSel.nomZon = $scope.direcciones[dir].nomZon;
                            nuevaDireccionSel.desRef = $scope.direcciones[dir].desRef;
                        }
                        if ($scope.direcciones[dir].tipDir === "A")
                        {
                            nuevaDireccionAct.dirId = $scope.direcciones[dir].dirId;
                            nuevaDireccionAct.tipDir = $scope.direcciones[dir].tipDir;
                            nuevaDireccionAct.nomDir = $scope.direcciones[dir].nomDir;
                            nuevaDireccionAct.depDir = $scope.direcciones[dir].depDir;
                            nuevaDireccionAct.proDir = $scope.direcciones[dir].proDir;
                            nuevaDireccionAct.disDir = $scope.direcciones[dir].disDir;
                            nuevaDireccionAct.nomZon = $scope.direcciones[dir].nomZon;
                            nuevaDireccionAct.desRef = $scope.direcciones[dir].desRef;
                        }
                    }
                    $scope.direcciones = [];
                    $scope.direcciones.push(nuevaDireccionSel);//pos0
                    $scope.direcciones.push(nuevaDireccionAct);//pos1
                    console.log("dir", $scope.direcciones);
                }
                changeOptionsDirectionsState($scope.direcciones);
            },function (data) {
                console.info(data);
            });
        }
        function changeOptionsDirectionsState(direcciones) {
            var caso = 1;
            if (direcciones.length === 0)
                caso = 0;
            if (direcciones.length === 2)
            {
                if (direcciones[1].nomDir === '' && direcciones[1].depDir === '  ' && direcciones[1].proDir === '  ' &&
                        direcciones[1].disDir === '  ' && direcciones[1].nomZon === '' && direcciones[1].desRef === '')
                {
                    caso = 3;
                } else
                    caso = 2;
            }
            switch (caso) {
                case 1:
                    $scope.optionsD = "SI";
                    $scope.status_1.statusGD = true;
                    $scope.statusDA = false;
                    $scope.nuevaDireccionDNI.dirId = direcciones[0].dirId;
                    $scope.nuevaDireccionDNI.tipDir = direcciones[0].tipDir;
                    $scope.nuevaDireccionDNI.nomDir = direcciones[0].nomDir;
                    $scope.nuevaDireccionDNI.depDir = direcciones[0].depDir;
                    $scope.nuevaDireccionDNI.proDir = direcciones[0].proDir;
                    $scope.nuevaDireccionDNI.disDir = direcciones[0].disDir;
                    $scope.nuevaDireccionDNI.nomZon = direcciones[0].nomZon;
                    $scope.nuevaDireccionDNI.desRef = direcciones[0].desRef;
                    break;

                case 2:
                    $scope.optionsD = "NO";
                    $scope.status_1.statusGD = true;
                    $scope.status_1.statusLD = true;
                    $scope.statusDA = true;
                    $scope.nuevaDireccionDNI.dirId = direcciones[0].dirId;
                    $scope.nuevaDireccionDNI.tipDir = direcciones[0].tipDir;
                    $scope.nuevaDireccionDNI.nomDir = direcciones[0].nomDir;
                    $scope.nuevaDireccionDNI.depDir = direcciones[0].depDir;
                    $scope.nuevaDireccionDNI.proDir = direcciones[0].proDir;
                    $scope.nuevaDireccionDNI.disDir = direcciones[0].disDir;
                    $scope.nuevaDireccionDNI.nomZon = direcciones[0].nomZon;
                    $scope.nuevaDireccionDNI.desRef = direcciones[0].desRef;

                    $scope.nuevaDireccionDA.dirId = direcciones[1].dirId;
                    $scope.nuevaDireccionDA.tipDir = direcciones[1].tipDir;
                    $scope.nuevaDireccionDA.nomDir = direcciones[1].nomDir;
                    $scope.nuevaDireccionDA.depDir = direcciones[1].depDir;
                    $scope.nuevaDireccionDA.proDir = direcciones[1].proDir;
                    $scope.nuevaDireccionDA.disDir = direcciones[1].disDir;
                    $scope.nuevaDireccionDA.nomZon = direcciones[1].nomZon;
                    $scope.nuevaDireccionDA.desRef = direcciones[1].desRef;
                    break;

                case 3:
                    $scope.optionsD = "SI";
                    $scope.status_1.statusGD = true;
                    $scope.status_1.statusLD = false;
                    $scope.statusDA = false;
                    $scope.nuevaDireccionDNI.dirId = direcciones[0].dirId;
                    $scope.nuevaDireccionDNI.tipDir = direcciones[0].tipDir;
                    $scope.nuevaDireccionDNI.nomDir = direcciones[0].nomDir;
                    $scope.nuevaDireccionDNI.depDir = direcciones[0].depDir;
                    $scope.nuevaDireccionDNI.proDir = direcciones[0].proDir;
                    $scope.nuevaDireccionDNI.disDir = direcciones[0].disDir;
                    $scope.nuevaDireccionDNI.nomZon = direcciones[0].nomZon;
                    $scope.nuevaDireccionDNI.desRef = direcciones[0].desRef;

                    $scope.nuevaDireccionDA.dirId = direcciones[1].dirId;
                    $scope.nuevaDireccionDA.tipDir = direcciones[1].tipDir;
                    $scope.nuevaDireccionDA.nomDir = direcciones[1].nomDir;
                    $scope.nuevaDireccionDA.depDir = direcciones[1].depDir;
                    $scope.nuevaDireccionDA.proDir = direcciones[1].proDir;
                    $scope.nuevaDireccionDA.disDir = direcciones[1].disDir;
                    $scope.nuevaDireccionDA.nomZon = direcciones[1].nomZon;
                    $scope.nuevaDireccionDA.desRef = direcciones[1].desRef;
                    break;
            }
        }


        $http.get('../recursos/json/departamentos.json').success(function (data) {
            $scope.departamentosLugNac = data;
            $scope.departamentosDirDNI = data;
            $scope.departamentosDirDA = data;
        });

        $scope.loadProLugNac = function () {
            $http.get('../recursos/json/provincias.json').success(function (data) {
                var dep = $scope.departamentosLugNac.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.nuevaPersona.depNac;
                })[0];
                $scope.provinciasLugNac = data[dep.id_ubigeo];
                $scope.distritosLugNac = [];
            });
        };

        $scope.loadDisLugNac = function () {
            $http.get('../recursos/json/distritos.json').success(function (data) {
                var pro = $scope.provinciasLugNac.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.nuevaPersona.proNac;
                })[0];
                $scope.distritosLugNac = data[pro.id_ubigeo];
            });
        };

        $scope.loadProDirDNI = function () {
            $http.get('../recursos/json/provincias.json').success(function (data) {
                var dep = $scope.departamentosDirDNI.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.nuevaDireccionDNI.depDir;
                })[0];
                $scope.provinciasDirDNI = data[dep.id_ubigeo];
                $scope.distritosDirDNI = [];
            });
        };
        
        $scope.loadDisDirDNI = function () {
            $http.get('../recursos/json/distritos.json').success(function (data) {
                var pro = $scope.provinciasDirDNI.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.nuevaDireccionDNI.proDir;
                })[0];
                $scope.distritosDirDNI = data[pro.id_ubigeo];
            });
        };

        $scope.loadProDirDA = function () {
            $http.get('../recursos/json/provincias.json').success(function (data) {
                var dep = $scope.departamentosDirDA.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.nuevaDireccionDA.depDir;
                })[0];
                $scope.provinciasDirDA = data[dep.id_ubigeo];
                $scope.distritosDirDA = [];
            });
        };

        $scope.loadDisDirDA = function () {
            $http.get('../recursos/json/distritos.json').success(function (data) {
                var pro = $scope.provinciasDirDA.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.nuevaDireccionDA.proDir;
                })[0];
                $scope.distritosDirDA = data[pro.id_ubigeo];
            });
        };
        
        $scope.showNuevaFormacionEducativa = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarFormacionEducativa.html",
                controller: "agregarNuevaFormacionEducativaCtrl",
                inputs: {
                    title: "Nueva Formacion Educativa",
                    fichaEscalafonariaId: $scope.nuevaFichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };

        $scope.showNuevoEstudioComplementario = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarEstudioComplementario.html",
                controller: "agregarNuevoEstudioComplementarioCtrl",
                inputs: {
                    title: "Nuevo Estudio Complementario",
                    fichaEscalafonariaId: $scope.nuevaFichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.showNuevaExposicion = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarExposicion.html",
                controller: "agregarNuevaExposicionCtrl",
                inputs: {
                    title: "Nueva Exposicion y/o Ponencia",
                    fichaEscalafonariaId: $scope.nuevaFichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.showNuevaPublicacion = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarPublicacion.html",
                controller: "agregarNuevaPublicacionCtrl",
                inputs: {
                    title: "Nueva Publicacion",
                    fichaEscalafonariaId: $scope.nuevaFichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.showNuevoDesplazamiento = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarDesplazamiento.html",
                controller: "agregarNuevoDesplazamientoCtrl",
                inputs: {
                    title: "Nueva Desplazamiento",
                    fichaEscalafonariaId: $scope.nuevaFichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        
        $scope.showNuevaColegiatura = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarColegiatura.html",
                controller: "agregarNuevaColegiaturaCtrl",
                inputs: {
                    title: "Nueva Colegiatura",
                    fichaEscalafonariaId: $scope.nuevaFichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };
        
        
        $scope.showNuevoAscenso = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarAscenso.html",
                controller: "agregarNuevoAscensoCtrl",
                inputs: {
                    title: "Nueva Ascenso",
                    fichaEscalafonariaId: $scope.nuevaFichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };
        
        $scope.showNuevoReconocimiento = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarReconocimiento.html",
                controller: "agregarNuevoReconocimientoCtrl",
                inputs: {
                    title: "Nueva Reconocimiento",
                    fichaEscalafonariaId: $scope.nuevaFichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };
        
        
        $scope.showNuevoEstudioPostgrado = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarEstudioPostgrado.html",
                controller: "agregarNuevoEstudioPostgradoCtrl",
                inputs: {
                    title: "Nueva Estudio Postgrado",
                    fichaEscalafonariaId: $scope.nuevaFichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };
        
        
        $scope.showNuevoDemerito = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarDemerito.html",
                controller: "agregarNuevoDemeritoCtrl",
                inputs: {
                    title: "Nueva Demerito",
                    fichaEscalafonariaId: $scope.nuevaFichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };
        
        $scope.showNuevaCapacitacion = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarCapacitacion.html",
                controller: "agregarNuevaCapacitacionCtrl",
                inputs: {
                    title: "Nueva capacitación",
                    fichaEscalafonariaId: $scope.nuevaFichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarPariente = function (p) {
            var parienteSel = JSON.parse(JSON.stringify(p));
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarPariente.html",
                controller: "editarParienteCtrl",
                inputs: {
                    title: "Editar datos del pariente",
                    pariente: parienteSel,
                    personaId: $scope.nuevaPersona.perId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarFormacionEducativa = function (fe) {
            var formacionEducativaSel = JSON.parse(JSON.stringify(fe));
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarFormacionEducativa.html",
                controller: "editarFormacionEducativaCtrl",
                inputs: {
                    title: "Editar datos de Formacion Educativa",
                    formacionEducativa: formacionEducativaSel,
                    fichaEscalafonariaId: $scope.nuevafichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });

        };
        
        $scope.prepararEditarColegiatura = function (c) {
            var colegiaturaSel = JSON.parse(JSON.stringify(c));
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarColegiatura.html",
                controller: "editarColegiaturaCtrl",
                inputs: {
                    title: "Editar datos de Colegiatura",
                    colegiatura: colegiaturaSel,
                    fichaEscalafonariaId: $scope.nuevafichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });

        };
        
        $scope.prepararEditarEstudioComplementario = function (e) {
            var estComSel = JSON.parse(JSON.stringify(e));
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarEstudioComplementario.html",
                controller: "editarEstudioComplementarioCtrl",
                inputs: {
                    title: "Editar datos de Estudio Complementario",
                    estudioComplementario: estComSel,
                    fichaEscalafonariaId: $scope.nuevafichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });

        };
        
        $scope.prepararEditarEstudioPostgrado = function (e) {
            var estPosSel = JSON.parse(JSON.stringify(e));
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarEstudioPostgrado.html",
                controller: "editarEstudioPostgradoCtrl",
                inputs: {
                    title: "Editar datos de Estudio de Postgrado",
                    estudioPostgrado: estPosSel,
                    fichaEscalafonariaId: $scope.nuevafichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarExposicion = function (e) {
            var expSel = JSON.parse(JSON.stringify(e));
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarExposicion.html",
                controller: "editarExposicionCtrl",
                inputs: {
                    title: "Editar datos de Exposicion",
                    exposicion: expSel,
                    fichaEscalafonariaId: $scope.nuevafichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarPublicacion = function (p) {
            var pubSel = JSON.parse(JSON.stringify(p));
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarPublicacion.html",
                controller: "editarPublicacionCtrl",
                inputs: {
                    title: "Editar datos de Publicacion",
                    publicacion: pubSel,
                    fichaEscalafonariaId: $scope.nuevafichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarDesplazamiento = function (d) {
            var desSel = JSON.parse(JSON.stringify(d));
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarDesplazamiento.html",
                controller: "editarDesplazamientoCtrl",
                inputs: {
                    title: "Editar datos de Desplazamiento",
                    desplazamiento: desSel,
                    fichaEscalafonariaId: $scope.nuevafichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarAscenso = function (a) {
            var ascSel = JSON.parse(JSON.stringify(a));
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarAscenso.html",
                controller: "editarAscensoCtrl",
                inputs: {
                    title: "Editar datos de Ascenso",
                    ascenso: ascSel,
                    fichaEscalafonariaId: $scope.nuevafichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarCapacitacion = function (c) {
            var capSel = JSON.parse(JSON.stringify(c));
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarCapacitacion.html",
                controller: "editarCapacitacionCtrl",
                inputs: {
                    title: "Editar datos de Capacitacion",
                    capacitacion: capSel,
                    fichaEscalafonariaId: $scope.nuevafichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarReconocimiento = function (r) {
            var recSel = JSON.parse(JSON.stringify(r));
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarReconocimiento.html",
                controller: "editarReconocimientoCtrl",
                inputs: {
                    title: "Editar datos de Reconocimiento",
                    reconocimiento: recSel,
                    fichaEscalafonariaId: $scope.nuevafichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarDemerito = function (d) {
            var demSel = JSON.parse(JSON.stringify(d));
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarDemerito.html",
                controller: "editarDemeritoCtrl",
                inputs: {
                    title: "Editar datos de Demerito",
                    demerito: demSel,
                    fichaEscalafonariaId: $scope.nuevafichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        //ELIMINAR LEGAJO ???
        $scope.eliminarPariente = function(p){   
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el pariente?", function () {
                var request = crud.crearRequest('familiares', 1, 'eliminarFamiliar');
                request.setData({parId: p.parId, perId: $scope.personaSel.perId});
                
                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModParientes.settings().dataset, function(item){
                            return p === item;
                        });
                        $rootScope.tablaModParientes.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    }else {
                            modal.mensaje("ERROR",response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR","El servidor no responde");
                });
            }, '400'); 
        };

    }]);