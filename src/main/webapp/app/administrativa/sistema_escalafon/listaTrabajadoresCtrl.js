app.requires.push('angularModalService');
app.requires.push('ngAnimate');
var seApp = angular.module('app');
seApp.config(['$routeProvider',function($routeProvider){
    $routeProvider.when('/actualizar_ficha_escalafonaria/:data',{
            templateUrl:'administrativa/sistema_escalafon/actualizarFichaEscalafonaria.html',
            controller:'actualizarFichaEscalafonariaCtrl',
            controllerAs:'actualizarFichaCtrl'
        }).when('/legajo_personal/:ficha',{
            templateUrl:'administrativa/sistema_escalafon/verLegajoPersonal.html',
            controller:'verLegajoPersonalCtrl',
            controllerAs:'verLegajoCtrl'
        });
}]);
app.controller("listaTrabajadoresCtrl", ["$location", "$rootScope", "$scope", "NgTableParams", "crud", "modal", "ModalService", function ($location, $rootScope, $scope, NgTableParams, crud, modal, ModalService) {

//arreglo donde estan todas las cuentas contables
        $scope.trabajadores = [];
        //variable que servira para crear una nueva cuenta contable
        $scope.trabajadorSel = {};
        //Variables para manejo de la tabla
        $rootScope.paramsTrabajadores = {count: 10};
        $rootScope.settingTrabajadores = {counts: []};
        $rootScope.tablaTrabajadores = new NgTableParams($rootScope.paramsTrabajadores, $rootScope.settingTrabajadores);
        $scope.listarTrabajadores = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('datos_personales', 1, 'listarPorOrganizacion');
            request.setData({orgId: $rootScope.usuMaster.organizacion.organizacionID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingTrabajadores.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingTrabajadores.dataset);
                $rootScope.tablaTrabajadores.settings($rootScope.settingTrabajadores);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.prepararModificarFichaEscalafonaria = function (t) {
            var trabajadorSel = JSON.parse(JSON.stringify(t));
            trabajadorSel.opcion = 1;
            console.log(trabajadorSel);
            var request = crud.crearRequest('datos_personales',1,'buscarFichaPorDNI');
            request.setData(trabajadorSel);
            crud.listar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    console.log(response.data);
                    $location.url('/actualizar_ficha_escalafonaria/'+btoa(JSON.stringify(response.data)));
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function (data) {
                console.info(data);
            });
        };

        $scope.prepararVerLegajoPersonal = function (t) {

            var trabajadorSel = JSON.parse(JSON.stringify(t));
            trabajadorSel.opcion = 200;
            var request = crud.crearRequest('datos_personales',1,'buscarFichaPorDNI');
            request.setData(trabajadorSel);
            crud.listar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    $location.url('/legajo_personal/'+btoa(JSON.stringify(response.data)));
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function (data) {
                console.info(data);
            });
        };
        
        $scope.verFichaEscalafonaria = function(t){
            var trabajadorSel = JSON.parse(JSON.stringify(t));   
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/modalSeleccionados.html",
                controller: "modalSeleccionadosCtrl",
                inputs: {
                    title: "Modal seleccionados",
                    ficEscId: trabajadorSel.ficEscId,
                    traId: trabajadorSel.traId,
                    perDni: trabajadorSel.perDni
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                        
                    }
                });
            });
            
        };
        
        $scope.eliminarTrabajador = function(t){   
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el trabajador?", function () {
                var request = crud.crearRequest('datos_personales', 1, 'eliminarTrabajador');
                request.setData({traId: t.traId});
                
                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaTrabajadores.settings().dataset, function(item){
                            return t === item;
                        });
                        $rootScope.tablaTrabajadores.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    }else {
                            modal.mensaje("ERROR",response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR","El servidor no responde");
                });
            }, '400'); 
        };
    }]);
//Datos Personales
app.controller('actualizarFichaEscalafonariaCtrl', ['$routeParams', '$scope', '$rootScope', '$http', 'NgTableParams', 'crud','modal', 'ModalService', function ($routeParams, $scope, $rootScope, $http, NgTableParams, crud, modal, ModalService) {
        
        
        //Variables y funciones para controlar la vista
        $scope.optionsDatosPersonales = [true, false, false, false, false];
        $scope.optionsDatosAcademicos = [true, false, false, false, false];
        $scope.optionsExperienciaLaboral = [true, false, false];
        
        $scope.updateOptions = function (numOption, optionsSet) {
            for (var i = 0; i < optionsSet.length; i += 1) {
               optionsSet[i] = false;
            }
           optionsSet[numOption] = true;
        };
        
        $scope.status_1 = {close: true, statusGD: false, statusLD: false};
        $scope.changeDiscapacidadSi = function () {
            $scope.fichaEscalafonariaSel.perDis = true;
        };
        $scope.changeDiscapacidadNo = function () {
            $scope.fichaEscalafonariaSel.perDis = false;
        };
        
        $scope.statusDA = false;
        $scope.changeDASi = function () {
            $scope.statusDA = false;
        };
        $scope.changeDANo = function () {
            $scope.statusDA = true;
        };
        
        function changeSistemaPensionesState(sisPen) {
            if(sisPen === "ONP")
                $scope.optionsSP = "NO";
            else
                $scope.optionsSP = "SI";
        };
        
        function changeDishabilityState(perDis) {
            if(perDis === true)
                $scope.discapacidad = "SI";
            else
                $scope.discapacidad = "NO";
        };
        
        $scope.sexo = [{id: "F", title: "Femenino"}, {id: "M", title: "Masculino"}];
        $scope.estadoCivil = [{id: "S", title: "Soltero(a)"}, {id: "C", title: "Casado(a)"}];
        $scope.tipoTrabajador = [{id: "N", title: "Nombrado"}, {id: "V", title: "Contratado"}];
        $scope.grupoOcupacional = [{id: "A", title: "Administrativo"}, {id: "D", title: "Docente"}];
        
        $rootScope.paramsModParientes = {count: 10};
        $rootScope.settingModParientes = {counts: []};
        $rootScope.tablaModParientes = new NgTableParams($rootScope.paramsModParientes, $rootScope.settingModParientes);
        
        $rootScope.paramsModForEdu = {count: 10};
        $rootScope.settingModForEdu  = {counts: []};
        $rootScope.tablaModForEdu  = new NgTableParams($rootScope.paramsModForEdu, $rootScope.settingModForEdu);
        
        $rootScope.paramsModEstCom = {count: 10};
        $rootScope.settingModEstCom  = {counts: []};
        $rootScope.tablaModEstCom  = new NgTableParams($rootScope.paramsModEstCom, $rootScope.settingModEstCom);
        
        $rootScope.paramsModExpPon = {count: 10};
        $rootScope.settingModExpPon  = {counts: []};
        $rootScope.tablaModExpPon  = new NgTableParams($rootScope.paramsModExpPon, $rootScope.settingModExpPon);
        
        $rootScope.paramsModPublicaciones = {count: 10};
        $rootScope.settingModPublicaciones  = {counts: []};
        $rootScope.tablaModPublicaciones = new NgTableParams($rootScope.paramsModPublicaciones, $rootScope.settingModPublicaciones);
        
        $rootScope.paramsModDesplazamientos = {count: 10};
        $rootScope.settingModDesplazamientos  = {counts: []};
        $rootScope.tablaModDesplazamientos= new NgTableParams($rootScope.paramsModDesplazamientos, $rootScope.settingModDesplazamientos);

        $rootScope.paramsModColegiaturas = {count: 10};
        $rootScope.settingModColegiaturas  = {counts: []};
        $rootScope.tablaModColegiaturas = new NgTableParams($rootScope.paramsModModColegiaturas, $rootScope.settingcColegiaturas);
        
        $rootScope.paramsModAscensos = {count: 10};
        $rootScope.settingModAscensos  = {counts: []};
        $rootScope.tablaModAscensos = new NgTableParams($rootScope.paramModsAscensos, $rootScope.settingModAscensos);
        
        $rootScope.paramsModCapacitaciones = {count: 10};
        $rootScope.settingModCapacitaciones  = {counts: []};
        $rootScope.tablaModCapacitaciones = new NgTableParams($rootScope.paramsModCapacitaciones, $rootScope.settingModCapacitaciones);
        
        $rootScope.paramsModReconocimientos= {count: 10};
        $rootScope.settingModReconocimientos  = {counts: []};
        $rootScope.tablaModReconocimientos = new NgTableParams($rootScope.paramsModReconocimientos, $rootScope.settingModReconocimientos);
        
        $rootScope.paramsModEstPos = {count: 10};
        $rootScope.settingModEstPos  = {counts: []};
        $rootScope.tablaModEstPos = new NgTableParams($rootScope.paramsModEstPos, $rootScope.settingModEstPos);
        
        $rootScope.paramsModDemeritos = {count: 10};
        $rootScope.settingModDemeritos  = {counts: []};
        $rootScope.tablaModDemeritos = new NgTableParams($rootScope.paramsModDemeritos, $rootScope.settingModDemeritos);
        
        var datosPersonales = JSON.parse(atob($routeParams.data));
        
        $scope.personaSel = {
            perId: datosPersonales.persona.perId, 
            apePat: datosPersonales.persona.apePat, 
            apeMat: datosPersonales.persona.apeMat, 
            nom: datosPersonales.persona.nom, 
            dni: datosPersonales.persona.dni, 
            fecNac: convertirFechaStr(datosPersonales.persona.fecNac),
            num1: datosPersonales.persona.num1, 
            num2: datosPersonales.persona.num2, 
            fij: datosPersonales.persona.fij, 
            email: datosPersonales.persona.email, 
            sex: datosPersonales.persona.sex, 
            estCiv: datosPersonales.persona.estCiv, 
            depNac: datosPersonales.persona.depNac, 
            proNac: datosPersonales.persona.proNac, 
            disNac: datosPersonales.persona.disNac, 
            estado: 'A'
        };
        
        $scope.direccionDNISel = {
            tipDir: "R", 
            nomDir: "", 
            depDir: "", 
            proDir: "", 
            disDir: "", 
            nomZon: "", 
            desRef: ""
        };
        $scope.direccionDASel = {
            tipDir: "A", 
            nomDir: "", 
            depDir: "", 
            proDir: "", 
            disDir: "", 
            nomZon: "", 
            desRef: ""
        };
        
        $scope.direcciones = [];
        function listarDirecciones(){
            var request = crud.crearRequest('datos_personales',1,'listarDirecciones');
            request.setData({perId:datosPersonales.persona.perId});
            crud.listar('/sistema_escalafon',request,function(response){
                $scope.direcciones = response.data;
                if($scope.direcciones.length!==0)
                {
                    nuevaDireccionSel=[];
                    nuevaDireccionAct=[];
                    for (var dir in $scope.direcciones)
                    {
                        if ($scope.direcciones[dir].tipDir === "R")
                        {
                            nuevaDireccionSel.dirId = $scope.direcciones[dir].dirId;
                            nuevaDireccionSel.tipDir = $scope.direcciones[dir].tipDir;
                            nuevaDireccionSel.nomDir = $scope.direcciones[dir].nomDir;
                            nuevaDireccionSel.depDir = $scope.direcciones[dir].depDir;
                            nuevaDireccionSel.proDir = $scope.direcciones[dir].proDir;
                            nuevaDireccionSel.disDir = $scope.direcciones[dir].disDir;
                            nuevaDireccionSel.nomZon = $scope.direcciones[dir].nomZon;
                            nuevaDireccionSel.desRef = $scope.direcciones[dir].desRef;
                        }
                        if ($scope.direcciones[dir].tipDir === "A")
                        {
                            nuevaDireccionAct.dirId = $scope.direcciones[dir].dirId;
                            nuevaDireccionAct.tipDir = $scope.direcciones[dir].tipDir;
                            nuevaDireccionAct.nomDir = $scope.direcciones[dir].nomDir;
                            nuevaDireccionAct.depDir = $scope.direcciones[dir].depDir;
                            nuevaDireccionAct.proDir = $scope.direcciones[dir].proDir;
                            nuevaDireccionAct.disDir = $scope.direcciones[dir].disDir;
                            nuevaDireccionAct.nomZon = $scope.direcciones[dir].nomZon;
                            nuevaDireccionAct.desRef = $scope.direcciones[dir].desRef;
                        }
                    }
                    $scope.direcciones = [];
                    $scope.direcciones.push(nuevaDireccionSel);//pos0
                    $scope.direcciones.push(nuevaDireccionAct);//pos1
                    //console.log("dir", $scope.direcciones);
                }
                changeOptionsDirectionsState($scope.direcciones);
            },function (data) {
                console.info(data);
            });
        }
         function changeOptionsDirectionsState(direcciones) {
            var caso=1;
            if(direcciones.length===0)
                caso=0;
            if(direcciones.length===2)
            {
                if(direcciones[1].nomDir==='' && direcciones[1].depDir==='  '  && direcciones[1].proDir==='  '  &&
                        direcciones[1].disDir==='  '  && direcciones[1].nomZon===''  && direcciones[1].desRef===''  )
                {    
                    caso=3;
                }else
                    caso=2;
            }
            switch (caso) {
                case 1:
                    $scope.optionsDirections = "SI";
                    $scope.status_1.statusGD = true;
                    $scope.statusDA = false;
                    $scope.direccionDNISel.dirId = direcciones[0].dirId;
                    $scope.direccionDNISel.tipDir = direcciones[0].tipDir;
                    $scope.direccionDNISel.nomDir = direcciones[0].nomDir;
                    $scope.direccionDNISel.depDir = direcciones[0].depDir;
                    $scope.direccionDNISel.proDir = direcciones[0].proDir;
                    $scope.direccionDNISel.disDir = direcciones[0].disDir;
                    $scope.direccionDNISel.nomZon = direcciones[0].nomZon;
                    $scope.direccionDNISel.desRef = direcciones[0].desRef;
                    break;

                case 2:
                    $scope.optionsDirections = "NO";
                    $scope.status_1.statusGD = true;
                    $scope.status_1.statusLD = true;
                    $scope.statusDA = true;
                    $scope.direccionDNISel.dirId = direcciones[0].dirId;
                    $scope.direccionDNISel.tipDir = direcciones[0].tipDir;
                    $scope.direccionDNISel.nomDir = direcciones[0].nomDir;
                    $scope.direccionDNISel.depDir = direcciones[0].depDir;
                    $scope.direccionDNISel.proDir = direcciones[0].proDir;
                    $scope.direccionDNISel.disDir = direcciones[0].disDir;
                    $scope.direccionDNISel.nomZon = direcciones[0].nomZon;
                    $scope.direccionDNISel.desRef = direcciones[0].desRef;

                    $scope.direccionDASel.dirId = direcciones[1].dirId;
                    $scope.direccionDASel.tipDir = direcciones[1].tipDir;
                    $scope.direccionDASel.nomDir = direcciones[1].nomDir;
                    $scope.direccionDASel.depDir = direcciones[1].depDir;
                    $scope.direccionDASel.proDir = direcciones[1].proDir;
                    $scope.direccionDASel.disDir = direcciones[1].disDir;
                    $scope.direccionDASel.nomZon = direcciones[1].nomZon;
                    $scope.direccionDASel.desRef = direcciones[1].desRef;
                    break;
                    
                case 3:
                    $scope.optionsDirections = "SI";
                    $scope.status_1.statusGD = true;
                    $scope.status_1.statusLD = false;
                    $scope.statusDA = false;
                    $scope.direccionDNISel.dirId = direcciones[0].dirId;
                    $scope.direccionDNISel.tipDir = direcciones[0].tipDir;
                    $scope.direccionDNISel.nomDir = direcciones[0].nomDir;
                    $scope.direccionDNISel.depDir = direcciones[0].depDir;
                    $scope.direccionDNISel.proDir = direcciones[0].proDir;
                    $scope.direccionDNISel.disDir = direcciones[0].disDir;
                    $scope.direccionDNISel.nomZon = direcciones[0].nomZon;
                    $scope.direccionDNISel.desRef = direcciones[0].desRef;

                    $scope.direccionDASel.dirId = direcciones[1].dirId;
                    $scope.direccionDASel.tipDir = direcciones[1].tipDir;
                    $scope.direccionDASel.nomDir = direcciones[1].nomDir;
                    $scope.direccionDASel.depDir = direcciones[1].depDir;
                    $scope.direccionDASel.proDir = direcciones[1].proDir;
                    $scope.direccionDASel.disDir = direcciones[1].disDir;
                    $scope.direccionDASel.nomZon = direcciones[1].nomZon;
                    $scope.direccionDASel.desRef = direcciones[1].desRef;
                    break;
            }
        }
        listarDirecciones();
        
        $scope.trabajadorSel = {
            traId: datosPersonales.trabajador.traId,
            traCon: datosPersonales.trabajador.traCon 
        };
        
        $scope.asegurado = datosPersonales.ficha.autEss!=="               "?true:false;
        
        $scope.fichaEscalafonariaSel = {
            ficEscId: datosPersonales.ficha.ficEscId,
            sisPen: datosPersonales.ficha.sisPen,
            nomAfp: datosPersonales.ficha.nomAfp,
            estAseEss: datosPersonales.ficha.autEss!=="               "?true:false,
            autEss: datosPersonales.ficha.autEss==="               "?"":datosPersonales.ficha.autEss,
            codCuspp: datosPersonales.ficha.codCuspp,
            fecIngAfp: convertirFechaStr(datosPersonales.ficha.fecIngAfp),
            perDis: datosPersonales.ficha.perDis, 
            regCon: datosPersonales.ficha.regCon,
            gruOcu: datosPersonales.ficha.gruOcu
        };
        
        $scope.boolSalud = {
            valS: ($scope.fichaEscalafonariaSel.estAseEss)===true? 'SI' : 'NO'
        };
        
        changeSistemaPensionesState($scope.fichaEscalafonariaSel.sisPen);
        changeDishabilityState($scope.fichaEscalafonariaSel.perDis);
        
        var tiposFormacion = [
            {id: "1", nom: "Estudio Básico"}, 
            {id: "2", nom: "Superior"}, 
            {id: "3", nom: "Postgrado"},
            {id: "4", nom: "Otros"}
        ];
        var nivelesAcademicos = [
            {id: "1", nom: "Primaria", pad: "1"}, 
            {id: "2", nom: "Secundaria", pad: "1"}, 
            {id: "3", nom: "Superior", pad: "2"},
            {id: "4", nom: "Tecnica", pad: "2"},
            {id: "5", nom: "Bachiller", pad: "2"},
            {id: "6", nom: "Titulo", pad: "2"},
            {id: "7", nom: "Magister", pad: "3"},
            {id: "8", nom: "Doctorado", pad: "3"},
            {id: "9", nom: "Otros", pad: "4"}
        ];
        
        var tiposEstCom = [
            {id: "1", nom: "Informatica"}, 
            {id: "2", nom: "Idiomas"}, 
            {id: "3", nom: "Certificación"},
            {id: "4", nom: "Diplomado"},
            {id: "5", nom: "Especialización"}, 
            {id: "6", nom: "Otros"}
        ];
        
        var nivelesEstCom = [
            {id: "N", nom: "Ninguno"}, 
            {id: "B", nom: "Básico"}, 
            {id: "I", nom: "Intermedio"},
            {id: "A", nom: "Avanzado"}
        ];
   
        var tiposDesplazamientos = [
            {id: "1", nom: "Designación"},
            {id: "2", nom: "Rotación"},
            {id: "3", nom: "Reasignación"}, 
            {id: "4", nom: "Destaque"}, 
            {id: "5", nom: "Permuta"}, 
            {id: "6", nom: "Encargo"},
            {id: "7", nom: "Comisión de servicio"}, 
            {id: "8", nom: "Transferencia"}, 
            {id: "9", nom: "Otros"}
        ];
        
        var motivos = [
            {id: "1", nom: "Mérito"},
            {id: "2", nom: "Felicitación"},
            {id: "3", nom: "25 años de servicio"}, 
            {id: "4", nom: "30 años de servicio"}, 
            {id: "5", nom: "Luto y sepelio"},
            {id: "6", nom: "Otros"}
        ];
        
        var escalas = [
            {id:"I", title: "I"}, 
            {id:"II", title: "II"}, 
            {id:"III", title: "III"}, 
            {id:"IV", title: "IV"}, 
            {id:"V", title: "V"}, 
            {id:"VI", title: "VI"},
            {id:"VII", title: "VII"}, 
            {id:"VIII", title: "VIII"}
        ];
        
        $http.get('../recursos/json/departamentos.json').success(function (data) {
            $scope.departamentosLugNac = data;
            $scope.departamentosDirDNI = data;
            $scope.departamentosDirDA = data;
        });

        $scope.loadProLugNac = function () {
            $http.get('../recursos/json/provincias.json').success(function (data) {
                var dep = $scope.departamentosLugNac.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.personaSel.depNac;
                })[0];
                $scope.provinciasLugNac = data[dep.id_ubigeo];
                $scope.distritosLugNac = [];
            });
        };

        $scope.loadDisLugNac = function () {
            $http.get('../recursos/json/distritos.json').success(function (data) {
                var pro = $scope.provinciasLugNac.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.personaSel.proNac;
                })[0];
                $scope.distritosLugNac = data[pro.id_ubigeo];
            });
        };

        $scope.loadProDirDNI = function () {
            $http.get('../recursos/json/provincias.json').success(function (data) {
                var dep = $scope.departamentosDirDNI.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.direccionDNISel.depDir;
                })[0];
                $scope.provinciasDirDNI = data[dep.id_ubigeo];
                $scope.distritosDirDNI = [];
            });
        };

        $scope.loadDisDirDNI = function () {
            $http.get('../recursos/json/distritos.json').success(function (data) {
                var pro = $scope.provinciasDirDNI.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.direccionDNISel.proDir;
                })[0];
                $scope.distritosDirDNI = data[pro.id_ubigeo];
            });
        };

        $scope.loadProDirDA = function () {
            $http.get('../recursos/json/provincias.json').success(function (data) {
                var dep = $scope.departamentosDirDA.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.direccionDASel.depDir;
                })[0];
                $scope.provinciasDirDA = data[dep.id_ubigeo];
                $scope.distritosDirDA = [];
            });
        };

        $scope.loadDisDirDA = function () {
            $http.get('../recursos/json/distritos.json').success(function (data) {
                var pro = $scope.provinciasDirDA.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.direccionDASel.proDir;
                })[0];
                $scope.distritosDirDA = data[pro.id_ubigeo];
            });
        };
       
        //Funciones para listar los datos de tablas
        $scope.listarParientes = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('familiares', 1, 'listarParientes');
            request.setData($scope.trabajadorSel);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingModParientes.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModParientes.dataset);
                $rootScope.tablaModParientes.settings($rootScope.settingModParientes);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.listarFormacionesEducativas = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('formacion_educativa', 1, 'listarFormacionesEducativas');
            request.setData($scope.fichaEscalafonariaSel);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item){
                    item.estConDes = item.estCon===""?"No registrado":(item.estCon?"Si":"No");
                    item.tipFor = buscarContenido(tiposFormacion, 'id', 'nom', item.tip);
                    item.nivAca = buscarContenido(nivelesAcademicos, 'id', 'nom', item.niv);
                });
                $rootScope.settingModForEdu.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModForEdu.dataset);
                $rootScope.tablaModForEdu.settings($rootScope.settingModForEdu);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.listarColegiaturas = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('colegiatura', 1, 'listarColegiaturas');
            request.setData($scope.fichaEscalafonariaSel);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item){
                    item.conRegDes = item.conReg?"Habilitado":"No habilitado";
                });
                $rootScope.settingModColegiaturas.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModColegiaturas.dataset);
                $rootScope.tablaModColegiaturas.settings($rootScope.settingModColegiaturas);
            }, function (data) {
                console.info(data);
            });
        };

        $scope.listarEstudiosComplementarios = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('estudio_complementario', 1, 'listarEstudiosComplementarios');
            request.setData($scope.fichaEscalafonariaSel);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item){
                    item.tipDes = buscarContenido(tiposEstCom, 'id', 'nom', item.tip);
                    item.nivDes = buscarContenido(nivelesEstCom, 'id', 'nom', item.niv);
                });
                $rootScope.settingModEstCom.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModEstCom.dataset);
                $rootScope.tablaModEstCom.settings($rootScope.settingModEstCom);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.listarEstudiosPostgrado = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('estudio_postgrado', 1, 'listarEstudiosPostgrado');
            request.setData($scope.fichaEscalafonariaSel);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item){
                    item.tipDes = buscarContenido(tiposEstPos, 'id', 'title', item.tip);
                });
                $rootScope.settingModEstPos.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModEstPos.dataset);
                $rootScope.tablaModEstPos.settings($rootScope.settingModEstPos);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.listarExposiciones = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('exposicion_ponencia', 1, 'listarExposiciones');
            request.setData($scope.fichaEscalafonariaSel);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingModExpPon.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModExpPon.dataset);
                $rootScope.tablaModExpPon.settings($rootScope.settingModExpPon);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.listarPublicaciones = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('publicacion', 1, 'listarPublicaciones');
            request.setData($scope.fichaEscalafonariaSel);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingModPublicaciones.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModPublicaciones.dataset);
                $rootScope.tablaModPublicaciones.settings($rootScope.settingModPublicaciones);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.listarDesplazamientos = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('desplazamiento', 1, 'listarDesplazamientos');
            request.setData($scope.fichaEscalafonariaSel);
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item){
                    item.tipDes = buscarContenido(tiposDesplazamientos, 'id', 'nom', item.tip);
                });
                $rootScope.settingModDesplazamientos.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModDesplazamientos.dataset);
                $rootScope.tablaModDesplazamientos.settings($rootScope.settingModDesplazamientos);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.listarAscensos = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('ascenso', 1, 'listarAscensos');
            request.setData($scope.fichaEscalafonariaSel);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item){
                    item.esc = buscarContenido(escalas, 'id', 'title', item.esc);
                });
                $rootScope.settingModAscensos.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModAscensos.dataset);
                $rootScope.tablaModAscensos.settings($rootScope.settingModAscensos);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.listarCapacitaciones = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('capacitacion', 1, 'listarCapacitaciones');
            request.setData($scope.fichaEscalafonariaSel);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingModCapacitaciones.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModCapacitaciones.dataset);
                $rootScope.tablaModCapacitaciones.settings($rootScope.settingModCapacitaciones);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.listarReconocimientos = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('reconocimientos', 1, 'listarReconocimientos');
            request.setData($scope.fichaEscalafonariaSel);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item){
                    item.motDes = buscarContenido(motivos, 'id', 'nom', item.mot);
                });
                $rootScope.settingModReconocimientos.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModReconocimientos.dataset);
                $rootScope.tablaModReconocimientos.settings($rootScope.settingModReconocimientos);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.listarDemeritos = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('demeritos', 1, 'listarDemeritos');
            request.setData($scope.fichaEscalafonariaSel);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item){
                    item.sepDes = item.sep?"Si":"No";
                });
                $rootScope.settingModDemeritos.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModDemeritos.dataset);
                $rootScope.tablaModDemeritos.settings($rootScope.settingModDemeritos);
            }, function (data) {
                console.info(data);
            });
        };
        
        //Actualizar Datos Personales
        $scope.actualizarDatosGenerales = function () {
            
            //Direcciones DNI
            $scope.direccionesToSend = [];
            $scope.direccionesToSend.push($scope.direccionDNISel);
            if($scope.statusDA){
                $scope.direccionesToSend.push($scope.direccionDASel);
            }
            else{
                $scope.direccionDASel.nomDir="";//Por si el domicilio actual coincide con el DNI(con datos)-> Limpiar caso "No"(DNISel).
                $scope.direccionDASel.depDir="";
                $scope.direccionDASel.proDir="";
                $scope.direccionDASel.disDir="";
                $scope.direccionDASel.nomZon="";
                $scope.direccionDASel.desRef="";
                $scope.direccionesToSend.push($scope.direccionDASel);
            }
            
            var request = crud.crearRequest('datos_personales', 1, 'actualizarDatosPersonales');
            request.setData({persona: $scope.personaSel, trabajador: $scope.trabajadorSel, fichaEscalafonaria:$scope.fichaEscalafonariaSel, direcciones: $scope.direccionesToSend, orgId: $rootScope.usuMaster.organizacion.organizacionID});
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                
                $scope.personaSel.apePat = response.data.persona.apePat;
                $scope.personaSel.apeMat = response.data.persona.apeMat;
                $scope.personaSel.nom = response.data.persona.nom;
                $scope.personaSel.dni = response.data.persona.dni;
                $scope.personaSel.fecNac = convertirFechaStr(response.data.persona.fecNac);
                $scope.personaSel.num1 = response.data.persona.num1;
                $scope.personaSel.num2 = response.data.persona.num2;
                $scope.personaSel.fij = response.data.persona.fij;
                $scope.personaSel.email = response.data.persona.email;
                $scope.personaSel.sex = response.data.persona.sex;
                $scope.personaSel.estCiv = response.data.persona.estCiv;
                $scope.personaSel.depNac = response.data.persona.depNac;
                $scope.personaSel.proNac = response.data.persona.proNac;
                $scope.personaSel.disNac = response.data.persona.disNac;
                
                $scope.trabajadorSel.traCon = response.data.trabajador.traCon;
                
                $scope.fichaEscalafonariaSel.autEss = response.data.ficha.autEss==="               "?"":response.data.ficha.autEss;
                $scope.fichaEscalafonariaSel.sisPen = response.data.ficha.sisPen;
                $scope.fichaEscalafonariaSel.nomAfp = response.data.ficha.nomAfp;
                $scope.fichaEscalafonariaSel.codCuspp = response.data.ficha.codCuspp;
                $scope.fichaEscalafonariaSel.fecIngAfp = convertirFechaStr(response.data.ficha.fecIngAfp);
                $scope.fichaEscalafonariaSel.perDis = response.data.ficha.perDis;
                $scope.fichaEscalafonariaSel.regCon = response.data.ficha.regCon;
                $scope.fichaEscalafonariaSel.gruOcu = response.data.ficha.gruOcu;
                
                listarDirecciones();
                /*switch(response.data.direcciones.lenght){
                    case 1: $scope.direccionDNISel.tipDir = response.data.direcciones[0].tipDir;
                            $scope.direccionDNISel.nomDir = response.data.direcciones[0].nomDir;
                            $scope.direccionDNISel.depDir = response.data.direcciones[0].depDir;
                            $scope.direccionDNISel.proDir = response.data.direcciones[0].proDir;
                            $scope.direccionDNISel.disDir = response.data.direcciones[0].disDir;
                            $scope.direccionDNISel.nomZon = response.data.direcciones[0].nomZon;
                            $scope.direccionDNISel.desRef = response.data.direcciones[0].desRef;
                            
                            break;
                    case 2: $scope.direccionDNISel.tipDir = response.data.direcciones[0].tipDir;
                            $scope.direccionDNISel.nomDir = response.data.direcciones[0].nomDir;
                            $scope.direccionDNISel.depDir = response.data.direcciones[0].depDir;
                            $scope.direccionDNISel.proDir = response.data.direcciones[0].proDir;
                            $scope.direccionDNISel.disDir = response.data.direcciones[0].disDir;
                            $scope.direccionDNISel.nomZon = response.data.direcciones[0].nomZon;
                            $scope.direccionDNISel.desRef = response.data.direcciones[0].desRef;
                            
                            $scope.direccionDASel.tipDir = response.data.direcciones[1].tipDir;
                            $scope.direccionDASel.nomDir = response.data.direcciones[1].nomDir;
                            $scope.direccionDASel.depDir = response.data.direcciones[1].depDir;
                            $scope.direccionDASel.proDir = response.data.direcciones[1].proDir;
                            $scope.direccionDASel.disDir = response.data.direcciones[1].disDir;
                            $scope.direccionDASel.nomZon = response.data.direcciones[1].nomZon;
                            $scope.direccionDASel.desRef = response.data.direcciones[1].desRef;
                            break;
                }*/
                
            }, function (data) {
                console.info(data);
            });
        };
        
        //Funciones para agregar
        $scope.showNuevoPariente = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarPariente.html",
                controller: "agregarNuevoParienteCtrl",
                inputs: {
                    title: "Nuevo Pariente",
                    personaId: $scope.personaSel.perId,
                    sexo: $scope.sexo,
                    actDni: datosPersonales.persona.dni,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };
        
        $scope.showNuevaFormacionEducativa = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarFormacionEducativa.html",
                controller: "agregarNuevaFormacionEducativaCtrl",
                inputs: {
                    title: "Nueva Formacion Educativa",
                    tipFors: tiposFormacion,
                    nivAcas: nivelesAcademicos,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };
        
        $scope.showNuevoEstudioComplementario = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarEstudioComplementario.html",
                controller: "agregarNuevoEstudioComplementarioCtrl",
                inputs: {
                    title: "Nuevo Estudio Complementario",
                    tipEstComs: tiposEstCom,
                    nivEstComs: nivelesEstCom,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.showNuevaExposicion = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarExposicion.html",
                controller: "agregarNuevaExposicionCtrl",
                inputs: {
                    title: "Nueva Exposicion y/o Ponencia",
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.showNuevaPublicacion = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarPublicacion.html",
                controller: "agregarNuevaPublicacionCtrl",
                inputs: {
                    title: "Nueva Publicacion",
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.showNuevoDesplazamiento = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarDesplazamiento.html",
                controller: "agregarNuevoDesplazamientoCtrl",
                inputs: {
                    title: "Nueva Desplazamiento",
                    tipDespls: tiposDesplazamientos,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.showNuevaColegiatura = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarColegiatura.html",
                controller: "agregarNuevaColegiaturaCtrl",
                inputs: {
                    title: "Nueva Colegiatura",
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };
        
        $scope.showNuevoAscenso = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarAscenso.html",
                controller: "agregarNuevoAscensoCtrl",
                inputs: {
                    title: "Nueva Ascenso",
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };
        
        $scope.showNuevoReconocimiento = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarReconocimiento.html",
                controller: "agregarNuevoReconocimientoCtrl",
                inputs: {
                    title: "Nueva Reconocimiento",
                    mots: motivos,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };
        
        $scope.showNuevoEstudioPostgrado = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarEstudioPostgrado.html",
                controller: "agregarNuevoEstudioPostgradoCtrl",
                inputs: {
                    title: "Nueva Estudio Postgrado",
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };
        
        $scope.showNuevoDemerito = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarDemerito.html",
                controller: "agregarNuevoDemeritoCtrl",
                inputs: {
                    title: "Nueva Demerito",
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };
        
        $scope.showNuevaCapacitacion = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarCapacitacion.html",
                controller: "agregarNuevaCapacitacionCtrl",
                inputs: {
                    title: "Nueva capacitación",
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        //Funciones para editar
        $scope.prepararEditarPariente = function (p) {
            var parienteSel = JSON.parse(JSON.stringify(p));
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarPariente.html",
                controller: "editarParienteCtrl",
                inputs: {
                    title: "Editar datos del pariente",
                    pariente: parienteSel,
                    personaId: $scope.personaSel.perId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarFormacionEducativa = function (fe) {
            var formacionEducativaSel = JSON.parse(JSON.stringify(fe));
            console.log(formacionEducativaSel);
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarFormacionEducativa.html",
                controller: "editarFormacionEducativaCtrl",
                inputs: {
                    title: "Editar datos de Formacion Educativa",
                    formacionEducativa: formacionEducativaSel,
                    tipFors: tiposFormacion,
                    nivAcas: nivelesAcademicos,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });

        };
        
        $scope.prepararEditarColegiatura = function (c) {
            var colegiaturaSel = JSON.parse(JSON.stringify(c));
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarColegiatura.html",
                controller: "editarColegiaturaCtrl",
                inputs: {
                    title: "Editar datos de Colegiatura",
                    colegiatura: colegiaturaSel,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });

        };
        
        $scope.prepararEditarEstudioComplementario = function (e) {
            var estComSel = JSON.parse(JSON.stringify(e));
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarEstudioComplementario.html",
                controller: "editarEstudioComplementarioCtrl",
                inputs: {
                    title: "Editar datos de Estudio Complementario",
                    estudioComplementario: estComSel,
                    tipEstComs: tiposEstCom,
                    nivEstComs: nivelesEstCom,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });

        };
        
        $scope.prepararEditarEstudioPostgrado = function (e) {
            var estPosSel = JSON.parse(JSON.stringify(e));
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarEstudioPostgrado.html",
                controller: "editarEstudioPostgradoCtrl",
                inputs: {
                    title: "Editar datos de Estudio de Postgrado",
                    estudioPostgrado: estPosSel,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarExposicion = function (e) {
            var expSel = JSON.parse(JSON.stringify(e));
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarExposicion.html",
                controller: "editarExposicionCtrl",
                inputs: {
                    title: "Editar datos de Exposicion",
                    exposicion: expSel,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarPublicacion = function (p) {
            var pubSel = JSON.parse(JSON.stringify(p));
            console.log(pubSel);
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarPublicacion.html",
                controller: "editarPublicacionCtrl",
                inputs: {
                    title: "Editar datos de Publicacion",
                    publicacion: pubSel,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarDesplazamiento = function (d) {
            var desSel = JSON.parse(JSON.stringify(d));
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarDesplazamiento.html",
                controller: "editarDesplazamientoCtrl",
                inputs: {
                    title: "Editar datos de Desplazamiento",
                    desplazamiento: desSel,
                    tipDespls: tiposDesplazamientos,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarAscenso = function (a) {
            var ascSel = JSON.parse(JSON.stringify(a));
            console.log(ascSel);
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarAscenso.html",
                controller: "editarAscensoCtrl",
                inputs: {
                    title: "Editar datos de Ascenso",
                    ascenso: ascSel,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarCapacitacion = function (c) {
            var capSel = JSON.parse(JSON.stringify(c));
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarCapacitacion.html",
                controller: "editarCapacitacionCtrl",
                inputs: {
                    title: "Editar datos de Capacitacion",
                    capacitacion: capSel,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarReconocimiento = function (r) {
            var recSel = JSON.parse(JSON.stringify(r));
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarReconocimiento.html",
                controller: "editarReconocimientoCtrl",
                inputs: {
                    title: "Editar datos de Reconocimiento",
                    reconocimiento: recSel,
                    mots: motivos,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.prepararEditarDemerito = function (d) {
            var demSel = JSON.parse(JSON.stringify(d));
            console.log(demSel);
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarDemerito.html",
                controller: "editarDemeritoCtrl",
                inputs: {
                    title: "Editar datos de Demerito",
                    demerito: demSel,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        //Funciones para eliminar
        $scope.eliminarPariente = function(p){   
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el pariente?", function () {
                var request = crud.crearRequest('familiares', 1, 'eliminarFamiliar');
                request.setData({parId: p.parId, perId: $scope.personaSel.perId});
                
                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModParientes.settings().dataset, function(item){
                            return p === item;
                        });
                        $rootScope.tablaModParientes.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    }else {
                            modal.mensaje("ERROR",response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR","El servidor no responde");
                });
            }, '400'); 
        };

        $scope.eliminarFormacionEducativa = function (fe) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la formación educativa?", function () {
                var request = crud.crearRequest('formacion_educativa', 1, 'eliminarFormacionEducativa');
                request.setData({forEduId: fe.forEduId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModForEdu.settings().dataset, function (item) {
                            return fe === item;
                        });
                        $rootScope.tablaModForEdu.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarColegiatura = function (c) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la colegiatura?", function () {
                var request = crud.crearRequest('colegiatura', 1, 'eliminarColegiatura');
                request.setData({colId: c.colId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModColegiaturas.settings().dataset, function (item) {
                            return c === item;
                        });
                        $rootScope.tablaModColegiaturas.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarEstudioComplementario = function (ec) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el estudio complementario?", function () {
                var request = crud.crearRequest('estudio_complementario', 1, 'eliminarEstudioComplementario');
                request.setData({estComId: ec.estComId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModEstCom.settings().dataset, function (item) {
                            return ec === item;
                        });
                        $rootScope.tablaModEstCom.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarEstudioPostgrado = function (ep) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el estudio de postgrado?", function () {
                var request = crud.crearRequest('estudio_postgrado', 1, 'eliminarEstudioPostgrado');
                request.setData({estPosId: ep.estPosId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModEstPos.settings().dataset, function (item) {
                            return ep === item;
                        });
                        $rootScope.tablaModEstPos.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarExposicion = function (ep) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la exposicion y/o ponencia?", function () {
                var request = crud.crearRequest('exposicion_ponencia', 1, 'eliminarExposicion');
                request.setData({expId: ep.expId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModExpPon.settings().dataset, function (item) {
                            return ep === item;
                        });
                        $rootScope.tablaModExpPon.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarPublicacion = function (p) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la publicacion?", function () {
                var request = crud.crearRequest('publicacion', 1, 'eliminarPublicacion');
                request.setData({pubId: p.pubId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModPublicaciones.settings().dataset, function (item) {
                            return p === item;
                        });
                        $rootScope.tablaModPublicaciones.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarDesplazamiento = function (d) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el desplazamiento?", function () {
                var request = crud.crearRequest('desplazamiento', 1, 'eliminarDesplazamiento');
                request.setData({desId: d.desId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModDesplazamientos.settings().dataset, function (item) {
                            return d === item;
                        });
                        $rootScope.tablaModDesplazamientos.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarAscenso = function (a) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el ascenso?", function () {
                var request = crud.crearRequest('ascenso', 1, 'eliminarAscenso');
                request.setData({ascId: a.ascId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModAscensos.settings().dataset, function (item) {
                            return a === item;
                        });
                        $rootScope.tablaModAscensos.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarCapacitacion = function (c) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la capacitacion?", function () {
                var request = crud.crearRequest('capacitacion', 1, 'eliminarCapacitacion');
                request.setData({capId: c.capId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModCapacitaciones.settings().dataset, function (item) {
                            return c === item;
                        });
                        $rootScope.tablaModCapacitaciones.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarReconocimiento = function (r) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la reconocimiento?", function () {
                var request = crud.crearRequest('reconocimientos', 1, 'eliminarReconocimiento');
                console.log(r);
                request.setData({recId: r.recId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModReconocimientos.settings().dataset, function (item) {
                            return r === item;
                        });
                        $rootScope.tablaModReconocimientos.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarDemerito = function (d) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la demerito?", function () {
                var request = crud.crearRequest('demeritos', 1, 'eliminarDemerito');
                request.setData({demId: d.demId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModDemeritos.settings().dataset, function (item) {
                            return d === item;
                        });
                        $rootScope.tablaModDemeritos.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };
        
    }]);
//Agregar
app.controller('agregarNuevoParienteCtrl', ['$scope', '$rootScope', '$element','title', 'personaId', 'sexo', 'actDni', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, personaId, sexo, actDni, fichaEscalafonariaId, close, crud, modal, ModalService) {
        //variable que servira para registrar un nuevo pariente
        $scope.nuevoPariente = {perId:personaId, tipoPariente: "", parApePat: "", parApeMat: "", parNom: "", parDni: "", parFecNac:"", parSex: "", parNum1: "", parNum2: "", parFij: "", parEmail: ""};
        $scope.nuevoLegajoPar = {ficEscId:fichaEscalafonariaId ,nom:"", archivo:"", des:"DNI-Pariente", url:"", catLeg:"2", subCat:"1", codAspOri:""};
        $scope.title = title;
        $scope.sexoPariente = sexo;
        $scope.tipoParentesco = [];

        listarTipoParentesco();
        function listarTipoParentesco() {
            //preparamos un objeto request
            var request = crud.crearRequest('familiares', 1, 'listarTipoParentesco');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las funciones de exito y error
            crud.listar("/sistema_escalafon", request, function (data) {
                $scope.tipoParentesco = data.data;
            }, function (data) {
                console.info(data);
            });
        };

        $scope.boolCampos = true;
        $scope.estadoPar = true;
        $scope.mostrar = true;

        $scope.agregarPariente = function () {
            var request = crud.crearRequest('familiares', 1, 'agregarPariente');
            request.setData($scope.nuevoPariente);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {   
                    console.log(response);
                    response.data.parentesco = buscarContenido($scope.tipoParentesco, 'tpaId', 'tpaDes', response.data.tipoParienteId);
                    insertarElemento($rootScope.settingModParientes.dataset, response.data);
                    $rootScope.tablaModParientes.reload();
                    
                    if ($scope.nuevoLegajoPar.archivo){
                        $scope.nuevoLegajoPar.codAspOri = response.data.parId;
                        console.log($scope.nuevoLegajoPar);
                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoPar);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                    
                }
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.reiniciarDatosP = function () {
            $scope.mostrar = true;
            $scope.boolCampos = true;
            $scope.estadoPar = true;
        };
        
        $scope.buscarPersona = function () {
            if ($scope.nuevoPariente.parDni === '') {
                modal.mensaje("ALERTA", "Ingrese DNI");
                return;
            }
            if ($scope.nuevoPariente.parDni === actDni) {
                modal.mensaje("ALERTA", "Usted no puede agregarse como pariente");
                return;
            }
            var request = crud.crearRequest('familiares', 1, 'buscarPersonaPariente');
            request.setData({
                dni: $scope.nuevoPariente.parDni,
                perId: personaId
            });
            crud.listar("/sistema_escalafon", request, function (data) {
                modal.mensaje("CONFIRMACION", data.responseMsg);
                console.log(data);
                $scope.mostrar = false;
                if (!data.responseSta) {//No existe la Persona 
                    $scope.boolCampos = false;
                    $scope.nuevoPariente.parDni = request.data.dni;
                    $scope.nuevoPariente.parDni2 = request.data.dni;
                    $scope.nuevoPariente.perId = request.data.perId;
                    $scope.nuevoPariente.tipoPariente = "";
                    $scope.nuevoPariente.parApePat = "";
                    $scope.nuevoPariente.parApeMat = "";
                    $scope.nuevoPariente.parNom = "";
                    $scope.nuevoPariente.parSex = "";
                    $scope.nuevoPariente.parFij = "";
                    $scope.nuevoPariente.parNum1 = "";
                    $scope.nuevoPariente.parNum2 = "";
                    $scope.nuevoPariente.parEmail = "";
                    $scope.nuevoPariente.parFecNac = "";
                    $scope.nuevoPariente.estado = 'A';
                    $scope.nuevoPariente.caso = 0;
                    $scope.estadoPar = false;
                } else {
                    if (data.data.persona.caso === 1)//Solo agregar Parentesco
                    {
                        $scope.boolCampos = true;
                        $scope.nuevoPariente = {
                            parId: data.data.persona.parId,
                            tipoPariente: "",
                            parApePat: data.data.persona.apePat,
                            parApeMat: data.data.persona.apeMat,
                            parNom: data.data.persona.nom,
                            parSex: data.data.persona.sex,
                            parFij: data.data.persona.fij,
                            parNum1: data.data.persona.num1,
                            parNum2: data.data.persona.num2,
                            parEmail: data.data.persona.email,
                            parFecNac: convertirFechaStr(data.data.persona.fecNac),
                            caso: data.data.persona.caso
                        };
                        $scope.nuevoPariente.parDni = request.data.dni;
                        $scope.nuevoPariente.parDni2 = request.data.dni;
                        $scope.nuevoPariente.perId = request.data.perId;
                        $scope.estadoPar = false;
                    }
                    if (data.data.persona.caso === 2)//Ya tiene parentesco con el DNI ingresado
                    {
                        $scope.boolCampos = true;
                        $scope.nuevoPariente = {
                            tipoPariente: data.data.persona.tipoPariente, //Recibir de backend
                            parApePat: data.data.persona.apePat,
                            parApeMat: data.data.persona.apeMat,
                            parNom: data.data.persona.nom,
                            parSex: data.data.persona.sex,
                            parFij: data.data.persona.fij,
                            parNum1: data.data.persona.num1,
                            parNum2: data.data.persona.num2,
                            parEmail: data.data.persona.email,
                            parFecNac: convertirFechaStr(data.data.persona.fecNac),
                            retJud: data.data.persona.retJud,
                            caso: data.data.persona.caso
                        };
                        $scope.nuevoPariente.parDni = request.data.dni;
                        $scope.nuevoPariente.parDni2 = request.data.dni;
                        $scope.estadoPar = true;
                    }
                }
            }, function (data) {
                console.info(data);
            });

        };
        
    }]);
app.controller('agregarNuevaFormacionEducativaCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'tipFors', 'nivAcas', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, tipFors, nivAcas, close, crud, modal, ModalService) {
        $scope.title = title;
        $scope.tipFors = tipFors;
        $scope.nivAcas = nivAcas;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevaForEdu = {ficEscId:fichaEscalafonariaId, tip:"", niv:"", numTit:"", esp:"", estCon:false, fecExp:"", cenEst:""};
        $scope.nuevoLegajoForEdu = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", des:" Titulo y/o Certificado", url:"", catLeg:"3", subCat:"1", codAspOri:""};
        
        
        $scope.loadTipoFormacion = function() {
            var form = $scope.nuevaForEdu.tip;
            var datos = [];
            $scope.comNiveles = {};
                for (var i in nivAcas) {
                if(nivAcas[i]["pad"]==form){
                    datos.push(nivAcas[i]);
                }
            }
            $scope.comNiveles =datos;
        }
        
        $scope.agregarFormacionEducativa = function () {
            var request = crud.crearRequest('formacion_educativa', 1, 'agregarFormacionEducativa');
            request.setData($scope.nuevaForEdu);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.tipFor = buscarContenido($scope.tipFors,'id','nom',response.data.tip);
                    response.data.nivAca = buscarContenido($scope.nivAcas,'id','nom',response.data.niv);
                    response.data.estConDes = response.data.estCon?"Si":"No";
                    insertarElemento($rootScope.settingModForEdu.dataset, response.data);
                    $rootScope.tablaModForEdu.reload();
                    
                    if($scope.nuevoLegajoForEdu.archivo){
                        $scope.nuevoLegajoForEdu.codAspOri = response.data.forEduId;
                        var request = crud.crearRequest('sistema_escalafon', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoForEdu);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
app.controller('agregarNuevaColegiaturaCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevaColegiatura = {ficEscId:fichaEscalafonariaId, nomColPro:"", numRegCol:"", conReg:false};
        $scope.nuevoLegajoCol = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", url:"", des:"Constancia", catLeg:"3", subCat:"2", codAspOri:""};
        
        $scope.agregarColegiatura = function () {
            var request = crud.crearRequest('colegiatura', 1, 'agregarColegiatura');
            request.setData($scope.nuevaColegiatura);
            console.log(JSON.stringify($scope.nuevaColegiatura));
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.conRegDes = response.data.conReg?"Habilitado":"No habilitado";
                    insertarElemento($rootScope.settingModColegiaturas.dataset, response.data);
                    $rootScope.tablaModColegiaturas.reload();
                    
                    if($scope.nuevoLegajoCol.archivo){
                        $scope.nuevoLegajoCol.codAspOri = response.data.colId;
                        
                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoCol);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                    
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
app.controller('agregarNuevoEstudioComplementarioCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'tipEstComs', 'nivEstComs', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, tipEstComs, nivEstComs, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevoEstCom = {ficEscId:fichaEscalafonariaId, tip:"", des:"", niv:"", insCer:"", tipPar:"", fecIni:"", fecTer:"", horLec:0};
        $scope.nuevoLegajoEC = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", des:"Certificado y/o Titulo", url:"", catLeg:"3", subCat:"3", codAspOri:""};
        $scope.tiposEstCom = tipEstComs;
        $scope.nivsEstCom = nivEstComs;
   
        $scope.agregarEstudioComplementario = function () {
            var request = crud.crearRequest('estudio_complementario', 1, 'agregarEstudioComplementario');
            request.setData($scope.nuevoEstCom);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    
                    response.data.tipDes= buscarContenido($scope.tiposEstCom, 'id','nom', response.data.tip);
                    response.data.nivDes= buscarContenido($scope.nivsEstCom, 'id','nom', response.data.niv);
                    
                    insertarElemento($rootScope.settingModEstCom.dataset, response.data);
                    $rootScope.tablaModEstCom.reload();
                    
                    if ($scope.nuevoLegajoEC.archivo){
                        $scope.nuevoLegajoEC.codAspOri = response.data.estComId;
                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoEC);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
app.controller('agregarNuevoEstudioPostgradoCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevoEstPos = {ficEscId:fichaEscalafonariaId, tip:"", des:"", niv:"", insCer:"", tipPar:"", fecIniEst:"", fecTerEst:"", horLec:""};
        $scope.nuevoLegajoEP = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", des:"Titulo", url:"", catLeg:"3", subCat:"4", codAspOri: ""};
        $scope.tiposEstPos = [{id: "1", title: "Maestria"}, {id: "2", title: "Doctorado"}];
   
        $scope.agregarEstudioPostgrado= function () {
            var request = crud.crearRequest('estudio_postgrado', 1, 'agregarEstudioPostgrado');
            request.setData($scope.nuevoEstPos);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    var auxTip = buscarObjeto($scope.tiposEstPos,"id",response.data.tip);
                    response.data.tip = auxTip.title;
                    insertarElemento($rootScope.settingModEstPos.dataset, response.data);
                    $rootScope.tablaModEstPos.reload();
                    
                    if ($scope.nuevoLegajoEP.archivo){
                        $scope.nuevoLegajoEP.codAspOri = response.data.estPosId;
                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoEP);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
app.controller('agregarNuevaExposicionCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevaExpPon = {ficEscId:fichaEscalafonariaId, des:"", insOrg:"", tipPar:"", fecIni:"", fecTer:"", horLec:0};
        $scope.nuevoLegajoExp = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", des:"Certificado de participación", url:"", catLeg:"3", subCat:"5", codAspOri:""};
        
        $scope.agregarExposicion = function () {
            var request = crud.crearRequest('exposicion_ponencia', 1, 'agregarExposicion');
            request.setData($scope.nuevaExpPon);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    insertarElemento($rootScope.settingModExpPon.dataset, response.data);
                    $rootScope.tablaModExpPon.reload();
                    
                    if ($scope.nuevoLegajoExp.archivo){
                        $scope.nuevoLegajoExp.codAspOri = response.data.expId;
                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoExp);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
app.controller('agregarNuevaPublicacionCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevaPublicacion = {ficEscId:fichaEscalafonariaId, nomEdi:"", tipPub:"", titPub:"", graPar:"", lug:"", fecPub:"", numReg:""};
        $scope.nuevoLegajoPub = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", url:"", des:"Constancia", catLeg:"3", subCat:"6", codAspOri:""};
        
        $scope.agregarPublicacion = function () {
            var request = crud.crearRequest('publicacion', 1, 'agregarPublicacion');
            request.setData($scope.nuevaPublicacion);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    insertarElemento($rootScope.settingModPublicaciones.dataset, response.data);
                    $rootScope.tablaModPublicaciones.reload();
                    
                    if ($scope.nuevoLegajoPub.archivo){
                        $scope.nuevoLegajoPub.codAspOri = response.data.pubId;
                        
                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoPub);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
app.controller('agregarNuevoDesplazamientoCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'tipDespls', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, tipDespls, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevoDesplazamiento = {ficEscId:fichaEscalafonariaId, tip:"", numRes:"", fecRes:"", insEdu:"", car:"", jorLab:"", fecIni:"", fecTer:""};
        $scope.nuevoLegajoDes = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", des:"Resolución", url:"", catLeg:"4", subCat:"1", cosAspOri:""};
        
        $scope.tiposDesplazamientos = tipDespls;
        
        $scope.agregarDesplazamiento = function () {
            var request = crud.crearRequest('desplazamiento', 1, 'agregarDesplazamiento');
            request.setData($scope.nuevoDesplazamiento);
            console.log(JSON.stringify($scope.nuevoDesplazamiento));
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.tipDes= buscarContenido($scope.tiposDesplazamientos, 'id','nom', response.data.tip);
                    insertarElemento($rootScope.settingModDesplazamientos.dataset, response.data);
                    $rootScope.tablaModDesplazamientos.reload();
                    
                    if ($scope.nuevoLegajoDes.archivo){
                        $scope.nuevoLegajoDes.cosAspOri = response.data.desId;
                        
                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoDes);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
app.controller('agregarNuevoAscensoCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevoAscenso = {ficEscId:fichaEscalafonariaId, numRes:"", fecRes:"", fecEfe:"", esc:""};
        $scope.nuevoLegajoAsc = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", des:"Resolución", url:"", catLeg:"4", subCat:"2", codAspOri:""};
        
        $scope.escalas = [
            {id:"I", title: "I"}, 
            {id:"II", title: "II"}, 
            {id:"III", title: "III"}, 
            {id:"IV", title: "IV"}, 
            {id:"V", title: "V"}, 
            {id:"VI", title: "VI"},
            {id:"VII", title: "VII"}, 
            {id:"VIII", title: "VIII"}
        ];
        
        $scope.agregarAscenso = function () {
            var request = crud.crearRequest('ascenso', 1, 'agregarAscenso');
            request.setData($scope.nuevoAscenso);
            console.log(JSON.stringify($scope.nuevoAscenso));
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.esc= buscarContenido($scope.escalas, 'id','title', response.data.esc);
                    insertarElemento($rootScope.settingModAscensos.dataset, response.data);
                    $rootScope.tablaModAscensos.reload();
                    
                    if ($scope.nuevoLegajoAsc.archivo){
                        $scope.nuevoLegajoAsc.codAspOri = response.data.ascId;
                        
                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoAsc);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
app.controller('agregarNuevaCapacitacionCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevaCapacitacion = {ficEscId:fichaEscalafonariaId, nom:"", tip:"", fec:"", cal:0, lug:""};
        $scope.nuevoLegajoCap = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", des:"Certificado", url:"", catLeg:"4", subCat:"3", codAspOri:""};
        

        $scope.agregarCapacitacion = function () {
            var request = crud.crearRequest('capacitacion', 1, 'agregarCapacitacion');
            request.setData($scope.nuevaCapacitacion);
            console.log(JSON.stringify($scope.nuevaCapacitacion));
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    insertarElemento($rootScope.settingModCapacitaciones.dataset, response.data);
                    $rootScope.tablaModCapacitaciones.reload();
                    
                    if ($scope.nuevoLegajoCap.archivo) {
                        $scope.nuevoLegajoCap.codAspOri = response.data.capId;
                        
                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoCap);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
app.controller('agregarNuevoReconocimientoCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'mots', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, mots, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevoReconocimiento = {ficEscId:fichaEscalafonariaId, mot:"", numRes:"", fecRes:"", entEmi:""};
        $scope.nuevoLegajoRec = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", des:"Resolucion", url:"", catLeg:"5", subCat:"1", codAspOri:""};
        
        $scope.motivos = mots;
                
        $scope.agregarReconocimiento = function () {
            var request = crud.crearRequest('reconocimientos', 1, 'agregarReconocimiento');
            request.setData($scope.nuevoReconocimiento);
            console.log(JSON.stringify($scope.nuevoReconocimiento));
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    
                    response.data.motDes= buscarContenido($scope.motivos, 'id','nom', response.data.mot);
                    insertarElemento($rootScope.settingModReconocimientos.dataset, response.data);
                    $rootScope.tablaModReconocimientos.reload();
                    
                    if ($scope.nuevoLegajoRec.archivo){
                        $scope.nuevoLegajoRec.codAspOri = response.data.recId;
                        
                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoRec);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                    /*var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                    request.setData($scope.nuevoLegajoRec);
                    crud.insertar("/sistema_escalafon", request, function (response) {
                        modal.mensaje("CONFIRMACION", response.responseMsg);
                    }, function (data) {
                        console.info(data);
                    });*/
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
app.controller('agregarNuevoDemeritoCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevoDemerito = {ficEscId:fichaEscalafonariaId, entEmi:"", numRes:"", fecRes:"", sep:false, fecIni:"", fecFin:"", mot:""};
        $scope.nuevoLegajoDem = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", url:"", des:"Resolución", catLeg:"6", subCat:"1", codAspOri:""};
        
        $scope.agregarDemerito = function () {
            var request = crud.crearRequest('demeritos', 1, 'agregarDemerito');
            request.setData($scope.nuevoDemerito);
            console.log(JSON.stringify($scope.nuevoDemerito));
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.sepDes = response.data.sep?"Si":"No";
                    insertarElemento($rootScope.settingModDemeritos.dataset, response.data);
                    $rootScope.tablaModDemeritos.reload();
                    
                    if ($scope.nuevoLegajoDem.archivo){
                        $scope.nuevoLegajoDem.codAspOri = response.data.demId;
                        
                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoDem);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
//Editar
app.controller('editarParienteCtrl', ['$rootScope','$scope', '$element', 'title', 'pariente', 'personaId', 'close', 'crud', 'modal', function ($rootScope ,$scope, $element, title, pariente, personaId, close, crud, modal) {
        $scope.title = title;
        
        $scope.sexo = [{id: "F", title: "Femenino"}, {id: "M", title: "Masculino"}];
        $scope.tipoPariente = [];
        
        function listarTipoParentesco() {
            var request = crud.crearRequest('familiares', 1, 'listarTipoParentesco');
            crud.listar("/sistema_escalafon", request, function (data) {
                $scope.tipoPariente = data.data;
            }, function (data) {
                console.info(data);
            });
        }
        ;
        listarTipoParentesco();
        
        
        $scope.parienteSel = {
            parId: pariente.parId,
            perId: personaId, 
            tipoParienteId: pariente.tipoParienteId, 
            parentesco: "",
            parApePat: pariente.parApePat, 
            parApeMat: pariente.parApeMat, 
            parNom: pariente.parNom, 
            parDni: pariente.parDni, 
            parFecNac: convertirFechaStr(pariente.parFecNac), 
            parSex: pariente.parSex, 
            parNum1: pariente.parNum1, 
            parNum2: pariente.parNum2, 
            parFij: pariente.parFij, 
            parEmail: pariente.parEmail
        };
        
        $scope.actualizarPariente = function () {
            var request = crud.crearRequest('familiares', 1, 'actualizarFamiliar');
            request.setData($scope.parienteSel);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.parentesco = buscarContenido($scope.tipoPariente,'tpaId','tpaDes', response.data.tipoParienteId);
                    response.data.i = pariente.i;
                    $rootScope.settingModParientes.dataset[pariente.i] = response.data;
                    $rootScope.tablaModParientes.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
    }]);
app.controller('editarFormacionEducativaCtrl', ['$rootScope','$scope', '$element', 'title', 'formacionEducativa', 'fichaEscalafonariaId', 'close', 'crud', 'tipFors', 'nivAcas', 'modal', function ($rootScope ,$scope, $element, title, formacionEducativa, fichaEscalafonariaId, close, crud, tipFors, nivAcas, modal) {
        $scope.title = title;
        $scope.tipFors = tipFors;
        $scope.nivAcas = nivAcas;
        
        //variable que servira para registrar una nueva formacion academica
        var datos = [];
        for (var i in nivAcas) {
            if(nivAcas[i]["pad"]==formacionEducativa.tip)
                datos.push(nivAcas[i]);
        }
        $scope.comNiveles = datos;
        
        $scope.forEduSel = {
            i: formacionEducativa.i,
            forEduId: formacionEducativa.forEduId, 
            tip: formacionEducativa.tip,
            niv: formacionEducativa.niv, 
            numTit: formacionEducativa.numTit,
            esp: formacionEducativa.esp, 
            estCon: formacionEducativa.estCon==""?false:formacionEducativa.estCon, 
            fecExp: convertirFechaStr(formacionEducativa.fecExp), 
            cenEst: formacionEducativa.cenEst
        };
        
        console.log($scope.forEduSel);
        
        $scope.loadTipoFormacion = function(){
            var form = $scope.forEduSel.tip;
            var datos = [];
            $scope.comNiveles = {};
            for (var i in nivAcas) {
                if(nivAcas[i]["pad"]==form)
                    datos.push(nivAcas[i]);
            }
            $scope.comNiveles = datos;
        }

        $scope.actualizarFormacionEducativa = function () {
            var request = crud.crearRequest('formacion_educativa', 1, 'actualizarFormacionEducativa');
            request.setData($scope.forEduSel);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.estConDes = response.data.estCon?"Si":"No";
                    response.data.tipFor = buscarContenido($scope.tipFors, 'id', 'nom', response.data.tip);
                    response.data.nivAca = buscarContenido($scope.nivAcas, 'id', 'nom', response.data.niv);
                    response.data.i = formacionEducativa.i;
                    $rootScope.settingModForEdu.dataset[formacionEducativa.i] = response.data;
                    $rootScope.tablaModForEdu.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
app.controller('editarColegiaturaCtrl', ['$rootScope','$scope', '$element', 'title', 'colegiatura', 'fichaEscalafonariaId', 'close', 'crud', 'modal', function ($rootScope ,$scope, $element, title, colegiatura, fichaEscalafonariaId, close, crud, modal) {
        $scope.title = title;

        //variable que servira para registrar una nueva formacion academica
        $scope.colegiaturaSel = {
            i: colegiatura.i,
            colId: colegiatura.colId, 
            nomColPro: colegiatura.nomColPro, 
            numRegCol: colegiatura.numRegCol, 
            conReg: colegiatura.conReg
        };

        $scope.actualizarColegiatura = function () {
            var request = crud.crearRequest('colegiatura', 1, 'actualizarColegiatura');
            request.setData($scope.colegiaturaSel);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.conRegDes = response.data.conReg?"Habilitado":"No Habilitado";
                    response.data.i = colegiatura.i;
                    $rootScope.settingModColegiaturas.dataset[ colegiatura.i] = response.data;
                    $rootScope.tablaModColegiaturas.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
app.controller('editarEstudioComplementarioCtrl', ['$rootScope','$scope', '$element', 'title', 'estudioComplementario', 'fichaEscalafonariaId', 'close', 'crud', 'tipEstComs', 'nivEstComs', 'modal', function ($rootScope ,$scope, $element, title, estudioComplementario, fichaEscalafonariaId, close, crud, tipEstComs, nivEstComs, modal) {
        $scope.title = title;

        //variable que servira para registrar una nueva formacion academica
        $scope.estComSel = {
            i: estudioComplementario.i,
            estComId: estudioComplementario.estComId,
            tip: estudioComplementario.tip, 
            des: estudioComplementario.des, 
            niv: estudioComplementario.niv, 
            insCer: estudioComplementario.insCer, 
            tipPar: estudioComplementario.tipPar, 
            fecIni: convertirFechaStr(estudioComplementario.fecIni), 
            fecTer: convertirFechaStr(estudioComplementario.fecTer), 
            horLec: estudioComplementario.horLec
        };
        
        $scope.tiposEstCom = tipEstComs;
        $scope.nivelesEstCom = nivEstComs;
    
        $scope.actualizarEstudioComplementario = function () {
            var request = crud.crearRequest('estudio_complementario', 1, 'actualizarEstudioComplementario');
            request.setData($scope.estComSel);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.tipDes = buscarContenido($scope.tiposEstCom, 'id', 'nom', response.data.tip);
                    response.data.nivDes = buscarContenido($scope.nivelesEstCom, 'id', 'nom', response.data.niv);
                    response.data.i = estudioComplementario.i;
                    $rootScope.settingModEstCom.dataset[ estudioComplementario.i] = response.data;
                    $rootScope.tablaModEstCom.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
app.controller('editarEstudioPostgradoCtrl', ['$rootScope','$scope', '$element', 'title', 'estudioPostgrado', 'fichaEscalafonariaId', 'close', 'crud', 'modal', function ($rootScope ,$scope, $element, title, estudioPostgrado, fichaEscalafonariaId, close, crud, modal) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.estPosSel = {
            i: estudioPostgrado.i,
            estPosId: estudioPostgrado.estPosId,
            ficEscId: fichaEscalafonariaId, 
            tip: estudioPostgrado.tip, 
            numRes: estudioPostgrado.numRes,
            fecRes: new Date(estudioPostgrado.fecRes),
            fecIniEst: new Date(estudioPostgrado.fecIniEst), 
            fecTerEst: new Date(estudioPostgrado.fecTerEst), 
            ins: estudioPostgrado.ins
        };
        
        $scope.tiposEstPos = [
            {id: "1", title: "Maestria"}, 
            {id: "2", title: "Doctorado"}
        ];
        
        $scope.actualizarEstudioPostgrado = function () {
            var request = crud.crearRequest('estudio_postgrado', 1, 'actualizarEstudioPostgrado');
            request.setData($scope.estPosSel);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.tipDes = buscarContenido($scope.tiposEstPos, 'id', 'title', response.data.tip);
                    response.data.i = estudioPostgrado.i;
                    $rootScope.settingModEstPos.dataset[estudioPostgrado.i] = response.data;
                    $rootScope.tablaModEstPos.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
app.controller('editarExposicionCtrl', ['$rootScope','$scope', '$element', 'title', 'exposicion', 'fichaEscalafonariaId', 'close', 'crud', 'modal', function ($rootScope ,$scope, $element, title, exposicion, fichaEscalafonariaId, close, crud, modal) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.expPonSel = {
            i: exposicion.i,
            expId: exposicion.expId,
            ficEscId:fichaEscalafonariaId, 
            des: exposicion.des, 
            insOrg: exposicion.insOrg, 
            tipPar: exposicion.tipPar, 
            fecIni: convertirFechaStr(exposicion.fecIni), 
            fecTer: convertirFechaStr(exposicion.fecTer), 
            horLec: exposicion.horLec
        };
        
        $scope.actualizarExposicion = function () {
            var request = crud.crearRequest('exposicion_ponencia', 1, 'actualizarExposicion');
            request.setData($scope.expPonSel);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.i = exposicion.i;
                    $rootScope.settingModExpPon.dataset[exposicion.i] = response.data;
                    $rootScope.tablaModExpPon.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
app.controller('editarPublicacionCtrl', ['$rootScope','$scope', '$element', 'title', 'publicacion', 'fichaEscalafonariaId', 'close', 'crud', 'modal', function ($rootScope ,$scope, $element, title, publicacion, fichaEscalafonariaId, close, crud, modal) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.publicacionSel = {
            i: publicacion.i,
            pubId: publicacion.pubId,
            nomEdi: publicacion.nomEdi, 
            tipPub: publicacion.tipPub, 
            titPub: publicacion.titPub, 
            graPar: publicacion.graPar, 
            lug: publicacion.lug, 
            fecPub: convertirFechaStr(publicacion.fecPub), 
            numReg: publicacion.numReg
        };
        
        $scope.actualizarPublicacion= function () {
            var request = crud.crearRequest('publicacion', 1, 'actualizarPublicacion');
            request.setData($scope.publicacionSel);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.i = publicacion.i;
                    $rootScope.settingModPublicaciones.dataset[publicacion.i] = response.data;
                    $rootScope.tablaModPublicaciones.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
app.controller('editarDesplazamientoCtrl', ['$rootScope','$scope', '$element', 'title', 'desplazamiento', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'tipDespls',function ($rootScope ,$scope, $element, title, desplazamiento, fichaEscalafonariaId, close, crud, modal, tipDespls) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.desplazamientoSel = {
            i: desplazamiento.i,
            desId: desplazamiento.desId,
            tip: desplazamiento.tip, 
            numRes: desplazamiento.numRes, 
            fecRes: convertirFechaStr(desplazamiento.fecRes), 
            insEdu: desplazamiento.insEdu, 
            car: desplazamiento.car, 
            jorLab: desplazamiento.jorLab, 
            fecIni: convertirFechaStr(desplazamiento.fecIni), 
            fecTer: convertirFechaStr(desplazamiento.fecTer)
        };
        
        $scope.tiposDesplazamientos = tipDespls;
        
        $scope.actualizarDesplazamiento = function () {
            var request = crud.crearRequest('desplazamiento', 1, 'actualizarDesplazamiento');
            request.setData($scope.desplazamientoSel);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.tipDes = buscarContenido($scope.tiposDesplazamientos, 'id', 'nom', response.data.tip);
                    response.data.i = desplazamiento.i;
                    $rootScope.settingModDesplazamientos.dataset[desplazamiento.i] = response.data;
                    $rootScope.tablaModDesplazamientos.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
app.controller('editarAscensoCtrl', ['$rootScope','$scope', '$element', 'title', 'ascenso', 'fichaEscalafonariaId', 'close', 'crud', 'modal', function ($rootScope ,$scope, $element, title, ascenso, fichaEscalafonariaId, close, crud, modal) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.ascensoSel = {
            i: ascenso.i,
            ascId: ascenso.ascId,
            numRes: ascenso.numRes, 
            fecRes: convertirFechaStr(ascenso.fecRes), 
            fecEfe: convertirFechaStr(ascenso.fecEfe), 
            esc: (ascenso.esc).split(" ").join("")
        };
        $scope.escalas = [
            {id:"I", title: "I"}, 
            {id:"II", title: "II"}, 
            {id:"III", title: "III"}, 
            {id:"IV", title: "IV"}, 
            {id:"V", title: "V"}, 
            {id:"VI", title: "VI"},
            {id:"VII", title: "VII"}, 
            {id:"VIII", title: "VIII"}
        ];
        
        $scope.actualizarAscenso = function () {
            var request = crud.crearRequest('ascenso', 1, 'actualizarAscenso');
            request.setData($scope.ascensoSel);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.esc= buscarContenido($scope.escalas, 'id','title', response.data.esc);
                    response.data.i = ascenso.i;
                    $rootScope.settingModAscensos.dataset[ascenso.i] = response.data;
                    $rootScope.tablaModAscensos.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
app.controller('editarCapacitacionCtrl', ['$rootScope','$scope', '$element', 'title', 'capacitacion', 'fichaEscalafonariaId', 'close', 'crud', 'modal', function ($rootScope ,$scope, $element, title, capacitacion, fichaEscalafonariaId, close, crud, modal) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.capacitacionSel = {
            i: capacitacion.i,
            capId: capacitacion.capId,
            nom: capacitacion.nom, 
            tip: capacitacion.tip, 
            fec: convertirFechaStr(capacitacion.fec), 
            cal: capacitacion.cal, 
            lug: capacitacion.lug
        };
        
        $scope.actualizarCapacitacion = function () {
            var request = crud.crearRequest('capacitacion', 1, 'actualizarCapacitacion');
            request.setData($scope.capacitacionSel);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.i = capacitacion.i;
                    $rootScope.settingModCapacitaciones.dataset[capacitacion.i] = response.data;
                    $rootScope.tablaModCapacitaciones.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
app.controller('editarReconocimientoCtrl', ['$rootScope','$scope', '$element', 'title', 'reconocimiento', 'fichaEscalafonariaId', 'close', 'crud', 'modal','mots', function ($rootScope ,$scope, $element, title, reconocimiento, fichaEscalafonariaId, close, crud, modal,mots) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.reconocimientoSel = {
            i: reconocimiento.i,
            recId: reconocimiento.recId,
            mot: reconocimiento.mot, 
            numRes: reconocimiento.numRes, 
            fecRes: convertirFechaStr(reconocimiento.fecRes), 
            entEmi: reconocimiento.entEmi
        };
        
        $scope.motivos = mots;
        
        $scope.actualizarReconocimiento = function () {
            var request = crud.crearRequest('reconocimientos', 1, 'actualizarReconocimiento');
            request.setData($scope.reconocimientoSel);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.motDes = buscarContenido($scope.motivos, 'id', 'nom', response.data.mot);
                    response.data.i = reconocimiento.i;
                    $rootScope.settingModReconocimientos.dataset[reconocimiento.i] = response.data;
                    $rootScope.tablaModReconocimientos.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
app.controller('editarDemeritoCtrl', ['$rootScope','$scope', '$element', 'title', 'demerito', 'fichaEscalafonariaId', 'close', 'crud', 'modal', function ($rootScope ,$scope, $element, title, demerito, fichaEscalafonariaId, close, crud, modal) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.demeritoSel = {
            i: demerito.i,
            demId: demerito.demId,
            entEmi: demerito.entEmi,
            numRes: demerito.numRes, 
            fecRes: convertirFechaStr(demerito.fecRes), 
            sep: demerito.sep, 
            fecIni: convertirFechaStr(demerito.fecIni), 
            fecFin: convertirFechaStr(demerito.fecFin), 
            mot: demerito.mot
        };
        
        
        $scope.actualizarDemerito = function () {
            var request = crud.crearRequest('demeritos', 1, 'actualizarDemerito');
            request.setData($scope.demeritoSel);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.sepDes = response.data.sep?"Si":"No";
                    response.data.i = demerito.i;
                    $rootScope.settingModDemeritos.dataset[demerito.i] = response.data;
                    $rootScope.tablaModDemeritos.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
//Legajo
app.controller('verLegajoPersonalCtrl', ['$routeParams', '$rootScope','$scope', 'crud', 'NgTableParams','ModalService', 'modal', function ($routeParams, $rootScope, $scope, crud, NgTableParams, ModalService, modal) {
        var datos = JSON.parse(atob($routeParams.ficha));
        
        var categorias = [
            {idCat: "1", idSubCat:"1", subCatDes: "Datos personales"},
            {idCat: "1", idSubCat:"2", subCatDes: "Nacimiento"},
            {idCat: "1", idSubCat:"3", subCatDes: "Salud"},
            {idCat: "1", idSubCat:"4", subCatDes: "Direccion"},
            {idCat: "2", idSubCat:"1", subCatDes: "Datos familiares"},
            {idCat: "3", idSubCat:"1", subCatDes: "Formacion educativa"},
            {idCat: "3", idSubCat:"2", subCatDes: "Colegiatura"},
            {idCat: "3", idSubCat:"3", subCatDes: "Estudios complementarios"},
            {idCat: "3", idSubCat:"4", subCatDes: "Estudios postgrado"},
            {idCat: "3", idSubCat:"5", subCatDes: "Exposicion y/o Ponencias"},
            {idCat: "3", idSubCat:"6", subCatDes: "Publicaciones"},
            {idCat: "4", idSubCat:"1", subCatDes: "Desplazamientos"},
            {idCat: "4", idSubCat:"2", subCatDes: "Ascensos"},
            {idCat: "4", idSubCat:"3", subCatDes: "Capacitaciones"},
            {idCat: "5", idSubCat:"1", subCatDes: "Reconocimientos"},
            {idCat: "6", idSubCat:"1", subCatDes: "Demeritos"},
            {idCat: "7", idSubCat:"1", subCatDes: "Otros"}
        ];
        
        $rootScope.paramsLegDatosPersonales = {count: 10};
        $rootScope.settingLegDatosPersonales  = {counts: []};
        $rootScope.tablaLegDatosPersonales = new NgTableParams($rootScope.paramsLegDatosPersonales, $rootScope.settingLegDatosPersonales);
        
        $rootScope.paramsLegDatosFamiliares = {count: 10};
        $rootScope.settingLegDatosFamiliares  = {counts: []};
        $rootScope.tablaLegDatosFamiliares = new NgTableParams($rootScope.paramsLegDatosFamiliares, $rootScope.settingLegDatosFamiliares);
        
        $rootScope.paramsLegDatosAcademicos = {count: 10};
        $rootScope.settingLegDatosAcademicos  = {counts: []};
        $rootScope.tablaLegDatosAcademicos  = new NgTableParams($rootScope.paramsLegDatosAcademicos, $rootScope.settingLegDatosAcademicos);
        
        $rootScope.paramsLegExperienciaLaboral = {count: 10};
        $rootScope.settingLegExperienciaLaboral  = {counts: []};
        $rootScope.tablaLegExperienciaLaboral  = new NgTableParams($rootScope.paramsLegExperienciaLaboral, $rootScope.settingLegExperienciaLaboral);
        
        $rootScope.paramsLegReconocimientos = {count: 10};
        $rootScope.settingLegReconocimientos  = {counts: []};
        $rootScope.tablaLegReconocimientos  = new NgTableParams($rootScope.paramsLegReconocimientos, $rootScope.settingLegReconocimientos);
        
        $rootScope.paramsLegDemeritos = {count: 10};
        $rootScope.settingLegDemeritos   = {counts: []};
        $rootScope.tablaLegDemeritos   = new NgTableParams($rootScope.paramsLegDemeritos , $rootScope.settingLegDemeritos );
        
        $rootScope.paramsLegOtros = {count: 10};
        $rootScope.settingLegOtros   = {counts: []};
        $rootScope.tablaLegOtros   = new NgTableParams($rootScope.paramsLegOtros , $rootScope.settingLegOtros );
        
        var legDatosPersonales = [];
        var legDatosFamiliares = [];
        var legDatosAcademicos = [];
        var legExperienciaLaboral = [];
        var legReconocimientos = [];
        var legDemeritos = [];
        var legOtros = [];
        
        listarLegajos();
        function listarLegajos() {
            //preparamos un objeto request
            var request = crud.crearRequest('legajo_personal', 1, 'listarLegajos');
            request.setData(datos);
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item){
                    var subCategorias = buscarObjetos(categorias, 'idCat', item.catLeg );
                    item.subCatDes = buscarContenido(subCategorias, 'idSubCat', 'subCatDes', item.subCat);
                    
                    switch(item.catLeg){
                        case '1': legDatosPersonales.push(item);
                                  break;
                        case '2': legDatosFamiliares.push(item);
                                  break;
                        case '3': legDatosAcademicos.push(item);
                                  break;
                        case '4': legExperienciaLaboral.push(item);
                                  break;
                        case '5': legReconocimientos.push(item);
                                  break;
                        case '6': legDemeritos.push(item);
                                  break;
                        default: legOtros.push(item);
                    }
                });
                
                $rootScope.settingLegDatosPersonales.dataset = legDatosPersonales;
                iniciarPosiciones($rootScope.settingLegDatosPersonales.dataset);
                $rootScope.tablaLegDatosPersonales.settings($rootScope.settingLegDatosPersonales);
                
                $rootScope.settingLegDatosFamiliares.dataset = legDatosFamiliares;
                iniciarPosiciones($rootScope.settingLegDatosFamiliares.dataset);
                $rootScope.tablaLegDatosFamiliares.settings($rootScope.settingLegDatosFamiliares);
                
                $rootScope.settingLegDatosAcademicos.dataset = legDatosAcademicos;
                iniciarPosiciones($scope.settingLegDatosAcademicos.dataset);
                $rootScope.tablaLegDatosAcademicos.settings($rootScope.settingLegDatosAcademicos);
                
                $scope.settingLegExperienciaLaboral.dataset = legExperienciaLaboral;
                iniciarPosiciones($rootScope.settingLegExperienciaLaboral.dataset);
                $rootScope.tablaLegExperienciaLaboral.settings($rootScope.settingLegExperienciaLaboral);
                
                $rootScope.settingLegReconocimientos.dataset = legReconocimientos;
                iniciarPosiciones($rootScope.settingLegReconocimientos.dataset);
                $rootScope.tablaLegReconocimientos.settings($rootScope.settingLegReconocimientos);
                
                $rootScope.settingLegDemeritos.dataset = legDemeritos;
                iniciarPosiciones($rootScope.settingLegDatosPersonales.dataset);
                $rootScope.tablaLegDemeritos.settings($rootScope.settingLegDemeritos);
                
                $rootScope.settingLegOtros.dataset = legOtros;
                iniciarPosiciones($rootScope.settingLegOtros.dataset);
                $rootScope.tablaLegOtros.settings($rootScope.settingLegOtros);
                
                
            }, function (data) {
                console.info(data);
            });
           
        };
        
        $scope.showNuevoLegajo = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarLegajoPersonal.html",
                controller: "agregarNuevoLegajoCtrl",
                inputs: {
                    title: "Nuevo Legajo",
                    fichaEscalafonariaId: datos.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };
        
        $scope.prepararEditarLegajo = function (lp) {
            var legajoSel = JSON.parse(JSON.stringify(lp));
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarLegajo.html",
                controller: "editarLegajoCtrl",
                inputs: {
                    title: "Editar datos del legajo",
                    legajo: legajoSel
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };
        
        $scope.eliminarLegajo = function(l){   
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el legajo?", function () {
                var request = crud.crearRequest('legajo_personal', 1, 'eliminarLegajo');
                request.setData({legId: l.legId});
                
                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaLegOtros.settings().dataset, function(item){
                            return l === item;
                        });
                        $rootScope.tablaLegOtros.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    }else {
                            modal.mensaje("ERROR",response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR","El servidor no responde");
                });
            }, '400'); 
        };
        
    }]);
app.controller('agregarNuevoLegajoCtrl', ['$scope', '$rootScope', '$element','title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService) {
        //variable que servira para registrar un nuevo pariente
        $scope.nuevoLegajo = {
            ficEscId: fichaEscalafonariaId, 
            nomDoc: "", 
            archivo: "", 
            des: "", 
            url: "", 
            catLeg: "7", 
            subCat: "1", 
            codAspOri: "0"
        };
        $scope.title = title;


        $scope.agregarLegajo = function () {
            var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
            request.setData($scope.nuevoLegajo);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {   
                    insertarElemento($rootScope.settingLegOtros.dataset, response.data);
                    $rootScope.tablaLegOtros.reload();
 
                }
            }, function (data) {
                console.info(data);
            });
        };
    }]);
app.controller('editarLegajoCtrl', ['$rootScope','$scope', '$element', 'title', 'legajo', 'close', 'crud', 'modal', function ($rootScope ,$scope, $element, title, legajo, close, crud, modal) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.legajoSel = {
            i: legajo.i,
            ficEscId: legajo.ficEscId, 
            legId: legajo.legId, 
            nomDoc: legajo.nomDoc, 
            archivo: legajo.archivo, 
            des: legajo.des, 
            url: legajo.url, 
            catLeg: legajo.catleg, 
            subCat: legajo.subCat, 
            codAspOri: legajo.codAspOri
        };
        
        
        $scope.actualizarLegajo = function () {
            var request = crud.crearRequest('legajo_personal', 1, 'actualizarLegajo');
            request.setData($scope.legajoSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.i = legajo.i;
                    $rootScope.settingLegOtros.dataset[legajo.i] = response.data;
                    $rootScope.tablaLegOtros.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
        
    }]);
//Reporte
app.controller('modalSeleccionadosCtrl', ['$scope', '$rootScope', '$element','title', 'ficEscId', 'traId', 'perDni','close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, ficEscId, traId, perDni, close, crud, modal, ModalService) {
        //variable que servira para registrar un nuevo pariente
        
        $scope.title = title;
        $scope.seleccionados = [false, false,false, false, false,false, false, false,false, false, false,false, false,false,false];

        $scope.verFichaEscalafonaria = function () {
            var request = crud.crearRequest('reportes',1,'reporteFichaEscalafonaria');
            request.setData({traId:traId, ficEscId:ficEscId, perDni:perDni, seleccionados: $scope.seleccionados });
            crud.insertar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    verDocumentoPestana(response.data.file);
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde');
            }); 
};
    }]);