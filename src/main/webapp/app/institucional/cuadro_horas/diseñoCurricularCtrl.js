app.controller("diseñoCurricularCtrl",["$rootScope","$scope","$window","NgTableParams","crud","modal", function ($rootScope,$scope,$window,NgTableParams,crud,modal){       
    
    $scope.nuevoDiseno = {nombre:"",descripcion:"",resolucion:"",tipo:'N',estado:'A'};
    $scope.disenoSel = {};
    
    var params = {count: 1};
    var setting = { counts: []};
    $scope.tabla = new NgTableParams(params, setting);
    
    $scope.nivel = {};
    $scope.ciclo = {};    
    $scope.jornada = {};
    $scope.grado = {};
    $scope.area = {tipo:false};
    $scope.gradoArea = {};
    $scope.finFase1 = false;
    
    $scope.optionsFases = [true, false, false];
    $scope.matris = {};
    $scope.matris2 = {};
    
    $scope.passwordToAuthenticate = "";
    $scope.usuarioValidado = false;
    $scope.cancelar=function(){
        $scope.nivel = {};
        $scope.ciclo = {};    
        $scope.jornada = {};
        $scope.grado = {};
        $scope.area = {tipo:false};
    };
    
    $scope.reloadRoute = function() {
        $window.location.reload();
    };
        $scope.listarDisenos = function(orgID){
        //preparamos un objeto request
        var request = crud.crearRequest('diseñoCurricular',1,'listarDiseñoCurricular');
        //request.setData({orgid:orgID});
        crud.listar("/cuadroHoras",request,function(response){
            setting.dataset = response.data;
            for(var i=0; i<response.data.length;i++){
                console.log("fecha: "+response.data[i].fecha+"orgid: "+response.data[i].organizacionID+", "+orgID);
                if( response.data[i].organizacionID == orgID && response.data[i].fecha.search((new Date()).getFullYear()) == 0){
                    $scope.estaCreado = true;
                    $scope.tabla.settings(setting);
                    $scope.tabla.calcularMatriz();
                    $scope.tabla.calcularMatrizDetalle();
                    break;
                }
            }
            
            
            //$scope.calcularMatriz();
        },function(data){
            console.info(data);
        });
        
        request = crud.crearRequest('diseñoCurricular',1,'listarJornadaEscolar');
        crud.listar("/cuadroHoras",request,function(response){
            $scope.jornadas = response.data;
        },function(data){
            console.info(data);
        });
    };
    
    $scope.agregarDiseno = function(orgID){
        
        $scope.nuevoDiseno.organizacionID = orgID;
        
        var request = crud.crearRequest('diseñoCurricular',1,'insertarDiseñoCurricular');
        request.setData($scope.nuevoDiseno);
        
        crud.insertar("/cuadroHoras",request,function(response){
            
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                //recuperamos las variables que nos envio el servidor
                $scope.nuevoDiseno.diseñoID = response.data.diseñoID;
                $scope.nuevoDiseno.fecha = response.data.fecha;
                $scope.nuevoDiseno.modalidad = response.data.modalidad;
                
                //insertamos el elemento a la lista
                setting.dataset.push($scope.nuevoDiseno);
                $scope.tabla.reload();
                //reiniciamos las variables
                
                $scope.nuevoDiseno = {nombre:"",descripcion:"",resolucion:"",tipo:'N',estado:'A'};
                $scope.estaCreado = true;
                $scope.usuarioValidado = false;
                //cerramos la ventana modal
                $('#modalNuevo').modal('hide');
                $scope.reloadRoute();
            }            
        },function(data){
            console.info(data);
        });
        
    };
    $scope.eliminarDiseno = function(i,o){
        modal.mensajeConfirmacion($scope,"Seguro que desea eliminar este registro?",function(){
            var request = crud.crearRequest('diseñoCurricular',1,'eliminarDiseñoCurricular');
            request.setData({diseñoID:o.diseñoID});
            crud.eliminar("/cuadroHoras",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    eliminarElemento(setting.dataset,i);
                    $scope.tabla.reload();
                }
            },function(data){
                console.info(data);
            });            
        });
    };
    $scope.modals = ["Educacion Basica Regular","Educacion Basica Especial", "Educacion Basica Alternativa"];
    $scope.prepararEditar = function(index,d){
        $scope.disenoSel = JSON.parse(JSON.stringify(d));
        $scope.disenoSel.i=index;
        $('#modalEditar').modal('show');
    };
    $scope.checkPhase1 = function(){
        return $scope.disenoActual !== undefined && ($scope.disenoActual.niveles !== undefined && $scope.disenoActual.niveles.length !== 0 && $scope.disenoActual.ciclos !== undefined && $scope.disenoActual.ciclos.length !== 0 && $scope.disenoActual.grados !== undefined && $scope.disenoActual.grados.length !== 0 && $scope.disenoActual.areas !== undefined && $scope.disenoActual.areas.length !== 0);
    }
    $scope.iniFase2=false;
    $scope.checkPhase2 = function(){
        fase2 = $scope.matris !== undefined && $scope.matris.gradoAreas !== undefined && $scope.matris.gradoAreas.length!==0;
        return fase2;
    }
    $scope.nextPhase = function(){
        if($scope.checkPhase1()){
            $scope.iniFase2=true;            
            $scope.current=1;
        }
        else{
            alert("Datos incompletos, revise los ítems de la primera fase.");
        }
    }
    
    $scope.updateOptions = function (numOption, optionsSet) {
            for (var i = 0; i < optionsSet.length; i += 1) {
                optionsSet[i] = false;
            }
            optionsSet[numOption] = $scope.iniFase2=true; ;
        };
    
    $scope.editarDiseno = function(){
        
        var request = crud.crearRequest('diseñoCurricular',1,'actualizarDiseñoCurricular');
        console.info($scope.disenoSel);
        request.setData($scope.disenoSel);
                
        crud.actualizar("/cuadroHoras",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                //actualizando
                setting.dataset[$scope.disenoSel.i] = $scope.disenoSel;                
                $scope.tabla.reload();
                $('#modalEditar').modal('hide');
            }
        },function(data){
            console.info(data);
        });
    };
    
    /* CICLO EDUCATIVO */    
    $scope.agregarCiclo = function(){
        
        var request = crud.crearRequest('diseñoCurricular',1,'insertarCiclo');
        
        if($scope.ciclo.abreviacion!==undefined&&$scope.ciclo.nombre!==undefined&&$scope.ciclo.descripcion!==undefined
            &&$scope.ciclo.abreviacion!==""&&$scope.ciclo.nombre!==""&&$scope.ciclo.descripcion!==""    ){
            if($scope.ciclo.abreviacion.length<4 && $scope.ciclo.nombre.length<15){    
                $scope.ciclo.diseñoID = $scope.disenoActual.diseñoID;
                request.setData($scope.ciclo);

                modal.mensajeConfirmacion($scope,"Seguro que desea agregar el ciclo educativo?",function(){
                    crud.insertar("/cuadroHoras",request,function(response){
                        modal.mensaje("CONFIRMACION",response.responseMsg);
                        if(response.responseSta){
                            $scope.ciclo.cicloID = response.data.cicloID;
                            $scope.disenoActual.ciclos.push($scope.ciclo);
                            $scope.ciclo = {};
                        }            
                    },function(data){
                        console.info(data);
                    });
                },'400');
            }else{
                alert("Los campos Abr o Nombre exceden su tamaño, Abr=3 Nombre=15")
            }    
        }else{
            alert("Completa los campos para añadir un ciclo");
        }
    };
    
    $scope.editarCiclo = function(i,c){
        //si estamso editando
        if(c.edi){
            if(c.copia.abreviacion!==undefined&&c.copia.nombre!==undefined&&c.copia.descripcion!==undefined
         &&c.copia.abreviacion!==""&&c.copia.nombre!==""&&c.copia.descripcion!==""){
                var request = crud.crearRequest('diseñoCurricular',1,'insertarCiclo');        
                request.setData(c.copia);
                modal.mensajeConfirmacion($scope,"Seguro que desea editar el ciclo?",function(){
                    crud.insertar("/cuadroHoras",request,function(response){
                        modal.mensaje("CONFIRMACION",response.responseMsg);
                        if(response.responseSta){
                            $scope.disenoActual.ciclos[i] = c.copia;
                        }            
                    },function(data){
                        console.info(data);
                    });
                },'400');
         }else{
                alert("Completa los campos para editar un ciclo");
            }    
        }
        //si queremos editar
        else{
            c.copia = JSON.parse(JSON.stringify(c));
            c.edi =true;            
        }
    };
    
    $scope.eliminar = function(i,r){
        //si estamso cancelando la edicion
        if(r.edi){
            r.edi = false;
            delete r.copia;
        }
        //si queremos eliminar el elemento
        /*else{
            $scope.usuarioSel.sessiones.splice(i,1);
        }*/
    };
    
    $scope.eliminarCiclo = function(i, c){
        var request = crud.crearRequest('diseñoCurricular',1,'eliminarCiclo');
        request.setData(c);
        modal.mensajeConfirmacion($scope,"Seguro que desea eliminar el ciclo?",function(){
            crud.eliminar("/cuadroHoras",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    eliminarElemento($scope.disenoActual.ciclos,i);
                }            
            },function(data){
                console.info(data);
            });
        },'400');
    }
    
    
    /* CICLOS FIN*/
    /* NIVEL EDUCATIVO */    
    $scope.agregarNivel = function(){
        
       var request = crud.crearRequest('diseñoCurricular',1,'insertarNivel');
       if($scope.nivel.abreviacion!==undefined&&$scope.nivel.nombre!==undefined&&$scope.nivel.descripcion!==undefined
         &&$scope.nivel.abreviacion!==""&&$scope.nivel.nombre!==""&&$scope.nivel.descripcion!==""){
           if($scope.nivel.abreviacion.length<=4 && $scope.nivel.nombre.length<=16){
                $scope.nivel.diseñoID = $scope.disenoActual.diseñoID;
                $scope.nivel.modalidadID = $scope.disenoActual.modalidad.modalidadID;
                request.setData($scope.nivel);

                modal.mensajeConfirmacion($scope,"Seguro que desea agregar el nivel educativo?",function(){
                    crud.insertar("/cuadroHoras",request,function(response){
                        modal.mensaje("CONFIRMACION",response.responseMsg);
                        if(response.responseSta){
                            $scope.nivel.nivelID = response.data.nivelID;
                            $scope.disenoActual.niveles.push($scope.nivel);
                            $scope.nivel = {};
                            $scope.tabla.calcularMatriz();
                            $scope.tabla.calcularMatrizDetalle();
                        }
                    },function(data){
                        console.info(data);
                    });
                },'400');
            }
            else{
                alert("Los campos Abr o Nombre exceden su tamaño, Abr=3 Nombre=15")
            }
        }
        else{
            alert("Completa los campos para añadir un nivel");
        }
    };
    $scope.editarNivel = function(i,n){
        //si estamso editando
        if(n.edi){
            if(n.copia.abreviacion!==undefined&&n.copia.nombre!==undefined&&n.copia.descripcion!==undefined
         &&n.copia.abreviacion!==""&&n.copia.nombre!==""&&n.copia.descripcion!==""){
                var request = crud.crearRequest('diseñoCurricular',1,'insertarNivel');        
                request.setData(n.copia);
                modal.mensajeConfirmacion($scope,"Sseguro que desea editar el nivel educativo?",function(){
                    crud.insertar("/cuadroHoras",request,function(response){
                        modal.mensaje("CONFIRMACION",response.responseMsg);
                        if(response.responseSta){
                            $scope.disenoActual.niveles[i] = n.copia;
                        }            
                    },function(data){
                        console.info(data);
                    });
                },'400');
            }else{
                alert("Completa los campos para editar un nivel");
            }
            
        }
        //si queremos editar
        else{
            n.copia = JSON.parse(JSON.stringify(n));
            n.edi =true;            
        }
    };
    
     $scope.eliminarNivel = function(i,n){
        var request = crud.crearRequest('diseñoCurricular',1,'eliminarNivel');
        request.setData(n);
        modal.mensajeConfirmacion($scope,"Seguro que desea eliminar el nivel?",function(){
            crud.eliminar("/cuadroHoras",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    eliminarElemento($scope.disenoActual.niveles,i);
                }            
            },function(data){
                console.info(data);
            });
        },'400');
    }
    
    /*NIVEL FIN*/
    /* GRADO ESCOLAR */    
    $scope.agregarGrado = function(){
        
        var request = crud.crearRequest('diseñoCurricular',1,'insertarGrado');
        if($scope.grado.abreviacion!==undefined&&$scope.grado.nombre!==undefined&&$scope.grado.descripcion!==undefined && $scope.grado.nivelID!==undefined && $scope.grado.cicloID!==undefined
         &&$scope.grado.abreviacion!==""&&$scope.grado.nombre!==""&&$scope.grado.descripcion!=="" && $scope.grado.nivelID!=="" && $scope.grado.cicloID!=="" ){
            if($scope.grado.abreviacion.length<=4 && $scope.grado.nombre.length<=16){
                $scope.grado.diseñoID = $scope.disenoActual.diseñoID;
                request.setData($scope.grado);

                modal.mensajeConfirmacion($scope,"Seguro que desea agregar el grado?",function(){
                    crud.insertar("/cuadroHoras",request,function(response){
                        modal.mensaje("CONFIRMACION",response.responseMsg);
                        if(response.responseSta){
                            $scope.grado.gradoID = response.data.gradoID;
                            $scope.grado.ciclo = buscarContenido($scope.disenoActual.ciclos,"cicloID","abreviacion",$scope.grado.cicloID);
                            $scope.grado.nivel = buscarContenido($scope.disenoActual.niveles,"nivelID","nombre",$scope.grado.nivelID);                    
                            insertarElemento($scope.disenoActual.grados,$scope.grado);
                            $scope.grado = {};
                            $scope.tabla.calcularMatriz();
                        }
                    },function(data){
                        console.info(data);
                    });
                },'400');
            }else{
                alert("Los campos Abr o Nombre exceden su tamaño, Abr=3 Nombre=15")
            }   
        }else{
            alert("Completa los campos para añadir un grados");
        }
            
    };
    $scope.editarGrado = function(i,g){
        //si estamso editando
        if(g.edi){
            if(g.copia.abreviacion!==undefined&&g.copia.nombre!==undefined&&g.copia.descripcion!==undefined && g.copia.nivelID!==undefined && g.copia.cicloID!==undefined
         &&g.copia.abreviacion!==""&&g.copia.nombre!==""&&g.copia.descripcion!=="" && g.copia.nivelID!=="" && g.copia.cicloID!=="" ){
         
                var request = crud.crearRequest('diseñoCurricular',1,'insertarGrado');        
                request.setData(g.copia);
                modal.mensajeConfirmacion($scope,"Seguro que desea editar el grado?",function(){
                    crud.insertar("/cuadroHoras",request,function(response){
                        modal.mensaje("CONFIRMACION",response.responseMsg);
                        if(response.responseSta){
                            g.copia.ciclo = buscarContenido($scope.disenoActual.ciclos,"cicloID","abreviacion",g.copia.cicloID);
                            g.copia.nivel = buscarContenido($scope.disenoActual.niveles,"nivelID","nombre",g.copia.nivelID); 
                            $scope.disenoActual.grados[i] = g.copia;
                        }            
                    },function(data){
                        console.info(data);
                    });
                },'400');
            }else{
                alert("Completa los campos para editar un grado");
            }
        }
        //si queremos editar
        else{
            g.copia = JSON.parse(JSON.stringify(g));
            g.edi =true;            
        }
    };
    
    $scope.eliminarGrado = function(i,g){
        var request = crud.crearRequest('diseñoCurricular',1,'eliminarGrado');
        request.setData(g);
        modal.mensajeConfirmacion($scope,"Seguro que desea eliminar el grado?",function(){
            crud.eliminar("/cuadroHoras",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    eliminarElemento($scope.disenoActual.grados,i);
                }            
            },function(data){
                console.info(data);
            });
        },'400');
    }
    /*GRADO FIN*/
    /* AREA CURRICULAR*/    
    $scope.agregarArea = function(){
        
        var request = crud.crearRequest('diseñoCurricular',1,'insertarArea');
        if($scope.area.abreviacion!==undefined&&$scope.area.nombre!==undefined&&$scope.area.descripcion!==undefined 
         &&$scope.area.abreviacion!==""&&$scope.area.nombre!==""&&$scope.area.descripcion!==""){
            if($scope.area.abreviacion.length<=4 && $scope.area.nombre.length<=16){
                $scope.area.diseñoID = $scope.disenoActual.diseñoID;
                request.setData($scope.area);

                modal.mensajeConfirmacion($scope,"Seguro que desea agregar el área curricular?",function(){
                    crud.insertar("/cuadroHoras",request,function(response){
                        modal.mensaje("CONFIRMACION",response.responseMsg);
                        if(response.responseSta){
                            $scope.area.areaID = response.data.areaID;
                            insertarElemento($scope.disenoActual.areas,$scope.area);
                            $scope.area = {tipo:false};
                        }            
                    },function(data){
                        console.info(data);
                    });
                },'400');
            }
            else{
                alert("Los campos Abr o Nombre exceden su tamaño, Abr=3 Nombre=15")
            }
         }else{
            alert("Completa los campos para añadir un area curricular");
        }
    };
    $scope.editarArea = function(i,a){
        //si estamso editando
        if(a.edi){
            if(a.copia.abreviacion!==undefined&&a.copia.nombre!==undefined&&a.copia.descripcion!==undefined
         &&a.copia.abreviacion!==""&&a.copia.nombre!==""&&a.copia.descripcion!==""){
                var request = crud.crearRequest('diseñoCurricular',1,'insertarArea');        
                request.setData(a.copia);
                modal.mensajeConfirmacion($scope,"Seguro que desea editar el área escolar?",function(){
                    crud.insertar("/cuadroHoras",request,function(response){
                        modal.mensaje("CONFIRMACION",response.responseMsg);
                        if(response.responseSta){
                            $scope.disenoActual.areas[i] = a.copia;
                        }            
                    },function(data){
                        console.info(data);
                    });
                },'400');
            }else{
                alert("Ingrese todos los campos para edita area curricular");
            }
        }
        //si queremos editar
        else{
            a.copia = JSON.parse(JSON.stringify(a));
            a.edi =true;            
        }
    };
    

    $scope.eliminarArea = function(i,a){
        var request = crud.crearRequest('diseñoCurricular',1,'eliminarArea');        
        request.setData(a);
        modal.mensajeConfirmacion($scope,"Seguro que desea eliminar el área?",function(){
            crud.eliminar("/cuadroHoras",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    eliminarElemento($scope.disenoActual.areas,i);
                }            
            },function(data){
                console.info(data);
            });
        },'400');
        
    }
    /*AREA CURRICULAR FIN*/
    
    /* JORNADA ESCOLAR*/    
    $scope.agregarJornada = function(){
        
        if($scope.jornada.hObligatoria < 0 ||  $scope.jornada.hObligatoria > 50){//en la curricula se establece el limite
            modal.mensaje("ALERTA","las horas obligatorias sobrepasan el limite establecido");
            return;
        }
         if($scope.jornada.hLibre < 0 ||  $scope.jornada.hLibre > 4){
            modal.mensaje("ALERTA","las horas Libre sobrepasan el limite establecido");
            return;
        }
         if($scope.jornada.hTutoria < 0 ||  $scope.jornada.hTutoria > 5){
            modal.mensaje("ALERTA","las horas Tutoria sobrepasan el limite establecido");
            return;
        }
        
        var request = crud.crearRequest('diseñoCurricular',1,'insertarJornada');
        if($scope.jornada.abreviacion!==undefined&&$scope.jornada.nombre!==undefined&&$scope.jornada.descripcion!==undefined &&$scope.jornada.hObligatoria!==undefined &&$scope.jornada.hLibre!==undefined &&$scope.jornada.hTutoria!==undefined 
         &&$scope.jornada.abreviacion!==""&&$scope.jornada.nombre!==""&&$scope.jornada.descripcion!=="" &&$scope.jornada.hObligatoria!==null &&$scope.jornada.hLibre!==null &&$scope.jornada.hTutoria!==null ){
            $scope.jornada.diseñoID = $scope.disenoActual.diseñoID;
            request.setData($scope.jornada);

            modal.mensajeConfirmacion($scope,"Seguro que desea agregar la jornada?",function(){
                crud.insertar("/cuadroHoras",request,function(response){
                    modal.mensaje("CONFIRMACION",response.responseMsg);
                    if(response.responseSta){
                        $scope.jornada.jornadaID = response.data.jornadaID;
                        $scope.jornada.hTotal = response.data.total;
                        $scope.jornada.nivel = buscarContenido($scope.disenoActual.niveles,"nivelID","nombre",$scope.jornada.nivelID);
                        $scope.disenoActual.jornadas.push($scope.jornada);
                        $scope.jornada.areas = [];
                        insertarElemento($scope.jornadas,$scope.jornada);
                        $scope.jornada = {};
                    }            
                },function(data){
                    console.info(data);
                });
            },'400');
         }else{
            alert("Completa los campos para añadir distribucion");
        }
    };   
    $scope.editarJornada = function(i,j){
        
              
        //si estamso editando
        if(j.edi){
            
        if(j.copia.hObligatoria < 0 || j.copia.hObligatoria > 50){//en la curricula se establece el limite
            modal.mensaje("ALERTA","las horas obligatorias sobrepasan el limite establecido(0-50)");
            return;
        }
         if(j.copia.hLibre < 0 ||  j.copia.hLibre > 4){
            modal.mensaje("ALERTA","las horas Libre sobrepasan el limite establecido(0-4)");
            return;
        }
         if(j.copia.hTutoria < 0 ||  j.copia.hTutoria > 5){
            modal.mensaje("ALERTA","las horas Tutoria sobrepasan el limite establecido(0-5)");
            return;
        }
        
        
        var request = crud.crearRequest('diseñoCurricular',1,'insertarJornada');
        if(j.copia.abreviacion!==undefined&&j.copia.nombre!==undefined&&j.copia.descripcion!==undefined &&j.copia.hTutoria!==undefined &&j.copia.hLibre!==undefined &&j.copia.hObligatoria!==undefined
         &&j.copia.abreviacion!==""&&j.copia.nombre!==""&&j.copia.descripcion!=="" &&j.copia.hTutoria!==null &&j.copia.hLibre!==null &&j.copia.hObligatoria!==null){
            
                request.setData(j.copia);
                modal.mensajeConfirmacion($scope,"Seguro que desea editar la jornada escolar?",function(){
                    crud.insertar("/cuadroHoras",request,function(response){
                        modal.mensaje("CONFIRMACION",response.responseMsg);
                        if(response.responseSta){
                            j.copia.nivel = buscarContenido($scope.disenoActual.niveles,"nivelID","nombre",j.copia.nivelID);
                            j.copia.hTotal=j.copia.hObligatoria+j.copia.hTutoria+j.copia.hLibre;
                            $scope.disenoActual.jornadas[i] = j.copia;
                            
                        }            
                    },function(data){
                        console.info(data);
                    });
                },'400');
            }else{
                alert("Complete los campos para editar");
            }
        }
        //si queremos editar
        else{
            j.copia = JSON.parse(JSON.stringify(j));
            j.edi =true;            
        }
    };
    /*JORNADA ESCOLAR FIN*/
    
    /* GRADO AREA*/    
    $scope.agregarGradoArea = function(){
        
        if(!$scope.gradoArea.grado || !$scope.gradoArea.area){
            modal.mensaje("ALERTA","seleccione el area para el grado");
            return;
        }
        
        if($scope.matris.gradoAreas[$scope.gradoArea.area.i] && $scope.matris.gradoAreas[$scope.gradoArea.area.i][$scope.gradoArea.grado.i]){
            modal.mensaje("ALERTA","ya se registro el area seleccionada");
            $scope.gradoArea.area = {};
            return;
        }
        
        var request = crud.crearRequest('diseñoCurricular',1,'insertarGradoArea');
        
        var nuevoGradoHora = {gradoID:$scope.gradoArea.grado.gradoID,gradoPos:$scope.gradoArea.grado.i ,areaID:$scope.gradoArea.area.areaID,areaPos:$scope.gradoArea.area.i};
        
        request.setData(nuevoGradoHora);
        
        modal.mensajeConfirmacion($scope,"Seguro que desea agregar el área al grado?",function(){
            crud.insertar("/cuadroHoras",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    
                    var areas = $scope.matris.gradoAreas[$scope.gradoArea.area.i];
                    if(!areas)
                        areas = $scope.matris.gradoAreas[$scope.gradoArea.area.i] = [];
                    
                    nuevoGradoHora.nivelID = $scope.gradoArea.grado.nivelID;
                    nuevoGradoHora.nombre = $scope.gradoArea.area.nombre;
                    areas[$scope.gradoArea.grado.i] = nuevoGradoHora;
                    
                    $scope.disenoActual.gradoAreas.push(nuevoGradoHora);

                    //areas[$scope.gradoArea.grado.i] = JSON.parse(JSON.stringify($scope.gradoArea.area));
                    
                    $scope.gradoArea = {};
                    
                }            
            },function(data){
                console.info(data);
            });
        },'400');
    };
    $scope.editarGradoArea = function(o){
        if(!o)
            return;
        
        if( o.edi){
            o.edi =false;
        }
        //si queremos editar
        else{
            o.copia = JSON.parse(JSON.stringify(o));
            o.edi =true;            
        }
    };
    
    $scope.eliminarGradoArea = function(o){
        modal.mensajeConfirmacion($scope,"Seguro que desea quitar el área seleccionada al grado?",function(){
            var request = crud.crearRequest('diseñoCurricular',1,'eliminarGradoArea');
            request.setData({gradoID:o.gradoID,areaID:o.areaID});
            crud.eliminar("/cuadroHoras",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    $scope.matris.gradoAreas[o.areaPos][o.gradoPos] = {};
                }
            },function(data){
                console.info(data);
            });
        });
    };
    
    
    /*GRADO AREA FIN*/
    
    /* AREA HORA*/    
    $scope.agregarAreaHora = function(o,jor,i){
                
        if(($scope.matris2.grados[i].total - o.hora) + o.copia.hora > jor.hObligatoria ){
            modal.mensaje("ALERTA","la hora sobrepasa lo establecido por la jornada");
            return;
        }
        if( o.copia.hora > 10 ){
            modal.mensaje("ALERTA","No se pueden Asignar mas de 10 horas");
            return;
        }    
        
        var request = crud.crearRequest('diseñoCurricular',1,'insertarAreaHora');
        request.setData({areaHoraID:o.areaHoraID,jornadaID:jor.jornadaID ,gradoID:o.gradoID,areaID:o.areaID,hora:o.copia.hora});
        
        modal.mensajeConfirmacion($scope,"Seguro que desea cambiar la hora asignada al área y al grado?",function(){
            crud.insertar("/cuadroHoras",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    $scope.matris2.grados[i].total = ($scope.matris2.grados[i].total - o.hora)+ o.copia.hora;
                    $scope.matris2.gradoAreas[o.areaPos][o.gradoPos].areaHoraID = response.data.areaHoraID;
                    $scope.matris2.gradoAreas[o.areaPos][o.gradoPos].hora = o.copia.hora;
                    $scope.matris2.gradoAreas[o.areaPos][o.gradoPos].edi = null;
                    
                    var jorSel = buscarObjeto($scope.jornadas,'jornadaID',jor.jornadaID);
                    for(var k =0;jorSel.horas && k< jorSel.horas.length; k++ ){
                        if(jorSel.horas[k].gradoID == o.gradoID && jorSel.horas[k].areaID == o.areaID ){
                            jorSel.horas[k].hora = o.hora;
                            break;
                        }
                    }
                }
            },function(data){
                console.info(data);
            });
        },'400');
    };
    $scope.editarAreaHora = function(o){
        if(!o)
            return;
        
        if( o.edi){
            o.edi =false;
        }
        //si queremos editar
        else{
            o.copia = JSON.parse(JSON.stringify(o));
            o.edi =true;            
        }
    };
    
    $scope.eliminarAreaHora = function(o,jor,i){
        if(($scope.matris2.grados[i].total - o.hora) + o.copia.hora > jor.hObligatoria ){
            modal.mensaje("ALERTA","la hora sobrepasa lo establecido por la jornada");
            return;
        }
            
        var request = crud.crearRequest('diseñoCurricular',1,'eliminarAreaHora');
        request.setData({areaHoraID:o.areaHoraID,jornadaID:jor.jornadaID ,gradoID:o.gradoID,areaID:o.areaID,hora:0});
        
        modal.mensajeConfirmacion($scope,"Seguro que desea elimanar la hora asignada al área y al grado?",function(){
            crud.eliminar("/cuadroHoras",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    $scope.matris2.grados[i].total = ($scope.matris2.grados[i].total - o.hora)+ o.copia.hora;
                    $scope.matris2.gradoAreas[o.areaPos][o.gradoPos].areaHoraID = response.data.areaHoraID;
                    $scope.matris2.gradoAreas[o.areaPos][o.gradoPos].hora = 0;
                    $scope.matris2.gradoAreas[o.areaPos][o.gradoPos].edi = null;
                    
                    var jorSel = buscarObjeto($scope.jornadas,'jornadaID',jor.jornadaID);
                    for(var k =0;jorSel.horas && k< jorSel.horas.length; k++ ){
                        if(jorSel.horas[k].gradoID == o.gradoID && jorSel.horas[k].areaID == o.areaID ){
                            jorSel.horas[k].hora = o.hora;
                            break;
                        }
                    }
                }
            },function(data){
                console.info(data);
            });
        },'400');
    }
    
    /*AREA HORA FIN*/
    
    
    $scope.tabla.calcularMatriz = function(){
        $scope.disenoActual = setting.dataset[$scope.tabla.page()-1];
        var disenoActual = $scope.disenoActual;
        if(disenoActual && disenoActual.grados ){
            var niveles = [];
            var nAnt = {id:0,c:1,nom:""};
            var ciclos = [];
            var cAnt = {id:0,c:1,nom:""};
            
            disenoActual.grados.forEach(function(item){

                if( nAnt.id == item.nivelID )
                    nAnt.c++;
                else{
                    nAnt = {id:item.nivelID,c:1,nom:item.nivel};
                    niveles.push(nAnt);
                }
                if( cAnt.id == item.cicloID )
                    cAnt.c++;
                else{
                    cAnt = {id:item.cicloID,c:1,nom:item.ciclo};
                    ciclos.push(cAnt);
                }
            });

            $scope.matris.niveles = niveles;
            
            $scope.matris.ciclos = ciclos;
            $scope.matris.c = disenoActual.grados.length; 
            
            $scope.matris2 = {};
        }
    };
    
    $scope.tabla.calcularMatrizDetalle = function(){
        $scope.matris.gradoAreas = [];
        if( $scope.disenoActual)
        $scope.disenoActual.gradoAreas.forEach(function (item){
            var areas = $scope.matris.gradoAreas[item.areaPos];
            if(!areas )
                areas = $scope.matris.gradoAreas[item.areaPos] = [];
            areas[item.gradoPos] = JSON.parse(JSON.stringify(item));
            areas[item.gradoPos].nombre = $scope.disenoActual.areas[item.areaPos].nombre;
            
        });
    };
    
    $scope.calcularMatriz2 = function(jornada){
        
        var disenoActual = $scope.disenoActual;
        if(jornada && disenoActual.grados ){
            
            var nivelID = jornada.nivelID;
            
            
            var grados = [];
            var areas = [];
            var ciclos = [];
            var cAnt = {id:0,c:1,nom:""};
            
            disenoActual.grados.forEach(function(item){

                if( nivelID == item.nivelID ){
                    item.total = 0;
                    grados.push(item);
                    if( cAnt.id == item.cicloID )
                        cAnt.c++;
                    else{
                        cAnt = {id:item.cicloID,c:1,nom:item.ciclo};
                        ciclos.push(cAnt);
                    }
                }
            });

            $scope.matris2.grados = grados;
            $scope.matris2.ciclos = ciclos;
            $scope.matris2.c = grados.length;
            
            $scope.matris2.gradoAreas = [];
            
            if(grados.length>0){
                var jorSel = buscarObjeto($scope.jornadas,'jornadaID',jornada.jornadaID);

                var posIni = grados[0].i;
                $scope.matris.gradoAreas.forEach(function(item,index){
                    if(item.length > 0 ){
                        var gradosN = [];
                        item.forEach(function(item2,index2){
                            if( nivelID == item2.nivelID ){
                                var nuevo = JSON.parse(JSON.stringify(item2));
                                nuevo.gradoPos = index2 - posIni;
                                nuevo.areaPos = $scope.matris2.gradoAreas.length;

                                nuevo.areaHoraID = 0;
                                nuevo.hora = 0;
                                for(var k =0;jorSel.horas && k< jorSel.horas.length; k++ ){
                                    if(jorSel.horas[k].gradoID == nuevo.gradoID && jorSel.horas[k].areaID == nuevo.areaID ){
                                        nuevo.hora = jorSel.horas[k].hora;
                                        nuevo.areaHoraID = jorSel.horas[k].areaHoraID;
                                        break;
                                    }
                                }
                                gradosN[index2 - posIni] = nuevo;
                                $scope.matris2.grados[index2 - posIni].total += nuevo.hora;

                            }
                        });                    
                        if(gradosN.length>0){
                            areas.push(disenoActual.areas[index].nombre);
                            $scope.matris2.gradoAreas.push(gradosN);
                        }
                    }
                });
            }
            $scope.matris2.areas = areas;
            
        }
    };
    $scope.autenticarUsuario = function(){
        console.log($scope.passwordToAuthenticate);
        var request = crud.crearRequest('diseñoCurricular', 1, 'validarPassword');
        request.setData({username: $rootScope.usuMaster.usuario.nombre, 
                         password: $scope.passwordToAuthenticate});
        
        crud.listar("/cuadroHoras", request, function (response) {
            if(response.responseSta){
               
                $scope.usuarioValidado = true;
            }else{
                
                modal.mensaje("ERROR","Autenticaciòn invalida");
            }
        }, function (data) {
            console.info(data);
        });
        
        //cerramos la ventana modal
        $('#modalAuntenticacionUsuario').modal('hide');
        
    };
}]);