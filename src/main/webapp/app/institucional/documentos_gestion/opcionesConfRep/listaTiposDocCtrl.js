app.controller("listaTiposDocCtrl", ["$scope", "NgTableParams","crud", "modal", function infoController($scope, NgTableParams, crud, modal) {
        $scope.tipDoc = {nom: "", abr: "", des: "", fec: new Date()};
        $scope.tiposDocs = [];
        
        var paramsTipoDoc= {count: 10};
        var settingsTipoDoc={counts: []};
        $scope.tablaTipoDoc=new NgTableParams(paramsTipoDoc,settingsTipoDoc);
        
        $scope.mostrar1=true;

        //Listar todos los tipos de documentos
        $scope.listarTipDocs = function () {
            var request = crud.crearRequest('entornoInicio', 1, 'listarTiposEsp');
            //request.setData({flagActivo: false});//Este se envia para que enliste todo.
            //request.setData({flagActivo: true});//Este se envia para que tan solo enliste los activos
            crud.listar("/documentosGestion", request, function (data) {
                //colocar la data de la base a la variable del ambito
               
                $scope.tiposDocs = data.data;
                settingsTipoDoc.dataset=data.data;
                $scope.tablaTipoDoc.settings(settingsTipoDoc);               
                
            }, function (data) {
                console.info(data);
            });
        };
        $scope.mostrarLisDoc=function(){
            if($scope.mostrar1)
                $scope.mostrar1=false;            
            else
                $scope.mostrar1=true;   
        };
//        
        $scope.editarTipoDoc = function () {

            var request = crud.crearRequest('entornoInicio', 1, 'actualizarTipoDoc');
            //Con el tipo de documento seleccionado y al tener sus datos ya conectados
            //con la variable tipoDocSel solo nos disponemos a enviarlo
            $scope.tipoDocSel.usuarioID=$scope.usuMaster.usuario.usuarioID;
            request.setData($scope.tipoDocSel);

            crud.actualizar("/documentosGestion", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    //actualizando la lista general de documentos solo con la posicion
                    $scope.tiposDocs[$scope.tipoDocSel.i] = $scope.tipoDocSel;
                    $scope.tablaTipoDoc.reload();
                    $('#modalEditarTipDoc').modal('hide');
                }
            }, function (data) {
                console.info(data);
            });
        };

        $scope.eliminarTipoDoc = function (i, idTipoDoc) {
            modal.mensajeConfirmacionAuth($scope, "Realmente desea desabilitar el tipo de documento y todos los documentos relacionados subidos en el repositorio", function () {
                var request = crud.crearRequest('entornoInicio', 1, 'eliminarTipoDoc');
                request.setData({tipoDocID: idTipoDoc});
                crud.eliminar("/documentosGestion", request, function (response) {
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                    eliminarElemento(settingsTipoDoc.dataset,i);
                    $scope.tablaTipoDoc.reload();
                    
                }, function (response) {
                    console.info(response);
                });
            });
        };
        
         $scope.habTipoDoc = function (i, idTipoDoc) {
            modal.mensajeConfirmacion($scope, "Desea habilitar el tipo de documento", function () {
                var request = crud.crearRequest('entornoInicio', 1, 'habilitarTipoDoc');
                request.setData({tipoDocID: idTipoDoc.id});
                console.info(idTipoDoc);
                crud.actualizar("/documentosGestion", request, function (response) {
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                    //eliminarElemento(settingsTipoDoc.dataset,i);
                    $scope.tablaTipoDoc.reload();
                    if (response.responseSta) {                      
                        //$scope.tiposTramites.splice(i,1);
                        
                    }

                    $scope.mostrar1=true;
                }, function (data) {
                });
            });
        };

        $scope.regTipDoc = function () {
            //Establecer registro de un nuevo tipo de documento
            console.info("Iniciando Configuracion");
            var request = crud.crearRequest('entornoInicio', 1, 'insertarTipoDoc');
            //insertando resultando del contenido del archivo
            $scope.tipDoc.usuarioID=$scope.usuMaster.usuario.usuarioID;
            request.setData($scope.tipDoc);

            if ($scope.tipDoc.nom !== "" && $scope.tipDoc.des !== "" && $scope.tipDoc.abr !== "") {
                crud.insertar("/documentosGestion", request, function (response) {
                    modal.mensaje("CONFIRMACION HECHA", response.responseMsg);
                    console.info("Insercion Hecha Tipo Documento");
                    if (response.responseSta) {
                        //reiniciamos las variables
                        $scope.tipDoc = {nom: "", abr: "", des: "", fec: new Date()};
                        $scope.listarTipDocs();
                        $('#modalNuevoTipDoc').modal('hide');
                    }
                }, function (data) {

                });
            } else {
                modal.mensaje("CONFIRMACIÓN", "Ingrese todos los campos");
            }
        };

        $scope.prepararEditar = function (i, t) {
            //Establecemos el tipo de documento con su id con el establecimiento de un decodificacion
            $scope.tipoDocSel = JSON.parse(JSON.stringify(t));
            //Establecemos el tipo de documento con su indice
            $scope.tipoDocSel.i = i;
            $('#modalEditarTipDoc').modal('show');
        };

    }]);


